package kz.inessoft.egz.consoleutils;

import java.sql.Connection;

/**
 * Created by alexey on 20.10.16.
 */
public abstract class ABaseDB {
    public void doActions() throws Exception {
        Connection conn = null;
        try {
            conn = DbConnectorFactory.getInstance().getConnection();

            processDB(conn);

            conn.commit();
        } finally {
            if (conn != null)
                conn.close();
        }
    }

    public abstract void processDB(Connection conn) throws Exception;
}

package kz.inessoft.egz.consoleutils;

import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by alexey on 18.01.17.
 */
public class DbConnectorFactory {
//    private static final String USER = "egz";
//    private static final String PASSWORD = "76g5Bml9pPFU9ikIYEaO";
//    private static final String DBURL = "jdbc:postgresql://192.168.73.150:5432/egzdb";

    private static final String USER = "egz";
    private static final String PASSWORD = "password";
    private static final String DBURL = "jdbc:postgresql://localhost:5432/egzdb";

    private static DbConnectorFactory ourInstance = new DbConnectorFactory();

    public static DbConnectorFactory getInstance() {
        return ourInstance;
    }

    private DbConnectorFactory() {
        try {
            DriverManager.registerDriver(new Driver());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection getConnection() {
        try {
            Properties connectionProps = new Properties();
            connectionProps.put("user", USER);
            connectionProps.put("password", PASSWORD);
            Connection connection = DriverManager.getConnection(DBURL, connectionProps);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

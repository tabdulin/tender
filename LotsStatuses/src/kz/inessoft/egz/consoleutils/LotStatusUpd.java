package kz.inessoft.egz.consoleutils;

import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejb.httpclient.AnonymousHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexey on 19.10.16.
 */
public class LotStatusUpd {
    private static Map<String, Long> statusesCache = new HashMap<>();

    public static void main(String[] args) {
        Connection conn = DbConnectorFactory.getInstance().getConnection();
        PreparedStatement pstmt = null;
        ResultSet rst = null;
        PreparedStatement pstmtUpd = null;
        try {
            initStatuses(conn);

            pstmtUpd = conn.prepareStatement("update t_lot_cross_announcement set dic_lot_status_id = ? where t_lot_id = ? and t_announcement_id = ?");

            pstmt = conn.prepareStatement("select ann.id, lot.t_lot_id, t_lot.plan_unit_id, ann.portal_id from t_lot_cross_announcement as lot inner JOIN t_announcement as ann on ann.id = lot.t_announcement_id inner join t_lot on t_lot.id = lot.t_lot_id");
            rst = pstmt.executeQuery();

            PreparedStatement pstmt1 = conn.prepareStatement("select lots_tab from t_annoucement_html where t_announcement_id = ?");
            int i = 0;
            while (rst.next()) {
                i++;
                if (i % 1000 == 0)
                    System.out.println(i);
                Long planUnitId = rst.getLong(3);
                Long lotId = rst.getLong(2);
                Long announcementId = rst.getLong(1);
                Long annPortalId = rst.getLong(4);

                while (true) {
                    pstmt1.setLong(1, announcementId);
                    ResultSet rst1 = pstmt1.executeQuery();
                    rst1.next();
                    String html = rst1.getString(1);
                    rst1.close();
                    Document doc = getDocument(html);

                    NodeList nodeList = XMLUtils.evaluateXPathNodeList("//table//tr[td='" + planUnitId + "']/td[13]", doc);
                    if (nodeList.getLength() != 1) {
                        System.out.println("Error while parse " + planUnitId + ". Rereq page");
                        AnonymousHttpClient client = null;
                        try {
                            client = HttpClientsPool.getInstance().getClient();
                            Response response = client.sendGetRequest(PortalUrlUtils.createAnnouncementLotsUrl(annPortalId, 1));
                            saveNewHtml(response.getContent(), conn, announcementId);
                        } finally {
                            HttpClientsPool.getInstance().freeClient(client);
                        }
                        continue;
                    }
                    String status = nodeList.item(0).getTextContent().trim();
                    Long statusId = statusesCache.get(status);
                    if (statusId == null)
                        throw new RuntimeException(planUnitId.toString());

                    pstmtUpd.setLong(1, statusId);
                    pstmtUpd.setLong(2, lotId);
                    pstmtUpd.setLong(3, announcementId);
                    pstmtUpd.executeUpdate();
                    break;
                }
            }
            System.out.println(i);
            pstmt1.close();
            conn.commit();
        } catch (SQLException | ParserConfigurationException | SAXException | IOException | XPathExpressionException | RequestException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                rst.close();
                pstmt.close();
                pstmtUpd.close();
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void initStatuses(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rst = null;
        try {
            pstmt = conn.prepareStatement("select name_ru, id from dic_lot_status");
            rst = pstmt.executeQuery();
            while (rst.next()) {
                statusesCache.put(rst.getString(1), rst.getLong(2));
            }
        } finally {
            if (rst != null) rst.close();
            if (pstmt != null) pstmt.close();
        }
    }

    private static Document getDocument(String xml) throws IOException, SAXException, ParserConfigurationException {
        // Конвертируем в XML
        Tidy tidy = new Tidy();
        tidy.setXmlOut(true);
        tidy.setInputEncoding("UTF-8");
        tidy.setOutputEncoding("UTF-8");
        tidy.setWraplen(0);
        tidy.setForceOutput(true);
        tidy.setErrout(new PrintWriter(new Writer() {
            @Override
            public void write(char[] cbuf, int off, int len) throws IOException {
            }

            @Override
            public void flush() throws IOException {
            }

            @Override
            public void close() throws IOException {
            }
        }));
        StringWriter writer = new StringWriter();
        tidy.parse(new StringReader(xml), writer);
        xml = writer.toString().replaceAll("&nbsp;", " ");

        // Парсим
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        ByteArrayInputStream bis = new ByteArrayInputStream(xml.getBytes("UTF-8"));
        Document document = builder.parse(bis);
        document.getDocumentElement().normalize();
        return document;
    }

    private static void saveNewHtml(String html, Connection conn, Long annId) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement("update t_annoucement_html set lots_tab = ? where t_announcement_id = ?");
        pstmt.setString(1, html);
        pstmt.setLong(2, annId);
        pstmt.executeUpdate();
        pstmt.close();
    }
}

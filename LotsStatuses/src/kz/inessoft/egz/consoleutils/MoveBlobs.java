package kz.inessoft.egz.consoleutils;

import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by alexey on 07.02.17.
 */
public class MoveBlobs {
    public static void main(String[] args) {
        Connection conLocal = DbConnectorFactory.getInstance().getConnection();
        Properties connectionProps = new Properties();
        connectionProps.put("user", "egz");
        connectionProps.put("password", "76g5Bml9pPFU9ikIYEaO");
        try {
            LargeObjectManager lobj = conLocal.unwrap(org.postgresql.PGConnection.class).getLargeObjectAPI();
            Connection connProd = DriverManager.getConnection("jdbc:postgresql://192.168.73.150:5432/egzdb", connectionProps);
            connProd.setAutoCommit(false);
            PreparedStatement sel = connProd.prepareStatement("select f.file_name, f.file_size, f.mime_type, f.file_content from t_portal_file f inner join t_portal_uploaded_file uf on f.id=uf.id where uf.portal_id=?");
            PreparedStatement selEmpty = conLocal.prepareStatement("select uf.portal_id, f.id from t_portal_file f inner join t_portal_uploaded_file uf on f.id=uf.id where f.file_name is null");
            PreparedStatement pstmtUpd = conLocal.prepareStatement("update t_portal_file set file_name=?, file_size=?, mime_type=?, file_content=? where id=?");
            ResultSet rst = selEmpty.executeQuery();
            while(rst.next()) {
                Long portalId = rst.getLong(1);
                Long id = rst.getLong(2);
                sel.setLong(1, portalId);
                ResultSet rst1 = sel.executeQuery();
                while (rst1.next()) {
                    String fileName = rst1.getString(1);
                    long fileSize = rst1.getLong(2);
                    String mime = rst1.getString(3);
                    Blob blob1 = rst1.getBlob(4);
                    byte[] bytes = blob1.getBytes(1, (int) blob1.length());


                    pstmtUpd.setString(1, fileName);
                    pstmtUpd.setLong(2, fileSize);
                    pstmtUpd.setString(3, mime);
                    long oid = lobj.createLO(LargeObjectManager.READ | LargeObjectManager.WRITE);
                    LargeObject obj = lobj.open(oid, LargeObjectManager.WRITE);
                    obj.write(bytes);
                    obj.close();
                    pstmtUpd.setLong(4, oid);
                    pstmtUpd.setLong(5, id);
                    pstmtUpd.executeUpdate();
                    conLocal.commit();
                }
                rst1.close();
            }
            rst.close();
            selEmpty.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

package kz.inessoft.egz.consoleutils;

import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscussionsInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by alexey on 15.02.17.
 */
public class ParseDiscussions {
    public static void main(String[] args) {
        Connection connection = DbConnectorFactory.getInstance().getConnection();
        try {
            PreparedStatement pstmtSeq = connection.prepareStatement("ALTER SEQUENCE seq$dic_discussion_request_type INCREMENT BY 1");
            pstmtSeq.executeUpdate();
            pstmtSeq.close();
            pstmtSeq = connection.prepareStatement("ALTER SEQUENCE seq$dic_discussion_response_type INCREMENT BY 1");
            pstmtSeq.executeUpdate();
            pstmtSeq.close();
            PreparedStatement pstmtSelReqType = connection.prepareStatement("select id from dic_discussion_request_type where name_ru = ?");
            PreparedStatement pstmtInsertReqType = connection.prepareStatement("insert into dic_discussion_request_type (id, name_ru)" +
                    " VALUES (nextval('seq$dic_discussion_request_type'), ?)");

            PreparedStatement pstmtSelRespType = connection.prepareStatement("select id from dic_discussion_response_type where name_ru = ?");
            PreparedStatement pstmtInsertRespType = connection.prepareStatement("insert into dic_discussion_response_type (id, name_ru)" +
                    " VALUES (nextval('seq$dic_discussion_response_type'), ?)");

            PreparedStatement pstmt = connection.prepareStatement("SELECT id, source_html FROM t_discussion");
            ResultSet rst = pstmt.executeQuery();
            PreparedStatement pstmtUpd = connection.prepareStatement("UPDATE t_discussion SET request_subject=?, " +
                    "dic_discussion_request_type_id=?, request_implementer=?, request_author=?, " +
                    "request_time=?, request_text=?, response_time=?, response_author=?, dic_discussion_response_type_id=?, " +
                    "response_description=?, response_reject_cause=?, response_explanation=? WHERE id=?");
            int i = 0;
            while (rst.next()) {
                long id = rst.getLong(1);
                System.out.println("id = " + id);
                pstmtUpd.setLong(13, id);
                String html = rst.getString(2);
                Document document = XMLUtils.getDocument(html);
                SiteDiscussionsInfo.SiteDiscussion.Request request = getRequest(document);
//                System.out.println("request author="+request.getAuthor().length()+" subj="+request.getSubject().length());
                pstmtUpd.setString(1, request.getSubject());
                pstmtUpd.setLong(2, getTypeId(request.getRequestType(), pstmtSelReqType, pstmtInsertReqType));
                pstmtUpd.setString(3, request.getImplementer());
                pstmtUpd.setString(4, request.getAuthor());
                pstmtUpd.setTimestamp(5, new Timestamp(request.getTime().getTime()));
                pstmtUpd.setString(6, request.getText());

                SiteDiscussionsInfo.SiteDiscussion.Responce responce = getResponce(document);
                if (responce == null) {
                    pstmtUpd.setNull(7, Types.TIMESTAMP);
                    pstmtUpd.setNull(8, Types.VARCHAR);
                    pstmtUpd.setNull(9, Types.BIGINT);
                    pstmtUpd.setNull(10, Types.VARCHAR);
                    pstmtUpd.setNull(11, Types.VARCHAR);
                    pstmtUpd.setNull(12, Types.VARCHAR);
                } else {
                    pstmtUpd.setTimestamp(7, new Timestamp(responce.getTime().getTime()));
                    pstmtUpd.setString(8, responce.getAuthor());
                    pstmtUpd.setLong(9, getTypeId(responce.getDecision(), pstmtSelRespType, pstmtInsertRespType));
                    setString(pstmtUpd, 10, responce.getDescription());
                    setString(pstmtUpd, 11, responce.getRejectCause());
                    setString(pstmtUpd, 12, responce.getExplanation());
                }
                pstmtUpd.executeUpdate();
                System.out.println(i++);
            }
            pstmtUpd.close();
            rst.close();
            pstmt.close();
            pstmtSeq = connection.prepareStatement("ALTER SEQUENCE seq$dic_discussion_request_type INCREMENT BY 10");
            pstmtSeq.executeUpdate();
            pstmtSeq.close();
            pstmtSeq = connection.prepareStatement("ALTER SEQUENCE seq$dic_discussion_response_type INCREMENT BY 10");
            pstmtSeq.executeUpdate();
            pstmtSeq.close();
            pstmtSelReqType.close();
            pstmtInsertReqType.close();
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    private static void setString(PreparedStatement pstmt, int column, String value) throws SQLException {
        if (value == null)
            pstmt.setNull(column, Types.VARCHAR);
        else
            pstmt.setString(column, value);
    }

    private static long getTypeId(String name, PreparedStatement pstmtSelType, PreparedStatement pstmtInsertType) throws SQLException {
        while(true) {
            pstmtSelType.setString(1, name);
            ResultSet resultSet = pstmtSelType.executeQuery();
            try {
                if (resultSet.next()) {
                    return resultSet.getLong(1);
                } else {
                    pstmtInsertType.setString(1, name);
                    pstmtInsertType.executeUpdate();
                }
            } finally {
                resultSet.close();
            }
        }
    }

    private static SiteDiscussionsInfo.SiteDiscussion.Request getRequest(Document document) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException, ParseException {
        SiteDiscussionsInfo.SiteDiscussion.Request request = new SiteDiscussionsInfo.SiteDiscussion.Request();
        request.setSubject(evaluateRequestField(document, "Тема сообщения"));
        request.setRequestType(evaluateRequestField(document, "Тип сообщения"));
        request.setImplementer(evaluateRequestField(document, "Поставщик"));
        request.setAuthor(evaluateRequestField(document, "Представитель поставщика"));
        request.setText(evaluateRequestField(document, "Текст сообщения"));
        request.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(evaluateRequestField(document, "Дата и время отправки сообщения")));
        return request;
    }

    private static SiteDiscussionsInfo.SiteDiscussion.Responce getResponce(Document document) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException, ParseException {
        Node node = XMLUtils.evaluateXPathNode("//div[@id='editMessage']", document);
        if (node != null) {
            SiteDiscussionsInfo.SiteDiscussion.Responce responce = new SiteDiscussionsInfo.SiteDiscussion.Responce();
            responce.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(evaluateResponceField(node, "Дата:")));
            responce.setAuthor(evaluateResponceField(node, "Автор:"));
            responce.setDecision(evaluateResponceField(node, "Решение:"));
            responce.setDescription(evaluateResponceField(node, "Описание внесения изменения"));
            responce.setRejectCause(evaluateResponceField(node, "Причина отклонения"));
            responce.setExplanation(evaluateResponceField(node, "Текст разъяснения"));
            return responce;
        }
        return null;
    }


    private static String evaluateRequestField(Document document, String fieldName) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        return XMLUtils.evaluateXPathNode("//div[@class='panel-body']/div/div[@class='row']/div[normalize-space(strong)='" + fieldName + "']/../div[@class='col-md-9']", document).getTextContent().trim();
    }

    private static String evaluateResponceField(Node node, String fieldName) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Node node1 = XMLUtils.evaluateXPathNode("./div[@class='row']/div[normalize-space(strong)='" + fieldName + "']/../div[@class='col-md-9']", node);
        if (node1 == null) return null;
        return node1.getTextContent().trim();
    }
}

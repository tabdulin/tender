package kz.inessoft.egz.consoleutils;

import kz.inessoft.egz.ejb.httpclient.AnonymousHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
public class RestoreLotHTML extends ABaseDB {
    public static void main(String[] args) {
        RestoreLotHTML restoreLotHTML = new RestoreLotHTML();
        try {
            restoreLotHTML.doActions();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void processDB(Connection conn) throws Exception {
        PreparedStatement pstmt = conn.prepareStatement(
                "select ann.portal_id, ann.id, tl.portal_id, tl.id " +
                        "from t_announcement as ann inner join t_lot_cross_announcement as lca on ann.id = lca.t_announcement_id " +
                        "inner join t_lot as tl on tl.id = lca.t_lot_id " +
                        "where lca.original_html = 'not downloaded'");
        ResultSet rst = pstmt.executeQuery();
        AnonymousHttpClient client = HttpClientsPool.getInstance().getClient();

        PreparedStatement pstmtUpd = conn.prepareStatement("update t_lot_cross_announcement set original_html = ? where t_lot_id = ? and t_announcement_id = ?");
        int i = 0;
        while (rst.next()) {
            if (++i % 1000 == 0) {
                System.out.println(i);
            }
            Long annPortalId = rst.getLong(1);
            Long annId = rst.getLong(2);
            Long lotPortalId = rst.getLong(3);
            Long lotId = rst.getLong(4);

            HttpPost httpPost = new HttpPost(PortalUrlUtils.createAnnouncementLotLoadAjaxUrl(annPortalId));
            List<NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("id", String.valueOf(lotPortalId)));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            Response lotResponse = client.sendPostRequest(httpPost);
            pstmtUpd.setString(1, lotResponse.getContent());
            pstmtUpd.setLong(2, lotId);
            pstmtUpd.setLong(3, annId);
            pstmtUpd.executeUpdate();
            conn.commit();
        }
        System.out.println(i);

        rst.close();
        pstmt.close();
        pstmtUpd.close();
    }
}

package kz.inessoft.egz.consoleutils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by alexey on 19.09.16.
 */
public class XMLUtils {
    public static Document getDocument(String content) throws ParserConfigurationException, IOException, SAXException {
        // Конвертируем в XML
        Tidy tidy = new Tidy();
        tidy.setXmlOut(true);
        tidy.setInputEncoding("UTF-8");
        tidy.setOutputEncoding("UTF-8");
        tidy.setWraplen(0);
        tidy.setForceOutput(true);
        tidy.setErrout(new PrintWriter(new Writer() {
            @Override
            public void write(char[] cbuf, int off, int len) throws IOException {
            }

            @Override
            public void flush() throws IOException {
            }

            @Override
            public void close() throws IOException {
            }
        }));
        StringWriter writer = new StringWriter();
        tidy.parse(new StringReader(content), writer);
        String xml = writer.toString().replaceAll("&nbsp;", " ");

        // Парсим
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        ByteArrayInputStream bis = new ByteArrayInputStream(xml.getBytes("UTF-8"));
        Document document = builder.parse(bis);
        document.getDocumentElement().normalize();
        return document;
    }

    public static NodeList evaluateXPathNodeList(String xpath, Node node) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        XPath xPath =  XPathFactory.newInstance().newXPath();
        return (NodeList) xPath.compile(xpath).evaluate(node, XPathConstants.NODESET);
    }

    public static Node evaluateXPathNode(String xpath, Node node) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        XPath xPath =  XPathFactory.newInstance().newXPath();
        return (Node) xPath.compile(xpath).evaluate(node, XPathConstants.NODE);
    }
}

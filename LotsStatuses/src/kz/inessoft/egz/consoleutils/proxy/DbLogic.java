package kz.inessoft.egz.consoleutils.proxy;

import kz.inessoft.egz.consoleutils.DbConnectorFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 01.02.17.
 */
public class DbLogic {
    private static DbLogic ourInstance = new DbLogic();

    public static DbLogic getInstance() {
        return ourInstance;
    }

    private DbLogic() {
    }

    public void saveInfo(Proxy proxy, EUnavailableCause unavailableCause) throws SQLException {
        Connection connection = DbConnectorFactory.getInstance().getConnection();
        PreparedStatement pstmt = connection.prepareStatement("select * from system_proxy where proxy_host=? and proxy_port=? and proxy_type=?");
        pstmt.setString(1, proxy.getHost());
        pstmt.setInt(2, proxy.getPort());
        String type;
        switch (proxy.getProxyType()) {
            case HTTP:
                type = "HTTP";
                break;
            case SOCKS:
                type = "SOCKS";
                break;
            default:
                throw new IllegalArgumentException("Unknown proxy type");
        }
        pstmt.setString(3, type);
        ResultSet rst = pstmt.executeQuery();
        if (rst.next()) {
            PreparedStatement pstmtUpd = connection.prepareStatement("update system_proxy set available=?, unavailable_cause=?, unavailable_time=? where proxy_host=? and proxy_port=? and proxy_type=?");
            pstmtUpd.setString(4, proxy.getHost());
            pstmtUpd.setInt(5, proxy.getPort());
            pstmtUpd.setString(6, type);
            if (unavailableCause != null) {
                pstmtUpd.setBoolean(1, false);
                pstmtUpd.setString(2, unavailableCause.name());
                pstmtUpd.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            } else {
                pstmtUpd.setBoolean(1, true);
                pstmtUpd.setNull(2, Types.VARCHAR);
                pstmtUpd.setNull(3, Types.TIMESTAMP);
            }
            pstmtUpd.executeUpdate();
            pstmtUpd.close();
        } else {
            if (unavailableCause == null) {
                PreparedStatement pstmtIns = connection.prepareStatement("insert into system_proxy (proxy_host, proxy_port, proxy_type, available, unavailable_cause, unavailable_time) values (?, ?, ?, ?, ?, ?)");
                pstmtIns.setString(1, proxy.getHost());
                pstmtIns.setInt(2, proxy.getPort());
                pstmtIns.setString(3, type);
                pstmtIns.setBoolean(4, true);
                pstmtIns.setNull(5, Types.VARCHAR);
                pstmtIns.setNull(6, Types.TIMESTAMP);
                pstmtIns.executeUpdate();
                pstmtIns.close();
            }
        }
        rst.close();
        pstmt.close();
        connection.commit();
        connection.close();
    }

    public List<Proxy> getAvailableProxies(EUnavailableCause unavailableCause) throws SQLException {
        Connection connection = DbConnectorFactory.getInstance().getConnection();
        PreparedStatement pstmt;
        if (unavailableCause == null) {
            pstmt = connection.prepareStatement("select * from system_proxy where available=true");
        } else {
            pstmt = connection.prepareStatement("select * from system_proxy where unavailable_cause=?");
            pstmt.setString(1, unavailableCause.name());
        }
        ResultSet rst = pstmt.executeQuery();
        List<Proxy> retVal = new LinkedList<>();
        while (rst.next()) {
            Proxy proxy = new Proxy();
            proxy.setHost(rst.getString("proxy_host"));
            proxy.setPort(rst.getInt("proxy_port"));
            String type = rst.getString("proxy_type");
            if ("HTTP".equals(type))
                proxy.setProxyType(java.net.Proxy.Type.HTTP);
            else if ("SOCKS".equals(type))
                proxy.setProxyType(java.net.Proxy.Type.SOCKS);
            else throw new RuntimeException("Wrong proxy type for " + proxy.getHost() + ":" + proxy.getPort());
            retVal.add(proxy);
        }
        connection.close();
        return retVal;
    }
}

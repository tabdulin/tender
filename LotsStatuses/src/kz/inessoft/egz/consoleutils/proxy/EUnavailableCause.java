package kz.inessoft.egz.consoleutils.proxy;

/**
 * Created by alexey on 01.02.17.
 */
public enum EUnavailableCause {
    MITM,
    CONNECTION_PROBLEMS,
    TOO_SLOW,
    BLOCKED
}

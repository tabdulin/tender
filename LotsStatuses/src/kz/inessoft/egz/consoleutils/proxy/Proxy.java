package kz.inessoft.egz.consoleutils.proxy;

/**
 * Created by alexey on 25.01.17.
 */
public class Proxy {
    private java.net.Proxy.Type proxyType;
    private String host;
    private int port;

    public java.net.Proxy.Type getProxyType() {
        return proxyType;
    }

    public void setProxyType(java.net.Proxy.Type proxyType) {
        this.proxyType = proxyType;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}

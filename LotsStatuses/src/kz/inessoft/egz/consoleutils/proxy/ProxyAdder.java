package kz.inessoft.egz.consoleutils.proxy;

import kz.inessoft.egz.consoleutils.XMLUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alexey on 18.01.17.
 */
public class ProxyAdder {
    public static final String PROPERTY_URL_MASK = "urlmask";
    public static final String PROPERTY_PAGE_MAX = "pagemax";
    public static final String PROPERTY_ROWS_PER_PAGE = "rowsPerPage";
    public static final String PROPERTY_SHOW_FIRST_PAGE = "showFirstPage";
    public static final String PROPERTY_CHECK_NEXT_PAGE = "xpathchecknextpage";
    public static final String PROPERTY_XPATH_ROW = "xpathrow";
    public static final String PROPERTY_XPATH_HOST = "xpathhost";
    public static final String PROPERTY_REGEXP_HOST = "hostregexp";
    public static final String PROPERTY_XPATH_PORT = "xpathport";
    public static final String PROPERTY_REGEXP_PORT = "portregexp";
    public static final String PROPERTY_XPATH_TYPE = "xpathtype";
    public static final String PROPERTY_CONSTANT_TYPE = "constanttype";
    public static final String PAGE_MASK = "{page}";
    public static final String NEXT_PAGE_MASK = "{pageNext}";

    private static final Map<String, java.net.Proxy.Type> TYPE_MAP = new HashMap<>();

    static {
        TYPE_MAP.put("HTTP", java.net.Proxy.Type.HTTP);
        TYPE_MAP.put("HTTPS", java.net.Proxy.Type.HTTP);
        TYPE_MAP.put("HTTP, HTTPS", java.net.Proxy.Type.HTTP);
        TYPE_MAP.put("SOCKS", java.net.Proxy.Type.SOCKS);
        TYPE_MAP.put("SOCKS4", java.net.Proxy.Type.SOCKS);
        TYPE_MAP.put("SOCKS5", java.net.Proxy.Type.SOCKS);
        TYPE_MAP.put("SOCKS4, SOCKS5", java.net.Proxy.Type.SOCKS);
    }

    public static void main(String[] args) {
        try {
            String configFile = args[0];
            Properties properties = new Properties();
            FileInputStream inStream = new FileInputStream(configFile);
            properties.load(inStream);
            inStream.close();

            CloseableHttpClient client = ClosableHttpClientFactory.getInstance().createClient(null);
            String urlMask = properties.getProperty(PROPERTY_URL_MASK);
            int pageMax = Integer.valueOf(properties.getProperty(PROPERTY_PAGE_MAX));
            String property = properties.getProperty(PROPERTY_ROWS_PER_PAGE);
            int rowsPerPage = property == null ? 0 : Integer.valueOf(property);
            boolean showFirstPage = Boolean.valueOf(properties.getProperty(PROPERTY_SHOW_FIRST_PAGE));
            String xpathRow = properties.getProperty(PROPERTY_XPATH_ROW);
            String xpathHost = properties.getProperty(PROPERTY_XPATH_HOST);
            String regexpHost = properties.getProperty(PROPERTY_REGEXP_HOST);
            String xpathPort = properties.getProperty(PROPERTY_XPATH_PORT);
            String regexpPort = properties.getProperty(PROPERTY_REGEXP_PORT);
            String xpathType = properties.getProperty(PROPERTY_XPATH_TYPE);
            String xpathCheckNextPage = properties.getProperty(PROPERTY_CHECK_NEXT_PAGE);
            String constantType = properties.getProperty(PROPERTY_CONSTANT_TYPE);


            for (int i = 1; i <= pageMax; i++) {
                String url;
                if (i != 1 || showFirstPage) {
                    url = urlMask.replace(PAGE_MASK, String.valueOf(i));
                    url = url.replace("{rowNumber}", String.valueOf(i * rowsPerPage));
                } else {
                    url = urlMask.replace(PAGE_MASK, "");
                    url = url.replace("{rowNumber}", "");
                }

                HttpGet get = new HttpGet(url);

                CloseableHttpResponse closeableHttpResponse = client.execute(get);
                String html = IOUtils.toString(closeableHttpResponse.getEntity().getContent(), "UTF8");
                closeableHttpResponse.close();
                Document document = XMLUtils.getDocument(html);
                NodeList nodeList = XMLUtils.evaluateXPathNodeList(xpathRow, document);
                for (int j = 0; j < nodeList.getLength(); j++) {
                    Node node = nodeList.item(j);
                    String host = XMLUtils.evaluateXPathNode(xpathHost, node).getTextContent();
                    if (regexpHost != null) {
                        host = evaluateRegExp(host, regexpHost);
                    }
                    String port = XMLUtils.evaluateXPathNode(xpathPort, node).getTextContent();
                    if (regexpPort != null) {
                        port = evaluateRegExp(port, regexpPort);
                    }
                    String type = xpathType != null ? XMLUtils.evaluateXPathNode(xpathType, node).getTextContent() : constantType;
                    Proxy proxy = new Proxy();
                    proxy.setHost(host);
                    proxy.setPort(Integer.valueOf(port));
                    proxy.setProxyType(getProxyType(type));
                    ProxyCheckUtils.checkProxy(proxy);
                }
                if (XMLUtils.evaluateXPathNode(xpathCheckNextPage.replace(NEXT_PAGE_MASK, String.valueOf(i + 1)), document) == null)
                    break;
            }

            client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String evaluateRegExp(String string, String regexp) {
        Pattern p = Pattern.compile(regexp);
        Matcher matcher = p.matcher(string);
        matcher.find();
        string = matcher.group();
        return string;
    }

    private static java.net.Proxy.Type getProxyType(String type) {
        java.net.Proxy.Type retVal = TYPE_MAP.get(type);
        if (retVal == null)
            throw new RuntimeException("Unknown proxy type \"" + type + "\"");
        return retVal;
    }
}

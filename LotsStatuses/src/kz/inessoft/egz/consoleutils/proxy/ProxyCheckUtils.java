package kz.inessoft.egz.consoleutils.proxy;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.SQLException;

/**
 * Created by alexey on 01.02.17.
 */
public class ProxyCheckUtils {
    static void checkProxy(Proxy proxy) throws IOException, SQLException {
        System.out.println("Checking proxy " + proxy.getProxyType() + " " + proxy.getHost() + ":" + proxy.getPort());
        CloseableHttpClient clientProxy = ClosableHttpClientFactory.getInstance().createClient(proxy);
        HttpGet request = new HttpGet("https://v3bl.goszakup.gov.kz/images/favicon.ico");
        request.setConfig(RequestConfig.custom().setConnectTimeout(5 * 1000).setSocketTimeout(5 * 1000)
                .setConnectionRequestTimeout(5 * 1000).build());
        EUnavailableCause unavailableCause = null;
        try {
            long startTime = System.currentTimeMillis();
            CloseableHttpResponse response1 = clientProxy.execute(request);
            long checkTime = System.currentTimeMillis() - startTime;
            if (response1.getStatusLine().getStatusCode() == 403) {
                System.out.println("Blocked!!!");
                unavailableCause = EUnavailableCause.BLOCKED;
            } else if (checkTime < 3 * 1000)
                System.out.println("Passed " + response1.getStatusLine().getStatusCode());
            else {
                System.out.println("Too slow: " + checkTime + " ms.");
                unavailableCause = EUnavailableCause.TOO_SLOW;
            }

        } catch (SSLPeerUnverifiedException e) {
            System.out.println("MITM");
            unavailableCause = EUnavailableCause.MITM;
        } catch (InternalError | SSLException | ConnectTimeoutException | SocketTimeoutException | SocketException e) {
            System.out.println("Connection problem");
            unavailableCause = EUnavailableCause.CONNECTION_PROBLEMS;
        }
        DbLogic.getInstance().saveInfo(proxy, unavailableCause);
    }
}

package kz.inessoft.egz.consoleutils.proxy;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by alexey on 01.02.17.
 */
public class ProxyChecker {
    public static void main(String[] args) {
        try {
            EUnavailableCause status = null;
            if (args.length > 0)
                status = EUnavailableCause.valueOf(args[0]);
            List<Proxy> availableProxies = DbLogic.getInstance().getAvailableProxies(status);
            for (Proxy availableProxy : availableProxies) {
                ProxyCheckUtils.checkProxy(availableProxy);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}

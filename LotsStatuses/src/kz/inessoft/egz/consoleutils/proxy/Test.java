package kz.inessoft.egz.consoleutils.proxy;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by alexey on 01.02.17.
 */
public class Test {
    public static void main(String[] args) {
        try {
            Proxy proxy = new Proxy();
            proxy.setHost("70.248.28.23");
            proxy.setPort(800);
            proxy.setProxyType(java.net.Proxy.Type.HTTP);
            ProxyCheckUtils.checkProxy(proxy);

            proxy = new Proxy();
            proxy.setHost("163.121.188.3");
            proxy.setPort(8080);
            proxy.setProxyType(java.net.Proxy.Type.HTTP);
            ProxyCheckUtils.checkProxy(proxy);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package kz.inessoft.egz.clientapp;

import kz.inessoft.egz.clientapp.service.PortalService;
import kz.inessoft.egz.clientapp.service.announcement.Announcement;
import kz.inessoft.egz.clientapp.service.announcement.AnnouncementProposal;
import kz.inessoft.egz.clientapp.service.announcement.AnnouncementService;
import kz.inessoft.egz.clientapp.service.proposal.Proposal;
import kz.inessoft.egz.clientapp.service.proposal.ProposalService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Talgat.Abdulin
 */
public class Application {
    private static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String... args) {
        if (args != null && args.length < 1) {
            help();
            login();
            return;
        }

        try {
            String command = args[0];
            switch (command) {
                case "download": {
                    String announcementNumber = args[1];
                    download(announcementNumber);
                    break;
                }
                case "monitoring": {
                    String announcementNumber = args[1];
                    monitor(announcementNumber);
                    break;
                }
                case "upload": {
                    String url = args[1];
                    String directory = args[2];
                    upload(url, directory);
                    break;
                }
                case "propose": {
                    propose(args[1], args.length == 2 ? null: args[2]);
                    break;
                }
                case "login": {
                    login();
                    break;
                }
                default: {
                    help();
                    login();
                }
            }
        } catch (Exception e) {
            logger.error("Announcement was not processed", e);
        }
    }

    public static void download(String announcementNumber) throws AnnouncementService.AnnouncementException {
        PortalService portal = new PortalService();
        portal.openChromeBrowser();
        portal.startSignWorkaround();
        portal.login();
        AnnouncementService announcementService = new AnnouncementService();
        announcementService.downloadAnnouncementDocuments(announcementNumber);
        announcementService.downloadAnnouncementDiscussions(announcementNumber);
        announcementService.downloadAnnouncementProtocols(announcementNumber);
        for (AnnouncementProposal member : announcementService.getAnnouncementProposals(announcementNumber)) {
            announcementService.downloadAnnouncementProposalDocuments(member);
            if (member.getAddition()) {
                announcementService.downloadAnnouncementProposalAdditionDocuments(member);
            }
        }

        portal.closeBrowser();
    }

    public static void monitor(String announcementNumber) throws AnnouncementService.AnnouncementException {
        PortalService portal = new PortalService();
        portal.openChromeBrowser();
        AnnouncementService announcementService = new AnnouncementService();
        Announcement announcement = announcementService.getAnnouncement(announcementNumber);
        announcementService.registerEvents(announcement);
        portal.closeBrowser();
    }

    public static void upload(String url, String directory) {
        PortalService portal = new PortalService();
        portal.openChromeBrowser();
        portal.login();
        portal.uploadFiles(url, directory);
    }

    public static void login() {
        PortalService portal = new PortalService();
        portal.startSignWorkaround();
        portal.openChromeBrowser(24 * 60 * 60 * 1000);
        portal.login();
    }

    public static void propose(String announcementExcelOrNumber, String lot) {
        PortalService portal = new PortalService();
        portal.startSignWorkaround();
        portal.openChromeBrowser();
        portal.login();
        ProposalService proposalService = new ProposalService();
        try {
            Proposal proposal;
            if (StringUtils.endsWith(announcementExcelOrNumber.toLowerCase(), ".xlsx")) {
                proposal = proposalService.loadProposalExcel(announcementExcelOrNumber);
            } else {
                proposal = proposalService.loadDefaultProposal(announcementExcelOrNumber, lot);
            }

            proposalService.createProposal(proposal);
            proposalService.selectLots(proposal);
            proposalService.fillApplications(proposal);
        } catch (ProposalService.ProposalException e) {
            logger.error("Cannot make proposal", e);
        }
    }

    private static void help() {
        System.out.println("Usage: java -jar tender.jar <command> <parameters>");
        System.out.println("Possible commands: \n" +
                "\tlogin - login into portal (default) \n" +
                "\tdownload - download all accessible information, parameter - <announcement-number>\n" +
                "\tmonitoring - adds tender to monitoring (google calendar), parameter - <announcement-number>\n" +
                "\tupload - uploads files to url from directory, parameters - <url> <directory>]");
    }
}

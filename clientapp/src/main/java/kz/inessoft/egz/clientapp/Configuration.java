package kz.inessoft.egz.clientapp;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

/**
 * Created by alexey on 19.07.16.
 */
public class Configuration {
    private static final Logger logger = LoggerFactory.getLogger(Configuration.class);
    private static Configuration ourInstance = new Configuration();

    private static final String PORTAL_PASSWORD_PROPERTY = "portal.password";
    private static final String PORTAL_SIGN_WORK_AROUND = "portal.signworkaround";
    private static final String BROWSER_PROFILE_DIRECTORY = "browser.profile.directory";
    private static final String BROWSER_BINARY_LOCATION = "browser.binary";
    private static final String GOOGLE_OAUTH_CREDENTIALS = "google.oauth.credentials.file";
    private static final String GOOGLE_DATASTORE = "google.store.directory";
    private static final String GOOGLE_CALENDAR_ID = "google.calendar.id";
    private static final String TENDERS_INFO_DIRECTORY = "tenders.info.directory";

    private Properties props = null;

    public static Configuration getInstance() {
        return ourInstance;
    }

    private Configuration() {
        props = new Properties();
        String configFile = System.getProperty("configFile", "config.properties");
        try (InputStream is = FileUtils.openInputStream(FileUtils.getFile(configFile))) {
            props.load(is);
        } catch (Exception e) {
            logger.error("Configuration was not loaded", e);
        }
    }

    public String getPortalPassword() {
        return props.getProperty(PORTAL_PASSWORD_PROPERTY);
    }

    public String getPortalSignWorkAround() {
        return props.getProperty(PORTAL_SIGN_WORK_AROUND);
    }

    public File getPortalCompanyDirectory() {
        return FileUtils.getFile(props.getProperty("portal.company.directory"));
    }

    public File getBrowserProfileDir() {
        return new File(props.getProperty(BROWSER_PROFILE_DIRECTORY));
    }

    public File getBrowserBinary() {
        String location = props.getProperty(BROWSER_BINARY_LOCATION);
        if (location == null) {
            return null;
        }

        return new File(location);
    }

    public String getGoogleOAuthCredentialsJSON() {
        try (InputStream is = new FileInputStream(new File(props.getProperty(GOOGLE_OAUTH_CREDENTIALS)))) {
            return IOUtils.toString(is, "UTF-8");
        } catch (Exception e) {
            logger.error("Google calendar credentials were not loaded", e);
            return null;
        }
    }

    public File getGoogleDataStoreDir() {
        return new File(props.getProperty(GOOGLE_DATASTORE));
    }

    public String getTendersInfoDirectory() {
        return props.getProperty(TENDERS_INFO_DIRECTORY);
    }

    public String getGoogleCalendarId() {
        return props.getProperty(GOOGLE_CALENDAR_ID);
    }

    public String getChromeSeleniumDriver() {
        return props.getProperty("browser.chrome.driver");
    }

    public String getDefaultProposal() {
        return props.getProperty("portal.company.proposal");
    }
}

package kz.inessoft.egz.clientapp.service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import kz.inessoft.egz.clientapp.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * @author Talgat.Abdulin
 * @see <a href="https://developers.google.com/google-apps/calendar/quickstart/java">quickstart</a>
 * <p>
 */
public class GoogleCalendarService {
    private static final Logger logger = LoggerFactory.getLogger(GoogleCalendarService.class);
    private static final String APPLICATION_NAME = "TenderCalendar";
    private Calendar calendar;

    public GoogleCalendarService() {
        try {
            HttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            FileDataStoreFactory DATA_STORE_FACTORY = new FileDataStoreFactory(
                    Configuration.getInstance().getGoogleDataStoreDir());
            JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
            // Load client secrets.
            String json = Configuration.getInstance().getGoogleOAuthCredentialsJSON();
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new StringReader(json));

            // Build flow and trigger user authorization request.
            GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                            HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, CalendarScopes.all())
                            .setDataStoreFactory(DATA_STORE_FACTORY)
                            .build();
            Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
                    .authorize("inessoft.tender");
            this.calendar = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                    .setApplicationName(APPLICATION_NAME)
                    .build();
        } catch (Throwable t) {
            logger.error("Cannot initialize GoogleCalendarService", t);
        }
    }

    public void deleteEvents() throws IOException {
        String calendarId = Configuration.getInstance().getGoogleCalendarId();
        try {
            for (Event event : calendar.events().list(calendarId)
                    .execute()
                    .getItems()) {
                logger.debug("Deleting event: {}", event.toPrettyString());
                calendar.events().delete(calendarId, event.getId()).execute();
            }
        } catch (GoogleJsonResponseException e) {
            logger.error("Cannot delete all events", e);
            throw e;
        }
    }


    public void deleteEvent(String id) throws IOException {
        String calendarId = Configuration.getInstance().getGoogleCalendarId();
        try {
            for (Event event : calendar.events().list(calendarId)
                    .setPrivateExtendedProperty(Arrays.asList("tender.event.id=" + id))
                    .execute()
                    .getItems()) {
                logger.debug("Deleting event: {}", event.toPrettyString());
                calendar.events().delete(calendarId, event.getId()).execute();
            }
        } catch (GoogleJsonResponseException e) {
            logger.error("Cannot delete event with id: {}", id, e);
            throw e;
        }
    }

    public void addEvent(Date end,  String id, String title,  String description, String color) throws IOException {
        String calendarId = Configuration.getInstance().getGoogleCalendarId();
        deleteEvent(id);

        Event event = new Event();
        Event.ExtendedProperties properties = new Event.ExtendedProperties();
        Map<String,String> map = new HashMap<String, String>();
        map.put("tender.event.id", id);
        properties.setPrivate(map);
        event.setExtendedProperties(properties);
        event.setSummary(title);
        event.setColorId(color);
        event.setDescription(description);

        EventDateTime endEvent = new EventDateTime().setDateTime(new DateTime(end));
        event.setStart(endEvent);
        event.setEnd(endEvent);
        calendar.events().insert(calendarId, event).execute();
    }

}

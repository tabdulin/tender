package kz.inessoft.egz.clientapp.service;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import kz.inessoft.egz.clientapp.service.utils.PortalUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byValue;
import static com.codeborne.selenide.Selenide.*;

/**
 * @author Talgat.Abdulin
 */
public class PortalService {
    private static Logger logger = LoggerFactory.getLogger(PortalService.class);

    public void openFirefoxBrowser() {
        FirefoxProfile profile = new FirefoxProfile(kz.inessoft.egz.clientapp.Configuration.getInstance().getBrowserProfileDir());
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(true);
        File ffBinaryFile = kz.inessoft.egz.clientapp.Configuration.getInstance().getBrowserBinary();
        FirefoxBinary firefoxBinary = ffBinaryFile == null ? new FirefoxBinary() : new FirefoxBinary(ffBinaryFile);
        WebDriverRunner.setWebDriver(new FirefoxDriver(firefoxBinary, profile));

        Configuration.timeout = 2 * 60 * 1000;
        Configuration.pollingInterval = 500;
        Configuration.holdBrowserOpen = true;
    }

    public void openChromeBrowser() {
        System.setProperty("webdriver.chrome.driver",
                kz.inessoft.egz.clientapp.Configuration.getInstance().getChromeSeleniumDriver());
        System.setProperty("selenide.chrome.switches", "--disable-extensions --start-maximized");
        Configuration.browser = WebDriverRunner.CHROME;
        Configuration.timeout = 2 * 60 * 1000;
        Configuration.startMaximized = true;
        Configuration.pollingInterval = 500;
        Configuration.holdBrowserOpen = true;
    }

    public void openChromeBrowser(int timeout) {
        openChromeBrowser();
        Configuration.timeout = timeout;
    }

    public void startSignWorkaround() {
        try {
            String signWorkAround = kz.inessoft.egz.clientapp.Configuration.getInstance().getPortalSignWorkAround();
            logger.info("Starting SignWorkAround: {}", signWorkAround);
            Process process = Runtime.getRuntime().exec("wmic process where \"name='sign.exe'\" get ExecutablePath, ProcessId");
            String pid = null;
            String executable = null;
            for (String line : IOUtils.readLines(process.getInputStream())) {
                if (StringUtils.contains(line, "sign.exe")) {
                    logger.debug("SignWorkAround Process: {}", line);
                    executable = StringUtils.split(line)[0];
                    pid = StringUtils.split(line)[1];
                    break;
                }
            }

            if (StringUtils.equals(FilenameUtils.normalize(executable, !SystemUtils.IS_OS_WINDOWS),
                    FilenameUtils.normalize(signWorkAround, !SystemUtils.IS_OS_WINDOWS))) {
                logger.info("SignWorkAround is already running: {}", executable);
                return;
            }

            if (pid != null) {
                logger.info("Stopping SignWorkAround: {}", pid);
                Runtime.getRuntime().exec("taskkill /F /T /PID " + pid);
                logger.info("Stopped SignWorkAround");
            }

            logger.info("Starting SignWorkAround: {}", signWorkAround);
            Runtime.getRuntime().exec(signWorkAround);
            logger.info("Started SignWorkAround");
        } catch (Exception e) {
            logger.error("SignWorkaround was not started", e);
        }
    }

    public void stopSignWorkAround() {
        try {
            String signWorkAround = kz.inessoft.egz.clientapp.Configuration.getInstance().getPortalSignWorkAround();
            logger.info("Stopping SignWorkAround: {}", signWorkAround);
            Process process = Runtime.getRuntime().exec("wmic process where \"name='sign.exe'\" get ExecutablePath, ProcessId");
            String pid = null;
            for (String line : IOUtils.readLines(process.getInputStream())) {
                if (StringUtils.contains(line, "sign.exe")) {
                    logger.debug("SignWorkAround Process: {}", line);
                    pid = StringUtils.split(line)[1];
                    break;
                }
            }

            if (pid == null) {
                logger.info("SignWorkAround was already stopped");
                return;
            }

            Runtime.getRuntime().exec("taskkill /F /T /PID " + pid);
            sleep(5000);
            logger.info("Stopped SignWorkAround");
        } catch (Exception e) {
            logger.error("SignWorkaround was not started", e);
        }
    }

    public void login() {
        open("https://v3bl.goszakup.gov.kz/ru/user/login");
        $("#selectP12File").shouldBe(visible).click();
        $(By.name("password")).shouldBe(visible)
                .val(kz.inessoft.egz.clientapp.Configuration.getInstance().getPortalPassword());
        $(".btn-success").click();
        $("#online-status").$(byText("Войти")).shouldNotBe(visible);
    }

    public void openPage(String url) {
        open(url);
        // workaround: sometimes there is forbidden access while navigating and downloading documents
        if ($(byText("Доступ запрещен")).is(visible)) {
            logger.warn("Access denied: reloginning");
            login();
            open(url);
        }
    }

    public void closeBrowser() {
        WebDriverRunner.getWebDriver().close();
    }

    public String search(String number) {
        open("https://v3bl.goszakup.gov.kz/ru/searchanno");
        $("#filter").click();
        $("#numberAnno").val(number);
        $("button[type=submit]").click();

        $("div.panel table.table tbody > tr > td > a").shouldBe(visible);
        return $("div.panel table.table tbody > tr > td > a").attr("href");
    }

    public void uploadFiles(String url, String directory) {
        open(url);
        int i = 0;
        // workaround: sometimes sign.exe freezes after some consequent uploads and it's restart helps
        final int RESTART_SIGN_WORKAROUND_THRESHOLD = 10;
        for (File file : FileUtils.listFiles(FileUtils.getFile(directory), null, true)) {
            if (i++ > RESTART_SIGN_WORKAROUND_THRESHOLD) {
                stopSignWorkAround();
                startSignWorkaround();
                i = 0;
            }

            PortalUtils.upload(file.getAbsolutePath());
        }
    }

    public void requestTaxDebt() {
        open(PortalUtils.createTaxDebtUrl());
        $(byText("Сведения о налоговой задолженности")).shouldBe(visible);
        $(byValue("Получить новые сведения")).click();
        if ($(byText("Вы можете отправлять запрос на получение сведений о налоговой задолженности только один раз в сутки"))
                .is(visible)) {
            logger.info("Tax Debt request was already sent today");
            return;
        }
    }
}

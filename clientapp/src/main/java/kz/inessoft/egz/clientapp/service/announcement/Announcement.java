package kz.inessoft.egz.clientapp.service.announcement;

import java.util.Date;
import java.util.List;
import java.util.SortedMap;

/**
 * @author Talgat.Abdulin
 */
public class Announcement {
    private String number;
    private String name;
    private Date publicationDate;
    private Date startDiscussionTime;
    private Date endDiscussionTime;
    private Date startProposalTime;
    private Date endProposalTime;
    private Date startProposalAdditionTime;
    private Date endProposalAdditionTime;
    private Float price;
    private List<AnnouncementLot> lots;
    private SortedMap<Integer,String> versions;

    public String getId() {
        return versions.get(versions.lastKey());
    }

    public Date getStartDiscussionTime() {
        return startDiscussionTime;
    }

    public Date getEndDiscussionTime() {
        return endDiscussionTime;
    }

    public Date getStartProposalTime() {
        return startProposalTime;
    }

    public Date getEndProposalTime() {
        return endProposalTime;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {return number;}

    public Date getPublicationDate() {
        return publicationDate;
    }

    public Date getStartProposalAdditionTime() {
        return startProposalAdditionTime;
    }

    public Date getEndProposalAdditionTime() {
        return endProposalAdditionTime;
    }

    public Float getPrice() {
        return price;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public void setStartDiscussionTime(Date startDiscussionTime) {
        this.startDiscussionTime = startDiscussionTime;
    }

    public void setEndDiscussionTime(Date endDiscussionTime) {
        this.endDiscussionTime = endDiscussionTime;
    }

    public void setStartProposalTime(Date startProposalTime) {
        this.startProposalTime = startProposalTime;
    }

    public void setEndProposalTime(Date endProposalTime) {
        this.endProposalTime = endProposalTime;
    }

    public void setStartProposalAdditionTime(Date startProposalAdditionTime) {
        this.startProposalAdditionTime = startProposalAdditionTime;
    }

    public void setEndProposalAdditionTime(Date endProposalAdditionTime) {
        this.endProposalAdditionTime = endProposalAdditionTime;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public SortedMap<Integer, String> getVersions() {
        return versions;
    }

    public void setVersions(SortedMap<Integer, String> versions) {
        this.versions = versions;
    }

    public List<AnnouncementLot> getLots() {
        return lots;
    }

    public void setLots(List<AnnouncementLot> lots) {
        this.lots = lots;
    }

    public String getLastVersionId() {
        return versions.get(versions.lastKey());
    }
}

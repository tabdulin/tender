package kz.inessoft.egz.clientapp.service.announcement;

/**
 * @author Talgat.Abdulin
 */
public class AnnouncementProposal {
    private String announcementNumber;
    private String memberName;
    private String memberUin;
    private String proposalNumber;
    private String proposalUrl;
    private Boolean addition = Boolean.FALSE;

    public String getProposalNumber() {
        return proposalNumber;
    }

    public void setProposalNumber(String proposalNumber) {
        this.proposalNumber = proposalNumber;
    }

    public Boolean getAddition() {
        return addition;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getAnnouncementNumber() {
        return announcementNumber;
    }

    public void setAnnouncementNumber(String announcementNumber) {
        this.announcementNumber = announcementNumber;
    }

    public String getProposalUrl() {
        return proposalUrl;
    }

    public void setProposalUrl(String proposalUrl) {
        this.proposalUrl = proposalUrl;
    }

    public void setMemberUin(String memberUin) {
        this.memberUin = memberUin;
    }

    public String getMemberUin() {
        return memberUin;
    }

    public void setAddition(Boolean addition) {
        this.addition = addition;
    }
}

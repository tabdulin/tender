package kz.inessoft.egz.clientapp.service.announcement;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import kz.inessoft.egz.clientapp.service.GoogleCalendarService;
import kz.inessoft.egz.clientapp.service.PortalService;
import kz.inessoft.egz.clientapp.service.utils.AnnouncementUtils;
import kz.inessoft.egz.clientapp.service.utils.DirectoryUtils;
import kz.inessoft.egz.clientapp.service.utils.PortalUtils;
import kz.inessoft.egz.clientapp.service.utils.SelenideUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.*;

/**
 * @author talgat.abdulin
 */
public class AnnouncementService {
    private static Logger logger = LoggerFactory.getLogger(AnnouncementService.class);
    private PortalService portal = new PortalService();

    public Announcement getAnnouncement(String number) throws AnnouncementException {
        try {
            open(PortalUtils.createAnnouncementUrl(number));
            $(".content-block").shouldBe(visible);

            String announcementNumber = $(By.xpath("//div[label='Номер объявления']/div/input[@type='text']")).getValue();
            String firstVersionId = StringUtils.substringBefore(announcementNumber, "-");
            String currentVersion = StringUtils.substringAfter(announcementNumber, "-");
            if (!StringUtils.equals(currentVersion, "1")) {
                logger.warn("Announcement number is not of first version: {}. Opening first version", announcementNumber);
                open(PortalUtils.createAnnouncementUrl(firstVersionId));
                $(".content-block").shouldBe(visible);
            }

            Announcement a = new Announcement();
            a.setNumber(firstVersionId);
            SortedMap<Integer, String> versions = new TreeMap<>();
            versions.put(1, firstVersionId);
            ElementsCollection elements = $$(".content-block .panel-default .alert-success a");
            for (SelenideElement e : elements) {
                if (!StringUtils.startsWith(e.getText(), number)
                        && !StringUtils.contains(e.getText(), "-")) {
                    continue;
                }

                Integer version = Integer.valueOf(StringUtils.substringAfter(e.getText(), "-"));
                String id = StringUtils.substringAfterLast(e.attr("href"), "/");
                logger.debug("Announcement number {}-{}, id: {}", firstVersionId, version, id);
                versions.put(version, id);
            }

            a.setVersions(versions);
            if (versions.lastKey() > 1) {
                open(PortalUtils.createAnnouncementUrl(versions.get(versions.lastKey())));
                $(".content-block").shouldBe(visible);
            }

            a.setName($(By.xpath("//div[label='Наименование объявления']/div/input[@type='text']")).getValue());
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strPublicationDate = $(By.xpath("//div[label='Дата публикации объявления']/div/input[@type='text']")).getValue();
            a.setPublicationDate(sf.parse(strPublicationDate));
            String strStartDate = $(By.xpath("//div[label='Срок начала приема заявок' or label='Предварительный срок начала приема заявок']/div/input[@type='text']")).getValue();
            String strEndDate = $(By.xpath("//div[label='Срок окончания приема заявок' or label='Предварительный срок окончания приема заявок']/div/input[@type='text']")).getValue();
            a.setStartProposalTime(sf.parse(strStartDate));
            a.setEndProposalTime(sf.parse(strEndDate));

            if ($(By.xpath("//div[label='Срок начала приема дополнения заявок']")).exists()) {
                String strStartProposalAdditionTime = $(By.xpath("//div[label='Срок начала приема дополнения заявок']/div/input[@type='text']")).getValue();
                a.setStartProposalAdditionTime(sf.parse(strStartProposalAdditionTime));
            }

            if ($(By.xpath("//div[label='Срок окончания приема дополнения заявок']")).exists()) {
                String strEndProposalAdditionTime = $(By.xpath("//div[label='Срок окончания приема дополнения заявок']/div/input[@type='text']")).getValue();
                a.setEndProposalAdditionTime(sf.parse(strEndProposalAdditionTime));
            }

            if($(By.xpath("//div[label='Срок начала обсуждения']")).exists()) {
                String strStartDiscussion = $(By.xpath("//div[label='Срок начала обсуждения']/div/input[@type='text']")).getValue();
                a.setStartDiscussionTime(sf.parse(strStartDiscussion));
            }

            if($(By.xpath("//div[label='Срок окончания обсуждения']")).exists()) {
                String strEndDiscussion = $(By.xpath("//div[label='Срок окончания обсуждения']/div/input[@type='text']")).getValue();
                a.setEndDiscussionTime(sf.parse(strEndDiscussion));
            }

            a.setPrice(Float.parseFloat($(By.xpath("//tr[th='Сумма закупки']/td")).getText()));
            a.setLots(getAnnouncementLots(number));
            return a;
        } catch (Exception e) {
            throw new AnnouncementException("Announcement can be built for announcement number: " + number, e);
        }
    }

    private List<AnnouncementLot> getAnnouncementLots(String number) {
        List<AnnouncementLot> lots = new LinkedList<AnnouncementLot>();
        open(PortalUtils.createAnnouncementLotsUrl(number));
        $(".tab-content").shouldBe(visible);
        ElementsCollection rows = $$(".tab-content .table tr");
        for (SelenideElement row : rows) {
            ElementsCollection cells = row.findAll("td");
            if (cells.isEmpty()) {
                continue;
            }

            AnnouncementLot lot = new AnnouncementLot();
            lot.setNumber(cells.get(1).text());
            lot.setCustomer(cells.get(2).text());
            lot.setName(cells.get(3).text());
            lot.setDetails(cells.get(4).text());
            lot.setPrice(Float.parseFloat(cells.get(5).text()));
            lots.add(lot);
        }
        return lots;
    }

    public void downloadAnnouncementDocuments(String number) throws AnnouncementException {
        try {
            Announcement a = getAnnouncement(number);
            File directory = DirectoryUtils.createAnnouncementDirectory(a.getNumber());
            SelenideUtils.screenshot(directory, "announcement");
            for (Integer version : a.getVersions().keySet()) {
                logger.info("Downloading documents for contest: {}", a.getVersions().get(version));
                File documentsDirectory = DirectoryUtils.createAnnouncementDocumentsDirectory(a.getNumber(), version);
                open(PortalUtils.createAnnouncementDocumentsUrl(a.getVersions().get(version)));
                $(".tab-content .panel-heading").scrollTo();
                for (SelenideElement button : $$(".table tr .btn")) {
                    button.click();
                    $("#ModalShowFiles").shouldBe(visible);
                    for (SelenideElement link : $$("#ModalShowFilesBody tr td a")) {
                        link.shouldBe(visible);
                        PortalUtils.downloadFile(link, documentsDirectory);
                    }

                    $("#ModalShowFiles .modal-footer button.btn").click();
                    $("#ModalShowFiles").shouldNotBe(visible);
                    $(".tab-content .panel-heading").scrollTo();
                }

                logger.info("Downloaded documents for announcement: {}-{}", a.getNumber(),version);
            }
        } catch (Exception e) {
            throw new AnnouncementException("Cannot download announcement: " + number, e);
        }
    }

    public void downloadAnnouncementDiscussions(String id) throws AnnouncementException {
        try {
            Announcement announcement = getAnnouncement(id);
            open(PortalUtils.createAnnouncementDiscussionsUrl(announcement.getNumber()));
            File discussionsDirectory = DirectoryUtils.createAnnouncementDiscussionsDirectory(id);
            SelenideUtils.screenshot(discussionsDirectory, "discussions");
            int discussionsNumber = $$(".tab-content .table tr td a").size();
            logger.debug("Announcement {} discussions number: {}", id, discussionsNumber);
            if (discussionsNumber == 0) {
                logger.info("There is no discussions for announcement: {}", id);
                return;
            }

            for (int i = 0; i < discussionsNumber; i++) {
                $(".tab-content").scrollTo();
                if (i > 2) {
                    $$(".tab-content .table a").get(i - 2).scrollTo();
                }

                SelenideElement discussion = $$(".tab-content .table a").get(i);
                String title = discussion.text();
                logger.debug("Downloading discussion: {}", title);
                if (FileUtils.getFile(discussionsDirectory + "/" + title + ".png").exists()) {
                    logger.debug("Discussion is already downloaded: {}", title);
                    continue;
                }

                discussion.click();
                $(withText("Просмотр сообщения")).shouldBe(visible);
                SelenideUtils.screenshot(discussionsDirectory, title);
                $(byText("Вернуться к объявлению")).click();
                $(".tab-content").shouldBe(visible);
                logger.debug("Discussion was downloaded: {}", title);
            }
        } catch (Exception e) {
            throw new AnnouncementException("Discussions were not downloaded for announcement: " + id, e);
        }
    }

    public void downloadAnnouncementProtocols(String id) throws AnnouncementException {
        Announcement announcement = getAnnouncement(id);
        open(PortalUtils.createAnnouncementProtocolUrl(announcement.getLastVersionId()));
        File protocolsDirectory = DirectoryUtils.createAnnouncementProtocolsDirectory(announcement.getNumber());
        SelenideUtils.screenshot(protocolsDirectory, "protocols");

        for (SelenideElement panel : $$(".tab-content .panel")) {
            String section = panel.find(".panel-heading b").text();
            logger.debug("Processing protocol section: {}", section);
            for (SelenideElement protocolLink : panel.findAll(".panel-body a")) {
                PortalUtils.downloadFile(protocolLink, protocolsDirectory, section);
            }
        }
    }    

    public void registerEvents(Announcement announcement) throws AnnouncementException {
        try {
            GoogleCalendarService googleCalendarService = new GoogleCalendarService();
            for (AnnouncementEvent event : AnnouncementEvent.values()) {
                Date eventDate = null;
                switch (event) {
                    case StartDiscussion:
                        eventDate = announcement.getStartDiscussionTime();
                        break;
                    case EndDiscussion:
                        eventDate = announcement.getEndDiscussionTime();
                        break;
                    case StartProposal:
                        eventDate = announcement.getStartProposalTime();
                        break;
                    case EndProposal:
                        eventDate = announcement.getEndProposalTime();
                        break;
                    case StartProposalAddition:
                        eventDate = announcement.getStartProposalAdditionTime();
                        break;
                    case EndProposalAddition:
                        eventDate = announcement.getEndProposalAdditionTime();
                        break;
                    default:
                        logger.warn("Event is not supported yet: {}", event.name());
                }

                if (eventDate != null) {
                    googleCalendarService.addEvent(eventDate, event.getId(announcement), event.getName(announcement),
                            AnnouncementUtils.toString(announcement), event.getColor());
                }
            }
        } catch (Exception e) {
            throw  new AnnouncementException("Some events may not be registered due to errors of announcement: "
                    + announcement.getNumber(), e);
        }
    }

    public List<AnnouncementProposal> getAnnouncementProposals(String id) throws AnnouncementException {
        Announcement announcement = getAnnouncement(id);
        List<AnnouncementProposal> members = new LinkedList<>();
        open(PortalUtils.createAnnouncementProposalsUrl(announcement.getLastVersionId()));
        if ($$("#applications").isEmpty()) {
            logger.info("No member applications yet for announcement: {}", announcement.getNumber());
            return members;
        }

        SelenideUtils.screenshot(DirectoryUtils.createProposalsDirectory(id).getAbsolutePath(), "members");
        for (SelenideElement member : $$("#applications table tr")) {
            ElementsCollection membersData = member.findAll("td");
            if (membersData.size() == 0) {
                continue;
            }

            AnnouncementProposal announcementProposal = new AnnouncementProposal();
            announcementProposal.setAnnouncementNumber(announcement.getNumber());
            announcementProposal.setProposalNumber(membersData.get(0).text());
            announcementProposal.setMemberName(membersData.get(2).text());
            announcementProposal.setProposalUrl(membersData.get(0).find("a").attr("href"));
            announcementProposal.setMemberUin(membersData.get(1).getText());
            announcementProposal.setAddition(!StringUtils.equals(
                    StringUtils.substringAfterLast(membersData.get(0).find("a").attr("href"), "/"), membersData.get(0).text()));
            members.add(announcementProposal);
            logger.debug("Member application: {}", ToStringBuilder.reflectionToString(announcementProposal));
        }

        return members;
    }

    public void downloadAnnouncementProposalDocuments(AnnouncementProposal proposal) throws AnnouncementException {
        String proposalString = ToStringBuilder.reflectionToString(proposal, ToStringStyle.SHORT_PREFIX_STYLE);
        logger.debug("Downloading announcement proposal documents: {}", proposalString);
        if (StringUtils.isBlank(proposal.getAnnouncementNumber())
                || StringUtils.isBlank(proposal.getProposalNumber())
                || StringUtils.isBlank(proposal.getProposalUrl())
                || StringUtils.isBlank(proposal.getMemberName())) {
            throw new AnnouncementException("Invalid member application: " + proposalString);
        }

        portal.openPage(proposal.getProposalUrl());
        $(byText("Документация")).shouldBe(visible).click();
        ElementsCollection panels = $$("#app_lot_docs .panel .panel-title")
                .shouldBe(CollectionCondition.sizeGreaterThan(0));
        for(SelenideElement panel : panels) {
            logger.debug("Panel title: {}", panel.text());
            SelenideElement title = panel.find("a.accordion-toggle");
            SelenideElement badge = panel.find(".badge");
            if (StringUtils.isNumeric(badge.text()) && Integer.parseInt(badge.text()) == 0) {
                logger.debug("No documents for application: {}", title.text());
                continue;
            }

            File directory = DirectoryUtils.createProposalDocumentsDirectory(proposal.getAnnouncementNumber(),
                    proposal.getProposalNumber(), proposal.getMemberName(), title.text());
            String panelId = "#" + StringUtils.substringAfterLast(title.attr("href"), "#");
            title.click();
            $(panelId).shouldBe(visible);
            for (SelenideElement link : $(panelId).$$("a")) {
                PortalUtils.downloadFile(link, directory);
            }

            title.click();
            $(panelId).shouldNotBe(visible);
            title.scrollTo();
        }

        logger.debug("Announcement proposal documents were downloaded: {}", proposalString);
    }

    public void downloadAnnouncementProposalAdditionDocuments(AnnouncementProposal proposal) throws AnnouncementException {
        String proposalString = ToStringBuilder.reflectionToString(proposal, ToStringStyle.SHORT_PREFIX_STYLE);
        logger.debug("Downloading announcement proposal additional documents: {}", proposalString);
        if (StringUtils.isBlank(proposal.getAnnouncementNumber())
                || StringUtils.isBlank(proposal.getProposalNumber())
                || StringUtils.isBlank(proposal.getProposalUrl())
                || StringUtils.isBlank(proposal.getMemberName())
                || !proposal.getAddition()) {
            throw new AnnouncementException("Invalid member application: " + proposalString);
        }

        portal.openPage(proposal.getProposalUrl());
        if (!$(byText("Документация (для повторного рассмотрения)")).is(visible)) {
            logger.debug("No announcement proposal additional documents");
            return;
        }

        $(byText("Документация (для повторного рассмотрения)")).shouldBe(visible).click();
        ElementsCollection panels = $$("#app_lot_docs .panel-success .panel-title")
                .shouldBe(CollectionCondition.sizeGreaterThan(0));
        for (SelenideElement panel : panels) {
            SelenideElement title =  panel.find("a.accordion-toggle");
            SelenideElement badge = panel.find(".badge");
            logger.debug("Panel title: {}", panel.text());
            if (StringUtils.isNumeric(badge.text()) && Integer.parseInt(badge.text()) == 0) {
                logger.debug("No documents for application: {}", title.text());
                continue;
            }

            File directory = DirectoryUtils.createProposalAdditionDocumentsDirectory(proposal.getAnnouncementNumber(),
                    proposal.getProposalNumber(), proposal.getMemberName(), title.text());
            String panelId = "#" + StringUtils.substringAfterLast(title.attr("href"), "#");
            title.click();
            $(panelId).shouldBe(visible);
            for (SelenideElement link : $(panelId).$$("a")) {
                PortalUtils.downloadFile(link, directory);
            }

            title.click();
            $(panelId).shouldNotBe(visible);
            title.scrollTo();
        }

        logger.debug("Announcement proposal additional documents were downloaded: {}", proposalString);
    }

    /**
     * @author talgat.abdulin
     */
    public static class AnnouncementException extends Exception {
        private String message;

        public AnnouncementException(String s) {
            this.message = s;
        }

        public AnnouncementException(String s, Exception e) {
            super(e);
            this.message = s;
        }

        @Override
        public String getMessage() {
            return message;
        }
    }
}

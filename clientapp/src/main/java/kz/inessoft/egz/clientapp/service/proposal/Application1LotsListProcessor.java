package kz.inessoft.egz.clientapp.service.proposal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by alexey on 22.07.16.
 */
class Application1LotsListProcessor implements ApplicationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(Application1LotsListProcessor.class);

    public void process(Proposal proposal) {
        $(byText("Приложение 1 (Перечень лотов)")).shouldBe(visible);
        if ($(byValue("Сформировать документ")).is(visible)) {
            $(byValue("Сформировать документ")).click();
            $(byText("Подписать")).shouldBe(visible).click();
            $("#show_doc_block1").should(exist);
        } else {
            logger.info("Application 1 has been already formed");
        }

        $(byText("Вернуться в список документов")).click();
    }
}

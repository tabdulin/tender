package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.utils.PortalUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
/**
 * Created by alexey on 22.07.16.
 */
class Application2TSProcessor implements ApplicationProcessor {
    public void process(Proposal proposal) {
        $(byText("Техническая спецификация/Проектно-сметная документация")).shouldBe(visible);
        if (StringUtils.isEmpty(proposal.getLot())) {
            $$("tr a.btn").stream().forEach((button) -> {
                button.shouldBe(visible).click();
                PortalUtils.upload(proposal.getApplication2documents());
                $(byText("Вернуться в список лотов")).click();
            });
        } else {
            $$("tr").filterBy(text(proposal.getLot())).first().find(By.cssSelector("a.btn")).click(); // Добавить или Просмотреть
            $(byText("Вернуться в список лотов")).click();
        }

        $(byText("Вернуться в заявку")).click();
    }
}

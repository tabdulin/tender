package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Condition;
import kz.inessoft.egz.clientapp.service.utils.SelenideUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byValue;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.visible;

/**
 * Created by alexey on 22.07.16.
 */
class Application4KonkursAgreementProcessor implements ApplicationProcessor {
    private static Logger logger = LoggerFactory.getLogger(Application4KonkursAgreementProcessor.class);

    public void process(Proposal proposal) {
        $(".panel-heading").$(byText("Приложение 4 (Соглашение об участии в конкурсе)")).shouldBe(visible);
        if ($(byValue("Принять соглашение")).isDisplayed()) {
            $(byValue("Принять соглашение")).shouldBe(Condition.visible).click();
            SelenideUtils.waitPageLoadComplete();
            $(byText("Подписать")).shouldBe(Condition.visible).click();
            $(byText("Файлы успешно прикреплены")).shouldBe(Condition.exist);
        } else {
            logger.info("Application 4 has been already formed");
        }

        $(byText("Вернуться в список документов")).click();
    }
}

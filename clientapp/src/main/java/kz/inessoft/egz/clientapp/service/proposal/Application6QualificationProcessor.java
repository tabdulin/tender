package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Condition;
import kz.inessoft.egz.clientapp.service.utils.PortalUtils;
import kz.inessoft.egz.clientapp.service.utils.SelenideUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static kz.inessoft.egz.clientapp.service.utils.SelenideUtils.withSubElement;
import static org.openqa.selenium.By.cssSelector;

/**
 * Created by alexey on 22.07.16.
 */
class Application6QualificationProcessor implements ApplicationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(Application6QualificationProcessor.class);

    public void process(Proposal proposal) {
        $(byText("Сведения о квалификации при закупках услуг")).shouldBe(visible);

        if ($(byText("Удалить приложение")).is(visible)) {
            logger.info("Application 6 is already filled");
            $(byText("Вернуться в список документов")).click();
            return;
        }

        $$("tr a.doc_show").stream().forEach((link) -> {
            String lot = link.closest("tr").findAll("td").get(1).text();
            if (StringUtils.isNotEmpty(proposal.getLot())
                    && !StringUtils.equalsIgnoreCase(proposal.getLot(), lot)) {
                return;
            }

            link.click();
            try {
                fillServices(proposal);
                fillHardware(proposal);
                fillStaff(proposal);
            } catch (Exception e) {
                logger.error("Application 6 was not formed for lot: {}", lot, e);
            }

            $(withText("Вернуться")).click();
        });

        $(byText("Сведения о квалификации при закупках услуг")).shouldBe(visible);
        proposal.getApplication6employees().stream()
                .forEach((employee) -> PortalUtils.upload(employee.getDocumentFile()));
        proposal.getApplication6hardware().stream()
                .forEach((hardware) -> PortalUtils.upload(hardware.getDocumentFile()));
        proposal.getApplication6services().stream()
                .forEach((s) -> PortalUtils.upload(s.getDocumentFile()));

        $$(byName("lot_id[]")).stream().forEach((input) -> {
            String lot = input.closest("tr").findAll("td").get(1).text();
            if (StringUtils.isNotEmpty(proposal.getLot())
                    && !StringUtils.equalsIgnoreCase(proposal.getLot(), lot)) {
                return;
            }

            input.setSelected(true);
        });

        $(byText("Сформировать приложение")).click();
        $(byText("Подписать")).shouldBe(visible).click();
        $(byText("Документ успешно сформирован и подписан")).shouldBe(Condition.visible);

        $(byText("Вернуться в список документов")).click();
    }

    private void fillServices(Proposal proposal) throws IOException, ParseException {
        $(".nav-tabs").$(byText("Объем оказанных поставщиком услуг")).click();
        $(".nav-tabs .active").$(byText("Объем оказанных поставщиком услуг")).shouldBe(visible);

        for (Proposal.Service service : proposal.getApplication6services()) {
            $$(".btn").filterBy(visible).filterBy(withSubElement(cssSelector(".glyphicon-plus"))).first().click();
            $("#work").val(service.getName());
            $("#y1").val(service.getStartYear());
            $(byName("m1")).selectOptionByValue(service.getStartMonth());
            $("#y2").val(service.getStartYear());
            $(byName("m2")).selectOptionByValue(service.getStartMonth());
            $("#pay").val(service.getPrice());
            $(byName("country_code")).selectOption(service.getCountry());
            $("#place").val(service.getCustomerAddress());
            $("#doc_type_name").val(service.getDocumentName());
            $("#doc_num").val(service.getDocumentNumber());
            $("#doc_date").val(service.getDocumentDate());
            $("#iin").val(service.getCustomerUIN());
            $("#name").val(service.getCustomerName());

            $(byText("Сохранить")).click();
            SelenideUtils.waitPageLoadComplete();
            $(withText("Вернуться")).click();
        }

        $(".nav-tabs .active").$(byText("Объем оказанных поставщиком услуг")).shouldBe(visible);
    }

    private void fillHardware(Proposal proposal) throws IOException, ParseException {
        $(".nav-tabs").$(byText("Cведения о наличии оборудования")).click();
        $(".nav-tabs .active").$(byText("Cведения о наличии оборудования")).shouldBe(visible);
        for (Proposal.Hardware hardware : proposal.getApplication6hardware()) {
            $$(".btn").filterBy(visible).filter(withSubElement(cssSelector(".glyphicon-plus"))).first().click();
            $(".panel-heading").$(byText("Cведения о наличии оборудования")).shouldBe(visible);
            $("#name").val(hardware.getName());
            $("#cnt").val(hardware.getAmount());
            $("#state").val(hardware.getCondition());
            $(byName("is_rent")).selectRadio(StringUtils.equals("Арендованное", hardware.getRented()) ? "1" : "0");
            $("#rent_name").val(hardware.getOwner());
            $("#doc_type_name").val(hardware.getDocumentName());
            $("#doc_num").val(hardware.getDocumentNumber());
            $("#doc_date").val(hardware.getDocumentDate());
            $(byText("Сохранить")).click();
            SelenideUtils.waitPageLoadComplete();
            $(withText("Вернуться")).click();
        }

        $(".nav-tabs .active").$(byText("Cведения о наличии оборудования")).shouldBe(visible);
    }

    private void fillStaff(Proposal proposal) throws IOException {
        $(".nav-tabs").$(byText("Cведения о квалифицированных работников")).click();
        $(".nav-tabs .active").$(byText("Cведения о квалифицированных работников")).shouldBe(visible);

        for (Proposal.Employee employee : proposal.getApplication6employees()) {
            $$(".btn").filterBy(visible).filterBy(withSubElement(cssSelector(".glyphicon-plus"))).first().click();
            $(".panel-heading").$(byText("Cведения о квалифицированных работников")).shouldBe(visible);
            /*
            // TODO: make iin processing (actually it't not neccessary)
            if (StringUtils.isNotEmpty(employee.getIin())) {
                $("#iin").val(employee.getIin());
                $(byValue("find")).click();
            }
            */

            $(byValue("nokz")).click();
            $(byName("country_code")).shouldBe(visible).selectOption(employee.getCountry());
            $("#fio").val(employee.getName());
            $("#staj").val(employee.getExperience());
            $("#dipl").val(employee.getQualification());
            $("#kateg").val(employee.getCategory());
            $(byText("Сохранить")).click();
            SelenideUtils.waitPageLoadComplete();
            $(withText("Вернуться")).click();
        }

        $(".nav-tabs .active").$(byText("Cведения о квалифицированных работников")).shouldBe(visible);
    }
}

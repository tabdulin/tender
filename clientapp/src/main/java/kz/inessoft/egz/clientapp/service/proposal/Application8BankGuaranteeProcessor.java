package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.utils.PortalUtils;
import org.apache.commons.lang3.StringUtils;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Пока добавляется только электронная копия платежного поручения. Электронная банковская гарантия пока не реализовано
 * todo Сделать электронную банковскую гарантию (если это вообще надо)
 */
class Application8BankGuaranteeProcessor implements ApplicationProcessor {
    public void process(Proposal proposal) {
        $(byText("Добавление обеспечения заявки")).shouldBe(visible);
        $$("tr a.btn").stream().forEach((button) -> {
            String lot = button.closest("tr").findAll("td").get(0).text();
            if (!StringUtils.isEmpty(proposal.getLot()) && !StringUtils.equalsIgnoreCase(lot, proposal.getLot())) {
                return;
            }

            button.shouldBe(visible).click();
            $(".panel-heading").$(byText("Добавление обеспечения заявки")).shouldBe(visible);
            $(byName("typeDoc")).selectOption("Платёжное поручение / банковская гарантия(электронная копия)");

            PortalUtils.upload(proposal.getApplication8documents());
            $(byText("Назад")).click();
        });

        $(byText("Вернуться в заявку")).shouldBe(visible).click();
    }
}

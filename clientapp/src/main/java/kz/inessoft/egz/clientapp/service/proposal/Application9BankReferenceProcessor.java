package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.utils.PortalUtils;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.visible;

/**
 * Created by alexey on 25.07.16.
 */
class Application9BankReferenceProcessor implements ApplicationProcessor {
    public void process(Proposal proposal) {
        $(".panel-heading").$(byText("Приложение 9 (Справка банка об отсутствии задолженности)")).shouldBe(visible);
        PortalUtils.upload(proposal.getApplication9documents());
        $(byText("Вернуться в список документов")).shouldBe(visible).click();
    }
}

package kz.inessoft.egz.clientapp.service.proposal;

/**
 * @author talgat.abdulin
 */
interface ApplicationProcessor {
    // TODO: think about correct exception processing
    void process(Proposal proposal);
}

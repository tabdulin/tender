package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.utils.PortalUtils;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by alexey on 25.07.16.
 */
class CertificatesProcessor implements ApplicationProcessor {

    public void process(Proposal proposal) {
        $(".panel-heading").$(byText("Свидетельства, сертификаты, дипломы и другие документы")).shouldBe(visible);
        PortalUtils.upload(proposal.getCertificatesDocuments());
        $(byText("Вернуться в список документов")).click();
    }
}

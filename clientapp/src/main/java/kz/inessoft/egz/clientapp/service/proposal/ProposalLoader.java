package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.Configuration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * @author Talgat.Abdulin
 */
// TODO: think about abstracting methods by loading types
class ProposalLoader {
    private static final Logger logger = LoggerFactory.getLogger(ProposalLoader.class);
    private File companyDirectory;
    private File announcementDirectory;
    private File announcementExcel;

    public ProposalLoader(String announcement) {
        this.announcementExcel = FileUtils.getFile(announcement);
        this.announcementDirectory = announcementExcel.getParentFile();
        this.companyDirectory = Configuration.getInstance().getPortalCompanyDirectory();
    }

    void loadAnnouncement(Proposal proposal) throws ProposalService.ProposalException {
        try (InputStream is = FileUtils.openInputStream(announcementExcel)) {
            XSSFWorkbook wb = new XSSFWorkbook(is);
            Sheet sheet = wb.getSheet("Заявка");
            for (Row row : sheet) {
                String key = getValue(row, 0);
                String value = getValue(row, 1);
                switch (key) {
                    case "Номер объявления": {
                        proposal.setAnnouncement(value);
                        break;
                    }
                    case "Номер лота": {
                        proposal.setLot(value);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            throw new ProposalService.ProposalException(e);
        }
    }

    void loadRequisites(Proposal proposal) throws ProposalService.ProposalException {
        try (InputStream is = FileUtils.openInputStream(announcementExcel)) {
            XSSFWorkbook wb = new XSSFWorkbook(is);
            Sheet sheet = wb.getSheet("Реквизиты");
            for (Row row : sheet) {
                String requisite = getValue(row, 0);
                String value = getValue(row, 1);
                switch (requisite) {
                    case "Юридический адрес": {
                        proposal.setAddress(value);
                        break;
                    }
                    case "ИИК": {
                        proposal.setIik(value);
                        break;
                    }
                    case "Признак консорциума": {
                        proposal.setConsortium(StringUtils.equalsIgnoreCase("да", value));
                        break;
                    }
                }
            }
        } catch (Exception e) {
            throw new ProposalService.ProposalException(e);
        }
    }

    void loadApplication2documents(Proposal proposal) throws ProposalService.ProposalException {
        proposal.setApplication2documents(new LinkedList<>());
        Arrays.asList(FileUtils.getFile(announcementDirectory, "application-2").listFiles())
                .stream()
                .forEach((file) -> proposal.getApplication2documents().add(file.getAbsolutePath()));
    }

    void loadApplication8documents(Proposal proposal) throws ProposalService.ProposalException {
        proposal.setApplication8documents(new LinkedList<>());
        Arrays.asList(FileUtils.getFile(announcementDirectory, "application-8").listFiles())
                .stream()
                .forEach((file) -> proposal.getApplication8documents().add(file.getAbsolutePath()));
    }

    void loadApplication9documents(Proposal proposal) throws ProposalService.ProposalException {
        proposal.setApplication9documents(new LinkedList<>());
        Arrays.asList(FileUtils.getFile(announcementDirectory, "application-9").listFiles())
                .stream()
                .forEach((file) -> proposal.getApplication9documents().add(file.getAbsolutePath()));
    }

    void loadServices(Proposal proposal) throws ProposalService.ProposalException {
        try (InputStream is = FileUtils.openInputStream(announcementExcel)) {
            XSSFWorkbook wb = new XSSFWorkbook(is);
            Sheet sheet = wb.getSheet("Услуги");
            Map<String, Integer> columns = new HashMap<>();
            for (Cell header : sheet.getRow(0)) {
                columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
            }

            File servicesDirectory = FileUtils.getFile(companyDirectory, "application-6-services");
            List<Proposal.Service> services = new LinkedList<>();
            proposal.setApplication6services(services);
            for (Row row : sheet) {
                if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                    continue;
                }

                Proposal.Service service = new Proposal.Service();
                columns.keySet().stream().forEach((column) -> {
                    String value = getValue(row, columns.get(column));
                    switch (column) {
                        case "Наименование услуги": {
                            service.setName(value);
                            break;
                        }
                        case "С": {
                            String[] date = StringUtils.split(value, "-");
                            service.setStartMonth(getMonth(date[1]));
                            service.setStartYear(date[2]);
                            break;
                        }
                        case "До": {
                            String[] date = StringUtils.split(value, "-");
                            service.setEndMonth(getMonth(date[1]));
                            service.setEndYear(date[2]);
                            break;
                        }
                        case "Стоимость договора (тенге)": {
                            service.setPrice(value);
                            break;
                        }
                        case "Страна": {
                            service.setCountry(value);
                            break;
                        }
                        case "Наименование Заказчика": {
                            service.setCustomerName(value);
                            break;
                        }
                        case "Адрес Заказчика": {
                            service.setCustomerAddress(value);
                            break;
                        }
                        case "Подтверждающий документ": {
                            service.setDocumentName(value);
                            break;
                        }
                        case "Номер подтверждющего документа": {
                            service.setDocumentNumber(value);
                            break;
                        }
                        case "Дата подтверждающего документа": {
                            service.setDocumentDate(value);
                            break;
                        }
                        case "Название файла в папке application-6-services": {
                            service.setDocumentFile(FileUtils.getFile(servicesDirectory, value).getAbsolutePath());
                            break;
                        }
                    }
                });
                services.add(service);
            }
        } catch (Exception e) {
            throw new ProposalService.ProposalException(e);
        }
    }

    void loadEmployees(Proposal proposal) throws ProposalService.ProposalException {
        try (InputStream is = FileUtils.openInputStream(announcementExcel)) {
            XSSFWorkbook wb = new XSSFWorkbook(is);
            Sheet sheet = wb.getSheet("Сотрудники");
            Map<String, Integer> columns = new HashMap<>();
            for (Cell header : sheet.getRow(0)) {
                columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
            }

            File employeesDirectory = FileUtils.getFile(companyDirectory, "application-6-employees");
            List<Proposal.Employee> employees = new LinkedList<>();
            proposal.setApplication6employees(employees);
            for (Row row : sheet) {
                if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                    continue;
                }

                final Proposal.Employee employee = new Proposal.Employee();
                columns.keySet().stream().forEach((column) -> {
                    String value = getValue(row, columns.get(column));
                    switch (column) {
                        case "ИИН": {
                            employee.setIin(value);
                            break;
                        }
                        case "Страна": {
                            employee.setCountry(value);
                            break;
                        }
                        case "ФИО": {
                            employee.setName(value);
                            break;
                        }
                        case "Стаж работы в сфере оказания услуг": {
                            employee.setExperience(value);
                            break;
                        }
                        case "Квалификация (специальность) по диплому, свидетельству и др. документам об образ": {
                            employee.setQualification(value);
                            break;
                        }
                        case "Категория, разряд, класс по специальности": {
                            employee.setCategory(value);
                            break;
                        }
                        case "Название файла в папке application-6-employees": {
                            employee.setDocumentFile(FileUtils.getFile(employeesDirectory, value).getAbsolutePath());
                            break;
                        }
                    }
                });

                employees.add(employee);
            }
        } catch (Exception e) {
            throw new ProposalService.ProposalException(e);
        }
    }

    public void loadHardware(Proposal proposal) throws ProposalService.ProposalException {
        try (InputStream is = FileUtils.openInputStream(announcementExcel)) {
            XSSFWorkbook wb = new XSSFWorkbook(is);
            Sheet sheet = wb.getSheet("Оборудование");
            Map<String, Integer> columns = new HashMap<>();
            for (Cell header : sheet.getRow(0)) {
                columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
            }

            File hardwareDirectory = FileUtils.getFile(companyDirectory, "application-6-hardware");
            List<Proposal.Hardware> hardwares = new LinkedList<>();
            proposal.setApplication6hardware(hardwares);
            for (Row row : sheet) {
                if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                    continue;
                }

                final Proposal.Hardware hardware = new Proposal.Hardware();
                columns.keySet().stream().forEach((column) -> {
                    String value = getValue(row, columns.get(column));
                    switch (column) {
                        case "Наименование оборудования (механизмов, машин)": {
                            hardware.setName(value);
                            break;
                        }
                        case "Количество имеющихся единиц": {
                            hardware.setAmount(value);
                            break;
                        }
                        case "Состояние (новое, хорошее, плохое)": {
                            hardware.setCondition(value);
                            break;
                        }
                        case "Собственное / Арендованное": {
                            hardware.setRented(value);
                            break;
                        }
                        case "У кого арендовано": {
                            hardware.setOwner(value);
                            break;
                        }
                        case "Наименование подтверждающего документа": {
                            hardware.setDocumentName(value);
                            break;
                        }
                        case "Номер подтверждающего документа": {
                            hardware.setDocumentNumber(value);
                            break;
                        }
                        case "Дата подтверждающего документа": {
                            hardware.setDocumentDate(value);
                            break;
                        }
                        case "Название файла папке application-6-hardware": {
                            hardware.setDocumentFile(FileUtils.getFile(hardwareDirectory, value).getAbsolutePath());
                            break;
                        }
                    }
                });

                hardwares.add(hardware);
            }
        } catch (Exception e) {
            throw new ProposalService.ProposalException(e);
        }
    }

    public void loadCertificates(Proposal proposal) throws ProposalService.ProposalException {
        try (InputStream is = FileUtils.openInputStream(announcementExcel)) {
            XSSFWorkbook wb = new XSSFWorkbook(is);
            Sheet sheet = wb.getSheet("Сертификаты компании");
            Map<String, Integer> columns = new HashMap<>();
            for (Cell header : sheet.getRow(0)) {
                columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
            }

            File certificatesDirectory = FileUtils.getFile(companyDirectory, "application-certificates");
            proposal.setCertificatesDocuments(new LinkedList<>());
            for (Row row : sheet) {
                if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                    continue;
                }

                columns.keySet().stream().forEach((column) -> {
                    String value = getValue(row, columns.get(column));
                    switch (column) {
                        case "Название файла в папке application-certificates": {
                            proposal.getCertificatesDocuments().add(
                                    FileUtils.getFile(certificatesDirectory, value).getAbsolutePath());
                            break;
                        }
                    }
                });
            }
        } catch (Exception e) {
            throw new ProposalService.ProposalException(e);
        }
    }

    public void loadVatPayerDocuments(Proposal proposal) throws ProposalService.ProposalException {
        proposal.setVatPayerDocuments(new LinkedList<>());
        Arrays.asList(FileUtils.getFile(companyDirectory, "application-vat").listFiles())
            .stream()
            .forEach((file) -> proposal.getVatPayerDocuments().add(file.getAbsolutePath()));
    }

    private String getValue(Row row, int column) {
        String value;
        switch (row.getCell(column).getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
                value = StringUtils.removeEnd(Double.toString(row.getCell(column).getNumericCellValue()), ".0");
                break;
            default:
                value = StringUtils.trim(row.getCell(column).getStringCellValue());
                break;
        }

        return StringUtils.removeEnd(value, "\0x160");
    }

    private String getMonth(String month) {
        return Integer.toString(Integer.parseInt(month));
    }

}

package kz.inessoft.egz.clientapp.service.proposal;

import com.google.common.base.Function;
import kz.inessoft.egz.clientapp.service.utils.DirectoryUtils;
import kz.inessoft.egz.clientapp.service.utils.SelenideUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nullable;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by alexey on 22.07.16.
 */
class ProposalNextButtonWaiter implements Function<WebDriver, Boolean> {
    private Exception exception;
    private String announcementNumber;
    private String url;
    private Integer page;

    public ProposalNextButtonWaiter(String announcementNumber) {
        this.announcementNumber = announcementNumber;
    }

    public ProposalNextButtonWaiter(String announcementNumber, String url, Integer page) {
        this.announcementNumber = announcementNumber;
        this.url = url;
        this.page = page;
    }

    @Nullable
    public Boolean apply(@Nullable WebDriver webDriver) {
        String url = webDriver.getCurrentUrl();
        if (checkUrl(url))
            return true;
        if ($("#errors").isDisplayed()) {
            SelenideUtils.screenshot(DirectoryUtils.createProposalsDirectory(announcementNumber),
                    "proposal_p" + getPageNumber() + "_error");
            exception = new Exception("Error while processing proposal page " + getPageNumber() +
                    ". Look at proposal_p" + getPageNumber() + "_error.png screenshot.");
            return true;
        }

        return false;
    }

    public Exception getException() {
        return exception;
    }

    protected boolean checkUrl(String url) {
        return StringUtils.contains(url, this.url);
    };

    protected int getPageNumber() {
        return page;
    }
}

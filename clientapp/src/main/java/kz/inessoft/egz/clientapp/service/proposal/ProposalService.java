package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import kz.inessoft.egz.clientapp.Configuration;
import kz.inessoft.egz.clientapp.service.utils.DirectoryUtils;
import kz.inessoft.egz.clientapp.service.utils.PortalUtils;
import kz.inessoft.egz.clientapp.service.utils.SelenideUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

/**
 * @author talgat.abdulin
 */
public class ProposalService {
    private static final Logger logger = LoggerFactory.getLogger(ProposalService.class);
    private static final Map<String, Class<? extends ApplicationProcessor>> DOC_MAP = new HashMap<String, Class<? extends ApplicationProcessor>>();
    static {
        DOC_MAP.put("Приложение 1 (Перечень лотов)", Application1LotsListProcessor.class);
        DOC_MAP.put("Приложение 2 (Техническая спецификация) либо Проектно - сметная документация/Технико - экономическое обоснование", Application2TSProcessor.class);
        DOC_MAP.put("Приложение 4 (Соглашение об участии в конкурсе)", Application4KonkursAgreementProcessor.class);
        DOC_MAP.put("Приложение 6 (Сведения о квалификации Поставщика при закупках услуг)", Application6QualificationProcessor.class);
        DOC_MAP.put("Приложение 8 (Обеспечение заявки, либо гарантийный денежный взнос)", Application8BankGuaranteeProcessor.class);
        DOC_MAP.put("Приложение 9 (Справка банка об отсутствии задолженности)", Application9BankReferenceProcessor.class);
        DOC_MAP.put("Сведения об отсутствии налоговой задолженности", TaxDebtProcessor.class);
        DOC_MAP.put("Свидетельства, сертификаты, дипломы и другие документы", CertificatesProcessor.class);
        DOC_MAP.put("Свидетельство о постановке на учет по НДС", VatPayerProcessor.class);
    }

    public void deleteProposal(Proposal proposal) {
        open(PortalUtils.createMyPropposalsUrl());
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void createProposal(Proposal proposal) {
        try {
            open(PortalUtils.createProposalUrl(proposal.getAnnouncement()));
            if (!$(By.xpath("//div[label='Юридический адрес']")).exists()) {
                logger.info("Proposal already created");
                return;
            }

            $(By.xpath("//div[label='Юридический адрес']/div/select")).selectOption(proposal.getAddress());
            $(By.xpath("//div[label='ИИК']/div/select")).selectOption(proposal.getIik());

            if (proposal.getConsortium())
                $(By.xpath("//input[@type='radio' and @name='consortium' and @value='1']")).click();
            else
                $(By.xpath("//input[@type='radio' and @name='consortium' and @value='0']")).click();

            SelenideUtils.screenshot(DirectoryUtils.createProposalsDirectory(proposal.getAnnouncement()), "proposal_p1");
            $("#next").click();
            ProposalNextButtonWaiter waiter = new ProposalNextButtonWaiter(proposal.getAnnouncement(), "/ru/application/lots", 1);
            Selenide.Wait().until(waiter);
            if (waiter.getException() != null) {
                throw waiter.getException();
            }
        } catch (Exception e) {
            logger.error("No proposal was created for announcement: {}", proposal.getAnnouncement(), e);
        }
    }

    public void selectLots(Proposal proposal) {
        try {
            open(PortalUtils.createProposalUrl(proposal.getAnnouncement()));
            $(byText("Добавление лотов для участия в закупке")).shouldBe(visible);
            if (!$("#select_lots").exists()) {
                logger.info("Lots were already selected");
                return;
            }

            while ($$("#select_lots tbody tr").size() > 0) {
                $$("#select_lots tbody tr").stream().forEach((row) -> {
                    String lot = row.findAll("td").get(1).text();
                    if (StringUtils.isNotEmpty(proposal.getLot()) && !StringUtils.equalsIgnoreCase(proposal.getLot(), lot)) {
                        return;
                    }

                    row.find(byName("selectLots[]")).setSelected(true);
                });

                $("#add_lots").click();
                $(byText("Удалить лоты из заявки")).shouldBe(visible);
                $(".panel-heading").scrollTo();
                $(byAttribute("href", "#select_lots")).click();
            }

            $("#next").shouldBe(visible).click();
            ProposalNextButtonWaiter finishedChecker = new ProposalNextButtonWaiter(proposal.getAddress(), "/ru/application/docs/", 2);
            Selenide.Wait().until(finishedChecker);
            if (finishedChecker.getException() != null) {
                throw finishedChecker.getException();
            }
        } catch (Exception e) {
            logger.error("No lots were selected for announcement: {}", proposal.getAnnouncement(), e);
        }
    }

    public void fillApplications(Proposal propsal) {
        try {
            open(PortalUtils.createProposalUrl(propsal.getAnnouncement()));
            String xpath = "//table//tr/td[normalize-space(text())='Обязателен']/../td/a";
            List<String> applications = new LinkedList<String>();
            for (SelenideElement applicationLink : $$(By.xpath(xpath))) {
                applications.add(applicationLink.text());
            }

            for (String application : applications) {
                $(byText(application)).shouldBe(visible).click();
                Class<? extends ApplicationProcessor> aClass = DOC_MAP.get(application);
                if (aClass == null) {
                    throw new Exception("Can't instantiate processor for " + application);
                }

                ApplicationProcessor processor = aClass.newInstance();
                processor.process(propsal);
            }

            $("#next").shouldBe(visible).click();
            ProposalNextButtonWaiter finishedChecker = new ProposalNextButtonWaiter(propsal.getAnnouncement(), "/ru/application/priceoffers/", 3);
            Selenide.Wait().until(finishedChecker);
            if (finishedChecker.getException() != null) {
                throw finishedChecker.getException();
            }
        } catch (Exception e) {
            logger.error("Documents were not filled for announcement: {}", propsal.getAnnouncement(), e);
        }
    }

    public Proposal loadProposalExcel(String excel) throws ProposalException {
        try {
            logger.debug("Loading proposal from excel: {}", excel);
            Proposal proposal = new Proposal();
            ProposalLoader loader = new ProposalLoader(excel);
            loader.loadAnnouncement(proposal);
            loader.loadRequisites(proposal);
            loader.loadApplication2documents(proposal);
            loader.loadServices(proposal);
            loader.loadHardware(proposal);
            loader.loadEmployees(proposal);
            loader.loadApplication8documents(proposal);
            loader.loadApplication9documents(proposal);
            loader.loadCertificates(proposal);
            loader.loadVatPayerDocuments(proposal);
            logger.debug("Loaded proposal: {}", ToStringBuilder.reflectionToString(proposal, new RecursiveToStringStyle()));
            return proposal;
        } catch (Exception e) {
            if (e instanceof ProposalException) {
                throw e;
            }

            throw new ProposalException(e);
        }
    }

    public Proposal loadDefaultProposal(String announcement) throws ProposalException {
        logger.debug("Loading default proposal for announcement: {}", announcement);
        Proposal proposal = loadProposalExcel(Configuration.getInstance().getDefaultProposal());
        proposal.setAnnouncement(announcement);
        logger.debug("Loaded proposal: {}", ToStringBuilder.reflectionToString(proposal, new RecursiveToStringStyle()));
        return proposal;
    }

    public Proposal loadDefaultProposal(String announcement, String lot) throws ProposalException {
        logger.debug("Loading default proposal for announcement {} and lot {}", announcement, lot);
        Proposal proposal = loadDefaultProposal(announcement);
        proposal.setLot(lot);
        logger.debug("Loaded proposal: {}", ToStringBuilder.reflectionToString(proposal, new RecursiveToStringStyle()));
        return proposal;
    }

    public static class ProposalException extends Exception {
        public ProposalException(Exception e) {
            super(e);
        }
    }
}

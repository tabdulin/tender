package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.utils.PortalUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byValue;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by alexey on 25.07.16.
 */
class TaxDebtProcessor implements ApplicationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(TaxDebtProcessor.class);

    public void process(Proposal proposal) {
        $(".panel-heading").$(byText("Получение сведений о налоговой задолженности")).shouldBe(visible);
        if (proposal.getTaxDebtDocuments() == null || proposal.getTaxDebtDocuments().isEmpty()) {
            // TODO: implement getting tax debt by service
            if ($(byValue("Удалить")).is(visible)) {
                logger.info("Tax debt is already filled");
                $(byText("Вернуться к списку документов")).shouldBe(visible).click();
                return;
            }

            $(byName("tax_id")).setSelected(true);
            $(byValue("Прикрепить")).click();
            $(byText("Сведения успешно прикреплены")).shouldBe(visible);
            $(byText("Вернуться к списку документов")).shouldBe(visible).click();
        } else {
            PortalUtils.upload(proposal.getTaxDebtDocuments());
        }

        $(byText("Вернуться в список документов")).click();
    }
}

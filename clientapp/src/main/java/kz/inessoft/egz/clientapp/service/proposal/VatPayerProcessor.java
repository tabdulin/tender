package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.utils.PortalUtils;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.visible;

/**
 * Created by alexey on 25.07.16.
 */
class VatPayerProcessor implements ApplicationProcessor {

    public void process(Proposal proposal) {
        $(".panel-heading").$(byText("Свидетельство о постановке на учет по НДС")).shouldBe(visible);
        PortalUtils.upload(proposal.getVatPayerDocuments());
        $(byText("Вернуться в список документов")).shouldBe(visible).click();
    }
}

package kz.inessoft.egz.clientapp.service.utils;

import kz.inessoft.egz.clientapp.service.announcement.Announcement;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.text.NumberFormat;

/**
 * @author Talgat.Abdulin
 */
public class AnnouncementUtils {
    public static String shorten(String name) {
        String result = StringUtils.replace(name, "Электронные государственные закупки услуг ", "");
        result = StringUtils.replace(result, "Государственные закупки услуг ", "");
        result = StringUtils.replace(result, "государственные закупки услуг ", "");
        result = StringUtils.replace(result, " способом открытого конкурса", "");
        result = StringUtils.replace(result, " способом конкурса", "");
        result = StringUtils.replace(result, "Республики Казахстан", "РК");
        result = StringUtils.replace(result, "Автоматизированная информационная система", "АИС");
        result = StringUtils.replace(result, "информационных систем", "ИС");
        result = StringUtils.replace(result, "информационной системы", "ИС");
        result = StringUtils.replace(result, "по сопровождению", "сопровождение");
        result = StringUtils.replace(result, "программного обеспечения", "ПО");
        result = StringUtils.replace(result, "программное обеспечение", "ПО");
        result = StringUtils.replace(result, "прикладного программного обеспечения", "ППО");
        return result;
    }

    public static String toString(Announcement a) {
        String pattern = "yyyy-MM-dd hh:mm:ss";
        StringBuilder sb = new StringBuilder();
        if (a.getNumber() != null) {
            sb.append("Ссылка на конкурс: " + PortalUtils.createAnnouncementUrl(a.getId()) + "\n");
            sb.append("Номер объявления: " + a.getNumber() + "\n");
        }

        if (a.getPublicationDate() != null) {
            sb.append("Дата публикации объявления: " + DateFormatUtils.format(a.getPublicationDate(), pattern) +"\n");
        }

        if (a.getName() != null) {
            sb.append("Наименование объявления: " + a.getName() + "\n");
        }

        if (a.getPrice() != null) {
            sb.append("Стоимость закупки: " + NumberFormat.getIntegerInstance().format(a.getPrice()) + " тенге\n");
        }

        if (a.getStartDiscussionTime() != null) {
            sb.append("Срок начала обсуждения: " + DateFormatUtils.format(a.getStartDiscussionTime(), pattern) + "\n");
        }

        if (a.getEndDiscussionTime() != null) {
            sb.append("Срок завершения обсуждения: " + DateFormatUtils.format(a.getEndDiscussionTime(), pattern) + "\n");
        }

        if (a.getStartProposalTime() != null) {
            sb.append("Срок начала приема заявок: " + DateFormatUtils.format(a.getStartProposalTime(), pattern) + "\n");
        }

        if (a.getEndProposalTime() != null) {
            sb.append("Срок завершения приема заявок: " + DateFormatUtils.format(a.getEndProposalTime(), pattern) + "\n");
        }

        if (a.getStartProposalAdditionTime() != null) {
            sb.append("Срок начала приема дополнения заявок: " + DateFormatUtils.format(a.getStartProposalAdditionTime(), pattern) + "\n");
        }

        if (a.getEndProposalAdditionTime() != null) {
            sb.append("Срок окончания приема дополнения заявок: " + DateFormatUtils.format(a.getEndProposalAdditionTime(), pattern) + "\n");
        }

        return sb.toString();
    }

}

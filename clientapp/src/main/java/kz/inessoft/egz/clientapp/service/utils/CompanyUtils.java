package kz.inessoft.egz.clientapp.service.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Talgat.Abdulin
 */
public class CompanyUtils {
    public static String shorten(String name) {
        String result = StringUtils.replace(name, "Товарищество с ограниченной ответственностью", "ТОО");
        result = StringUtils.remove(result, "\"");
        return result;
    }
}

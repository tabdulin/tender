package kz.inessoft.egz.clientapp.service.utils;

import kz.inessoft.egz.clientapp.Configuration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author Talgat.Abdulin
 */
public class DirectoryUtils {
    private static final Logger logger = LoggerFactory.getLogger(DirectoryUtils.class);

    private static File getFirstAnnouncementDirectory(String announcementNumber) {
        File root = FileUtils.getFile(Configuration.getInstance().getTendersInfoDirectory());
        for (File file : root.listFiles()) {
            if (!file.isDirectory()) {
                continue;
            }

            if (StringUtils.startsWith(file.getName(), announcementNumber)) { // todo: possible bug with short numbers
                return file;
            }
        }

        return null;
    }

    public static File createAnnouncementDirectory(String announcementNumber) {
        String directoryName = Configuration.getInstance().getTendersInfoDirectory() + "/" + announcementNumber;
        com.codeborne.selenide.Configuration.reportsFolder = directoryName + "/.reports";
        File directory = getFirstAnnouncementDirectory(announcementNumber);
        if (directory != null && directory.exists()) {
            return directory;
        }

        directory = FileUtils.getFile(directoryName);
        directory.mkdirs();
        FileAppender fileAppender = new FileAppender();
        fileAppender.setName(announcementNumber + "-file-appender");
        fileAppender.setFile(directory.getAbsolutePath() + "/announcement.log");
        fileAppender.setEncoding("UTF-8");
        fileAppender.setLayout(new PatternLayout("%p %c: %m%n"));
        fileAppender.setThreshold(Level.DEBUG);
        fileAppender.activateOptions();
        org.apache.log4j.Logger.getRootLogger().addAppender(fileAppender);
        logger.info("Announcement directory: {}", directory.getAbsolutePath());
        return directory;
    }

    public static File createAnnouncementDocumentsDirectory(String announcementNumber, Integer version) {
        File directory = createAnnouncementDirectory(announcementNumber);
        String documentsDirectoryName = version == 1 ? "1-documents" : "3-documents";
        File documentsDirectory = FileUtils.getFile(directory.getAbsolutePath() + "/" + documentsDirectoryName);
        documentsDirectory.mkdirs();
        return documentsDirectory;
    }


    public static File createProposalsDirectory(String announcementNumber) {
        File directory = FileUtils.getFile(createAnnouncementDirectory(announcementNumber).getAbsolutePath()
                + "/4-proposals");
        directory.mkdirs();
        return directory;
    }

    public static File createProposalDirectory(String announcementNumber, String memberNumber, String memberName) {
        File directory = FileUtils.getFile(createProposalsDirectory(announcementNumber).getAbsolutePath()
                + "/" + memberNumber + " - " + CompanyUtils.shorten(memberName));
        directory.mkdirs();
        return directory;
    }

    public static File createProposalDocumentsDirectory(String announcementNumber, String memberNumber,
                                                        String memberName, String application) {
        File directory = FileUtils.getFile(createProposalDirectory(announcementNumber, memberNumber, memberName).getAbsolutePath()
                + "/" + StringUtils.replace(application, "/", ","));
        directory.mkdirs();
        return directory;
    }

    public static File createProposalAdditionsDirectory(String announcementNumber) {
        File directory = FileUtils.getFile(createAnnouncementDirectory(announcementNumber).getAbsolutePath()
                + "/5-proposals-additions");
        directory.mkdirs();
        return directory;
    }

    public static File createProposalAdditionDirectory(String announcementNumber, String proposalNumber,
                                                       String companyName) {
        File directory = FileUtils.getFile(createProposalAdditionsDirectory(announcementNumber)
                + "/" + proposalNumber + " - " + CompanyUtils.shorten(companyName));
        directory.mkdirs();
        return directory;

    }


    public static File createProposalAdditionDocumentsDirectory(String announcementNumber, String memberNumber,
                                                                String memberName, String application) {
        File directory = FileUtils.getFile(createProposalAdditionDirectory(announcementNumber, memberNumber, memberName)
                + "/" + StringUtils.replace(application, "/", ","));
        directory.mkdirs();
        return directory;
    }

    public static File createAnnouncementDiscussionsDirectory(String announcementNumber) {
        File directory = FileUtils.getFile(createAnnouncementDirectory(announcementNumber)
                + "/2-discussions");
        directory.mkdirs();
        return directory;
    }

    public static File createAnnouncementProtocolsDirectory(String announcementNumber) {
        File directory = FileUtils.getFile(createAnnouncementDirectory(announcementNumber)
                + "/6-protocols");
        directory.mkdirs();
        return directory;
    }
}
package kz.inessoft.egz.clientapp.service.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Talgat.Abdulin
 */
public class FilenameUtils {
    public static boolean isCyrillic(String str) {
        return StringUtils.containsAny(str,
                "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "К", "Л", "М", "Н", "О", "П",
                "Р", "С", "Т", "У", "Ф", "Х", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я",
                "а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "к", "л", "м", "н", "о", "п",
                "р", "с", "т", "у", "ф", "х", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я");
    }
}

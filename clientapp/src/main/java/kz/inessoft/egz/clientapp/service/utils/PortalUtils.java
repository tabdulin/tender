package kz.inessoft.egz.clientapp.service.utils;

import com.codeborne.selenide.SelenideElement;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * @author talgat.abdulin
 */
public class PortalUtils {
    private static final Logger logger = LoggerFactory.getLogger(PortalUtils.class);

    public static String createAnnouncementUrl(String id) {
        return "https://v3bl.goszakup.gov.kz/ru/announce/index/" + id;
    }

    public static String createAnnouncementDocumentsUrl(String id) {
        return createAnnouncementUrl(id) + "?tab=documents";
    }

    public static String createAnnouncementDiscussionsUrl(String id) {
        return createAnnouncementUrl(id) + "?tab=discussion";
    }

    public static String createAnnouncementProtocolUrl(String id) {
        return createAnnouncementUrl(id) + "/?tab=protocols";
    }

    public static String createAnnouncementLotsUrl(String id) {
        return createAnnouncementUrl(id) + "?tab=lots";
    }

    public static String createProposalUrl(String number) {
        return "https://v3bl.goszakup.gov.kz/ru/application/create/" + number;
    }

    public static String createTaxDebtUrl() {
        return "https://v3bl.goszakup.gov.kz/ru/cabinet/tax_debts";
    }

    public static String createMyPropposalsUrl() {
        return "https://v3bl.goszakup.gov.kz/ru/myapp";
    }

    public static void upload(String... files) {
        upload(Arrays.asList(files));
    }

    public static void upload(List<String> files) {
        for (String file : files) {
            logger.debug("Uploading file: {}", file);
            if ($(byLinkText(org.apache.commons.io.FilenameUtils.getName(file))).exists()) {
                logger.info("File was already uploaded: {}", file);
                continue;
            }

           $(byText("Добавить файл")).shouldBe(visible).click();
            $$(".form_upload").last()
                    .find(By.name("userfile"))
                    .sendKeys(org.apache.commons.io.FilenameUtils.normalize(file, !SystemUtils.IS_OS_WINDOWS));
            $$(".file_status").last().$(".check_file_status_ok").shouldBe(visible);
            SelenideElement saveButton = $("#save_form");
            if (!saveButton.exists()) {
                saveButton = $(byText("Сохранить"));
            }

            if (!saveButton.exists()) {
                saveButton = $(withText("Сохранить"));
            }

            saveButton.click();
            $(byText("Файлы успешно прикреплены")).shouldBe(visible);
            logger.debug("File was uploaded: {}", file);
        }
    }

    public static void downloadFile(SelenideElement link, File directory)  {
        downloadFile(link, directory, null);
    }

    public static void downloadFile(SelenideElement link, File directory, String prefix) {
        String url = link.attr("href");
        String name = link.text();
        if (!StringUtils.contains(url, "download_file")) {
            return;
        }

        logger.debug("Downloading file: {}, {}, {}", directory, name, url);
        if (FileUtils.getFile(directory.getAbsolutePath() + "/" + name).exists()) {
            logger.debug("File was already downloaded: {}, {}, {}", directory, name, url);
            return;
        }

        try {
            File file = link.download();
            StringBuilder filename = new StringBuilder();
            if (StringUtils.isNotBlank(prefix)) {
                filename.append(prefix + " - ");
            }

            boolean useFilenameFromPage = !link.has(cssClass("btn")) && FilenameUtils.isCyrillic(name);
            filename.append(useFilenameFromPage ? name : file.getName());
            FileUtils.moveFile(file, new File(directory.getAbsolutePath() + "/" + filename.toString()));
            logger.debug("File was downloaded: {}, {}, {}", directory, name, url);
        } catch (FileExistsException e) {
            logger.debug("File was already downloaded: {}, {}, {}", directory, name, url);
        } catch (Exception e) {
            logger.error("File was not downloaded: {}, {}, {}", directory, name, url, e);
        }
    }

    public static String createAnnouncementProposalsUrl(String id) {
        return "https://v3bl.goszakup.gov.kz/ru/consideration/index/" + id;
    }
}

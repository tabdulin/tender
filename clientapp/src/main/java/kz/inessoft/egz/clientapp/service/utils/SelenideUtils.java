package kz.inessoft.egz.clientapp.service.utils;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.google.common.base.Function;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;

import static com.codeborne.selenide.Condition.cssClass;

/**
 * @author Talgat.Abdulin
 */
public class SelenideUtils {
    private static final Logger logger = LoggerFactory.getLogger(SelenideUtils.class);

    public static void screenshot(String directory, String file) {
        String saved = Configuration.reportsFolder;
        Configuration.reportsFolder = directory;
        Selenide.screenshot(file);
        Configuration.reportsFolder = saved;
    }

    public static void screenshot(File directory, String file) {
        screenshot(directory.getAbsolutePath(), file);
    }

    public static void waitPageLoadComplete() {
        Selenide.Wait().until(new Function<WebDriver, Boolean>() {
            @Nullable
            public Boolean apply(@Nullable WebDriver webDriver) {
                return ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete");
            }
        });
    }

    public static Condition withSubElement(By selector) {
        return new WithSubElement(selector);
    }

    private static class  WithSubElement extends  Condition {
        private By selector;

        public WithSubElement(By selector) {
            super("withSubElement");
            this.selector = selector;
        }

        @Override
        public boolean apply(WebElement element) {
            return !element.findElements(selector).isEmpty();
        }
    }

}

-- Актуальные ИТ-конкурсы
select
  g.name_ru as "Заказчик",
  a.number as "Номер объялвения",
  status.name_ru as "Статус",
  a.name_ru as "Наименование объявления",
  date(a.publication_date) as "Дата публикации",
  date(a.start_rcv_time) as "Дата приема заявок",
  date(a.end_rcv_time) as "Дата окончания приема заявок",
  lca.lot_number as "Номер лота",
  l.short_characteristic_ru as "Краткая характеристика",
  l.additional_characteristic_ru as "Дополнительная характеристка",
  to_char(l.price_for_unit, '999 999 999.99') as "Сумма"
from egz.t_lot l
  inner join egz.t_lot_cross_announcement lca on l.id = lca.t_lot_id
  inner join egz.t_announcement a on a.id = lca.t_announcement_id
  left join egz.dic_tru tru on l.dic_tru_id = tru.id
  left join egz.dic_gov_organization g on l.dic_gov_organization_id = g.id
  left join egz.dic_announcement_status status on a.dic_announcement_status_id = status.id
where
  (tru.code like '62%' or tru.code like '63%')
  and status.name_ru in ('Опубликовано', 'Опубликовано (прием заявок)', 'Изменена документация')
  and date(now()) - date(a.publication_date) <= 25
order by
  a.end_rcv_time asc,
  l.price_for_unit desc;

-- ИТ конкурсы по зазакчикам
select
  g.name_ru as customer, count(*), to_char(sum(l.first_year_sum), '999 999 999 999.99') as budget
from t_lot_cross_announcement lca
  inner join t_announcement a on lca.t_announcement_id = a.id
  inner join t_lot l on lca.t_lot_id = l.id
  left join dic_gov_organization g on l.dic_gov_organization_id = g.id
  left join dic_tru tru on l.dic_tru_id = tru.id
where
  (tru.code like '62%' or tru.code like '63%')
group by
  g.id
order by
  budget desc;

-- ИТ конкурсы по услугам
select
  tru.code as code, tru.name_ru as service, count(*), to_char(sum(l.first_year_sum), '999 999 999 999.99') as budget
from t_lot_cross_announcement lca
  inner join t_announcement a on lca.t_announcement_id = a.id
  inner join t_lot l on lca.t_lot_id = l.id
  left join dic_gov_organization g on l.dic_gov_organization_id = g.id
  left join dic_tru tru on l.dic_tru_id = tru.id
where
  (tru.code like '62%' or tru.code like '63%')
group by
  tru.id
order by
  budget desc;


-- Типы ИТ услуг
select
  *
from
  dic_tru tru
where
  (tru.code like '62%' or tru.code like '63%');

-- количество конкурсов по датам
select
  dt, count(*)
from
  (select date(publication_date) from t_announcement) dt
group by
  dt
order by
  dt desc;
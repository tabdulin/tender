package kz.inessoft.egz.clientapp;

import kz.inessoft.egz.clientapp.service.utils.DirectoryUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 * @author Talgat.Abdulin
 */
public class ApplicationTest {

    @Test
    public void testMainDownloadVetlab() throws Exception {
        String announcement = "563689";
        FileUtils.deleteQuietly(DirectoryUtils.createAnnouncementDirectory(announcement));
        Application.main("download", announcement);
    }

    @Test
    public void testDownloadSono() throws Exception {
        String announcement = "789453";
        FileUtils.deleteQuietly(DirectoryUtils.createAnnouncementDirectory(announcement));
        Application.main("download", announcement);
    }

    @Test
    public void testDownloadNCE() throws Exception {
        String announcement = "808814";
        FileUtils.deleteQuietly(DirectoryUtils.createAnnouncementDirectory(announcement));
        Application.main("download", announcement);
    }


}
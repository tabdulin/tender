package kz.inessoft.egz.clientapp.service;

import org.junit.Test;

import java.util.Date;

/**
 * @author talgat.abdulin
 */
public class GoogleCalendarServiceTest {
    private GoogleCalendarService calendar = new GoogleCalendarService();

    @Test
    public void testAddEvent() throws Exception {
        calendar.addEvent(new Date(), "test1234", "test name", "test description", "4");
    }

    @Test
    public void testDeleteEvent() throws Exception {
        calendar.deleteEvent("test1234");
    }

    @Test
    public void testDeleteEvents() throws Exception {
        calendar.deleteEvents();
    }
}
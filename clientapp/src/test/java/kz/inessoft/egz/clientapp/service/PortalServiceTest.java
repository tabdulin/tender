package kz.inessoft.egz.clientapp.service;

import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class PortalServiceTest {

    @Test
    public void testStartSignWorkaround() throws Exception {
        PortalService portal = new PortalService();
        portal.startSignWorkaround();
    }

    @Test
    public void testUpload() throws Exception {
        PortalService portal = new PortalService();
        portal.stopSignWorkAround();
        portal.startSignWorkaround();
        portal.openFirefoxBrowser();
        portal.login();
        portal.uploadFiles("https://v3bl.goszakup.gov.kz/ru/application/show_doc/860701/4696367/41/3048997/1",
                "d:/tender/sc");
    }

    @Test
    public void testRequestTaxDebt() throws Exception {
        PortalService portal = new PortalService();
        portal.startSignWorkaround();
        portal.openFirefoxBrowser();
        portal.login();
        portal.requestTaxDebt();
    }
}
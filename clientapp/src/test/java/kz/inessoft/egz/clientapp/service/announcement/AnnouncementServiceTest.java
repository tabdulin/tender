package kz.inessoft.egz.clientapp.service.announcement;

import kz.inessoft.egz.clientapp.service.PortalService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author talgat.abdulin
 */
public class AnnouncementServiceTest {
    private AnnouncementService service = new AnnouncementService();
    private PortalService portal = new PortalService();

    @Before
    public void before() {
        portal.startSignWorkaround();
        portal.openFirefoxBrowser();
        portal.login();
    }

    @After
    public void after() {
        portal.closeBrowser();
    }

    @Test
    public void testGetAnnouncement796151() throws Exception {
        AnnouncementService service = new AnnouncementService();
        Announcement a = service.getAnnouncement("796151");
        assertEquals("796151", a.getNumber());
    }

    @Test
    public void testGetAnnouncement808814() throws Exception {
        Announcement a = service.getAnnouncement("808814");
        assertEquals(2, a.getVersions().size());
        assertEquals("808814", a.getVersions().get(1));
        assertEquals("847512", a.getVersions().get(2));
    }

    @Test
    public void testGetAnnouncement862857() throws Exception {
        Announcement a = service.getAnnouncement("894419");
        assertEquals(2, a.getVersions().size());
        assertEquals("862857", a.getVersions().get(1));
        assertEquals("894419", a.getVersions().get(2));
    }

    @Test
    public void testDownloadAnnouncementDocuments862857_2_894419() throws Exception {
        service.downloadAnnouncementDocuments("894419");
    }

    @Test
    public void testGetAnnouncementWithLots796387() throws Exception {
        Announcement a = service.getAnnouncement("796387");
        assertEquals(1, a.getLots().size());
        assertEquals("6063635-ОК1", a.getLots().get(0).getNumber());
        assertEquals("Государственное коммунальное предприятие \"Горводоканал\" отдела жилищно-коммунального хозяйства, пассажирского транспорта и автомобильных дорог акимата города Экибастуза\"",
                a.getLots().get(0).getCustomer());
        assertEquals("Услуги по проектированию и разработке программного обеспечения", a.getLots().get(0).getName());
        assertEquals("\"Разработка програмнного обеспечения \"\"Диспечер\"\"\"", a.getLots().get(0).getDetails());
        assertEquals(new Float(15000000), a.getLots().get(0).getPrice());
    }

    @Test
    public void testGetAnnouncement808814_2() throws Exception {
        AnnouncementService service = new AnnouncementService();
        Announcement a = service.getAnnouncement("808814");
        assertEquals(new Integer(2), a.getVersions().get(a.getVersions().lastKey()));
    }


    @Test
    public void testRegisterEvents() throws Exception {
        List<String> tenders = Arrays.asList(
                "623971", // assembly.kz контент
                "653011", // assembly.kz сопровождение
                "696061", // болашак модернизация МИС
                "696813", // болашак сопровождеие МИС
                "696897", // болашака разитие СЭД
                "716334", // минэнерго сайт
                "731671", //
                "741570", // тбд сопровод
                "744299", // мдгс мобилка
                "758902", // гп учет кадрами
                "755906", // жкх инвест проекты
                "769137", // кгд цоу
                "789453", // соно
                "790689", // til.gov.kz
                "796151", // niis
                "796069", // niis
                "796387", // экибастуз горводоканал диспетчер
                "757255", // gps астана су арнасы
                "808814", // нцэ
                "808676", // сиопсо
                "809621", // еисквэ
                "862857", // ск-фармация
                "875528", // оос
                "885145" // мкс экспо
        );
        for (String number : tenders) {
            Announcement a = service.getAnnouncement(number);
            service.registerEvents(a);
        }
    }

    @Test
    public void testRegisterEvents796151() throws Exception {
        Announcement a = service.getAnnouncement("796151");
        service.registerEvents(a);
    }

    @Test
    public void testDownloadAnnouncementProposalDocuments() throws Exception {
        AnnouncementProposal application = new AnnouncementProposal();
        application.setAnnouncementNumber("789453");
        application.setProposalNumber("4365964");
        application.setMemberName("ТОО Integrity Systems");
        application.setProposalUrl("https://v3bl.goszakup.gov.kz/ru/consideration/actionShowInfoSupplier/789453/4365964");
        service.downloadAnnouncementProposalDocuments(application);
    }

    @Test
    public void testDownloadAnnouncementProposalAdditionDocuments() throws Exception {
        AnnouncementProposal application = new AnnouncementProposal();
        application.setAnnouncementNumber("729008");
        application.setProposalNumber("3800760");
        application.setAddition(true);
        application.setMemberName("ТОО КАЗАХСТАНСКИЕ ИНФОРМАЦИОННЫЕ ТЕХНОЛОГИИ");
        application.setProposalUrl("https://v3bl.goszakup.gov.kz/ru/consideration/actionShowInfoSupplier/729008/4228430");
        service.downloadAnnouncementProposalAdditionDocuments(application);
    }

    @Test
    public void testDownloadAnnouncementDiscussionsWithManyQuestions() throws Exception {
        service.downloadAnnouncementDiscussions("621178");
    }
}
package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Selenide;
import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class Application2TSProcessorTest extends SingleLotProposalTest {
    @Test
    public void testProcessDocument() throws Exception {
        Selenide.open(application2Url);
        ApplicationProcessor processor = new Application2TSProcessor();
        processor.process(proposal);
    }
}
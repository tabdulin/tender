package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Selenide;
import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class Application6QualificationProcessorTest extends SingleLotProposalTest {

    @Test
    public void testProcessDocument() throws Exception {
        Selenide.open(application6url);
        ApplicationProcessor processor = new Application6QualificationProcessor();
        processor.process(proposal);
    }
}
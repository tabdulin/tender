package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Selenide;
import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class Application8BankGuaranteeProcessorTest extends SingleLotProposalTest{

    @Test
    public void testProcessDocument() throws Exception {
        Selenide.open(application8url);
        ApplicationProcessor processor = new Application8BankGuaranteeProcessor();
        processor.process(proposal);
    }
}
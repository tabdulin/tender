package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Selenide;
import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class Application9BankReferenceProcessorTest extends SingleLotProposalTest {

    @Test
    public void testProcessDocument() throws Exception {
        Selenide.open(application9url);
        ApplicationProcessor processor = new Application9BankReferenceProcessor();
        processor.process(proposal);
    }
}
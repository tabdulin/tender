package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Selenide;
import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class CertificatesProcessorTest extends SingleLotProposalTest {
    @Test
    public void testGetFilePaths() throws Exception {
        Selenide.open(certificatesApplicationUrl);
        CertificatesProcessor processor = new CertificatesProcessor();
        processor.process(proposal);
    }
}
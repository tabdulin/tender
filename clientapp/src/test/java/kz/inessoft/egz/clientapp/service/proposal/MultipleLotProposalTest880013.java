package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.Configuration;
import kz.inessoft.egz.clientapp.service.PortalService;
import org.junit.Test;

/**
 * @author Talgat.Abdulin
 */
public class MultipleLotProposalTest880013 {
    private PortalService portal = new PortalService();
    private Proposal proposal = new Proposal();
    private ProposalService proposalService = new ProposalService();
    private String proposalUrlPrefix = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/880013/4902079/";

    public MultipleLotProposalTest880013() throws ProposalService.ProposalException {
        ProposalLoader loader = new ProposalLoader(Configuration.getInstance().getDefaultProposal());
        proposal.setAnnouncement("880013");
        loader.loadRequisites(proposal);
        loader.loadApplication2documents(proposal);
        loader.loadServices(proposal);
        loader.loadHardware(proposal);
        loader.loadEmployees(proposal);
        loader.loadApplication8documents(proposal);

        portal.startSignWorkaround();
        portal.openChromeBrowser();
        portal.login();
    }

    @Test
    public void createProposal() {
        proposalService.createProposal(proposal);
    }

    @Test
    public void selectLots() {
        proposalService.selectLots(proposal);
    }

    @Test
    public void processApplication2() {
        portal.openPage(proposalUrlPrefix + "41");
        Application2TSProcessor application2TSProcessor = new Application2TSProcessor();
        application2TSProcessor.process(proposal);
    }

    @Test
    public void processApplication6() {
        portal.openPage(proposalUrlPrefix + "8");
        Application6QualificationProcessor processor = new Application6QualificationProcessor();
        processor.process(proposal);
    }

    @Test
    public void processApplication8() {
        portal.openPage(proposalUrlPrefix + "10");
        Application8BankGuaranteeProcessor processor = new Application8BankGuaranteeProcessor();
        processor.process(proposal);
    }
}

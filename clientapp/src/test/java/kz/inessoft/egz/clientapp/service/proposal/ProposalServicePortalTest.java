package kz.inessoft.egz.clientapp.service.proposal;

import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class ProposalServicePortalTest extends SingleLotProposalTest {
    private ProposalService service = new ProposalService();

    @Test
    public void testApplyEmptyProposal() throws Exception {
        service.createProposal(proposal);
        service.selectLots(proposal);
        service.fillApplications(proposal);
    }

    @Test
    public void testDeleteProposal() throws Exception {
        service.deleteProposal(proposal);
    }
}
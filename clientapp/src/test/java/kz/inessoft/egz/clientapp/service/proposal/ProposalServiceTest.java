package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.PortalService;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Talgat.Abdulin
 */
public class ProposalServiceTest {
    private ProposalService service = new ProposalService();

    @Test
    public void testPrepareProposal() throws Exception {
        System.setProperty("configFile", "src/main/resources/config.properties");

        Proposal proposal = service.loadProposalExcel("src/test/resources/proposal/announcement/announcement.xlsx");
        assertEquals("100100-2", proposal.getAnnouncement());
        assertEquals("25670-ОК1", proposal.getLot());
        assertEquals("Адрес компании, который нужно выбрать на портале из списка", proposal.getAddress());
        assertEquals("ИИК компании, который нужно выбрать на портале из списка", proposal.getIik());
        assertEquals(false, proposal.getConsortium());

        assertEquals(1, proposal.getApplication2documents().size());
        String expectedSpecification = "src/test/resources/proposal/announcement/application-2/technical-specification.pdf";
        String actualSpecification = FilenameUtils.normalize(proposal.getApplication2documents().get(0), true);
        assertNotEquals(expectedSpecification, actualSpecification);
        assertTrue(actualSpecification.endsWith(expectedSpecification));

        assertEquals(1, proposal.getApplication8documents().size());
        String expectedPayment = "src/test/resources/proposal/announcement/application-8/обеспечение.pdf";
        String actualPayment = FilenameUtils.normalize(proposal.getApplication8documents().get(0), true);
        assertNotEquals(expectedPayment, actualPayment);
        assertTrue(actualPayment.endsWith(expectedPayment));

        assertEquals(1, proposal.getApplication9documents().size());
        String expectedBankDebt = "src/test/resources/proposal/announcement/application-9/отсутствие задолжности.pdf";
        String actualBankDebt = FilenameUtils.normalize(proposal.getApplication9documents().get(0), true);
        assertNotEquals(expectedBankDebt, actualBankDebt);
        assertTrue(actualBankDebt.endsWith(expectedBankDebt));

        assertEquals(1, proposal.getApplication6services().size());
        Proposal.Service service = proposal.getApplication6services().get(0);
        assertEquals("Услуга по разработки", service.getName());
        assertEquals("2", service.getStartMonth());
        assertEquals("2014", service.getStartYear());
        assertEquals("8", service.getEndMonth());
        assertEquals("2014", service.getEndYear());
        assertEquals("900000", service.getPrice());
        assertEquals("КАЗАХСТАН", service.getCountry());
        assertEquals("КГД РК", service.getCustomerName());
        assertEquals("Астана", service.getCustomerAddress());
        assertEquals("116", service.getDocumentNumber());
        assertEquals("12-08-2014", service.getDocumentDate());
        assertNotEquals("договор-116-2014.zip", service.getDocumentFile());
        assertTrue(service.getDocumentFile().endsWith("договор-116-2014.zip"));

        assertEquals(2, proposal.getApplication6hardware().size());
        Proposal.Hardware computer = proposal.getApplication6hardware().get(0);
        assertEquals("Компьютер", computer.getName());
        assertEquals("2", computer.getAmount());
        assertEquals("хорошее", computer.getCondition());
        assertEquals("Собственное", computer.getRented());
        assertEquals("Счет-фактура", computer.getDocumentName());
        assertEquals("25", computer.getDocumentNumber());
        assertEquals("01-08-2014", computer.getDocumentDate());
        assertTrue(computer.getDocumentFile().endsWith("компьютеры-2.zip"));
        Proposal.Hardware notebook = proposal.getApplication6hardware().get(1);
        assertEquals("Ноутбук", notebook.getName());
        assertEquals("10", notebook.getAmount());
        assertEquals("новое", notebook.getCondition());
        assertEquals("Арендованное", notebook.getRented());
        assertEquals("Договор аренды", notebook.getDocumentName());
        assertEquals("ТОО Ноутбук", notebook.getOwner());
        assertEquals("16", notebook.getDocumentNumber());
        assertEquals("06-04-2015", notebook.getDocumentDate());
        assertTrue(notebook.getDocumentFile().endsWith("ноутбуки-10.zip"));

        assertEquals(1, proposal.getApplication6employees().size());
        Proposal.Employee employee = proposal.getApplication6employees().get(0);
        assertEquals("120987654321", employee.getIin());
        assertEquals("КАЗАХСТАН", employee.getCountry());
        assertEquals("Иванов Иван Иванович", employee.getName());
        assertEquals("4", employee.getExperience());
        assertEquals("Бакалавр информатики", employee.getQualification());
        assertEquals("Программист 2 категории", employee.getCategory());
        assertTrue(employee.getDocumentFile().endsWith("Иванов Иван Иванович.zip"));

        assertEquals(1, proposal.getCertificatesDocuments().size());
        assertTrue(proposal.getCertificatesDocuments().get(0).endsWith("сертификат1.pdf"));
    }

    @Test
    public void testApply_iserv_885145() throws Exception {
        PortalService portal = new PortalService();
        portal.startSignWorkaround();
        portal.openFirefoxBrowser();
        portal.login();
        Proposal proposal = service.loadProposalExcel("d:/tender/.iserv/.proposal/announcement.xlsx");
        service.createProposal(proposal);
        service.selectLots(proposal);
        service.fillApplications(proposal);
    }
}
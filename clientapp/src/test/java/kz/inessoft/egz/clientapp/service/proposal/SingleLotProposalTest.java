package kz.inessoft.egz.clientapp.service.proposal;

import kz.inessoft.egz.clientapp.service.PortalService;
import org.apache.commons.io.FilenameUtils;
import org.junit.After;
import org.junit.Before;

import java.util.Arrays;

/**
 * @author talgat.abdulin
 */
public class SingleLotProposalTest {
    protected Proposal proposal;
    protected PortalService portal;

    protected String announcement = "847512";
    protected String lot = "2046275-ОК1";
    protected String emptyDocument = "proposal/announcement/application-2/technical-specification.pdf";
    protected String application2Url = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/41";
    protected String application4url = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/5";
    protected String application6url = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/8";
    protected String application8url = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/10";
    protected String application9url = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/11";
    protected String vatApplicationUrl = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/18";
    protected String taxDebtApplicationUrl = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/22";
    protected String certificatesApplicationUrl = "https://v3bl.goszakup.gov.kz/ru/application/show_doc/847512/4444874/21";

    @Before
    public void before() {
        portal = new PortalService();
        portal.startSignWorkaround();
        portal.openFirefoxBrowser();
        portal.login();

        proposal = new Proposal();
        proposal.setAnnouncement(announcement);
        proposal.setLot(lot);
        proposal.setAddress("710000000, 010000, Казахстан, г. Астана, ул. ПОТАНИНА, д. 12/1");
        proposal.setIik("Банк: АО \"БТА Банк\" ИИК: KZ84319Y010006345777 БИК: ABKZKZKX КБе: 17");
        proposal.setConsortium(false);

        proposal.setApplication2documents(Arrays.asList(emptyDocument));

        Proposal.Service service = new Proposal.Service();
        service.setName("Услуги по разработке");
        service.setStartYear("2015");
        service.setStartMonth("3");
        service.setEndYear("2015");
        service.setEndYear("12");
        service.setPrice("500000");
        service.setCountry("КАЗАХСТАН");
        service.setCustomerAddress("Казахстан");
        service.setCustomerUIN("123456789012");
        service.setCustomerName("ТОО Компания");
        service.setDocumentName(FilenameUtils.getName(emptyDocument));
        service.setDocumentDate("30-03-2015");
        service.setDocumentNumber("124-5");
        service.setDocumentFile(emptyDocument);
        proposal.setApplication6services(Arrays.asList(service));

        Proposal.Hardware hardware = new Proposal.Hardware();
        hardware.setName("сервер");
        hardware.setAmount("2");
        hardware.setCondition("новое");
        hardware.setRented("false");
        hardware.setDocumentName(FilenameUtils.getName(emptyDocument));
        hardware.setDocumentNumber("1234");
        hardware.setDocumentDate("01-01-2016");
        hardware.setDocumentFile(emptyDocument);
        proposal.setApplication6hardware(Arrays.asList(hardware, hardware));

        Proposal.Employee employee = new Proposal.Employee();
        employee.setIin("123456789012");
        employee.setName("Иванов Иван Иванович");
        employee.setExperience("5");
        employee.setCountry("КАЗАХСТАН");
        employee.setQualification("Системный программист");
        employee.setCategory("3");
        proposal.setApplication6employees(Arrays.asList(employee));

        proposal.setApplication8documents(Arrays.asList(emptyDocument, emptyDocument));

        proposal.setApplication9documents(Arrays.asList(emptyDocument));
        proposal.setVatPayerDocuments(Arrays.asList(emptyDocument));
//        proposal.setTaxDebtDocuments(Arrays.asList(emptyDocument));
        proposal.setCertificatesDocuments(Arrays.asList(emptyDocument));
    }

    @After
    public void after() {
        portal.closeBrowser();
    }
}

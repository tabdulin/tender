package kz.inessoft.egz.clientapp.service.proposal;

import org.junit.Test;

import static com.codeborne.selenide.Selenide.open;

/**
 * @author talgat.abdulin
 */
public class TaxDebtProcessorTest extends SingleLotProposalTest {

    @Test
    public void testProcessDocument() throws Exception {
        open(taxDebtApplicationUrl);
        TaxDebtProcessor processor = new TaxDebtProcessor();
        processor.process(proposal);
    }

    @Test
    public void testGenerateTaxDebt_iserv_885145() {
        open("https://v3bl.goszakup.gov.kz/ru/application/show_doc/885145/4807692/22");
        TaxDebtProcessor processor = new TaxDebtProcessor();
        processor.process(proposal);
    }
}
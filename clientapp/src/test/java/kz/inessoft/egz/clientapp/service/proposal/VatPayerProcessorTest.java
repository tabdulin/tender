package kz.inessoft.egz.clientapp.service.proposal;

import com.codeborne.selenide.Selenide;
import org.junit.Test;

/**
 * @author talgat.abdulin
 */
public class VatPayerProcessorTest extends SingleLotProposalTest {
    @Test
    public void testProcessDocument() throws Exception {
        Selenide.open(vatApplicationUrl);
        ApplicationProcessor processor = new VatPayerProcessor();
        processor.process(proposal);
    }
}
package kz.inessoft.egz.clientapp.service.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat.Abdulin
 */
public class CompanyUtilsTest {

    @Test
    public void testShorten() throws Exception {
        Assert.assertEquals("ТОО Азия - Софт Сервис ЛТД",
                CompanyUtils.shorten("ТОО \"Азия - Софт Сервис ЛТД\""));
        Assert.assertEquals("ТОО InesSoft", CompanyUtils.shorten("ТОО \"InesSoft\""));
        Assert.assertEquals("ТОО Info - Service Inc",
                CompanyUtils.shorten("Товарищество с ограниченной ответственностью \"Info - Service Inc\""));
    }
}
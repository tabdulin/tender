package kz.inessoft.egz.clientapp.service.utils;

import kz.inessoft.egz.clientapp.Configuration;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import java.io.File;

/**
 * @author Talgat.Abdulin
 */
@Ignore
public class DirectoryUtilsTest {
    private String root = Configuration.getInstance().getTendersInfoDirectory();

    @Before
    @After
    public void clean() {
        File dir = DirectoryUtils.createAnnouncementDirectory("123456");
        if(dir.exists()) {
            FileUtils.deleteQuietly(dir);
        }
    }
}
package kz.inessoft.egz.clientapp.service.utils;

import kz.inessoft.egz.clientapp.service.utils.FilenameUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat.Abdulin
 */
public class FilenameUtilsTest {

    @Test
    public void testIsCyrillic() throws Exception {
        Assert.assertTrue(FilenameUtils.isCyrillic("документы конкурса 384596-2.rar"));
        Assert.assertFalse(FilenameUtils.isCyrillic("prot_pod_545398.pdf"));
    }
}
package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(ADBEntity.class)
public class ADBEntity_ {
    public static volatile SingularAttribute<ADBEntity, Long> id;
}

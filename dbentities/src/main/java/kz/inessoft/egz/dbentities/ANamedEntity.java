package kz.inessoft.egz.dbentities;

import org.hibernate.search.annotations.Field;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Created by alexey on 29.07.16.
 */
@MappedSuperclass
public abstract class ANamedEntity extends ADBEntity {
    @Column(name = "name_ru", nullable = false)
    @Field
    private String nameRu;

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String name) {
        this.nameRu = name;
    }
}

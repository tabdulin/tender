package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(ANamedEntity.class)
public class ANamedEntity_ extends ADBEntity_ {
    public static volatile SingularAttribute<ANamedEntity, String> nameRu;
}

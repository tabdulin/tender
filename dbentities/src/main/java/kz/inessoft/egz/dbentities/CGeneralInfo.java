package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by alexey on 07.10.16.
 */
@Entity
@Table(name = "c_general_info")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$c_general_info", allocationSize = 10)
public class CGeneralInfo extends ADBEntity {

    @Column(name = "portal_id", nullable = false)
    private Long portalId;

    @Column(name = "load_time")
    private Date loadTime;

    @Column(name = "general_tab", nullable = false)
    private String generalTabHtml;

    @Column(name = "objects_tab", nullable = false)
    private String objectsTabHtml;

    @Column(name = "customer_tab", nullable = false)
    private String customerTabHtml;

    @Column(name = "treasury_tab")
    private String treasuryTabHtml;

    @Column(name = "agreement_tab", nullable = false)
    private String agreementTabHtml;

    @ManyToOne
    @JoinColumn(name = "dic_contract_type_id", nullable = false)
    private DicContractType contractType;

    @Column(name = "reestr_number", nullable = false)
    private String reestrNumber;

    @Column(name = "number")
    private String number;

    @Column(name = "main_contract_reestr_number")
    private String mainContractReestrNumber;

    @Column(name = "main_contract_portal_id")
    private Long mainContractPortalId;

    @Embedded
    private CGeneralInfoAnnouncementInfo announcementInfo;

    @Column(name = "conclusion_date")
    private Date conclusionDate;

    @Column(name = "creation_time", nullable = false)
    private Date creationTime;

    @Column(name = "last_changed_time", nullable = false)
    private Date lastChangedTime;

    @Embedded
    private CGeneralInfoShordDescription shordDescription;

    @ManyToOne
    @JoinColumn(name = "dic_purchase_object_type_id", nullable = false)
    private DicPurchaseObjectType purchaseObjectType;

    @ManyToOne
    @JoinColumn(name = "dic_contract_conclusion_type_id", nullable = false)
    private DicContractConclusionType conclusionType;

    @ManyToOne
    @JoinColumn(name = "dic_contract_status_id", nullable = false)
    private DicContractStatus contractStatus;

    @ManyToOne
    @JoinColumn(name = "dic_contract_purchase_type_id", nullable = false)
    private DicContractPurchaseType contractPurchaseType;

    @Column(name = "financial_year", nullable = false)
    private int financialYear;

    @ManyToOne
    @JoinColumn(name = "dic_contract_budjet_type_id", nullable = false)
    private DicContractBudjetType budjetType;

    @Column(name = "financial_source")
    private String financialSource;

    @ManyToOne
    @JoinColumn(name = "planed_dic_purchase_mode_id", nullable = false)
    private DicPurchaseMode planedPurchaseMode;

    @ManyToOne
    @JoinColumn(name = "real_dic_purchase_mode_id", nullable = false)
    private DicPurchaseMode realPurchaseMode;

    @Column(name = "planed_sum", nullable = false)
    private Double planedSum;

    @Column(name = "result_contract_sum", nullable = false)
    private Double resultContractSum;

    @Column(name = "total_contract_sum", nullable = false)
    private Double totalContractSum;

    @Column(name = "real_contract_sum")
    private Double realContractSum;

    @ManyToOne
    @JoinColumn(name = "dic_currency_id", nullable = false)
    private DicCurrency currency;

    @Column(name = "rate", nullable = false)
    private Double rate;

    @Column(name = "contract_time", nullable = false)
    private Date contractTime;

    @Column(name = "planned_execution_date")
    private Date plannedExecutionDate;

    @Column(name = "real_execution_date")
    private Date realExecutionDate;

    @Column(name = "execution_mark_date", nullable = false)
    private Date executionMarkDate;

    @Embedded
    private CGeneralInfoReasonDocumentInfo reasonDocumentInfo;

    @Embedded
    private CGeneralInfoCustomerInfo customerInfo;

    @Embedded
    private CGeneralInfoImplementerInfo implementerInfo;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Date getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(Date loadTime) {
        this.loadTime = loadTime;
    }

    public String getGeneralTabHtml() {
        return generalTabHtml;
    }

    public void setGeneralTabHtml(String generalTabHtml) {
        this.generalTabHtml = generalTabHtml;
    }

    public String getObjectsTabHtml() {
        return objectsTabHtml;
    }

    public void setObjectsTabHtml(String objectsTabHtml) {
        this.objectsTabHtml = objectsTabHtml;
    }

    public String getCustomerTabHtml() {
        return customerTabHtml;
    }

    public void setCustomerTabHtml(String customerTabHtml) {
        this.customerTabHtml = customerTabHtml;
    }

    public String getTreasuryTabHtml() {
        return treasuryTabHtml;
    }

    public void setTreasuryTabHtml(String treasuryTabHtml) {
        this.treasuryTabHtml = treasuryTabHtml;
    }

    public String getAgreementTabHtml() {
        return agreementTabHtml;
    }

    public void setAgreementTabHtml(String agreementTabHtml) {
        this.agreementTabHtml = agreementTabHtml;
    }

    public DicContractType getContractType() {
        return contractType;
    }

    public void setContractType(DicContractType contractType) {
        this.contractType = contractType;
    }

    public String getReestrNumber() {
        return reestrNumber;
    }

    public void setReestrNumber(String reestrNumber) {
        this.reestrNumber = reestrNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMainContractReestrNumber() {
        return mainContractReestrNumber;
    }

    public void setMainContractReestrNumber(String mainContractReestrNumber) {
        this.mainContractReestrNumber = mainContractReestrNumber;
    }

    public Long getMainContractPortalId() {
        return mainContractPortalId;
    }

    public void setMainContractPortalId(Long mainContractPortalId) {
        this.mainContractPortalId = mainContractPortalId;
    }

    public CGeneralInfoAnnouncementInfo getAnnouncementInfo() {
        return announcementInfo;
    }

    public void setAnnouncementInfo(CGeneralInfoAnnouncementInfo announcementInfo) {
        this.announcementInfo = announcementInfo;
    }

    public Date getConclusionDate() {
        return conclusionDate;
    }

    public void setConclusionDate(Date conclusionDate) {
        this.conclusionDate = conclusionDate;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getLastChangedTime() {
        return lastChangedTime;
    }

    public void setLastChangedTime(Date lastChangedTime) {
        this.lastChangedTime = lastChangedTime;
    }

    public CGeneralInfoShordDescription getShordDescription() {
        return shordDescription;
    }

    public void setShordDescription(CGeneralInfoShordDescription shordDescription) {
        this.shordDescription = shordDescription;
    }

    public DicPurchaseObjectType getPurchaseObjectType() {
        return purchaseObjectType;
    }

    public void setPurchaseObjectType(DicPurchaseObjectType purchaseObjectType) {
        this.purchaseObjectType = purchaseObjectType;
    }

    public DicContractConclusionType getConclusionType() {
        return conclusionType;
    }

    public void setConclusionType(DicContractConclusionType conclusionType) {
        this.conclusionType = conclusionType;
    }

    public DicContractStatus getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(DicContractStatus contractStatus) {
        this.contractStatus = contractStatus;
    }

    public DicContractPurchaseType getContractPurchaseType() {
        return contractPurchaseType;
    }

    public void setContractPurchaseType(DicContractPurchaseType contractPurchaseType) {
        this.contractPurchaseType = contractPurchaseType;
    }

    public int getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(int financialYear) {
        this.financialYear = financialYear;
    }

    public DicContractBudjetType getBudjetType() {
        return budjetType;
    }

    public void setBudjetType(DicContractBudjetType budjetType) {
        this.budjetType = budjetType;
    }

    public String getFinancialSource() {
        return financialSource;
    }

    public void setFinancialSource(String financialSource) {
        this.financialSource = financialSource;
    }

    public DicPurchaseMode getPlanedPurchaseMode() {
        return planedPurchaseMode;
    }

    public void setPlanedPurchaseMode(DicPurchaseMode planedPurchaseMode) {
        this.planedPurchaseMode = planedPurchaseMode;
    }

    public DicPurchaseMode getRealPurchaseMode() {
        return realPurchaseMode;
    }

    public void setRealPurchaseMode(DicPurchaseMode realPurchaseMode) {
        this.realPurchaseMode = realPurchaseMode;
    }

    public Double getPlanedSum() {
        return planedSum;
    }

    public void setPlanedSum(Double planedSum) {
        this.planedSum = planedSum;
    }

    public Double getResultContractSum() {
        return resultContractSum;
    }

    public void setResultContractSum(Double resultContractSum) {
        this.resultContractSum = resultContractSum;
    }

    public Double getTotalContractSum() {
        return totalContractSum;
    }

    public void setTotalContractSum(Double totalContractSum) {
        this.totalContractSum = totalContractSum;
    }

    public Double getRealContractSum() {
        return realContractSum;
    }

    public void setRealContractSum(Double realContractSum) {
        this.realContractSum = realContractSum;
    }

    public DicCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(DicCurrency currency) {
        this.currency = currency;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Date getContractTime() {
        return contractTime;
    }

    public void setContractTime(Date contractTime) {
        this.contractTime = contractTime;
    }

    public Date getPlannedExecutionDate() {
        return plannedExecutionDate;
    }

    public void setPlannedExecutionDate(Date plannedExecutionDate) {
        this.plannedExecutionDate = plannedExecutionDate;
    }

    public Date getRealExecutionDate() {
        return realExecutionDate;
    }

    public void setRealExecutionDate(Date realExecutionDate) {
        this.realExecutionDate = realExecutionDate;
    }

    public Date getExecutionMarkDate() {
        return executionMarkDate;
    }

    public void setExecutionMarkDate(Date executionMarkDate) {
        this.executionMarkDate = executionMarkDate;
    }

    public CGeneralInfoReasonDocumentInfo getReasonDocumentInfo() {
        return reasonDocumentInfo;
    }

    public void setReasonDocumentInfo(CGeneralInfoReasonDocumentInfo reasonDocumentInfo) {
        this.reasonDocumentInfo = reasonDocumentInfo;
    }

    public CGeneralInfoCustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CGeneralInfoCustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }

    public CGeneralInfoImplementerInfo getImplementerInfo() {
        return implementerInfo;
    }

    public void setImplementerInfo(CGeneralInfoImplementerInfo implementerInfo) {
        this.implementerInfo = implementerInfo;
    }
}

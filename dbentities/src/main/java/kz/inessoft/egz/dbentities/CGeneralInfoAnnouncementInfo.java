package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CGeneralInfoAnnouncementInfo implements Serializable {
    @Column(name = "announcement_doc_name")
    private String docName;

    @Column(name = "announcement_number")
    private String docNumber;

    @Column(name = "announcement_date")
    private Date docDate;

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }
}

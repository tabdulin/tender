package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CGeneralInfoAnnouncementInfo.class)
public class CGeneralInfoAnnouncementInfo_ {
    public static volatile SingularAttribute<CGeneralInfoAnnouncementInfo, String> docName;
    public static volatile SingularAttribute<CGeneralInfoAnnouncementInfo, String> docNumber;
    public static volatile SingularAttribute<CGeneralInfoAnnouncementInfo, Date> docDate;

}

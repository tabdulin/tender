package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CGeneralInfoCustomerInfo implements Serializable {
    @ManyToOne
    @JoinColumn(name = "dic_gov_organization_id", nullable = false)
    private DicGovOrganization customer;

    @Column(name = "customer_name_kk", nullable = false)
    private String nameKk;

    @Column(name = "customer_name_ru", nullable = false)
    private String nameRu;

    @Column(name = "customer_bin", nullable = false)
    private String bin;

    @Column(name = "customer_rnn")
    private String rnn;

    public DicGovOrganization getCustomer() {
        return customer;
    }

    public void setCustomer(DicGovOrganization customer) {
        this.customer = customer;
    }

    public String getNameKk() {
        return nameKk;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getRnn() {
        return rnn;
    }

    public void setRnn(String rnn) {
        this.rnn = rnn;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CGeneralInfoCustomerInfo.class)
public class CGeneralInfoCustomerInfo_ {
    public static volatile SingularAttribute<CGeneralInfoCustomerInfo, DicGovOrganization> customer;
    public static volatile SingularAttribute<CGeneralInfoCustomerInfo, String> nameKk;
    public static volatile SingularAttribute<CGeneralInfoCustomerInfo, String> nameRu;
    public static volatile SingularAttribute<CGeneralInfoCustomerInfo, String> bin;
    public static volatile SingularAttribute<CGeneralInfoCustomerInfo, String> rnn;
}

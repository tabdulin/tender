package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CGeneralInfoImplementerInfo implements Serializable {
    @ManyToOne
    @JoinColumn(name = "dic_implementer_id", nullable = false)
    private DicImplementer implementer;

    @Column(name = "implementer_name_kk", nullable = false)
    private String nameKk;

    @Column(name = "implementer_name_ru", nullable = false)
    private String nameRu;

    @Column(name = "implementer_bin")
    private String bin;

    @Column(name = "implementer_iin")
    private String iin;

    @Column(name = "implementer_rnn")
    private String rnn;

    @Column(name = "implementer_vat_info", nullable = false)
    private String vatInfo;

    public DicImplementer getImplementer() {
        return implementer;
    }

    public void setImplementer(DicImplementer implementer) {
        this.implementer = implementer;
    }

    public String getNameKk() {
        return nameKk;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }

    public String getRnn() {
        return rnn;
    }

    public void setRnn(String rnn) {
        this.rnn = rnn;
    }

    public String getVatInfo() {
        return vatInfo;
    }

    public void setVatInfo(String vatInfo) {
        this.vatInfo = vatInfo;
    }
}

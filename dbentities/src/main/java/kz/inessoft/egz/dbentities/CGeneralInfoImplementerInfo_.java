package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CGeneralInfoImplementerInfo.class)
public class CGeneralInfoImplementerInfo_ {
    public static volatile SingularAttribute<CGeneralInfoImplementerInfo, DicImplementer> implementer;
    public static volatile SingularAttribute<CGeneralInfoImplementerInfo, String> nameKk;
    public static volatile SingularAttribute<CGeneralInfoImplementerInfo, String> nameRu;
    public static volatile SingularAttribute<CGeneralInfoImplementerInfo, String> bin;
    public static volatile SingularAttribute<CGeneralInfoImplementerInfo, String> iin;
    public static volatile SingularAttribute<CGeneralInfoImplementerInfo, String> rnn;
    public static volatile SingularAttribute<CGeneralInfoImplementerInfo, String> vatInfo;
}

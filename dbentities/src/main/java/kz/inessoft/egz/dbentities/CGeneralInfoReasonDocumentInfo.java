package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CGeneralInfoReasonDocumentInfo implements Serializable {
    @Column(name = "doc_name_ru", nullable = false)
    private String docNameRu;

    @Column(name = "doc_name_kk", nullable = false)
    private String docNameKk;

    @Column(name = "doc_number")
    private String docNumber;

    @Column(name = "doc_date")
    private Date docDate;

    public String getDocNameRu() {
        return docNameRu;
    }

    public void setDocNameRu(String docNameRu) {
        this.docNameRu = docNameRu;
    }

    public String getDocNameKk() {
        return docNameKk;
    }

    public void setDocNameKk(String docNameKk) {
        this.docNameKk = docNameKk;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }
}

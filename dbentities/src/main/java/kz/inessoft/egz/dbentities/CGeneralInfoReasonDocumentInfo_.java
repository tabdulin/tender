package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CGeneralInfoReasonDocumentInfo.class)
public class CGeneralInfoReasonDocumentInfo_ {
    public static volatile SingularAttribute<CGeneralInfoReasonDocumentInfo, String> docNameRu;
    public static volatile SingularAttribute<CGeneralInfoReasonDocumentInfo, String> docNameKk;
    public static volatile SingularAttribute<CGeneralInfoReasonDocumentInfo, String> docNumber;
    public static volatile SingularAttribute<CGeneralInfoReasonDocumentInfo, Date> docDate;
}

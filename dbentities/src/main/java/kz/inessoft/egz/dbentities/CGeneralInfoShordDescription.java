package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CGeneralInfoShordDescription implements Serializable {
    @Column(name = "short_description_kk")
    private String shortDescriptionKk;

    @Column(name = "short_description_ru")
    private String shortDescriptionRu;

    public String getShortDescriptionKk() {
        return shortDescriptionKk;
    }

    public void setShortDescriptionKk(String shortDescriptionKk) {
        this.shortDescriptionKk = shortDescriptionKk;
    }

    public String getShortDescriptionRu() {
        return shortDescriptionRu;
    }

    public void setShortDescriptionRu(String shortDescriptionRu) {
        this.shortDescriptionRu = shortDescriptionRu;
    }
}

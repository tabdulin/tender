package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CGeneralInfoShordDescription.class)
public class CGeneralInfoShordDescription_ {
    public static volatile SingularAttribute<CGeneralInfoShordDescription, String> shortDescriptionKk;
    public static volatile SingularAttribute<CGeneralInfoShordDescription, String> shortDescriptionRu;
}

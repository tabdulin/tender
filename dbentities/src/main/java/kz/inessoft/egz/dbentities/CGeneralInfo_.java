package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CGeneralInfo.class)
public class CGeneralInfo_ extends ADBEntity_ {
    public static volatile SingularAttribute<CGeneralInfo, Long> portalId;
    public static volatile SingularAttribute<CGeneralInfo, Date> loadTime;
    public static volatile SingularAttribute<CGeneralInfo, String> generalTabHtml;
    public static volatile SingularAttribute<CGeneralInfo, String> objectsTabHtml;
    public static volatile SingularAttribute<CGeneralInfo, String> customerTabHtml;
    public static volatile SingularAttribute<CGeneralInfo, String> treasuryTabHtml;
    public static volatile SingularAttribute<CGeneralInfo, String> agreementTabHtml;
    public static volatile SingularAttribute<CGeneralInfo, DicContractType> contractType;
    public static volatile SingularAttribute<CGeneralInfo, String> reestrNumber;
    public static volatile SingularAttribute<CGeneralInfo, String> number;
    public static volatile SingularAttribute<CGeneralInfo, String> mainContractReestrNumber;
    public static volatile SingularAttribute<CGeneralInfo, Long> mainContractPortalId;
    public static volatile SingularAttribute<CGeneralInfo, CGeneralInfoAnnouncementInfo> announcementInfo;
    public static volatile SingularAttribute<CGeneralInfo, Date> conclusionDate;
    public static volatile SingularAttribute<CGeneralInfo, Date> creationTime;
    public static volatile SingularAttribute<CGeneralInfo, Date> lastChangedTime;
    public static volatile SingularAttribute<CGeneralInfo, CGeneralInfoShordDescription> shordDescription;
    public static volatile SingularAttribute<CGeneralInfo, DicPurchaseObjectType> purchaseObjectType;
    public static volatile SingularAttribute<CGeneralInfo, DicContractConclusionType> conclusionType;
    public static volatile SingularAttribute<CGeneralInfo, DicContractStatus> contractStatus;
    public static volatile SingularAttribute<CGeneralInfo, DicContractPurchaseType> contractPurchaseType;
    public static volatile SingularAttribute<CGeneralInfo, Integer> financialYear;
    public static volatile SingularAttribute<CGeneralInfo, DicContractBudjetType> budjetType;
    public static volatile SingularAttribute<CGeneralInfo, String> financialSource;
    public static volatile SingularAttribute<CGeneralInfo, DicPurchaseMode> planedPurchaseMode;
    public static volatile SingularAttribute<CGeneralInfo, DicPurchaseMode> realPurchaseMode;
    public static volatile SingularAttribute<CGeneralInfo, Double> planedSum;
    public static volatile SingularAttribute<CGeneralInfo, Double> resultContractSum;
    public static volatile SingularAttribute<CGeneralInfo, Double> totalContractSum;
    public static volatile SingularAttribute<CGeneralInfo, Double> realContractSum;
    public static volatile SingularAttribute<CGeneralInfo, DicCurrency> currency;
    public static volatile SingularAttribute<CGeneralInfo, Double> rate;
    public static volatile SingularAttribute<CGeneralInfo, Date> contractTime;
    public static volatile SingularAttribute<CGeneralInfo, Date> plannedExecutionDate;
    public static volatile SingularAttribute<CGeneralInfo, Date> realExecutionDate;
    public static volatile SingularAttribute<CGeneralInfo, Date> executionMarkDate;
    public static volatile SingularAttribute<CGeneralInfo, CGeneralInfoReasonDocumentInfo> reasonDocumentInfo;
    public static volatile SingularAttribute<CGeneralInfo, CGeneralInfoCustomerInfo> customerInfo;
    public static volatile SingularAttribute<CGeneralInfo, CGeneralInfoImplementerInfo> implementerInfo;
}

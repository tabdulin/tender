package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alexey on 17.10.16.
 */
@Entity
@Table(name = "c_load_queue")
public class CLoadQueue implements Serializable {
    @Column(name = "portal_id")
    @Id
    private Long portalId;

    @Column(name = "queue_time", nullable = false)
    private Date queueTime;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Date getQueueTime() {
        return queueTime;
    }

    public void setQueueTime(Date queueTime) {
        this.queueTime = queueTime;
    }
}

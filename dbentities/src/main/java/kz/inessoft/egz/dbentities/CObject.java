package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by alexey on 07.10.16.
 */
@Entity
@Table(name = "c_object")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$c_object", allocationSize = 10)
public class CObject extends ADBEntity {

    public static enum EBuyMonth {
        January,
        February,
        Mart,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December,

        LastYear;
    }

    @Column(name = "portal_id", nullable = false)
    private Long portalId;

    @ManyToOne
    @JoinColumn(name = "c_general_info_id", nullable = false)
    private CGeneralInfo generalInfo;

    @Column(name = "original_html", nullable = false)
    private String originalHtml;

    @Column(name = "lot_portal_id")
    private Long lotPortalId;

    @Column(name = "plan_id")
    private Long planId;

    @Embedded
    private CObjectName name;

    @Column(name = "year", nullable = false)
    private int year;

    @ManyToOne
    @JoinColumn(name = "dic_tru_id", nullable = false)
    private DicTru tru;

    @ManyToOne
    @JoinColumn(name = "dic_purchase_object_type_id", nullable = false)
    private DicPurchaseObjectType purchaseObjectType;

    @Embedded
    private CObjectShortDescription shortDescription;

    @Embedded
    private CObjectAdditionalCharacteristic additionalCharacteristic;

    @Column(name = "price_without_vat", nullable = false)
    private Double priceWithoutVAT;

    @Column(name = "price_with_vat", nullable = false)
    private Double priceWithVAT;

    @Column(name = "value", nullable = false)
    private Double value;

    @ManyToOne
    @JoinColumn(name = "dic_unit_id", nullable = false)
    private DicUnit dicUnit;

    @Column(name = "contract_sum_without_vat", nullable = false)
    private Double contractSumWithoutVAT;

    @Column(name = "contract_vat_sum", nullable = false)
    private Double contractVATSum;

    @Column(name = "contract_total_sum", nullable = false)
    private Double contractTotalSum;

    @Column(name = "real_contract_sum")
    private Double realContractSum;

    @Column(name = "placement", nullable = false)
    private String placement;

    @Column(name = "payment_source")
    private String paymentSource;

    @Column(name = "planned_buy_month", nullable = false)
    @Enumerated(EnumType.STRING)
    private EBuyMonth plannedBuyMonth;

    @Column(name = "planed_execution_date")
    private Date planedExecutionDate;

    @Column(name = "real_execution_date")
    private Date realExecutionDate;

    @Column(name = "mark_execution_time")
    private Date markExecutionDate;

    @ManyToOne
    @JoinColumn(name = "dic_contract_object_purchase_type_id", nullable = false)
    private DicContractObjectPurchaseType contractObjectPurchaseType;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public CGeneralInfo getGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(CGeneralInfo generalInfo) {
        this.generalInfo = generalInfo;
    }

    public String getOriginalHtml() {
        return originalHtml;
    }

    public void setOriginalHtml(String originalHtml) {
        this.originalHtml = originalHtml;
    }

    public Long getLotPortalId() {
        return lotPortalId;
    }

    public void setLotPortalId(Long lotPortalId) {
        this.lotPortalId = lotPortalId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public CObjectName getName() {
        return name;
    }

    public void setName(CObjectName name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public DicTru getTru() {
        return tru;
    }

    public void setTru(DicTru tru) {
        this.tru = tru;
    }

    public DicPurchaseObjectType getPurchaseObjectType() {
        return purchaseObjectType;
    }

    public void setPurchaseObjectType(DicPurchaseObjectType purchaseObjectType) {
        this.purchaseObjectType = purchaseObjectType;
    }

    public CObjectShortDescription getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(CObjectShortDescription shortDescription) {
        this.shortDescription = shortDescription;
    }

    public CObjectAdditionalCharacteristic getAdditionalCharacteristic() {
        return additionalCharacteristic;
    }

    public void setAdditionalCharacteristic(CObjectAdditionalCharacteristic additionalCharacteristic) {
        this.additionalCharacteristic = additionalCharacteristic;
    }

    public Double getPriceWithoutVAT() {
        return priceWithoutVAT;
    }

    public void setPriceWithoutVAT(Double priceWithoutVAT) {
        this.priceWithoutVAT = priceWithoutVAT;
    }

    public Double getPriceWithVAT() {
        return priceWithVAT;
    }

    public void setPriceWithVAT(Double priceWithVAT) {
        this.priceWithVAT = priceWithVAT;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public DicUnit getDicUnit() {
        return dicUnit;
    }

    public void setDicUnit(DicUnit dicUnit) {
        this.dicUnit = dicUnit;
    }

    public Double getContractSumWithoutVAT() {
        return contractSumWithoutVAT;
    }

    public void setContractSumWithoutVAT(Double contractSumWithoutVAT) {
        this.contractSumWithoutVAT = contractSumWithoutVAT;
    }

    public Double getContractVATSum() {
        return contractVATSum;
    }

    public void setContractVATSum(Double contractVATSum) {
        this.contractVATSum = contractVATSum;
    }

    public Double getContractTotalSum() {
        return contractTotalSum;
    }

    public void setContractTotalSum(Double contractTotalSum) {
        this.contractTotalSum = contractTotalSum;
    }

    public Double getRealContractSum() {
        return realContractSum;
    }

    public void setRealContractSum(Double realContractSum) {
        this.realContractSum = realContractSum;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public String getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(String paymentSource) {
        this.paymentSource = paymentSource;
    }

    public EBuyMonth getPlannedBuyMonth() {
        return plannedBuyMonth;
    }

    public void setPlannedBuyMonth(EBuyMonth plannedBuyMonth) {
        this.plannedBuyMonth = plannedBuyMonth;
    }

    public Date getPlanedExecutionDate() {
        return planedExecutionDate;
    }

    public void setPlanedExecutionDate(Date planedExecutionDate) {
        this.planedExecutionDate = planedExecutionDate;
    }

    public Date getRealExecutionDate() {
        return realExecutionDate;
    }

    public void setRealExecutionDate(Date realExecutionDate) {
        this.realExecutionDate = realExecutionDate;
    }

    public Date getMarkExecutionDate() {
        return markExecutionDate;
    }

    public void setMarkExecutionDate(Date markExecutionDate) {
        this.markExecutionDate = markExecutionDate;
    }

    public DicContractObjectPurchaseType getContractObjectPurchaseType() {
        return contractObjectPurchaseType;
    }

    public void setContractObjectPurchaseType(DicContractObjectPurchaseType contractObjectPurchaseType) {
        this.contractObjectPurchaseType = contractObjectPurchaseType;
    }
}

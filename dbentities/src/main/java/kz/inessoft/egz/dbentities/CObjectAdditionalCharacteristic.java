package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CObjectAdditionalCharacteristic implements Serializable {
    @Column(name = "additional_characteristic_ru")
    private String additionalCharacteristicRu;

    @Column(name = "additional_characteristic_kk")
    private String additionalCharacteristicKk;

    public String getAdditionalCharacteristicRu() {
        return additionalCharacteristicRu;
    }

    public void setAdditionalCharacteristicRu(String additionalCharacteristicRu) {
        this.additionalCharacteristicRu = additionalCharacteristicRu;
    }

    public String getAdditionalCharacteristicKk() {
        return additionalCharacteristicKk;
    }

    public void setAdditionalCharacteristicKk(String additionalCharacteristicKk) {
        this.additionalCharacteristicKk = additionalCharacteristicKk;
    }
}

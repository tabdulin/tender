package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CObjectAdditionalCharacteristic.class)
public class CObjectAdditionalCharacteristic_ {
    public static volatile SingularAttribute<CObjectAdditionalCharacteristic, String> additionalCharacteristicRu;
    public static volatile SingularAttribute<CObjectAdditionalCharacteristic, String> additionalCharacteristicKk;
}

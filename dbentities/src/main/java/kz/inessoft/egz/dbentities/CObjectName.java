package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CObjectName implements Serializable {
    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    @Column(name = "name_kk", nullable = false)
    private String nameKk;

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameKk() {
        return nameKk;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }
}

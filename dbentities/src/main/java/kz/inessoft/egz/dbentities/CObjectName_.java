package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CObjectName.class)
public class CObjectName_ {
    public static volatile SingularAttribute<CObjectName, String> nameRu;
    public static volatile SingularAttribute<CObjectName, String> nameKk;
}

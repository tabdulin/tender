package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by alexey on 29.05.17.
 */
@Embeddable
public class CObjectShortDescription implements Serializable {
    @Column(name = "short_description_ru", nullable = false)
    private String shortDescriptionRu;

    @Column(name = "short_description_kk", nullable = false)
    private String shortDescriptionKk;

    public String getShortDescriptionRu() {
        return shortDescriptionRu;
    }

    public void setShortDescriptionRu(String shortDescriptionRu) {
        this.shortDescriptionRu = shortDescriptionRu;
    }

    public String getShortDescriptionKk() {
        return shortDescriptionKk;
    }

    public void setShortDescriptionKk(String shortDescriptionKk) {
        this.shortDescriptionKk = shortDescriptionKk;
    }
}

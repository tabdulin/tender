package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(CObjectShortDescription.class)
public class CObjectShortDescription_ {
    public static volatile SingularAttribute<CObjectShortDescription, String> shortDescriptionRu;
    public static volatile SingularAttribute<CObjectShortDescription, String> shortDescriptionKk;
}

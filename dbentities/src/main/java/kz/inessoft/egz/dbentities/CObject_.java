package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 26.05.17.
 */
@StaticMetamodel(CObject.class)
public class CObject_ extends ADBEntity_ {
    public static volatile SingularAttribute<CObject, Long> portalId;
    public static volatile SingularAttribute<CObject, CGeneralInfo> generalInfo;
    public static volatile SingularAttribute<CObject, String> originalHtml;
    public static volatile SingularAttribute<CObject, Long> lotPortalId;
    public static volatile SingularAttribute<CObject, Long> planId;
    public static volatile SingularAttribute<CObject, CObjectName> name;
    public static volatile SingularAttribute<CObject, Integer> year;
    public static volatile SingularAttribute<CObject, DicTru> tru;
    public static volatile SingularAttribute<CObject, DicPurchaseObjectType> purchaseObjectType;
    public static volatile SingularAttribute<CObject, CObjectShortDescription> shortDescription;
    public static volatile SingularAttribute<CObject, CObjectAdditionalCharacteristic> additionalCharacteristic;
    public static volatile SingularAttribute<CObject, Double> priceWithoutVAT;
    public static volatile SingularAttribute<CObject, Double> priceWithVAT;
    public static volatile SingularAttribute<CObject, Double> value;
    public static volatile SingularAttribute<CObject, DicUnit> dicUnit;
    public static volatile SingularAttribute<CObject, Double> contractSumWithoutVAT;
    public static volatile SingularAttribute<CObject, Double> contractVATSum;
    public static volatile SingularAttribute<CObject, Double> contractTotalSum;
    public static volatile SingularAttribute<CObject, Double> realContractSum;
    public static volatile SingularAttribute<CObject, String> placement;
    public static volatile SingularAttribute<CObject, String> paymentSource;
    public static volatile SingularAttribute<CObject, CObject.EBuyMonth> plannedBuyMonth;
    public static volatile SingularAttribute<CObject, Date> planedExecutionDate;
    public static volatile SingularAttribute<CObject, Date> realExecutionDate;
    public static volatile SingularAttribute<CObject, Date> markExecutionDate;
    public static volatile SingularAttribute<CObject, DicContractObjectPurchaseType> contractObjectPurchaseType;
}

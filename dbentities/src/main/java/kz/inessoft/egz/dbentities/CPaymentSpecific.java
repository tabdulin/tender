package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 07.10.16.
 */
@Entity
@Table(name = "c_payment_specific")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$c_payment_specific", allocationSize = 10)
public class CPaymentSpecific extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "c_object_id", nullable = false)
    private CObject cObject;

    @ManyToOne
    @JoinColumn(name = "dic_contract_object_specific_id", nullable = false)
    private DicContractObjectSpecific contractObjectSpecific;

    @Column(name = "financial_year", nullable = false)
    private int financialYear;

    @Column(name = "sum", nullable = false)
    private Double sum;

    public CObject getCObject() {
        return cObject;
    }

    public void setCObject(CObject cObject) {
        this.cObject = cObject;
    }

    public DicContractObjectSpecific getContractObjectSpecific() {
        return contractObjectSpecific;
    }

    public void setContractObjectSpecific(DicContractObjectSpecific contractObjectSpecific) {
        this.contractObjectSpecific = contractObjectSpecific;
    }

    public int getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(int financialYear) {
        this.financialYear = financialYear;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }
}

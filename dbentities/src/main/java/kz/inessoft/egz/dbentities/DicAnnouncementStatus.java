package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 * Тип объявления
 */
@Entity
@Table(name = "dic_announcement_status")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_announcement_status", allocationSize = 10)
public class DicAnnouncementStatus extends ANamedEntity {
    @Column(name = "final_status", nullable = false)
    private boolean finalStatus = false;

    @Column(name = "not_update_until_start_recv", nullable = false)
    private boolean notUpdateUntilStartRecv;

    @Column(name = "not_update_until_end_recv", nullable = false)
    private boolean notUpdateUntilEndRecv;

    public boolean isFinalStatus() {
        return finalStatus;
    }

    public void setFinalStatus(boolean finalStatus) {
        this.finalStatus = finalStatus;
    }

    public boolean isNotUpdateUntilStartRecv() {
        return notUpdateUntilStartRecv;
    }

    public void setNotUpdateUntilStartRecv(boolean notUpdateUntilStartRecv) {
        this.notUpdateUntilStartRecv = notUpdateUntilStartRecv;
    }

    public boolean isNotUpdateUntilEndRecv() {
        return notUpdateUntilEndRecv;
    }

    public void setNotUpdateUntilEndRecv(boolean notUpdateUntilEndRecv) {
        this.notUpdateUntilEndRecv = notUpdateUntilEndRecv;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 26.10.16.
 */
@StaticMetamodel(DicAnnouncementStatus.class)
public class DicAnnouncementStatus_ extends ANamedEntity_ {
    public static volatile SingularAttribute<DicAnnouncementStatus, Boolean> finalStatus;
    public static volatile SingularAttribute<DicAnnouncementStatus, Boolean> notUpdateUntilStartRecv;
    public static volatile SingularAttribute<DicAnnouncementStatus, Boolean> notUpdateUntilEndRecv;
}

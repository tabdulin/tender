package kz.inessoft.egz.dbentities;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 * Роль человека в тендерной комиссии
 */
@Entity
@Table(name = "dic_comission_role")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_comission_role", allocationSize = 10)
public class DicComissionRole extends ANamedEntity {
}

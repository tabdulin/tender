package kz.inessoft.egz.dbentities;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 * Вид условной скидки
 */
@Entity
@Table(name = "dic_conditional_discount")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_conditional_discount", allocationSize = 10)
public class DicConditionalDiscount extends ANamedEntity {
}

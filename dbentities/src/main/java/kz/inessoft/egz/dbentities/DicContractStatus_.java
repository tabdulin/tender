package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(DicContractStatus.class)
public class DicContractStatus_ extends ANamedEntity_ {
}

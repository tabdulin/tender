package kz.inessoft.egz.dbentities;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 07.10.16.
 */
@Entity
@Table(name = "dic_contract_type")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_contract_type", allocationSize = 10)
public class DicContractType extends ANamedEntity {
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 29.05.17.
 */
@StaticMetamodel(DicContractType.class)
public class DicContractType_ extends ANamedEntity_ {
}

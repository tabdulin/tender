package kz.inessoft.egz.dbentities;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 15.02.17.
 */
@Entity
@Table(name = "dic_discussion_response_type")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_discussion_response_type", allocationSize = 10)
public class DicDiscussionResponseType extends ANamedEntity {
}

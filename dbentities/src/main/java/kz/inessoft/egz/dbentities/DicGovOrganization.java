package kz.inessoft.egz.dbentities;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 * Гос. орган (заказчик/организатор конкурса)
 */
@Entity
@Table(name = "dic_gov_organization")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_gov_organization", allocationSize = 10)
@Indexed
public class DicGovOrganization extends ANamedEntity {
    @Column(name = "bin", nullable = false)
    @Field(analyze = Analyze.NO)
    private String bin;

    @Column(name = "address")
    private String address;

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

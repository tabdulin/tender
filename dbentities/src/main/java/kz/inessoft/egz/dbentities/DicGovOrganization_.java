package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicGovOrganization.class)
public class DicGovOrganization_ extends ANamedEntity_ {
    public static volatile SingularAttribute<DicGovOrganization, String> bin;
    public static volatile SingularAttribute<DicGovOrganization, String> address;
}

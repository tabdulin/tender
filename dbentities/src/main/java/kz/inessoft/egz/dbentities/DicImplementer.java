package kz.inessoft.egz.dbentities;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 * Исполнитель
 */
@Entity
@Table(name = "dic_implementer")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_implementer", allocationSize = 10)
@Indexed
public class DicImplementer extends ANamedEntity {
    @Column(name = "xin", nullable = false)
    @Field(analyze = Analyze.NO)
    private String xin;

    public String getXin() {
        return xin;
    }

    public void setXin(String xin) {
        this.xin = xin;
    }
}

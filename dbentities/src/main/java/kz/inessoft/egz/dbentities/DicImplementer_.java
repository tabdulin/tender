package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicImplementer.class)
public class DicImplementer_ extends ANamedEntity_ {
    public static volatile SingularAttribute<DicImplementer, String> xin;
}

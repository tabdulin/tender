package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alexey on 18.10.16.
 */
@Entity
@Table(name = "dic_interesting_tru")
public class DicInterestingTru implements Serializable {
    @Column(name = "id")
    @Id
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "update_date", nullable = false)
    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}

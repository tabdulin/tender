package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 13.01.17.
 */
@StaticMetamodel(DicInterestingTru.class)
public class DicInterestingTru_ {
    public static volatile SingularAttribute<DicInterestingTru, Long> id;
    public static volatile SingularAttribute<DicInterestingTru, String> code;
    public static volatile SingularAttribute<DicInterestingTru, Date> updateDate;
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 * Статус лота
 */
@Entity
@Table(name = "dic_lot_status")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_lot_status", allocationSize = 10)
public class DicLotStatus extends ANamedEntity {
    @Column(name = "contract_status", nullable = false)
    private boolean contractStatus;

    public boolean isContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(boolean contractStatus) {
        this.contractStatus = contractStatus;
    }
}

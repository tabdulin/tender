package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicLotStatus.class)
public class DicLotStatus_ extends ANamedEntity_ {
    public static volatile SingularAttribute<DicLotStatus, Boolean> contractStatus;
}

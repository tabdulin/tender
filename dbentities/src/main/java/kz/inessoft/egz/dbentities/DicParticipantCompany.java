package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 05.09.16.
 */
@Entity
@Table(name = "dic_participant_company")
public class DicParticipantCompany implements Serializable, IPortalUser {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    @Column(name = "portal_password", nullable = false)
    private String portalPassword;

    @Column(name = "eds_private_key", nullable = false)
    @Lob
    private byte[] edsPrivateKey;

    @Column(name = "eds_password", nullable = false)
    private String edsPassword;

    @Column(name = "default_refresh_credential", nullable = false)
    private boolean defaultRefreshCredential;

    @Column(name = "last_notice_time", nullable = false)
    private Date lastNoticeTime;

    @Column(name = "telegram_user_id", nullable = false)
    private String telegramUserId;

    @Column(name = "telegram_token", nullable = false)
    private String telegramToken;

    @Column(name = "telegram_password", nullable = false)
    private String telegramPassword;

    @Column(name = "load_announcement_priority", nullable = false)
    private long loadAnnouncementPriority;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "dic_participant_company_cross_template",
            joinColumns = @JoinColumn(name = "dic_participant_company_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "dic_participation_template_id", referencedColumnName = "id"))
    private List<DicParticipationTemplate> templates;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getPortalPassword() {
        return portalPassword;
    }

    public void setPortalPassword(String portalPassword) {
        this.portalPassword = portalPassword;
    }

    public byte[] getEdsPrivateKey() {
        return edsPrivateKey;
    }

    public void setEdsPrivateKey(byte[] edsPrivateKey) {
        this.edsPrivateKey = edsPrivateKey;
    }

    public String getEdsPassword() {
        return edsPassword;
    }

    public void setEdsPassword(String edsPassword) {
        this.edsPassword = edsPassword;
    }

    public boolean isDefaultRefreshCredential() {
        return defaultRefreshCredential;
    }

    public void setDefaultRefreshCredential(boolean defaultRefreshCredential) {
        this.defaultRefreshCredential = defaultRefreshCredential;
    }

    public Date getLastNoticeTime() {
        return lastNoticeTime;
    }

    public void setLastNoticeTime(Date lastNoticeTime) {
        this.lastNoticeTime = lastNoticeTime;
    }

    public String getTelegramUserId() {
        return telegramUserId;
    }

    public void setTelegramUserId(String telegramUserId) {
        this.telegramUserId = telegramUserId;
    }

    public String getTelegramToken() {
        return telegramToken;
    }

    public void setTelegramToken(String telegramToken) {
        this.telegramToken = telegramToken;
    }

    public String getTelegramPassword() {
        return telegramPassword;
    }

    public void setTelegramPassword(String telegramPassword) {
        this.telegramPassword = telegramPassword;
    }

    public long getLoadAnnouncementPriority() {
        return loadAnnouncementPriority;
    }

    public void setLoadAnnouncementPriority(long loadAnnouncementPriority) {
        this.loadAnnouncementPriority = loadAnnouncementPriority;
    }

    public List<DicParticipationTemplate> getTemplates() {
        return templates;
    }

    public void setTemplates(List<DicParticipationTemplate> templates) {
        this.templates = templates;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DicParticipantCompany && id.equals(((DicParticipantCompany) obj).getId());
    }

    @Override
    public int hashCode() {
        if (id == null)
            return 0;
        else
            return id.hashCode();
    }
}

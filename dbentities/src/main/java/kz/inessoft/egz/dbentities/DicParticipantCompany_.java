package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicParticipantCompany.class)
public class DicParticipantCompany_ {
    public static volatile SingularAttribute<DicParticipantCompany, Long> id;
    public static volatile SingularAttribute<DicParticipantCompany, String> nameRu;
    public static volatile SingularAttribute<DicParticipantCompany, String> portalPassword;
    public static volatile SingularAttribute<DicParticipantCompany, byte[]> edsPrivateKey;
    public static volatile SingularAttribute<DicParticipantCompany, String> edsPassword;
    public static volatile SingularAttribute<DicParticipantCompany, Boolean> defaultRefreshCredential;
    public static volatile SingularAttribute<DicParticipantCompany, Date> lastNoticeTime;
    public static volatile SingularAttribute<DicParticipantCompany, String> telegramUserId;
    public static volatile SingularAttribute<DicParticipantCompany, String> telegramToken;
    public static volatile SingularAttribute<DicParticipantCompany, String> telegramPassword;
    public static volatile SingularAttribute<DicParticipantCompany, Long> loadAnnouncementPriority;
    public static volatile ListAttribute<DicParticipantCompany, DicParticipationTemplate> templates;
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 05.09.16.
 */
@Entity
@Table(name = "dic_participation_template")
public class DicParticipationTemplate implements Serializable {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }
}

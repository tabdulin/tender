package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicParticipationTemplate.class)
public class DicParticipationTemplate_ {
    public static volatile SingularAttribute<DicParticipationTemplate, Long> id;
    public static volatile SingularAttribute<DicParticipationTemplate, String> nameRu;
}

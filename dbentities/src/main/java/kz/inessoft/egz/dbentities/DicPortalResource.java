package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.09.16.
 */
@Entity
@Table(name = "dic_portal_resource")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_portal_resource", allocationSize = 10)
public class DicPortalResource extends ADBEntity {
    @Column(name = "html_replacement", nullable = false)
    private String htmlReplacement;

    public String getHtmlReplacement() {
        return htmlReplacement;
    }

    public void setHtmlReplacement(String htmlReplacement) {
        this.htmlReplacement = htmlReplacement;
    }
}

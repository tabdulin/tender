package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by alexey on 20.02.17.
 */
@Entity
@Table(name = "dic_portal_resource_mask")
public class DicPortalResourceMask {
    @Id
    @Column(name = "html_source_text")
    private String htmlSourceText;

    @ManyToOne
    @JoinColumn(name = "dic_portal_resource_id", nullable = false)
    private DicPortalResource portalResource;

    public String getHtmlSourceText() {
        return htmlSourceText;
    }

    public void setHtmlSourceText(String htmlSourceText) {
        this.htmlSourceText = htmlSourceText;
    }

    public DicPortalResource getPortalResource() {
        return portalResource;
    }

    public void setPortalResource(DicPortalResource portalResource) {
        this.portalResource = portalResource;
    }
}

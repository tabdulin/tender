package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "dic_proposal_status")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_proposal_status", allocationSize = 10)
public class DicProposalStatus extends ANamedEntity {
    @Column(name = "skip_on_announcement_load", nullable = false)
    private boolean skipOnAnnouncementLoad;

    public boolean isSkipOnAnnouncementLoad() {
        return skipOnAnnouncementLoad;
    }

    public void setSkipOnAnnouncementLoad(boolean skipOnAnnouncementLoad) {
        this.skipOnAnnouncementLoad = skipOnAnnouncementLoad;
    }
}

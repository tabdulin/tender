package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 06.02.17.
 */
@StaticMetamodel(DicProposalStatus.class)
public class DicProposalStatus_ extends ANamedEntity_ {
    public static volatile SingularAttribute<DicProposalStatus, Boolean> skipOnAnnouncementLoad;
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicPurchaseMode.class)
public class DicPurchaseMode_ extends ANamedEntity_ {
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicPurchaseObjectType.class)
public class DicPurchaseObjectType_ extends ANamedEntity_ {
}

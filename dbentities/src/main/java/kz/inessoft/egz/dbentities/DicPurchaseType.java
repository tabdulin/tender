package kz.inessoft.egz.dbentities;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "dic_purchase_type")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_purchase_type", allocationSize = 10)
public class DicPurchaseType extends ANamedEntity {
}

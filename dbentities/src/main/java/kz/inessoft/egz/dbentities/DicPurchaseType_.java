package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicPurchaseType.class)
public class DicPurchaseType_ extends ANamedEntity_ {
}

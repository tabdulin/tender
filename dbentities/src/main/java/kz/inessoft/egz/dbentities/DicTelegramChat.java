package kz.inessoft.egz.dbentities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 13.09.16.
 */
@Entity
@Table(name = "dic_telegram_chat")
public class DicTelegramChat implements Serializable {
    @EmbeddedId
    private DicTelegramChatId id;

    public DicTelegramChatId getId() {
        return id;
    }

    public void setId(DicTelegramChatId id) {
        this.id = id;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 06.02.17.
 */
@Embeddable
public class DicTelegramChatId implements Serializable {
    @Column(name = "chat_id")
    private Long chatId;

    @ManyToOne
    @JoinColumn(name = "dic_participant_company_id", nullable = false)
    private DicParticipantCompany participantCompany;

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public DicParticipantCompany getParticipantCompany() {
        return participantCompany;
    }

    public void setParticipantCompany(DicParticipantCompany participantCompany) {
        this.participantCompany = participantCompany;
    }

// todo допилить
//    @Override
//    public int hashCode() {
//        int chatHash = chatId == null ? 0 : chatId.hashCode();
//        int partCompanyHash = participantCompany == null || participantCompany.getId() == null ? 0 : participantCompany.getId().hashCode();
//        return chatHash + partCompanyHash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (!(obj instanceof DicTelegramChatId))
//            return false;
//        DicTelegramChatId o1 = (DicTelegramChatId) obj;
//        if (chatId == null && o1.getChatId() != null ||
//                chatId != null && o1.getChatId() == null)
//            return false;
//        return super.equals(obj);
//    }
}

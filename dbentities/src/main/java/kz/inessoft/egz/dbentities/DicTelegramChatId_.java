package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 06.02.17.
 */
@StaticMetamodel(DicTelegramChatId.class)
public class DicTelegramChatId_ {
    public static volatile SingularAttribute<DicTelegramChatId, Long> chatId;
    public static volatile SingularAttribute<DicTelegramChatId, DicParticipantCompany> participantCompany;
}

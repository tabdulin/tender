package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 06.02.17.
 */
@StaticMetamodel(DicTelegramChat.class)
public class DicTelegramChat_ {
    public static volatile SingularAttribute<DicTelegramChat, DicTelegramChatId> id;
}

package kz.inessoft.egz.dbentities;

import javax.persistence.*;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "dic_tender_documentation")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_tender_documentation", allocationSize = 10)
public class DicTenderDocumentation extends ANamedEntity {
    @Column(name = "upload_to_jira", nullable = false)
    private boolean uploadToJira;

    public boolean isUploadToJira() {
        return uploadToJira;
    }

    public void setUploadToJira(boolean uploadToJira) {
        this.uploadToJira = uploadToJira;
    }
}

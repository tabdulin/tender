package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(DicTenderDocumentation.class)
public class DicTenderDocumentation_ extends ANamedEntity_ {
    public static volatile SingularAttribute<DicTenderDocumentation, Boolean> uploadToJira;
}

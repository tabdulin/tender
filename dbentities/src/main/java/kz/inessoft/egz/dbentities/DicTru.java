package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "dic_tru")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_tru", allocationSize = 10)
public class DicTru extends ANamedEntity {
    @Column(name = "code", nullable = false)
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

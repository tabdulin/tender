package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(DicTru.class)
public class DicTru_ extends ANamedEntity_ {
    public static volatile SingularAttribute<DicTru, String> code;
}

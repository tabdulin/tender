package kz.inessoft.egz.dbentities;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "dic_unit")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$dic_unit", allocationSize = 10)
public class DicUnit extends ANamedEntity {
}

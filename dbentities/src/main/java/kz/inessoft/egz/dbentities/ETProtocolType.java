package kz.inessoft.egz.dbentities;

/**
 * Created by alexey on 17.02.17.
 */
public enum ETProtocolType {
    DISCUSSION("Протокол обсуждения документации"),
    OPENING("Протокол вскрытия"),
    PRELIMINARY_ACCES("Протокол предварительного допуска"),
    ACCES("Протокол допуска"),
    RESULT("Протокол итогов"),
    RESULT_EXPERT_OPINION("Экспертное мнение");

    private String name;

    ETProtocolType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

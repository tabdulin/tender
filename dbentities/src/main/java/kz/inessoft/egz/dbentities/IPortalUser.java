package kz.inessoft.egz.dbentities;

/**
 * Created by alexey on 15.09.16.
 */
public interface IPortalUser {

    String getPortalPassword();

    byte[] getEdsPrivateKey();

    String getEdsPassword();
}

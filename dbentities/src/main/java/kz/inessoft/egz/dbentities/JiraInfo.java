package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by alexey on 27.01.17.
 */
@Embeddable
public class JiraInfo {
    @Column(name = "jira_id")
    private Long id;

    @Column(name = "jira_key")
    private String key;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}

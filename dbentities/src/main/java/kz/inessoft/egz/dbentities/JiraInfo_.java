package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(JiraInfo.class)
public class JiraInfo_ {
    public static volatile SingularAttribute<JiraInfo, Long> id;
    public static volatile SingularAttribute<JiraInfo, String> key;
}

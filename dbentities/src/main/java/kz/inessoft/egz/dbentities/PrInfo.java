package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by alexey on 01.11.16.
 */
@Entity
@Table(name = "pr_info")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$pr_info", allocationSize = 10)
public class PrInfo extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "dic_participant_company_id", nullable = false)
    private DicParticipantCompany participantCompany;

    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @Column(name = "portal_id")
    private Long portalId;

    @Column(name = "finished_flag", nullable = false)
    private boolean finishedFlag = false;

    @Column(name = "current_step_description")
    private String currentStepDescription;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "proposal_data", nullable = false)
    private long proposalData;

    public DicParticipantCompany getParticipantCompany() {
        return participantCompany;
    }

    public void setParticipantCompany(DicParticipantCompany participantCompany) {
        this.participantCompany = participantCompany;
    }

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public boolean isFinishedFlag() {
        return finishedFlag;
    }

    public void setFinishedFlag(boolean finishedFlag) {
        this.finishedFlag = finishedFlag;
    }

    public String getCurrentStepDescription() {
        return currentStepDescription;
    }

    public void setCurrentStepDescription(String currentStepDescription) {
        this.currentStepDescription = currentStepDescription;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public long getProposalData() {
        return proposalData;
    }

    public void setProposalData(long proposalData) {
        this.proposalData = proposalData;
    }
}

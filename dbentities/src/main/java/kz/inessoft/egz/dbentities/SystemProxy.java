package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by alexey on 17.01.17.
 */
@Entity
@Table(name = "system_proxy")
public class SystemProxy {
    @EmbeddedId
    private SystemProxyId id;

    /**
     * Пока это поле выставляется внешними утилитами в базе. Тут оно только читается.
     */
    @Column(name = "available", nullable = false, updatable = false)
    private boolean available;

    public SystemProxyId getId() {
        return id;
    }

    public void setId(SystemProxyId id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by alexey on 17.01.17.
 */
@Embeddable
public class SystemProxyId implements Serializable {
    @Column(name = "proxy_type", nullable = false)
    private String proxyType;

    @Column(name = "proxy_host", nullable = false)
    private String proxyHost;

    @Column(name = "proxy_port", nullable = false)
    private int proxyPort;

    public String getProxyType() {
        return proxyType;
    }

    public void setProxyType(String proxyType) {
        this.proxyType = proxyType;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }
}

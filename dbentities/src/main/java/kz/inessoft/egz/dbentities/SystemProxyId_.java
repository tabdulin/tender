package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 17.01.17.
 */
@StaticMetamodel(SystemProxyId.class)
public class SystemProxyId_ {
    public static volatile SingularAttribute<SystemProxyId, String> proxyType;
    public static volatile SingularAttribute<SystemProxyId, String> proxyHost;
    public static volatile SingularAttribute<SystemProxyId, Integer> proxyPort;
}

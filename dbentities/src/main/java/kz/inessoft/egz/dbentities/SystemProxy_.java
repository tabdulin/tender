package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 17.01.17.
 */
@StaticMetamodel(SystemProxy.class)
public class SystemProxy_ {
    public static volatile SingularAttribute<SystemProxy, SystemProxyId> id;
    public static volatile SingularAttribute<SystemProxy, Boolean> available;
}

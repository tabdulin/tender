package kz.inessoft.egz.dbentities;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "t_announcement")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_announcement", allocationSize = 10)
public class TAnnouncement extends ANamedEntity {
    @Column(name = "portal_id", nullable = false)
    private Long portalId;

    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "monitoring", nullable = false)
    private boolean monitoring;

    @Column(name = "load_time", nullable = false)
    private Date loadTime = new Date();

    @ManyToOne
    @JoinColumn(name = "dic_announcement_status_id", nullable = false)
    private DicAnnouncementStatus announcementStatus;

    @Column(name = "publication_date", nullable = false)
    private Date publicationDate;

    @Column(name = "start_rcv_time")
    private Date startRecvTime;

    @Column(name = "end_rcv_time")
    private Date endRecvTime;

    @Column(name = "start_rcv_appendix_time")
    private Date startRecvAppendixTime;

    @Column(name = "end_rcv_appendix_time")
    private Date endRecvAppendixTime;

    @Column(name = "start_discussion_time")
    private Date startDiscussionTime;

    @Column(name = "end_discussion_time")
    private Date endDiscussionTime;

    @ManyToOne
    @JoinColumn(name = "dic_purchase_mode_id", nullable = false)
    private DicPurchaseMode purchaseMode;

    @ManyToOne
    @JoinColumn(name = "dic_purchase_type_id")
    private DicPurchaseType purchaseType;

    @ManyToOne
    @JoinColumn(name = "dic_purchase_object_type_id", nullable = false)
    private DicPurchaseObjectType purchaseObjectType;

    @Column(name = "properties")
    private String properties;

    @ManyToOne
    @JoinColumn(name = "dic_gov_organizations_id", nullable = false)
    private DicGovOrganization organizator;

    @Column(name = "organizator_fio")
    private String organizatorFio;

    @Column(name = "organizator_job_position")
    private String organizatorJobPosition;

    @Column(name = "organizator_phone")
    private String organizatorPhone;

    @Column(name = "organizator_email")
    private String organizatorEmail;

    @Column(name = "organizator_bank_requisites")
    private String organizatorBankRequisites;

    @Embedded
    private JiraInfo jiraInfo;

    @ManyToOne
    @JoinColumn(name = "dic_participant_company_id", nullable = false)
    private DicParticipantCompany participantCompany;

    @Column(name = "general_tab", nullable = false)
    private String generalTab;

    @Column(name = "documentation_tab", nullable = false)
    private String documentationTab;

    @Column(name = "discussions_tab", nullable = true)
    private String discussionsTab;

    @Column(name = "proposals_info", nullable = true)
    private String proposalsInfo;

    @Column(name = "protocols_tab", nullable = false)
    private String protocolsTab;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "t_announcement_id")
    private List<TLotCrossAnnouncement> lotCrossAnnouncements;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "t_announcement_id")
    private List<TCrossAnnouncementParticipant> crossAnnouncementParticipants;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isMonitoring() {
        return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
        this.monitoring = monitoring;
    }

    public Date getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(Date loadTime) {
        this.loadTime = loadTime;
    }

    public DicAnnouncementStatus getAnnouncementStatus() {
        return announcementStatus;
    }

    public void setAnnouncementStatus(DicAnnouncementStatus announcementStatus) {
        this.announcementStatus = announcementStatus;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getStartRecvTime() {
        return startRecvTime;
    }

    public void setStartRecvTime(Date startRecvTime) {
        this.startRecvTime = startRecvTime;
    }

    public Date getEndRecvTime() {
        return endRecvTime;
    }

    public void setEndRecvTime(Date endRecvTime) {
        this.endRecvTime = endRecvTime;
    }

    public Date getStartRecvAppendixTime() {
        return startRecvAppendixTime;
    }

    public void setStartRecvAppendixTime(Date startRecvAppendixTime) {
        this.startRecvAppendixTime = startRecvAppendixTime;
    }

    public Date getEndRecvAppendixTime() {
        return endRecvAppendixTime;
    }

    public void setEndRecvAppendixTime(Date endRecvAppendixTime) {
        this.endRecvAppendixTime = endRecvAppendixTime;
    }

    public Date getStartDiscussionTime() {
        return startDiscussionTime;
    }

    public void setStartDiscussionTime(Date startDiscussionTime) {
        this.startDiscussionTime = startDiscussionTime;
    }

    public Date getEndDiscussionTime() {
        return endDiscussionTime;
    }

    public void setEndDiscussionTime(Date endDiscussionTime) {
        this.endDiscussionTime = endDiscussionTime;
    }

    public DicPurchaseMode getPurchaseMode() {
        return purchaseMode;
    }

    public void setPurchaseMode(DicPurchaseMode purchaseMode) {
        this.purchaseMode = purchaseMode;
    }

    public DicPurchaseType getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(DicPurchaseType purchaseType) {
        this.purchaseType = purchaseType;
    }

    public DicPurchaseObjectType getPurchaseObjectType() {
        return purchaseObjectType;
    }

    public void setPurchaseObjectType(DicPurchaseObjectType purchaseObjectType) {
        this.purchaseObjectType = purchaseObjectType;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public DicGovOrganization getOrganizator() {
        return organizator;
    }

    public void setOrganizator(DicGovOrganization organizator) {
        this.organizator = organizator;
    }

    public String getOrganizatorFio() {
        return organizatorFio;
    }

    public void setOrganizatorFio(String organizatorFio) {
        this.organizatorFio = organizatorFio;
    }

    public String getOrganizatorJobPosition() {
        return organizatorJobPosition;
    }

    public void setOrganizatorJobPosition(String organizatorJobPosition) {
        this.organizatorJobPosition = organizatorJobPosition;
    }

    public String getOrganizatorPhone() {
        return organizatorPhone;
    }

    public void setOrganizatorPhone(String organizatorPhone) {
        this.organizatorPhone = organizatorPhone;
    }

    public String getOrganizatorEmail() {
        return organizatorEmail;
    }

    public void setOrganizatorEmail(String organizatorEmail) {
        this.organizatorEmail = organizatorEmail;
    }

    public String getOrganizatorBankRequisites() {
        return organizatorBankRequisites;
    }

    public void setOrganizatorBankRequisites(String organizatorBankRequisites) {
        this.organizatorBankRequisites = organizatorBankRequisites;
    }

    public JiraInfo getJiraInfo() {
        return jiraInfo;
    }

    public void setJiraInfo(JiraInfo jiraInfo) {
        this.jiraInfo = jiraInfo;
    }

    public DicParticipantCompany getParticipantCompany() {
        return participantCompany;
    }

    public void setParticipantCompany(DicParticipantCompany participantCompany) {
        this.participantCompany = participantCompany;
    }

    public String getGeneralTab() {
        return generalTab;
    }

    public void setGeneralTab(String generalTab) {
        this.generalTab = generalTab;
    }

    public String getDocumentationTab() {
        return documentationTab;
    }

    public void setDocumentationTab(String documentationTab) {
        this.documentationTab = documentationTab;
    }

    public String getDiscussionsTab() {
        return discussionsTab;
    }

    public void setDiscussionsTab(String discussionsTab) {
        this.discussionsTab = discussionsTab;
    }

    public String getProposalsInfo() {
        return proposalsInfo;
    }

    public void setProposalsInfo(String proposalsInfo) {
        this.proposalsInfo = proposalsInfo;
    }

    public String getProtocolsTab() {
        return protocolsTab;
    }

    public void setProtocolsTab(String protocolsTab) {
        this.protocolsTab = protocolsTab;
    }

    public List<TLotCrossAnnouncement> getLotCrossAnnouncements() {
        return lotCrossAnnouncements;
    }

    public void setLotCrossAnnouncements(List<TLotCrossAnnouncement> lotCrossAnnouncements) {
        this.lotCrossAnnouncements = lotCrossAnnouncements;
    }

    public List<TCrossAnnouncementParticipant> getCrossAnnouncementParticipants() {
        return crossAnnouncementParticipants;
    }

    public void setCrossAnnouncementParticipants(List<TCrossAnnouncementParticipant> crossAnnouncementParticipants) {
        this.crossAnnouncementParticipants = crossAnnouncementParticipants;
    }

    public String getShortNameRu() {
        if (getNameRu() == null)
            return null;
        String result = StringUtils.replace(getNameRu(), "Электронные государственные закупки услуг ", "");
        result = StringUtils.replace(result, "Государственные закупки услуг ", "");
        result = StringUtils.replace(result, "государственные закупки услуг ", "");
        result = StringUtils.replace(result, " способом открытого конкурса", "");
        result = StringUtils.replace(result, " способом конкурса", "");
        result = StringUtils.replace(result, "Республики Казахстан", "РК");
        result = StringUtils.replace(result, "Автоматизированная информационная система", "АИС");
        result = StringUtils.replace(result, "информационных систем", "ИС");
        result = StringUtils.replace(result, "информационной системы", "ИС");
        result = StringUtils.replace(result, "по сопровождению", "сопровождение");
        result = StringUtils.replace(result, "программного обеспечения", "ПО");
        result = StringUtils.replace(result, "программное обеспечение", "ПО");
        result = StringUtils.replace(result, "прикладного программного обеспечения", "ППО");
        return result;
    }
}

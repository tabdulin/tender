package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 02.02.17.
 */
@StaticMetamodel(TAnnouncementLoadQueue.class)
public class TAnnouncementLoadQueue_ {
    public static volatile SingularAttribute<TAnnouncementLoadQueue, Long> portalId;
    public static volatile SingularAttribute<TAnnouncementLoadQueue, Date> queueTime;
}

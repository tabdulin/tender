package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 14.02.17.
 */
@Entity
@Table(name = "t_announcement_lots_tab_html")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_announcement_lots_tab_html", allocationSize = 10)
public class TAnnouncementLotsTabHtml extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    @Column(name = "page_number", nullable = false)
    private int pageNumber;

    @Column(name = "html", nullable = false)
    private String html;

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}

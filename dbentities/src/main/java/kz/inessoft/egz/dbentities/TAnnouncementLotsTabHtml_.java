package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 14.02.17.
 */
@StaticMetamodel(TAnnouncementLotsTabHtml.class)
public class TAnnouncementLotsTabHtml_ extends ADBEntity_ {
    public static volatile SingularAttribute<TAnnouncementLotsTabHtml, TAnnouncement> announcement;
    public static volatile SingularAttribute<TAnnouncementLotsTabHtml, Integer> pageNumber;
    public static volatile SingularAttribute<TAnnouncementLotsTabHtml, String> html;
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 26.10.16.
 */
@StaticMetamodel(TAnnouncement.class)
public class TAnnouncement_ extends ANamedEntity_ {
    public static volatile SingularAttribute<TAnnouncement, Long> portalId;
    public static volatile SingularAttribute<TAnnouncement, String> number;
    public static volatile SingularAttribute<TAnnouncement, Boolean> monitoring;
    public static volatile SingularAttribute<TAnnouncement, Date> loadTime;
    public static volatile SingularAttribute<TAnnouncement, DicAnnouncementStatus> announcementStatus;
    public static volatile SingularAttribute<TAnnouncement, Date> publicationDate;
    public static volatile SingularAttribute<TAnnouncement, Date> startRecvTime;
    public static volatile SingularAttribute<TAnnouncement, Date> endRecvTime;
    public static volatile SingularAttribute<TAnnouncement, Date> startRecvAppendixTime;
    public static volatile SingularAttribute<TAnnouncement, Date> endRecvAppendixTime;
    public static volatile SingularAttribute<TAnnouncement, Date> startDiscussionTime;
    public static volatile SingularAttribute<TAnnouncement, Date> endDiscussionTime;
    public static volatile SingularAttribute<TAnnouncement, DicPurchaseMode> purchaseMode;
    public static volatile SingularAttribute<TAnnouncement, DicPurchaseType> purchaseType;
    public static volatile SingularAttribute<TAnnouncement, DicPurchaseObjectType> purchaseObjectType;
    public static volatile SingularAttribute<TAnnouncement, String> properties;
    public static volatile SingularAttribute<TAnnouncement, DicGovOrganization> organizator;
    public static volatile SingularAttribute<TAnnouncement, String> organizatorFio;
    public static volatile SingularAttribute<TAnnouncement, String> organizatorJobPosition;
    public static volatile SingularAttribute<TAnnouncement, String> organizatorPhone;
    public static volatile SingularAttribute<TAnnouncement, String> organizatorEmail;
    public static volatile SingularAttribute<TAnnouncement, String> organizatorBankRequisites;
    public static volatile SingularAttribute<TAnnouncement, JiraInfo> jiraInfo;
    public static volatile SingularAttribute<TAnnouncement, DicParticipantCompany> participantCompany;
    public static volatile SingularAttribute<TAnnouncement, String> generalTab;
    public static volatile SingularAttribute<TAnnouncement, String> documentationTab;
    public static volatile SingularAttribute<TAnnouncement, String> discussionsTab;
    public static volatile SingularAttribute<TAnnouncement, String> proposalsInfo;
    public static volatile SingularAttribute<TAnnouncement, String> protocolsTab;
    public static volatile ListAttribute<TAnnouncement, TLotCrossAnnouncement> lotCrossAnnouncements;
    public static volatile ListAttribute<TAnnouncement, TCrossAnnouncementParticipant> crossAnnouncementParticipants;
}

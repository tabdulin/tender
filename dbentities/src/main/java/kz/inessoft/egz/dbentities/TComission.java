package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "t_comission")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_comission", allocationSize = 10)
public class TComission extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    @ManyToOne
    @JoinColumn(name = "dic_comission_role_id", nullable = false)
    private DicComissionRole comissionRole;

    @Column(name = "fio", nullable = false)
    private String fio;

    @Column(name = "job_position", nullable = false)
    private String jobPosition;

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }

    public DicComissionRole getComissionRole() {
        return comissionRole;
    }

    public void setComissionRole(DicComissionRole comissionRole) {
        this.comissionRole = comissionRole;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }
}

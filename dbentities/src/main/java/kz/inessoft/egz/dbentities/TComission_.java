package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 17.02.17.
 */
@StaticMetamodel(TComission.class)
public class TComission_ {
    public static volatile SingularAttribute<TComission, TAnnouncement> announcement;
    public static volatile SingularAttribute<TComission, DicComissionRole> comissionRole;
    public static volatile SingularAttribute<TComission, String> fio;
    public static volatile SingularAttribute<TComission, String> jobPosition;
}

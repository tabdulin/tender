package kz.inessoft.egz.dbentities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 05.09.16.
 */
@Entity
@Table(name = "t_cross_announcement_participant")
public class TCrossAnnouncementParticipant implements Serializable {

    @EmbeddedId
    private TCrossAnnouncementParticipantId id;

    @ManyToOne
    @JoinColumn(name = "dic_participation_template_id", nullable = false)
    private DicParticipationTemplate dicParticipationTemplate;

    public TCrossAnnouncementParticipantId getId() {
        return id;
    }

    public void setId(TCrossAnnouncementParticipantId id) {
        this.id = id;
    }

    public DicParticipationTemplate getDicParticipationTemplate() {
        return dicParticipationTemplate;
    }

    public void setDicParticipationTemplate(DicParticipationTemplate dicParticipationTemplate) {
        this.dicParticipationTemplate = dicParticipationTemplate;
    }
}

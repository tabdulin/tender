package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 13.01.17.
 */
@StaticMetamodel(TCrossAnnouncementParticipantId.class)
public class TCrossAnnouncementParticipantId_ {
    public static volatile SingularAttribute<TCrossAnnouncementParticipantId, TAnnouncement> announcement;
    public static volatile SingularAttribute<TCrossAnnouncementParticipantId, DicParticipantCompany> dicParticipantCompany;
}

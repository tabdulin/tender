package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(TCrossAnnouncementParticipant.class)
public class TCrossAnnouncementParticipant_ {

    public static volatile SingularAttribute<TCrossAnnouncementParticipant, TCrossAnnouncementParticipantId> id;
    public static volatile SingularAttribute<TCrossAnnouncementParticipant, DicParticipationTemplate> dicParticipationTemplate;
}

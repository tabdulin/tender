package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "t_discount")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_discount", allocationSize = 10)
public class TDiscount extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "t_implementer_lot_id", nullable = false)
    private TImplementerLot implementerLot;

    @ManyToOne
    @JoinColumn(name = "dic_conditional_discount_id", nullable = false)
    private DicConditionalDiscount conditionalDiscount;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "string_amount")
    private String stringAmount;

    public TImplementerLot getImplementerLot() {
        return implementerLot;
    }

    public void setImplementerLot(TImplementerLot implementerLot) {
        this.implementerLot = implementerLot;
    }

    public DicConditionalDiscount getConditionalDiscount() {
        return conditionalDiscount;
    }

    public void setConditionalDiscount(DicConditionalDiscount conditionalDiscount) {
        this.conditionalDiscount = conditionalDiscount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStringAmount() {
        return stringAmount;
    }

    public void setStringAmount(String stringAmount) {
        this.stringAmount = stringAmount;
    }
}

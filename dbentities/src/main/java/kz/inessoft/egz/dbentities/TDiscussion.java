package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 19.09.16.
 */
@Entity
@Table(name = "t_discussion")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_discussion", allocationSize = 10)
public class TDiscussion extends ADBEntity {
    @Column(name = "portal_id", nullable = false)
    private Long portalId;

    @Column(name = "source_html", nullable = false)
    private String sourceHtml;

    @Embedded
    private TDiscussionRequest request;

    @Embedded
    private TDiscussionResponse response;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public String getSourceHtml() {
        return sourceHtml;
    }

    public void setSourceHtml(String sourceHtml) {
        this.sourceHtml = sourceHtml;
    }

    public TDiscussionRequest getRequest() {
        return request;
    }

    public void setRequest(TDiscussionRequest request) {
        this.request = request;
    }

    public TDiscussionResponse getResponse() {
        return response;
    }

    public void setResponse(TDiscussionResponse responce) {
        this.response = responce;
    }
}

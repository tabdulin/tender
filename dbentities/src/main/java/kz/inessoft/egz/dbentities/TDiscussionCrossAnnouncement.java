package kz.inessoft.egz.dbentities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 23.09.16.
 */
@Entity
@Table(name = "t_discussion_cross_announcement")
public class TDiscussionCrossAnnouncement implements Serializable {

    @EmbeddedId
    private TDiscussionCrossAnnouncementId id;

    public TDiscussionCrossAnnouncementId getId() {
        return id;
    }

    public void setId(TDiscussionCrossAnnouncementId id) {
        this.id = id;
    }
}

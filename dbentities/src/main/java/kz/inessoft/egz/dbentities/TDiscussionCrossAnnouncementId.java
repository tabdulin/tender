package kz.inessoft.egz.dbentities;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 27.10.16.
 */
@Embeddable
public class TDiscussionCrossAnnouncementId implements Serializable {
    @ManyToOne
    @JoinColumn(name = "t_discussion_id", nullable = false)
    private TDiscussion discussion;

    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    public TDiscussion getDiscussion() {
        return discussion;
    }

    public void setDiscussion(TDiscussion discussion) {
        this.discussion = discussion;
    }

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }
}

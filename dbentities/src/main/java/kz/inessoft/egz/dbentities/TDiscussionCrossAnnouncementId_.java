package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 15.02.17.
 */
@StaticMetamodel(TDiscussionCrossAnnouncementId.class)
public class TDiscussionCrossAnnouncementId_ {
    public static volatile SingularAttribute<TDiscussionCrossAnnouncementId, TDiscussion> discussion;
    public static volatile SingularAttribute<TDiscussionCrossAnnouncementId, TAnnouncement> announcement;
}

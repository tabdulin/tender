package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 15.02.17.
 */
@StaticMetamodel(TDiscussionCrossAnnouncement.class)
public class TDiscussionCrossAnnouncement_ {
    public static volatile SingularAttribute<TDiscussionCrossAnnouncement, TDiscussionCrossAnnouncementId> id;
}

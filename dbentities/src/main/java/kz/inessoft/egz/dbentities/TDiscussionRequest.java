package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alexey on 15.02.17.
 */
@Embeddable
public class TDiscussionRequest implements Serializable {
    @Column(name = "request_subject", nullable = false)
    private String subject;

    @ManyToOne
    @JoinColumn(name = "dic_discussion_request_type_id", nullable = false)
    private DicDiscussionRequestType requestType;

    @Column(name = "request_implementer", nullable = false)
    private String implementer;

    @Column(name = "request_author", nullable = false)
    private String author;

    @Column(name = "request_time", nullable = false)
    private Date time;

    @Column(name = "request_text", nullable = false)
    private String text;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public DicDiscussionRequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(DicDiscussionRequestType requestType) {
        this.requestType = requestType;
    }

    public String getImplementer() {
        return implementer;
    }

    public void setImplementer(String implementer) {
        this.implementer = implementer;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

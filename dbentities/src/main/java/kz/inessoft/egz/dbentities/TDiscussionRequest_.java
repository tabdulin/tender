package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 15.02.17.
 */
@StaticMetamodel(TDiscussionRequest.class)
public class TDiscussionRequest_ {
    public static volatile SingularAttribute<TDiscussionRequest, String> subject;
    public static volatile SingularAttribute<TDiscussionRequest, DicDiscussionRequestType> requestType;
    public static volatile SingularAttribute<TDiscussionRequest, String> implementer;
    public static volatile SingularAttribute<TDiscussionRequest, String> author;
    public static volatile SingularAttribute<TDiscussionRequest, Date> time;
    public static volatile SingularAttribute<TDiscussionRequest, String> text;
}

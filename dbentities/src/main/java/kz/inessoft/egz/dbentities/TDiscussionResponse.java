package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alexey on 15.02.17.
 */
@Embeddable
public class TDiscussionResponse implements Serializable {
    @Column(name = "response_time")
    private Date time;

    @Column(name = "response_author")
    private String author;

    @ManyToOne
    @JoinColumn(name = "dic_discussion_response_type_id")
    private DicDiscussionResponseType responseType;

    @Column(name = "response_description")
    private String description;

    @Column(name = "response_reject_cause")
    private String rejectCause;

    @Column(name = "response_explanation")
    private String explanation;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public DicDiscussionResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(DicDiscussionResponseType responseType) {
        this.responseType = responseType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRejectCause() {
        return rejectCause;
    }

    public void setRejectCause(String rejectCause) {
        this.rejectCause = rejectCause;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 15.02.17.
 */
@StaticMetamodel(TDiscussionResponse.class)
public class TDiscussionResponse_ {
    public static volatile SingularAttribute<TDiscussionResponse, Date> time;
    public static volatile SingularAttribute<TDiscussionResponse, String> author;
    public static volatile SingularAttribute<TDiscussionResponse, DicDiscussionResponseType> responseType;
    public static volatile SingularAttribute<TDiscussionResponse, String> description;
    public static volatile SingularAttribute<TDiscussionResponse, String> rejectCause;
    public static volatile SingularAttribute<TDiscussionResponse, String> explanation;
}

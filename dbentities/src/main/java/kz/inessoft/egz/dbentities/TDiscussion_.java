package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 15.02.17.
 */
@StaticMetamodel(TDiscussion.class)
public class TDiscussion_ extends ADBEntity_ {
    public static volatile SingularAttribute<TDiscussion, Long> portalId;
    public static volatile SingularAttribute<TDiscussion, String> sourceHtml;
    public static volatile SingularAttribute<TDiscussion, TDiscussionRequest> request;
    public static volatile SingularAttribute<TDiscussion, TDiscussionResponse> response;
}

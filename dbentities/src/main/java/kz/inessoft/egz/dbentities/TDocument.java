package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "t_document")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_document", allocationSize = 10)
public class TDocument extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    @ManyToOne
    @JoinColumn(name = "dic_tender_documentation_id", nullable = false)
    private DicTenderDocumentation documentation;

    @Column(name = "required", nullable = false)
    private boolean required;

    @Column(name = "source_html")
    private String html;

    @OneToMany
    @JoinColumn(name = "t_document_id")
    private List<TDocumentCrossTPortalFile> portalFiles;

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }

    public DicTenderDocumentation getDocumentation() {
        return documentation;
    }

    public void setDocumentation(DicTenderDocumentation documentation) {
        this.documentation = documentation;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public List<TDocumentCrossTPortalFile> getPortalFiles() {
        return portalFiles;
    }

    public void setPortalFiles(List<TDocumentCrossTPortalFile> portalFiles) {
        this.portalFiles = portalFiles;
    }
}

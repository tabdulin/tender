package kz.inessoft.egz.dbentities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "t_document_cross_t_portal_file")
public class TDocumentCrossTPortalFile implements Serializable {

    @EmbeddedId
    private TDocumentCrossTPortalFileId id;

    public TDocumentCrossTPortalFileId getId() {
        return id;
    }

    public void setId(TDocumentCrossTPortalFileId id) {
        this.id = id;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 27.10.16.
 */
@Embeddable
public class TDocumentCrossTPortalFileId implements Serializable {
    // todo Переопределить hashCode и equals
    @ManyToOne
    @JoinColumn(name = "t_document_id", nullable = false)
    private TDocument document;

    @ManyToOne
    @JoinColumn(name = "t_portal_file_id", nullable = false)
    private TPortalFile portalFile;

    public TDocument getDocument() {
        return document;
    }

    public void setDocument(TDocument document) {
        this.document = document;
    }

    public TPortalFile getPortalFile() {
        return portalFile;
    }

    public void setPortalFile(TPortalFile portalFile) {
        this.portalFile = portalFile;
    }
}

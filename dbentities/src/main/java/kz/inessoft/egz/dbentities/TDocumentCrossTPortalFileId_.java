package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(TDocumentCrossTPortalFileId.class)
public class TDocumentCrossTPortalFileId_ {
    public static volatile SingularAttribute<TDocumentCrossTPortalFileId, TDocument> document;
    public static volatile SingularAttribute<TDocumentCrossTPortalFileId, TPortalFile> portalFile;
}

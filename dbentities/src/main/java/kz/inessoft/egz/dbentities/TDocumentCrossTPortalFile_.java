package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(TDocumentCrossTPortalFile.class)
public class TDocumentCrossTPortalFile_ {
    public static volatile SingularAttribute<TDocumentCrossTPortalFile, TDocumentCrossTPortalFileId> id;
}

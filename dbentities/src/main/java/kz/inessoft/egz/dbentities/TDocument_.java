package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(TDocument.class)
public class TDocument_ {
    public static volatile SingularAttribute<TDocument, TAnnouncement> announcement;
    public static volatile SingularAttribute<TDocument, DicTenderDocumentation> documentation;
    public static volatile SingularAttribute<TDocument, Boolean> required;
    public static volatile SingularAttribute<TDocument, String> html;
    public static volatile ListAttribute<TDocument, TDocumentCrossTPortalFile> portalFiles;
}

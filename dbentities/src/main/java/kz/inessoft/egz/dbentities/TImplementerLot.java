package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "t_implementer_lot")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_implementer_lot", allocationSize = 10)
public class TImplementerLot extends ADBEntity {
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "t_lot_id", referencedColumnName = "t_lot_id", nullable = false),
            @JoinColumn(name = "t_announcement_id", referencedColumnName = "t_announcement_id", nullable = false)
    })
    private TLotCrossAnnouncement lotCrossAnnouncement;

    @ManyToOne
    @JoinColumn(name = "dic_implementer_id", nullable = false)
    private DicImplementer implementer;

    @Column(name = "access_granted")
    private Boolean accessGranted;

    @Column(name = "access_denied_cause")
    private String accessDeniedCause;

    @Column(name = "proposal_time", nullable = false)
    private Date proposalTime;

    @Column(name = "requisites")
    private String requisites;

    @Column(name = "requested_price")
    private Double requestedPrice;

    @Column(name = "act26_price")
    private Double act26Price;

    @Column(name = "final_price")
    private Double finalPrice;

    @Column(name = "winner_flag", nullable = false)
    private boolean winnerFlag = false;

    public TLotCrossAnnouncement getLotCrossAnnouncement() {
        return lotCrossAnnouncement;
    }

    public void setLotCrossAnnouncement(TLotCrossAnnouncement lotCrossAnnouncement) {
        this.lotCrossAnnouncement = lotCrossAnnouncement;
    }

    public DicImplementer getImplementer() {
        return implementer;
    }

    public void setImplementer(DicImplementer implementer) {
        this.implementer = implementer;
    }

    public Boolean getAccessGranted() {
        return accessGranted;
    }

    public void setAccessGranted(Boolean accessGranted) {
        this.accessGranted = accessGranted;
    }

    public String getAccessDeniedCause() {
        return accessDeniedCause;
    }

    public void setAccessDeniedCause(String accessDeniedCause) {
        this.accessDeniedCause = accessDeniedCause;
    }

    public Date getProposalTime() {
        return proposalTime;
    }

    public void setProposalTime(Date proposalTime) {
        this.proposalTime = proposalTime;
    }

    public String getRequisites() {
        return requisites;
    }

    public void setRequisites(String requisites) {
        this.requisites = requisites;
    }

    public Double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(Double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public Double getAct26Price() {
        return act26Price;
    }

    public void setAct26Price(Double act26Price) {
        this.act26Price = act26Price;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public boolean isWinnerFlag() {
        return winnerFlag;
    }

    public void setWinnerFlag(boolean winnerFlag) {
        this.winnerFlag = winnerFlag;
    }
}

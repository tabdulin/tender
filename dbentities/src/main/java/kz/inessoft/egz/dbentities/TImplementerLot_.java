package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(TImplementerLot.class)
public class TImplementerLot_ extends ADBEntity_ {
    public static volatile SingularAttribute<TImplementerLot, TLotCrossAnnouncement> lotCrossAnnouncement;
    public static volatile SingularAttribute<TImplementerLot, DicImplementer> implementer;
    public static volatile SingularAttribute<TImplementerLot, Boolean> accessGranted;
    public static volatile SingularAttribute<TImplementerLot, String> accessDeniedCause;
    public static volatile SingularAttribute<TImplementerLot, Date> proposalTime;
    public static volatile SingularAttribute<TImplementerLot, String> requisites;
    public static volatile SingularAttribute<TImplementerLot, Double> requestedPrice;
    public static volatile SingularAttribute<TImplementerLot, Double> act26Price;
    public static volatile SingularAttribute<TImplementerLot, Double> finalPrice;
    public static volatile SingularAttribute<TImplementerLot, Boolean> winnerFlag;
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.07.16.
 */
@Entity
@Table(name = "t_lot")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_lot", allocationSize = 10)
public class TLot extends ADBEntity {
    @Column(name = "portal_id", nullable = false)
    private Long portalId;

    @Column(name = "plan_unit_id", nullable = false)
    private Long planUnitId;

    @ManyToOne
    @JoinColumn(name = "dic_gov_organization_id", nullable = false)
    private DicGovOrganization customer;

    @ManyToOne
    @JoinColumn(name = "dic_tru_id", nullable = false)
    private DicTru dicTru;

    @Column(name = "short_characteristic_ru")
    private String shortCharacteristicRu;

    @Column(name = "additional_characteristic_ru")
    private String additionalCharacteristicRu;

    @Column(name = "payment_source_ru")
    private String paymentSourceRu;

    @Column(name = "price_for_unit")
    private Double priceForUnit;

    @Column(name = "count", nullable = false)
    private Double count;

    @ManyToOne
    @JoinColumn(name = "dic_unit_id", nullable = false)
    private DicUnit unit;

    @Column(name = "first_year_sum", nullable = false)
    private Double firstYearSum;

    @Column(name = "second_year_sum", nullable = false)
    private Double secondYearSum;

    @Column(name = "third_year_sum", nullable = false)
    private Double thirdYearSum;

    @Column(name = "total_sum", nullable = false)
    private Double totalSum;

    @Column(name = "prepayment", nullable = false)
    private Double prepayment;

    @Column(name = "delivery_time")
    private String deliveryTime;

    @Column(name = "incoterms")
    private String incoterms;

    @Column(name = "ingeneering")
    private Boolean ingeneering;

    @Column(name = "demping")
    private String demping;

    @Column(name = "customer_fio")
    private String customerFIO;

    @Column(name = "customer_job_position")
    private String customerJobPosition;

    @Column(name = "customer_phone")
    private String customerPhone;

    @Column(name = "customer_email")
    private String customerEmail;

    @Column(name = "customer_bank_requisites")
    private String customerBankRequisites;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Long getPlanUnitId() {
        return planUnitId;
    }

    public void setPlanUnitId(Long planUnitId) {
        this.planUnitId = planUnitId;
    }

    public DicGovOrganization getCustomer() {
        return customer;
    }

    public void setCustomer(DicGovOrganization customer) {
        this.customer = customer;
    }

    public DicTru getDicTru() {
        return dicTru;
    }

    public void setDicTru(DicTru dicTru) {
        this.dicTru = dicTru;
    }

    public String getShortCharacteristicRu() {
        return shortCharacteristicRu;
    }

    public void setShortCharacteristicRu(String shortCharacteristicRu) {
        this.shortCharacteristicRu = shortCharacteristicRu;
    }

    public String getAdditionalCharacteristicRu() {
        return additionalCharacteristicRu;
    }

    public void setAdditionalCharacteristicRu(String additionalCharacteristicRu) {
        this.additionalCharacteristicRu = additionalCharacteristicRu;
    }

    public String getPaymentSourceRu() {
        return paymentSourceRu;
    }

    public void setPaymentSourceRu(String paymentSourceRu) {
        this.paymentSourceRu = paymentSourceRu;
    }

    public Double getPriceForUnit() {
        return priceForUnit;
    }

    public void setPriceForUnit(Double priceForUnit) {
        this.priceForUnit = priceForUnit;
    }

    public Double getCount() {
        return count;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public DicUnit getUnit() {
        return unit;
    }

    public void setUnit(DicUnit unit) {
        this.unit = unit;
    }

    public Double getFirstYearSum() {
        return firstYearSum;
    }

    public void setFirstYearSum(Double firstYearSum) {
        this.firstYearSum = firstYearSum;
    }

    public Double getSecondYearSum() {
        return secondYearSum;
    }

    public void setSecondYearSum(Double secondYearSum) {
        this.secondYearSum = secondYearSum;
    }

    public Double getThirdYearSum() {
        return thirdYearSum;
    }

    public void setThirdYearSum(Double thirdYearSum) {
        this.thirdYearSum = thirdYearSum;
    }

    public Double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Double totalSum) {
        this.totalSum = totalSum;
    }

    public Double getPrepayment() {
        return prepayment;
    }

    public void setPrepayment(Double prepayment) {
        this.prepayment = prepayment;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getIncoterms() {
        return incoterms;
    }

    public void setIncoterms(String incoterms) {
        this.incoterms = incoterms;
    }

    public Boolean getIngeneering() {
        return ingeneering;
    }

    public void setIngeneering(Boolean ingeneering) {
        this.ingeneering = ingeneering;
    }

    public String getDemping() {
        return demping;
    }

    public void setDemping(String demping) {
        this.demping = demping;
    }

    public String getCustomerFIO() {
        return customerFIO;
    }

    public void setCustomerFIO(String customerFIO) {
        this.customerFIO = customerFIO;
    }

    public String getCustomerJobPosition() {
        return customerJobPosition;
    }

    public void setCustomerJobPosition(String customerJobPosition) {
        this.customerJobPosition = customerJobPosition;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerBankRequisites() {
        return customerBankRequisites;
    }

    public void setCustomerBankRequisites(String customerBankRequisites) {
        this.customerBankRequisites = customerBankRequisites;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 31.08.16.
 */
@Entity
@Table(name = "t_lot_cross_announcement")
public class TLotCrossAnnouncement implements Serializable {
    @EmbeddedId
    private TLotCrossAnnouncementId id;

    @Column(name = "lot_number", nullable = false)
    private String lotNumber;

    @Column(name = "original_html", nullable = false)
    private String originalHtml;

    @ManyToOne
    @JoinColumn(name = "dic_lot_status_id", nullable = false)
    private DicLotStatus status;

    @Column(name = "has_contract", nullable = false)
    private boolean hasContract;

    public TLotCrossAnnouncementId getId() {
        return id;
    }

    public void setId(TLotCrossAnnouncementId id) {
        this.id = id;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getOriginalHtml() {
        return originalHtml;
    }

    public void setOriginalHtml(String originalHtml) {
        this.originalHtml = originalHtml;
    }

    public DicLotStatus getStatus() {
        return status;
    }

    public void setStatus(DicLotStatus status) {
        this.status = status;
    }

    public boolean isHasContract() {
        return hasContract;
    }

    public void setHasContract(boolean hasContract) {
        this.hasContract = hasContract;
    }
}

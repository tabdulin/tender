package kz.inessoft.egz.dbentities;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 27.10.16.
 */
@Embeddable
public class TLotCrossAnnouncementId implements Serializable {
    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    @ManyToOne
    @JoinColumn(name = "t_lot_id", nullable = false)
    private TLot lot;

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }

    public TLot getLot() {
        return lot;
    }

    public void setLot(TLot lot) {
        this.lot = lot;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof TLotCrossAnnouncementId))
            return false;
        TLotCrossAnnouncementId o1 = (TLotCrossAnnouncementId) obj;

        if (announcement == null || o1.announcement == null ||
                announcement.getId() == null || o1.announcement.getId() == null ||
                !announcement.getId().equals(o1.announcement.getId()))
            return false;

        if (lot == null || o1.lot == null ||
                lot.getId() == null || o1.lot.getId() == null ||
                !lot.getId().equals(o1.lot.getId()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int retVal = 0;
        if (announcement != null && announcement.getId() != null)
            retVal += announcement.getId().hashCode();
        if (lot != null && lot.getId() != null)
            retVal += lot.getId().hashCode();
        return retVal;
    }
}

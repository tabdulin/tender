package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(TLotCrossAnnouncementId.class)
public class TLotCrossAnnouncementId_ {
    public static volatile SingularAttribute<TLotCrossAnnouncementId, TAnnouncement> announcement;
    public static volatile SingularAttribute<TLotCrossAnnouncementId, TLot> lot;
}

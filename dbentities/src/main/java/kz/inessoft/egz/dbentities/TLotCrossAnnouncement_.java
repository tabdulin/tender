package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(TLotCrossAnnouncement.class)
public class TLotCrossAnnouncement_ {
    public static volatile SingularAttribute<TLotCrossAnnouncement, TLotCrossAnnouncementId> id;
    public static volatile SingularAttribute<TLotCrossAnnouncement, String> lotNumber;
    public static volatile SingularAttribute<TLotCrossAnnouncement, String> originalHtml;
    public static volatile SingularAttribute<TLotCrossAnnouncement, DicLotStatus> status;
    public static volatile SingularAttribute<TLotCrossAnnouncement, Boolean> hasContract;
}

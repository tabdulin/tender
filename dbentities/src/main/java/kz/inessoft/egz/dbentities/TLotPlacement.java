package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 29.08.16.
 */
@Entity
@Table(name = "t_lot_placement")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_lot_placement", allocationSize = 10)
public class TLotPlacement extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "t_lot_id", nullable = false)
    private TLot lot;

    @Column(name = "kato")
    private String kato;

    @Column(name = "address")
    private String address;

    @Column(name = "volume")
    private double volume;

    public TLot getLot() {
        return lot;
    }

    public void setLot(TLot lot) {
        this.lot = lot;
    }

    public String getKato() {
        return kato;
    }

    public void setKato(String kato) {
        this.kato = kato;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }
}

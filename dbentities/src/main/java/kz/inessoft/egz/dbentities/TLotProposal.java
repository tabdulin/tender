package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "t_lot_cross_t_proposal")
public class TLotProposal implements Serializable {

    @EmbeddedId
    private TLotProposalId id;

    @Column(name = "documents_html", nullable = false)
    private String html;

    public TLotProposalId getId() {
        return id;
    }

    public void setId(TLotProposalId id) {
        this.id = id;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}

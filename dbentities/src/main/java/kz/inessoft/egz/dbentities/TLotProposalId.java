package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 27.10.16.
 */
@Embeddable
public class TLotProposalId implements Serializable {
    // todo Переопределить hashCode и equals

    // Тут не получается замапить как ManyToOne, т.к. получатеся один и тот же столбец t_announcement_id мапится в двух сущностях. Хибернейту такое не нравится
    // а делать еще один столбец - криво.
    @Column(name = "t_lot_id", nullable = false)
    private Long lotId;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "t_announcement_id", referencedColumnName = "t_announcement_id", nullable = false),
            @JoinColumn(name = "dic_implementer_id", referencedColumnName = "dic_implementer_id", nullable = false)
    })
    private TProposal proposal;

    public Long getLotId() {
        return lotId;
    }

    public void setLotId(Long lotId) {
        this.lotId = lotId;
    }

    public TProposal getProposal() {
        return proposal;
    }

    public void setProposal(TProposal proposal) {
        this.proposal = proposal;
    }
}

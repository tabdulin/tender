package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.10.16.
 */
@StaticMetamodel(TLot.class)
public class TLot_ extends ADBEntity_ {
    public static volatile SingularAttribute<TLot, Long> portalId;
    public static volatile SingularAttribute<TLot, Long> planUnitId;
    public static volatile SingularAttribute<TLot, DicGovOrganization> customer;
    public static volatile SingularAttribute<TLot, DicTru> dicTru;
    public static volatile SingularAttribute<TLot, String> shortCharacteristicRu;
    public static volatile SingularAttribute<TLot, String> additionalCharacteristicRu;
    public static volatile SingularAttribute<TLot, String> paymentSourceRu;
    public static volatile SingularAttribute<TLot, Double> priceForUnit;
    public static volatile SingularAttribute<TLot, Double> count;
    public static volatile SingularAttribute<TLot, DicUnit> unit;
    public static volatile SingularAttribute<TLot, Double> firstYearSum;
    public static volatile SingularAttribute<TLot, Double> secondYearSum;
    public static volatile SingularAttribute<TLot, Double> thirdYearSum;
    public static volatile SingularAttribute<TLot, Double> totalSum;
    public static volatile SingularAttribute<TLot, Double> prepayment;
    public static volatile SingularAttribute<TLot, String> deliveryTime;
    public static volatile SingularAttribute<TLot, String> incoterms;
    public static volatile SingularAttribute<TLot, Boolean> ingeneering;
    public static volatile SingularAttribute<TLot, String> demping;
    public static volatile SingularAttribute<TLot, String> customerFIO;
    public static volatile SingularAttribute<TLot, String> customerJobPosition;
    public static volatile SingularAttribute<TLot, String> customerPhone;
    public static volatile SingularAttribute<TLot, String> customerEmail;
    public static volatile SingularAttribute<TLot, String> customerBankRequisites;
}

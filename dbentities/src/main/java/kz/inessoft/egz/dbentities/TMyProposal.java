package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alexey on 02.02.17.
 */
@Entity
@Table(name = "t_my_proposal")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_my_proposals", allocationSize = 10)
public class TMyProposal extends ADBEntity implements Serializable {
    @ManyToOne
    @JoinColumn(name = "dic_participant_company_id", nullable = false)
    private DicParticipantCompany dicParticipantCompany;

    @ManyToOne
    @JoinColumn(name = "dic_proposal_status_id", nullable = false)
    private DicProposalStatus dicProposalStatus;

    @Column(name = "portal_id", nullable = false)
    private long portalId;

    @Column(name = "proposal_number", nullable = false)
    private long proposalNumber;

    @Column(name = "announcement_portal_id", nullable = false)
    private long announcementPortalId;

    @Column(name = "html", nullable = false)
    private String html;

    public DicParticipantCompany getDicParticipantCompany() {
        return dicParticipantCompany;
    }

    public void setDicParticipantCompany(DicParticipantCompany dicParticipantCompany) {
        this.dicParticipantCompany = dicParticipantCompany;
    }

    public DicProposalStatus getDicProposalStatus() {
        return dicProposalStatus;
    }

    public void setDicProposalStatus(DicProposalStatus dicProposalStatus) {
        this.dicProposalStatus = dicProposalStatus;
    }

    public long getPortalId() {
        return portalId;
    }

    public void setPortalId(long portalId) {
        this.portalId = portalId;
    }

    public long getProposalNumber() {
        return proposalNumber;
    }

    public void setProposalNumber(long proposalNumber) {
        this.proposalNumber = proposalNumber;
    }

    public long getAnnouncementPortalId() {
        return announcementPortalId;
    }

    public void setAnnouncementPortalId(long announcementPortalId) {
        this.announcementPortalId = announcementPortalId;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}

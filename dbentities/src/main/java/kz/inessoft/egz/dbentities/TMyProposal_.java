package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 02.02.17.
 */
@StaticMetamodel(TMyProposal.class)
public class TMyProposal_ extends ADBEntity_ {
    public static volatile SingularAttribute<TMyProposal, DicParticipantCompany> dicParticipantCompany;
    public static volatile SingularAttribute<TMyProposal, DicProposalStatus> dicProposalStatus;
    public static volatile SingularAttribute<TMyProposal, Long> portalId;
    public static volatile SingularAttribute<TMyProposal, Long> proposalNumber;
    public static volatile SingularAttribute<TMyProposal, Long> announcementPortalId;
    public static volatile SingularAttribute<TMyProposal, String> html;
}

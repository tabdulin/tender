package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 16.09.16.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "t_portal_file")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_portal_file", allocationSize = 10)
public abstract class TPortalFile extends ADBEntity {
    @Column(name = "file_name")
    private String fileName;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "file_size")
    private long fileSize;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public abstract long getPortalId();
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(TPortalFile.class)
public class TPortalFile_ extends ADBEntity_ {
    public static volatile SingularAttribute<TPortalFile, String> fileName;
    public static volatile SingularAttribute<TPortalFile, String> mimeType;
    public static volatile SingularAttribute<TPortalFile, Long> fileSize;
}

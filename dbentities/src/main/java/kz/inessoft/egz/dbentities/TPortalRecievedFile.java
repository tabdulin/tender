package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "t_portal_recieved_file")
public class TPortalRecievedFile extends TPortalFile {
    @Column(name = "portal_id", nullable = false)
    private long portalId;

    @Column(name = "url", nullable = false)
    private String url;

    @ManyToOne
    @JoinColumn(name = "dic_participant_company_id", nullable = false)
    private DicParticipantCompany participantCompany;

    @Override
    public long getPortalId() {
        return portalId;
    }

    public void setPortalId(long portalId) {
        this.portalId = portalId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public DicParticipantCompany getParticipantCompany() {
        return participantCompany;
    }

    public void setParticipantCompany(DicParticipantCompany participantCompany) {
        this.participantCompany = participantCompany;
    }
}

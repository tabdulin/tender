package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(TPortalRecievedFile.class)
public class TPortalRecievedFile_ extends TPortalFile_ {
    public static volatile SingularAttribute<TPortalRecievedFile, Long> portalId;
    public static volatile SingularAttribute<TPortalRecievedFile, String> url;
    public static volatile SingularAttribute<TPortalRecievedFile, DicParticipantCompany> participantCompany;
}

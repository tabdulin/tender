package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "t_portal_uploaded_file")
public class TPortalUploadedFile extends TPortalFile {
    @Column(name = "portal_id", nullable = false)
    private long portalId;

    @Override
    public long getPortalId() {
        return portalId;
    }

    public void setPortalId(long portalId) {
        this.portalId = portalId;
    }
}

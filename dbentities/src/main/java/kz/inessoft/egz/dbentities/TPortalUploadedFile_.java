package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 27.01.17.
 */
@StaticMetamodel(TPortalUploadedFile.class)
public class TPortalUploadedFile_ extends TPortalFile_ {
    public static volatile SingularAttribute<TPortalUploadedFile, Long> portalId;
}

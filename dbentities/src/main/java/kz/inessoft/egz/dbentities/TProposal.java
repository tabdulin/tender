package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "t_proposal")
public class TProposal implements Serializable {

    @EmbeddedId
    private TProposalId id;

    @Column(name = "portal_id", nullable = false)
    private Long portalId;

    @Column(name = "proposal_time", nullable = false)
    private Date proposalTime;

    @ManyToOne
    @JoinColumn(name = "dic_proposal_status_id", nullable = false)
    private DicProposalStatus proposalStatus;

    @Column(name = "general_tab_info_html", nullable = false)
    private String generalTabHtml;

    @Column(name = "lots_tab_info_html", nullable = false)
    private String lotsTabHtml;

    @Column(name = "docs_tab_info_html", nullable = false)
    private String docsTabHtml;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "bank_name", nullable = false)
    private String bankName;

    @Column(name = "iik", nullable = false)
    private String iik;

    @Column(name = "bik", nullable = false)
    private String bik;

    @Column(name = "kbe", nullable = false)
    private String kbe;

    @Column(name = "fio", nullable = false)
    private String fio;

    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "job_position", nullable = false)
    private String jobPosition;

    @Column(name = "consorcium", nullable = false)
    private boolean consorcium;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "t_announcement_id", referencedColumnName = "t_announcement_id"),
            @JoinColumn(name = "dic_implementer_id", referencedColumnName = "dic_implementer_id")
    })
    private List<TProposalDocument> documents;

    public TProposalId getId() {
        return id;
    }

    public void setId(TProposalId id) {
        this.id = id;
    }

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Date getProposalTime() {
        return proposalTime;
    }

    public void setProposalTime(Date proposalTime) {
        this.proposalTime = proposalTime;
    }

    public DicProposalStatus getProposalStatus() {
        return proposalStatus;
    }

    public void setProposalStatus(DicProposalStatus proposalStatus) {
        this.proposalStatus = proposalStatus;
    }

    public String getGeneralTabHtml() {
        return generalTabHtml;
    }

    public void setGeneralTabHtml(String generalTabHtml) {
        this.generalTabHtml = generalTabHtml;
    }

    public String getLotsTabHtml() {
        return lotsTabHtml;
    }

    public void setLotsTabHtml(String lotsTabHtml) {
        this.lotsTabHtml = lotsTabHtml;
    }

    public String getDocsTabHtml() {
        return docsTabHtml;
    }

    public void setDocsTabHtml(String docsTabHtml) {
        this.docsTabHtml = docsTabHtml;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIik() {
        return iik;
    }

    public void setIik(String iik) {
        this.iik = iik;
    }

    public String getBik() {
        return bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public String getKbe() {
        return kbe;
    }

    public void setKbe(String kbe) {
        this.kbe = kbe;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public boolean isConsorcium() {
        return consorcium;
    }

    public void setConsorcium(boolean consorcium) {
        this.consorcium = consorcium;
    }

    public List<TProposalDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<TProposalDocument> documents) {
        this.documents = documents;
    }
}

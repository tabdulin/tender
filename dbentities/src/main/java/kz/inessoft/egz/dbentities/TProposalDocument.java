package kz.inessoft.egz.dbentities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "t_proposal_document")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_proposal_document", allocationSize = 10)
public class TProposalDocument extends ADBEntity {
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "t_announcement_id", referencedColumnName = "t_announcement_id", nullable = false),
            @JoinColumn(name = "dic_implementer_id", referencedColumnName = "dic_implementer_id", nullable = false),
            @JoinColumn(name = "t_lot_id", referencedColumnName = "t_lot_id", nullable = false)
    })
    private TLotProposal lotProposal;

    @ManyToOne
    @JoinColumn(name = "t_document_id", nullable = false)
    private TDocument document;

    @ManyToOne
    @JoinColumn(name = "t_portal_file_id", nullable = false)
    private TPortalFile portalFile;

    public TLotProposal getLotProposal() {
        return lotProposal;
    }

    public void setLotProposal(TLotProposal lotProposal) {
        this.lotProposal = lotProposal;
    }

    public TDocument getDocument() {
        return document;
    }

    public void setDocument(TDocument document) {
        this.document = document;
    }

    public TPortalFile getPortalFile() {
        return portalFile;
    }

    public void setPortalFile(TPortalFile portalFile) {
        this.portalFile = portalFile;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by alexey on 27.10.16.
 */
@Embeddable
public class TProposalId implements Serializable {
    // todo Переопределить hashCode и equals
    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    @ManyToOne
    @JoinColumn(name = "dic_implementer_id", nullable = false)
    private DicImplementer implementer;

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }

    public DicImplementer getImplementer() {
        return implementer;
    }

    public void setImplementer(DicImplementer implementer) {
        this.implementer = implementer;
    }
}

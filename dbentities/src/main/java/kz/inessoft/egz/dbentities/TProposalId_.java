package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 17.02.17.
 */
@StaticMetamodel(TProposalId.class)
public class TProposalId_ {
    public static volatile SingularAttribute<TProposalId, TAnnouncement> announcement;
    public static volatile SingularAttribute<TProposalId, DicImplementer> implementer;
}

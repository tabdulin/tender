package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by alexey on 17.02.17.
 */
@StaticMetamodel(TProposal.class)
public class TProposal_ {
    public static volatile SingularAttribute<TProposal, TProposalId> id;
    public static volatile SingularAttribute<TProposal, Long> portalId;
    public static volatile SingularAttribute<TProposal, Date> proposalTime;
    public static volatile SingularAttribute<TProposal, DicProposalStatus> proposalStatus;
    public static volatile SingularAttribute<TProposal, String> generalTabHtml;
    public static volatile SingularAttribute<TProposal, String> lotsTabHtml;
    public static volatile SingularAttribute<TProposal, String> docsTabHtml;
    public static volatile SingularAttribute<TProposal, String> address;
    public static volatile SingularAttribute<TProposal, String> bankName;
    public static volatile SingularAttribute<TProposal, String> iik;
    public static volatile SingularAttribute<TProposal, String> bik;
    public static volatile SingularAttribute<TProposal, String> kbe;
    public static volatile SingularAttribute<TProposal, String> fio;
    public static volatile SingularAttribute<TProposal, String> phone;
    public static volatile SingularAttribute<TProposal, String> jobPosition;
    public static volatile SingularAttribute<TProposal, Boolean> consorcium;
    public static volatile ListAttribute<TProposal, TProposalDocument> documents;
}

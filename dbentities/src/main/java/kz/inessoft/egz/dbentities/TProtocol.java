package kz.inessoft.egz.dbentities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by alexey on 20.09.16.
 */
@Entity
@Table(name = "t_protocol")
@SequenceGenerator(name = "idGenerator", sequenceName = "seq$t_protocol", allocationSize = 10)
public class TProtocol extends ADBEntity {
    @ManyToOne
    @JoinColumn(name = "t_announcement_id", nullable = false)
    private TAnnouncement announcement;

    @Enumerated(EnumType.STRING)
    @Column(name = "protocol_type", nullable = false)
    private ETProtocolType type;

    @ManyToOne
    @JoinColumn(name = "t_portal_file_id", nullable = false)
    private TPortalFile portalFile;

    @Column(name = "parsed", nullable = false)
    private boolean parsed;

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(TAnnouncement announcement) {
        this.announcement = announcement;
    }

    public ETProtocolType getType() {
        return type;
    }

    public void setType(ETProtocolType type) {
        this.type = type;
    }

    public TPortalFile getPortalFile() {
        return portalFile;
    }

    public void setPortalFile(TPortalFile portalFile) {
        this.portalFile = portalFile;
    }

    public boolean isParsed() {
        return parsed;
    }

    public void setParsed(boolean parsed) {
        this.parsed = parsed;
    }
}

package kz.inessoft.egz.dbentities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by alexey on 17.02.17.
 */
@StaticMetamodel(TProtocol.class)
public class TProtocol_ extends ADBEntity_ {
    public static volatile SingularAttribute<TProtocol, TAnnouncement> announcement;
    public static volatile SingularAttribute<TProtocol, ETProtocolType> type;
    public static volatile SingularAttribute<TProtocol, TPortalFile> portalFile;
    public static volatile SingularAttribute<TProtocol, Boolean> parsed;
}

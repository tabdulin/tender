package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.ANamedEntity;
import kz.inessoft.egz.dbentities.ANamedEntity_;
import kz.inessoft.egz.ejbapi.ISimpleDictionaryDAO;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
public abstract class ASimpleDictionaryDAO<T extends ANamedEntity> implements ISimpleDictionaryDAO<T> {
    @PersistenceContext(unitName = "egzUnit")
    protected EntityManager em;

    @Override
    public T getValueByName(String name) {
        name = StringUtils.trim(name);
        if (StringUtils.isEmpty(name))
            return null;
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(getEntityClass());
        Root<T> root = query.from(getEntityClass());
        query.where(builder.equal(root.get(ANamedEntity_.nameRu), name));
        List<T> resultList = em.createQuery(query).getResultList();
        T retVal;
        if (resultList.isEmpty()) {
            try {
                retVal = getEntityClass().newInstance();
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
            retVal.setNameRu(name);
            em.persist(retVal);
        } else {
            retVal = resultList.get(0);
        }
        return retVal;
    }

    protected abstract Class<T> getEntityClass();
}

package kz.inessoft.egz.ejb;

import org.flywaydb.core.Flyway;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * Created by alexey on 29.07.16.
 */
@Singleton
@Startup
@TransactionManagement(TransactionManagementType.BEAN)
public class DBMigration {
    @PostConstruct
    private void executeMigration() {
        try {
            DataSource dataSource = InitialContext.doLookup("java:jboss/datasources/EGZDS");
            Flyway flyway = new Flyway();
            flyway.setDataSource(dataSource);
            flyway.setLocations("/META-INF/dbmigration");
            flyway.setTable("egz_schema_version");
            flyway.setBaselineOnMigrate(true);
            flyway.setSchemas("egz");
            flyway.migrate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

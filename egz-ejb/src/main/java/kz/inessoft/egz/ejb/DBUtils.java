package kz.inessoft.egz.ejb;

import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.jboss.jca.adapters.jdbc.WrappedConnection;
import org.postgresql.PGConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by alexey on 23.02.17.
 */
public class DBUtils {
    public static class SaveDataInfo {
        private int dataLength;
        private long oid;

        public int getDataLength() {
            return dataLength;
        }

        void setDataLength(int dataLength) {
            this.dataLength = dataLength;
        }

        public long getOid() {
            return oid;
        }

        void setOid(long oid) {
            this.oid = oid;
        }
    }

    public static SaveDataInfo createBlob(EntityManager em, InputStream dataStream) throws IOException, SQLException {
        SaveDataInfo retVal = new SaveDataInfo();
        Connection connection = getConnection(em);
        PGConnection pgConnection = (PGConnection) connection;
        LargeObjectManager largeObjectManager = pgConnection.getLargeObjectAPI();
        long lo = largeObjectManager.createLO();
        retVal.setOid(lo);
        LargeObject largeObject = largeObjectManager.open(lo);
        retVal.setDataLength(IOUtils.copy(dataStream, largeObject.getOutputStream()));
        return retVal;
    }

    public static Connection getConnection(EntityManager em) {
        final Connection[] connections = new Connection[1];
        em.unwrap(Session.class).doWork(connection -> {
            connections[0] = ((WrappedConnection) connection).getUnderlyingConnection();
        });
        return connections[0];
    }
}

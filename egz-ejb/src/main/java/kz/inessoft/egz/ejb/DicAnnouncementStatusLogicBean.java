package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus;
import kz.inessoft.egz.ejbapi.DicAnnouncementStatusLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicAnnouncementStatusLogicBean extends ASimpleDictionaryDAO<DicAnnouncementStatus> implements DicAnnouncementStatusLogic {
    @Override
    protected Class<DicAnnouncementStatus> getEntityClass() {
        return DicAnnouncementStatus.class;
    }

    @Override
    public DicAnnouncementStatus getPublishedStatus() {
        return getValueByName("Опубликовано");
    }

    @Override
    public DicAnnouncementStatus getPublishedRecievingStatus() {
        return getValueByName("Опубликовано (прием заявок)");
    }

    @Override
    public DicAnnouncementStatus getDocumentationChangedStatus() {
        return getValueByName("Изменена документация");
    }

    @Override
    public List<DicAnnouncementStatus> getAnnouncementStatuses() {
        return em.createQuery("from DicAnnouncementStatus order by nameRu").getResultList();
    }
}

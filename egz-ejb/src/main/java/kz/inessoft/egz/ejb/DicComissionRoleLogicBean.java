package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicComissionRole;
import kz.inessoft.egz.ejbapi.DicComissionRoleLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 11.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicComissionRoleLogicBean extends ASimpleDictionaryDAO<DicComissionRole> implements DicComissionRoleLogic {
    @Override
    protected Class<DicComissionRole> getEntityClass() {
        return DicComissionRole.class;
    }
}

package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicConditionalDiscount;
import kz.inessoft.egz.ejbapi.DicConditionalDiscountLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 11.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicConditionalDiscountLogicBean extends  ASimpleDictionaryDAO<DicConditionalDiscount> implements DicConditionalDiscountLogic {
    @Override
    protected Class<DicConditionalDiscount> getEntityClass() {
        return DicConditionalDiscount.class;
    }
}

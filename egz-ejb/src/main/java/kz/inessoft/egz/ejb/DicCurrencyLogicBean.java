package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicCurrency;
import kz.inessoft.egz.ejbapi.DicCurrencyLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * Created by alexey on 14.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicCurrencyLogicBean implements DicCurrencyLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public DicCurrency getValueByCode(String code) {
        DicCurrency retVal;
        try {
            retVal = (DicCurrency) em.createQuery("from DicCurrency where code = :code")
                    .setParameter("code", code)
                    .getSingleResult();
        } catch (NoResultException e) {
            retVal = new DicCurrency();
            retVal.setCode(code);
            em.persist(retVal);
        }
        return retVal;
    }
}

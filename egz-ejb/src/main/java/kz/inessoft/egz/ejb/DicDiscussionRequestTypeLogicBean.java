package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicDiscussionRequestType;
import kz.inessoft.egz.ejbapi.DicDiscussionRequestTypeLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 15.02.17.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicDiscussionRequestTypeLogicBean extends  ASimpleDictionaryDAO<DicDiscussionRequestType> implements DicDiscussionRequestTypeLogic {
    @Override
    protected Class<DicDiscussionRequestType> getEntityClass() {
        return DicDiscussionRequestType.class;
    }
}

package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicDiscussionResponseType;
import kz.inessoft.egz.ejbapi.DicDiscussionResponseTypeLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 15.02.17.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicDiscussionResponseTypeLogicBean extends  ASimpleDictionaryDAO<DicDiscussionResponseType> implements DicDiscussionResponseTypeLogic {
    @Override
    protected Class<DicDiscussionResponseType> getEntityClass() {
        return DicDiscussionResponseType.class;
    }
}

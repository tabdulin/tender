package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicGovOrganization;
import kz.inessoft.egz.ejbapi.DicGovOrganizationLogic;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicGovOrganizationLogicBean implements DicGovOrganizationLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public DicGovOrganization getById(Long id) {
        return em.find(DicGovOrganization.class, id);
    }

    @Override
    public DicGovOrganization getValueByBin(String bin, String name, String address) {
        name = StringUtils.trim(name);
        if (StringUtils.isEmpty(name))
            return null;
        List resultList = em.createQuery("from DicGovOrganization where bin=:bin")
                .setParameter("bin", bin)
                .getResultList();
        DicGovOrganization retVal;
        if (resultList.isEmpty()) {
            retVal = new DicGovOrganization();
            retVal.setBin(bin);
            retVal.setNameRu(name);
            retVal.setAddress(address);
            em.persist(retVal);
        } else {
            retVal = (DicGovOrganization) resultList.get(0);
        }
        return retVal;
    }

    @Override
    public List<DicGovOrganization> getFirst10GOByFilter(String filter) {
        if (filter.matches("^\\d*$")) {
            return em.createQuery("from DicGovOrganization  where bin like :bin")
                    .setParameter("bin", filter + "%")
                    .setMaxResults(10)
                    .getResultList();
        }

        if (filter.isEmpty()) {
            return Collections.emptyList();
        }

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(DicGovOrganization.class).get();
        while (filter.contains("  "))
            filter = filter.replaceAll("  ", " ");
        String[] strings = filter.split(" ");
        BooleanJunction q1 = queryBuilder.bool();
        for (String string : strings) {
            q1 = q1.must(queryBuilder.keyword().fuzzy().onField("nameRu").matching(string).createQuery());
        }
        return fullTextEntityManager.createFullTextQuery(q1.createQuery(), DicGovOrganization.class).setMaxResults(10).getResultList();    }
}

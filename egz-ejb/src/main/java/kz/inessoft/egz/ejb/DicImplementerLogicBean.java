package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicImplementer;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.search.exception.EmptyQueryException;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;

/**
 * Created by alexey on 11.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicImplementerLogicBean implements DicImplementerLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public DicImplementer getValueByXin(String xin, String name) {
        name = StringUtils.trim(name);
        if (StringUtils.isEmpty(name))
            return null;
        List resultList = em.createQuery("from DicImplementer where xin=:xin")
                .setParameter("xin", xin)
                .getResultList();
        DicImplementer retVal;
        if (resultList.isEmpty()) {
            retVal = new DicImplementer();
            retVal.setXin(xin);
            retVal.setNameRu(name);
            em.persist(retVal);
        } else {
            retVal = (DicImplementer) resultList.get(0);
        }
        return retVal;
    }

    @Override
    public DicImplementer getValueById(long id) {
        return em.find(DicImplementer.class, id);
    }

    @Override
    public List<DicImplementer> getFirst10ImplementersByFilter(String filter) {
        if (filter.matches("^\\d*$")) {
            return em.createQuery("from DicImplementer  where xin like :xin")
                    .setParameter("xin", filter + "%")
                    .setMaxResults(10)
                    .getResultList();
        }

        if (filter.isEmpty()) {
            return Collections.emptyList();
        }

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(DicImplementer.class).get();
        while (filter.contains("  "))
            filter = filter.replaceAll("  ", " ");
        String[] strings = filter.split(" ");
        BooleanJunction q1 = queryBuilder.bool();
        for (String string : strings) {
            try {
                q1 = q1.must(queryBuilder.keyword().fuzzy().onField("nameRu").matching(string).createQuery());
            } catch (EmptyQueryException ignored) {}
        }
        return fullTextEntityManager.createFullTextQuery(q1.createQuery(), DicImplementer.class).setMaxResults(10).getResultList();
    }
}

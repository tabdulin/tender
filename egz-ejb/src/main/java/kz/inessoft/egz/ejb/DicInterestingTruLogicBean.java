package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicInterestingTru;
import kz.inessoft.egz.ejbapi.DicInterestingTruLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 02.02.17.
 */
@Stateless(name = "DicInterestingTruLogic")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicInterestingTruLogicBean implements DicInterestingTruLogic {
    @PersistenceContext(unitName = "egzUnit")
    protected EntityManager em;

    @Override
    public List<DicInterestingTru> getInterestingTRUs() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<DicInterestingTru> query = criteriaBuilder.createQuery(DicInterestingTru.class);
        query.from(DicInterestingTru.class);
        List<DicInterestingTru> resultList = em.createQuery(query).getResultList();
        return resultList;
    }

    @Override
    public void setLastUpdatedDate(Long dicInterestingTruId, Date date) {
        em.find(DicInterestingTru.class, dicInterestingTruId).setUpdateDate(date);
    }
}

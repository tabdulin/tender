package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicLotStatus;
import kz.inessoft.egz.ejbapi.DicLotStatusLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicLotStatusLogicBean extends ASimpleDictionaryDAO<DicLotStatus> implements DicLotStatusLogic {
    @Override
    protected Class<DicLotStatus> getEntityClass() {
        return DicLotStatus.class;
    }

    @Override
    public List<DicLotStatus> getLotStatuses() {
        return em.createQuery("from DicLotStatus order by nameRu").getResultList();
    }
}

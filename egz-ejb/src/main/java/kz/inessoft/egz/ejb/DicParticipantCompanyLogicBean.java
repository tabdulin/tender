package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.DicParticipantCompany_;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 07.09.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicParticipantCompanyLogicBean implements DicParticipantCompanyLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    public List<DicParticipantCompany> getDicParticipantCompanies() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<DicParticipantCompany> query = builder.createQuery(DicParticipantCompany.class);
        query.from(DicParticipantCompany.class);
        List<DicParticipantCompany> resultList = em.createQuery(query).getResultList();
        resultList.size();
        return resultList;
    }

    @Override
    public DicParticipantCompany getDefaultCompanyForRefreshAnnouncement() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<DicParticipantCompany> query = builder.createQuery(DicParticipantCompany.class);
        Root<DicParticipantCompany> root = query.from(DicParticipantCompany.class);
        query.where(builder.equal(root.get(DicParticipantCompany_.defaultRefreshCredential), Boolean.TRUE));
        return em.createQuery(query).getSingleResult();
    }

    @Override
    public void setLastNoticeTime(Date noticeTime, long id) {
        em.find(DicParticipantCompany.class, id).setLastNoticeTime(noticeTime);
    }
}

package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicPortalResourceMask;
import kz.inessoft.egz.ejbapi.DicPortalResourcesLogic;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 29.09.16.
 */
@Singleton
public class DicPortalResourcesLogicBean implements DicPortalResourcesLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    private Map<String, String> resources;

    @PostConstruct
    public void init() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<DicPortalResourceMask> query = builder.createQuery(DicPortalResourceMask.class);
        query.from(DicPortalResourceMask.class);
        List<DicPortalResourceMask> list = em.createQuery(query).getResultList();
        resources = new HashMap<>();
        for (DicPortalResourceMask dicPortalResourceMask : list) {
            resources.put(dicPortalResourceMask.getHtmlSourceText(), dicPortalResourceMask.getPortalResource().getHtmlReplacement());
        }
    }

    @Override
    public Map<String, String> getResourcesMap() {
        return resources;
    }

    @Override
    public String replaceResources(String source) {
        String retVal = source;
        for (String next : getResourcesMap().keySet())
            retVal = retVal.replace(next, getResourcesMap().get(next));
        return retVal;
    }
}

package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicProposalStatus;
import kz.inessoft.egz.ejbapi.DicProposalStatusLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 20.09.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicProposalStatusLogicBean extends ASimpleDictionaryDAO<DicProposalStatus> implements DicProposalStatusLogic {
    @Override
    protected Class<DicProposalStatus> getEntityClass() {
        return DicProposalStatus.class;
    }
}

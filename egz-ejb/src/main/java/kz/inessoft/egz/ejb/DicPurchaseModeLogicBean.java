package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicPurchaseMode;
import kz.inessoft.egz.ejbapi.DicPurchaseModeLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicPurchaseModeLogicBean extends ASimpleDictionaryDAO<DicPurchaseMode> implements DicPurchaseModeLogic {
    @Override
    protected Class<DicPurchaseMode> getEntityClass() {
        return DicPurchaseMode.class;
    }
}

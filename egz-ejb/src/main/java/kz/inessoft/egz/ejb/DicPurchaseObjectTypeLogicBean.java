package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicPurchaseObjectType;
import kz.inessoft.egz.ejbapi.DicPurchaseObjectTypeLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicPurchaseObjectTypeLogicBean extends ASimpleDictionaryDAO<DicPurchaseObjectType> implements DicPurchaseObjectTypeLogic {
    @Override
    protected Class<DicPurchaseObjectType> getEntityClass() {
        return DicPurchaseObjectType.class;
    }
}

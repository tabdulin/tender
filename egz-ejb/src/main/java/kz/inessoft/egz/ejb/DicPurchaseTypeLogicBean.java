package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicPurchaseType;
import kz.inessoft.egz.ejbapi.DicPurchaseTypeLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicPurchaseTypeLogicBean extends ASimpleDictionaryDAO<DicPurchaseType> implements DicPurchaseTypeLogic {
    @Override
    protected Class<DicPurchaseType> getEntityClass() {
        return DicPurchaseType.class;
    }
}

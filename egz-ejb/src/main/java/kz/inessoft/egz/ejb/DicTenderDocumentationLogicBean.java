package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicTenderDocumentation;
import kz.inessoft.egz.ejbapi.DicTenderDocumentationLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 10.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicTenderDocumentationLogicBean extends ASimpleDictionaryDAO<DicTenderDocumentation> implements DicTenderDocumentationLogic {
    @Override
    protected Class<DicTenderDocumentation> getEntityClass() {
        return DicTenderDocumentation.class;
    }
}

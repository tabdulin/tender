package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicInterestingTru;
import kz.inessoft.egz.dbentities.DicTru;
import kz.inessoft.egz.dbentities.DicTru_;
import kz.inessoft.egz.ejbapi.DicInterestingTruLogic;
import kz.inessoft.egz.ejbapi.DicTruLogic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicTruLogicBean implements DicTruLogic {
    @PersistenceContext(unitName = "egzUnit")
    protected EntityManager em;

    @EJB
    private DicInterestingTruLogic dicInterestingTruLogic;

    @Override
    public DicTru getTruByCode(String code, String name) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<DicTru> query = criteriaBuilder.createQuery(DicTru.class);
        Root<DicTru> root = query.from(DicTru.class);
        query.where(criteriaBuilder.equal(root.get(DicTru_.code), code));
        List resultList = em.createQuery(query).getResultList();
        DicTru retVal;
        if (resultList.isEmpty()) {
            retVal = new DicTru();
            retVal.setCode(code);
            retVal.setNameRu(name);
            em.persist(retVal);
        } else
            retVal = (DicTru) resultList.get(0);
        return retVal;
    }

    @Override
    public List<Long> getInterestingTruIds() {
        List<DicInterestingTru> resultList = dicInterestingTruLogic.getInterestingTRUs();

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> truCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<DicTru> root = truCriteriaQuery.from(DicTru.class);
        truCriteriaQuery.select(root.get(DicTru_.id));
        List<Predicate> predicates = new ArrayList<>();
        for (DicInterestingTru dicInterestingTru : resultList) {
            predicates.add(criteriaBuilder.like(root.get(DicTru_.code), dicInterestingTru.getCode() + "%"));
        }
        truCriteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()])));
        return em.createQuery(truCriteriaQuery).getResultList();
    }
}

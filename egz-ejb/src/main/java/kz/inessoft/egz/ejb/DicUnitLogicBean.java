package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicUnit;
import kz.inessoft.egz.ejbapi.DicUnitLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicUnitLogicBean extends ASimpleDictionaryDAO<DicUnit> implements DicUnitLogic {
    @Override
    protected Class<DicUnit> getEntityClass() {
        return DicUnit.class;
    }
}

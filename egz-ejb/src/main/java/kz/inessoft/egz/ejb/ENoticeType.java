package kz.inessoft.egz.ejb;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alexey on 22.09.16.
 */
public enum ENoticeType {
    REJECTED_RESEND("Отклонены по заявке № \\d+ с правом переподачи",
            ".*"),

    PUBLISHED_PPD("Опубликован ППД по объявлению \\d+-\\d+",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    OLD_PUBLISHED_PPD("Алдын ала рұқсат беру кезеңінің хаттамасын қалыптастыру және жариялау",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    PUBLISHED_PI("Опубликован ПИ по объявлению \\d+-\\d+",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    PUBLISHED_DISCUSSION_PROTOCOL("Опубликован протокол обсуждения по объявлению № \\d+-\\d+",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    PUBLISHED_OPENING_PROTOCOL("Опубликован протокол вскрытия по объявлению № \\d+-\\d+",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    PUBLISHED_ANNOUNCEMENT("Опубликовано объявление № .* для приема заявок",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    OLD_PUBLISHED_OPENING_PROTOCOL("Ашу хаттамасын жариялау",
            "Уважаемый \\(ая\\) .*! Опубликован протокол вскрытия на закупку способом конкурс № \\d+-\\d+, .*"),

    SEND_PROPOSAL("Подача заявки № \\d+ на участие в закупках",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    CUSTOMER_CHANGES_IN_CONTRACT("Заказчик внес изменения в договор", ".*"),

    IMPLEMENTER_CHANGES_IN_CONTRACT("Поставщик внес изменения в договор",
            "Поставщик внес изменения в договор №.*"),

    CUSTOMER_SIGNED_CONTRACT("Заказчик подписал договор",
            "Уважаемый .*! Заказчик .* по закупке №\\d+-\\d+ подписал договор .*\\. В связи с чем, Вам необходимо подписать данный договор сотрудником с правом подписи Вашей организации до \\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"),

    CUSTOMER_AGREED_CONTRACT("Заказчик утвердил договор",
            "Уважаемый .*! Договор № .* отправлен Заказчиком .* на согласование\\. В связи с чем, Вам необходимо согласовать данный договор до \\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"),

    CUSTOMER_REVOKED_AGREEMENT("Заказчик отозвал согласование",
            "Уважаемый .*! Заказчик внес изменения в Договор № .* отправлен на согласование\\. В связи с чем, Вам необходимо повторно согласовать договор после отправки на согласование"),

    CONTRACT_STOPPED("Произведена процедура приостановления заключения договора",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    CONTRACT_DELETED("Договор удален", "Договор .* удален"),

    OLD_CONTRACT_STOPPED("Приостановление заключения договора по объявлению № \\d+-\\d+",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    REVOKE_PROPOSAL("Отзыв заявка № \\d+ на участие в закупках",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    REVOKE_BUYING("Отмена закупок по объявлению № \\d+-\\d+",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    DECLINE_BUYING("Отказ от закупок по объявлению № .*",
            "Данным уведомлением доводим до Вашего сведения, .*"),

    CONTRACT_ADDITIONAL_AGREEMENT("Создано дополнительное соглашение",
            "К договору .* создано дополнительное соглашение с номером .*"),

    INVITATION("Приглашение к участию в закупках по объявлению .*", "Уважаемый участник .*"),

    TREASURE_CREATED("Создана заявка на регистрацию в КК", "Заявка на регистрацию договора"),

    OTHER(".*", ".*");

    public static class NoticeInfo {
        private String sourceNoticeText;
        ENoticeType noticeType;

        NoticeInfo(String sourceNoticeText, ENoticeType noticeType) {
            while (sourceNoticeText.contains("  "))
                sourceNoticeText = sourceNoticeText.replaceAll("  ", " ");
            sourceNoticeText = sourceNoticeText.replaceAll("Если указанная выше ссылка не открывается, скопируйте ее в буфер обмена, вставьте в адресную строку браузера и нажмите .Ввод.", "").trim();
            while (sourceNoticeText.endsWith("."))
                sourceNoticeText = sourceNoticeText.substring(0, sourceNoticeText.length() - 1).trim();
            this.sourceNoticeText = sourceNoticeText;
            this.noticeType = noticeType;
        }


        public String getNoticeText() {
            Pattern p = Pattern.compile(noticeType.noticeTextRegExp);
            Matcher matcher = p.matcher(sourceNoticeText);
            matcher.find();
            String retVal = matcher.group();
            return retVal;
        }

        public Long getAnnouncementPortalId() {
            // Обновим объявление, если на него есть ссылка в тексте
            Pattern pattern = Pattern.compile("/ru/announce/index/\\d+");
            Matcher matcher = pattern.matcher(sourceNoticeText);
            Pattern p2 = Pattern.compile("\\d+");
            Set<Long> processedIds = new HashSet<>();
            while (matcher.find()) {
                String group = matcher.group();
                Matcher matcher1 = p2.matcher(group);
                matcher1.find();
                Long id = Long.valueOf(matcher1.group());
                if (!processedIds.contains(id)) {
                    processedIds.add(id);
                }
            }
            if (processedIds.isEmpty())
                return null;
            if (processedIds.size() == 1)
                return processedIds.iterator().next();
            else
                throw new RuntimeException("There is several announcements in notice text \n" + sourceNoticeText);
        }
    }

    private String subjectRegExp;
    private String noticeTextRegExp;

    ENoticeType(String subjectRegExp, String noticeTextRegExp) {
        this.subjectRegExp = subjectRegExp;
        this.noticeTextRegExp = noticeTextRegExp;
    }

    public static ENoticeType getNoticeType(String subject) {
        while (subject.contains("  "))
            subject = subject.replaceAll("  ", " ");
        for (ENoticeType noticeType : ENoticeType.values()) {
            Pattern p = Pattern.compile(noticeType.subjectRegExp);
            Matcher matcher = p.matcher(subject);
            if (matcher.find())
                return noticeType;
        }
        return null;
    }

    public NoticeInfo getNoticeInfo(String sourceText) {
        return new NoticeInfo(sourceText, this);
    }
}

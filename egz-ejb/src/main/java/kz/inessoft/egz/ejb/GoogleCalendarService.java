package kz.inessoft.egz.ejb;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.ejb.announcement.AnnouncementEvent;
import kz.inessoft.egz.ejbapi.ESysParam;
import kz.inessoft.egz.ejbapi.SysConfig;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.security.GeneralSecurityException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Talgat.Abdulin
 * @see <a href="https://developers.google.com/google-apps/calendar/quickstart/java">quickstart</a>
 * <p>
 */
public class GoogleCalendarService {
    private static final Logger logger = LoggerFactory.getLogger(GoogleCalendarService.class);
    private static final String APPLICATION_NAME = "TenderCalendar";
    private Calendar calendar;
    private String calendarId;

    public GoogleCalendarService(SysConfig sysConfig) throws GeneralSecurityException, IOException {
        calendarId = sysConfig.getSysParamValue(ESysParam.GOOGLE_CALENDAR_ID);
        HttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        FileDataStoreFactory DATA_STORE_FACTORY = new FileDataStoreFactory(new File(sysConfig.getSysParamValue(ESysParam.GOOGLE_STORE_DIRECTORY)));
        JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
        // Load client secrets.
        String json = sysConfig.getSysParamValue(ESysParam.GOOGLE_CREDENTIALS);
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new StringReader(json));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, CalendarScopes.all())
                .setDataStoreFactory(DATA_STORE_FACTORY)
                .build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
                .authorize(sysConfig.getSysParamValue(ESysParam.GOOGLE_USER_ID));
        this.calendar = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }


    public void deleteEvent(String id) throws IOException {
        Events execute = null;
        do {
            Calendar.Events.List list = calendar.events().list(calendarId)
                    .setSingleEvents(true)
                    .setPrivateExtendedProperty(Arrays.asList("tender.event.id=" + id));
            if (execute != null) list.setPageToken(execute.getNextPageToken());
            execute = list.execute();
            for (Event event : execute.getItems()) {
                logger.debug("Deleting event: {}", event.toPrettyString());
                calendar.events().delete(calendarId, event.getId()).execute();
            }
        } while (execute.getNextPageToken() != null && !execute.getNextPageToken().isEmpty());
    }

    public void addEvent(Date end, String id, String title, String description, String color) throws IOException {
        deleteEvent(id);

        Event event = new Event();
        Event.ExtendedProperties properties = new Event.ExtendedProperties();
        Map<String, String> map = new HashMap<String, String>();
        map.put("tender.event.id", id);
        properties.setPrivate(map);
        event.setExtendedProperties(properties);
        event.setSummary(title);
        event.setColorId(color);
        event.setDescription(description);

        EventDateTime endEvent = new EventDateTime().setDateTime(new DateTime(end));
        event.setStart(endEvent);
        event.setEnd(endEvent);
        calendar.events().insert(calendarId, event).execute();
    }

    public void saveEventsToGoogle(TAnnouncement announcement) throws GeneralSecurityException, IOException {
        for (AnnouncementEvent event : AnnouncementEvent.values()) {
            Date eventDate = null;
            switch (event) {
                case StartDiscussion:
                    eventDate = announcement.getStartDiscussionTime();
                    break;
                case EndDiscussion:
                    eventDate = announcement.getEndDiscussionTime();
                    break;
                case StartProposal:
                    eventDate = announcement.getStartRecvTime();
                    break;
                case EndProposal:
                    eventDate = announcement.getEndRecvTime();
                    break;
                case StartProposalAddition:
                    eventDate = announcement.getStartRecvAppendixTime();
                    break;
                case EndProposalAddition:
                    eventDate = announcement.getEndRecvAppendixTime();
                    break;
                default:
                    logger.warn("Event is not supported yet: {}", event.name());
            }

            if (eventDate != null) {
                addEvent(eventDate, event.getId(announcement), event.getName(announcement),
                        toString(announcement), event.getColor());
            }
        }
    }

    private String toString(TAnnouncement announcement) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sb.append("Ссылка на конкурс: ").append(PortalUrlUtils.createAnnouncementUrl(announcement.getPortalId())).append("\n");
        sb.append("Номер объявления: ").append(announcement.getNumber()).append("\n");
        sb.append("Дата публикации объявления: ").append(sf.format(announcement.getPublicationDate())).append("\n");
        sb.append("Наименование объявления: ").append(announcement.getNameRu()).append("\n");

        double price = 0;
        for (TLotCrossAnnouncement crossAnnouncement : announcement.getLotCrossAnnouncements()) {
            if (crossAnnouncement.getId().getLot().getPriceForUnit() != null)
                price += crossAnnouncement.getId().getLot().getPriceForUnit();
        }
        sb.append("Стоимость закупки: ").append(NumberFormat.getIntegerInstance().format(price)).append(" тенге\n");

        if (announcement.getStartDiscussionTime() != null) {
            sb.append("Срок начала обсуждения: ").append(sf.format(announcement.getStartDiscussionTime())).append("\n");
        }

        if (announcement.getEndDiscussionTime() != null) {
            sb.append("Срок завершения обсуждения: ").append(sf.format(announcement.getEndDiscussionTime())).append("\n");
        }

        if (announcement.getStartRecvTime() != null) {
            sb.append("Срок начала приема заявок: ").append(sf.format(announcement.getStartRecvTime())).append("\n");
        }

        if (announcement.getEndRecvTime() != null) {
            sb.append("Срок завершения приема заявок: ").append(sf.format(announcement.getEndRecvTime())).append("\n");
        }

        if (announcement.getStartRecvAppendixTime() != null) {
            sb.append("Срок начала приема дополнения заявок: ").append(sf.format(announcement.getStartRecvAppendixTime())).append("\n");
        }

        if (announcement.getEndRecvAppendixTime() != null) {
            sb.append("Срок окончания приема дополнения заявок: ").append(sf.format(announcement.getEndRecvAppendixTime())).append("\n");
        }

        return sb.toString();
    }
}

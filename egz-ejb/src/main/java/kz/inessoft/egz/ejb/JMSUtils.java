package kz.inessoft.egz.ejb;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 * Created by alexey on 24.10.16.
 */
public final class JMSUtils {
    public static final String MESSAGE_TYPE_PROPERTY_NAME = "MessageType";
    public static final String CONTRACT_DOWNLOAD = "ContractDownload";
    public static final String ANNOUNCEMENT_DOWNLOAD = "AnnouncementDownload";
    public static final String FILE_DOWNLOAD = "FileDownload";
    public static final String JIRA_MESSAGE = "jiraMessage";
    public static final String MAKE_PROPOSAL_MESSAGE = "MakeProposal";
    public static final String FILE_ID = "FileId";
    public static final String ANNOUNCEMENT_ID_PARAM_NAME = "announcementPortalId";
    public static final String CONTRACT_ID_PARAM_NAME = "portalId";
    public static final String PROPOSAL_ID = "proposalId";
    public static final String STEP_PROPERTY = "Step";

    private JMSUtils() {}

    public static void delayDeliveryForNight(Message message) throws JMSException {
        // Проверим текущее время. Если сейчас позже 10-и вечера, но раньше чем 8 утра - поставим на загрузку с немедленным исполнением. В противном случае отсрочим загрузку до 10-и вечера.
//        Calendar cal = Calendar.getInstance();
//        if (cal.get(Calendar.HOUR_OF_DAY) >= 8 && cal.get(Calendar.HOUR_OF_DAY) < 22) {
            // Посчитаем время после 10-и часов вечера
//            cal.set(Calendar.HOUR_OF_DAY, 22);
//            message.setLongProperty("_AMQ_SCHED_DELIVERY", cal.getTimeInMillis());
//        }
    }
}

package kz.inessoft.egz.ejb;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Created by alexey on 03.08.16.
 */
public class JNDILookup {
    public static final String JNDINAME_PREFIX = "java:global/egz-ear/egz-ejb/";
    public static final String SYS_CONFIG_NAME = "SysConfig";
    public static final String ANNOUNCEMENT_LOGIC_NAME = "AnnouncementLogic";
    public static final String ANNOUNCEMENT_QUEUE_INFO = "AnnouncementQueueInfo";
    public static final String DIC_INTERESTING_TRU_LOGIC_NAME = "DicInterestingTruLogic";
    public static final String TELEGRAM_SERVICE = "TelegramService";
    public static final String CONTRACTS_LOGIC = "ContractLogicBean";
    public static final String PROXY_LOGIC = "ProxyLogic";

    public static Object lookupLocal(String ejbName) throws NamingException {
        return InitialContext.doLookup(JNDINAME_PREFIX + ejbName);
    }
}

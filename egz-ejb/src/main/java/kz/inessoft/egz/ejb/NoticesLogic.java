package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import org.telegram.telegrambots.TelegramApiException;

import javax.ejb.Local;

/**
 * Created by alexey on 07.02.17.
 */
@Local
public interface NoticesLogic {
    void processLastNotices(DicParticipantCompany portalUser) throws TelegramApiException;
}

package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.ejb.siteloader.http.HttpNoticesIterator;
import kz.inessoft.egz.ejb.siteloader.http.SiteNotice;
import kz.inessoft.egz.ejb.telegram.TelegramServiceLogic;
import kz.inessoft.egz.ejbapi.AnnouncementQueueInfo;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.TelegramApiException;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by alexey on 13.09.16.
 */
@Singleton
@Startup
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class NoticesLogicBean implements NoticesLogic {
    private static final Logger LOGGER = LoggerFactory.getLogger(NoticesLogicBean.class);

    @EJB
    private DicParticipantCompanyLogic dicParticipantCompanyLogic;

    @EJB
    private AnnouncementQueueInfo announcementQueueInfo;

    @EJB
    NoticesLogic noticesLogic;

    /**
     * Шедулер, производящий загрузку свежих уведомлений с портала. Раньше запускался автоматически каждые 2 минуты и запрашивал
     * как обычные уведомления, так и удаленные (на случай если кто-то удалил объявление, а сервер его еще не видел, то он найдет его в корзине).
     * Возможно нас по таким регулярным запросам в каждые 2 минуты вычислили. Что бы как-то рандомоизировать это дело теперь уведомления запрашиваются раз в 7 минут
     * с некоторой рандомной дополнительной задержкой. И запрашиваются не сразу обычные и удаленные, а через раз. Т.е. сначала обычные, через 7 мин. удаленные
     * еще через 7 мин. опять обычные. Так наверно будет сложнее вычислить в логах в куче других запросов.
     * Так же уведомления не обновляются с 20:00 до 9:00
     */
    @Schedule(dayOfWeek = "1,2,3,4,5", hour = "9,10,11,12,13,14,15,16,17,18,19,20", minute = "*/15", persistent = false)
    public void processLastNotices() {
        List<DicParticipantCompany> dicParticipantCompanies = dicParticipantCompanyLogic.getDicParticipantCompanies();
        try {
            for (DicParticipantCompany dicParticipantCompany : dicParticipantCompanies) {
                noticesLogic.processLastNotices(dicParticipantCompany);
            }
        } catch (TelegramApiException e) {
            LOGGER.error("Error while send message to telegram", e);
        }
    }

    /**
     * Загрузка уведомлений по отдельному пользователю. Запускается в отдельной транзакции, на случай если возникнет ошибка у одного
     * пользователя, то что бы не откатилась транзакция по уже отработанным.
     *
     * @param portalUser
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void processLastNotices(DicParticipantCompany portalUser) throws TelegramApiException {
        SiteNotice next = null;
        try {
            LOGGER.info("Start collection fresh notices from portal.");
            SimpleDateFormat sf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            Date notBeforeDate = portalUser.getLastNoticeTime();
            TelegramServiceLogic telegramServiceLogic = (TelegramServiceLogic) JNDILookup.lookupLocal(JNDILookup.TELEGRAM_SERVICE);
            Iterator<SiteNotice> notices = new HttpNoticesIterator(notBeforeDate, portalUser);
            Set<Long> processedPortalIds = new HashSet<>();
            Map<String, Set<String>> noticesCache = new HashMap<>();
            while (notices.hasNext()) {
                next = notices.next();
                Set<String> strings = noticesCache.get(next.getSubject());
                if (strings != null && strings.contains(next.getText()))
                    continue;
                ENoticeType noticeType = ENoticeType.getNoticeType(next.getSubject());

                if (noticeType == null) {
                    LOGGER.error("Unable to parse notification.\nSubject: {}\nBody: {}", next.getSubject(), next.getText());
                    continue;
                }

                ENoticeType.NoticeInfo noticeInfo = noticeType.getNoticeInfo(next.getText());
                Long portalId = noticeInfo.getAnnouncementPortalId();

                if (portalId != null) {
                    telegramServiceLogic.sendMessage("<b>" + sf.format(next.getNoticeDate()) + " " +
                            next.getSubject() + "</b>\n\n" + noticeInfo.getNoticeText() +
                            "\n\nhttps://tenders.inessoft.kz/egz/viewAnnouncement.xhtml?announcePortalId=" + portalId, portalUser.getId());
                    dicParticipantCompanyLogic.setLastNoticeTime(next.getNoticeDate(), portalUser.getId());
                    if (!processedPortalIds.contains(portalId)) {
                        // Если в списке есть два и более уведомлений по одному и тому же конкурсу, то незачем обновлять его дважды
                        processedPortalIds.add(portalId);
                        announcementQueueInfo.queueAnnouncementLoading(portalId);
                    }
                } else {
                    telegramServiceLogic.sendMessage("<b>" + sf.format(next.getNoticeDate()) + " " +
                            next.getSubject() + "</b>\n\n" + noticeInfo.getNoticeText(), portalUser.getId());
                    dicParticipantCompanyLogic.setLastNoticeTime(next.getNoticeDate(), portalUser.getId());
                }
                if (strings == null) {
                    strings = new HashSet<>();
                    noticesCache.put(next.getSubject(), strings);
                }
                strings.add(next.getText());
            }
        } catch (TelegramApiException e) {
            throw e;
        } catch (Throwable e) {
            if (next != null)
                LOGGER.error("Notice id: {}\ndate: {}\nsubj: {}\ntext: {}", next.getPortalId(), next.getNoticeDate(), next.getSubject(), next.getText());
            LOGGER.error("Error while processing fresh notifications", e);
        }
    }
}

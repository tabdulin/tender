package kz.inessoft.egz.ejb;

import kz.inessoft.egz.dbentities.SystemProperties;
import kz.inessoft.egz.ejbapi.ESysParam;
import kz.inessoft.egz.ejbapi.SysConfig;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by alexey on 01.08.16.
 */
@Stateless(name = "SysConfig")
public class SysConfigBean implements SysConfig {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    public String getSysParamValue(ESysParam param) {
        SystemProperties property = em.find(SystemProperties.class, param.getId());
        // Это нужно что бы объект всегда перечитывался из базы. В противном случае если в одной транзакции запрашивать
        // несколько раз из БД один и тот же объект то JPA берет его из кэша сессии, не обращаясь в БД
        em.detach(property);
        return property.getValue();
    }

    @Override
    public void saveSysParam(ESysParam param, String value) {
        em.find(SystemProperties.class, param.getId()).setValue(value);
    }
}

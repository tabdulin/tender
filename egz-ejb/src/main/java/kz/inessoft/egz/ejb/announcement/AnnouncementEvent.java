package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.dbentities.TAnnouncement;

/**
 * Created by alexey on 21.07.16.
 */
public enum AnnouncementEvent {
    StartDiscussion("1", "startdiscussion", "Начало обсуждения"),
    EndDiscussion("3", "enddiscussion", "Завершение обсуждения"),
    StartProposal("10", "startproposal", "Начало прием заявок"),
    EndProposal("4", "endproposal", "Завершение приема заявок"),
    StartProposalAddition("10", "startproposaladdtition", "Срок начала приема дополнения заявок"),
    EndProposalAddition("4", "endproposaladdtition", "Срок окончания приема дополнения заявок");

    /**
     * цвет события 1 - 11. см. google_calendar_api_event_color_chart.png
     */
    private String colorId;

    /**
     * суффикс, который добавляется к идентификатору объявления и получается идентификатор события.
     * Согласно требования google может состоять только из строчных латинских букв и цифр
     */
    private String idSuffix;
    private String nameSuffix;

    AnnouncementEvent(String colorId, String idSuffix, String nameSuffix) {
        this.colorId = colorId;
        this.idSuffix = idSuffix;
        this.nameSuffix = nameSuffix;
    }

    public String getColor() {
        return colorId;
    }

    public String getId(TAnnouncement a) {
        return a.getPortalId() + idSuffix;
    }

    public String getName(TAnnouncement a) {
        return a.getNumber() + " - " + nameSuffix + ". " + a.getShortNameRu();
    }
}

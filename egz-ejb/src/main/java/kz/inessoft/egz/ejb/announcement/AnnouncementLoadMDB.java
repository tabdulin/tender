package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.announcement.http.HTTPAnnouncementLoader;
import kz.inessoft.egz.ejb.announcement.http.SiteAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscussionsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteDocumentsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteLotsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteProposalsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteProtocolsInfo;
import kz.inessoft.egz.ejb.announcement.myproposals.MyProposalsLogic;
import kz.inessoft.egz.ejbapi.AnnouncementQueueInfo;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.naming.InitialContext;

/**
 * Created by alexey on 12.10.16.
 */
@MessageDriven(mappedName = "java:/jms/queue/AnnouncementsDownloadQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/AnnouncementsDownloadQueue"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5")
})
public class AnnouncementLoadMDB implements MessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementLoadMDB.class);

    @Resource
    private MessageDrivenContext mdc;

    @EJB
    private DicParticipantCompanyLogic participantCompanyLogic;

    @EJB
    private AnnouncementQueueInfo announcementQueueInfo;

    @EJB
    private MyProposalsLogic myProposalsLogic;

    @Override
    public void onMessage(Message inMsg) {
        long portalAnnouncementId = 0;
        try {
            portalAnnouncementId = inMsg.getLongProperty(JMSUtils.ANNOUNCEMENT_ID_PARAM_NAME);
            DicParticipantCompany portalUser = myProposalsLogic.getParticipantForAnnLoad(portalAnnouncementId);
            if (portalUser == null)
                portalUser = participantCompanyLogic.getDefaultCompanyForRefreshAnnouncement();

            LOGGER.info("Downloading announcement {}", portalAnnouncementId);
            HTTPAnnouncementLoader announcementLoader = new HTTPAnnouncementLoader(portalAnnouncementId, portalUser);
            SiteAnnouncement siteAnnouncement = announcementLoader.loadAnnouncement();
            SiteLotsInfo lots = announcementLoader.loadLots();
            SiteDocumentsInfo documents = announcementLoader.downloadAnnouncementFiles();
            SiteDiscussionsInfo siteDiscussionsInfo = announcementLoader.downloadAnnouncementDiscussions();
            SiteProtocolsInfo protocols = announcementLoader.downloadAnnouncementProtocols();
            SiteProposalsInfo siteProposalsInfo = announcementLoader.downloadProposalDocuments();

            // Сохранение данных в БД
            AnnouncementSiteProcessorLogic announcementLogic = InitialContext.doLookup(AnnouncementSiteProcessorLogic.FULL_JNDI_NAME);
            announcementLogic.saveAnnouncement(siteAnnouncement, lots, documents, siteDiscussionsInfo, siteProposalsInfo,
                    protocols, portalUser);
            announcementQueueInfo.unregisterAnnouncementLoading(portalAnnouncementId);

        } catch (Exception e) {
            LOGGER.error("Error while request announcement with portal id {}", portalAnnouncementId, e);
            mdc.setRollbackOnly();
        }
    }
}

package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus_;
import kz.inessoft.egz.dbentities.DicGovOrganization_;
import kz.inessoft.egz.dbentities.DicImplementer_;
import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.DicParticipationTemplate;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TAnnouncement_;
import kz.inessoft.egz.dbentities.TCrossAnnouncementParticipant;
import kz.inessoft.egz.dbentities.TCrossAnnouncementParticipantId;
import kz.inessoft.egz.dbentities.TImplementerLot;
import kz.inessoft.egz.dbentities.TImplementerLot_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement_;
import kz.inessoft.egz.dbentities.TLot_;
import kz.inessoft.egz.ejb.GoogleCalendarService;
import kz.inessoft.egz.ejbapi.AnnouncementFilter;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.AnnouncementMonitoringInfo;
import kz.inessoft.egz.ejbapi.AnnouncementQueueInfo;
import kz.inessoft.egz.ejbapi.AnnouncementSaveException;
import kz.inessoft.egz.ejbapi.DicTruLogic;
import kz.inessoft.egz.ejbapi.SysConfig;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 01.08.16.
 */
@Stateless(name = "AnnouncementLogic")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AnnouncementLogicBean implements AnnouncementLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Resource
    private EJBContext context;

    @EJB
    private SysConfig sysConfig;

    @EJB
    private DicTruLogic truLogic;

    @EJB
    private AnnouncementQueueInfo announcementQueueInfo;

    public void loadAnnouncement(long portalAnnouncementId) throws JMSException {
        announcementQueueInfo.queueAnnouncementLoading(portalAnnouncementId);
    }

    @Override
    public void loadAllAnnouncements() {
        new LoadFreshAnnmntsThread().start();
    }

    private void removeEventsFromGoogle(TAnnouncement announcement) throws GeneralSecurityException, IOException {
        GoogleCalendarService googleCalendarService = new GoogleCalendarService(sysConfig);
        for (AnnouncementEvent event : AnnouncementEvent.values()) {
            googleCalendarService.deleteEvent(event.getId(announcement));
        }
    }

    @Override
    public void setMonitoring(AnnouncementMonitoringInfo monitoringInfo) throws AnnouncementSaveException {
        TAnnouncement announcement = em.find(TAnnouncement.class, monitoringInfo.getAnnoucementId());
        boolean prevMonitoringStatus = announcement.isMonitoring();
        announcement.setMonitoring(monitoringInfo.isMonitorIt());
        em.createQuery("delete from TCrossAnnouncementParticipant where id.announcement = :announcement")
                .setParameter("announcement", announcement)
                .executeUpdate();
        if (!monitoringInfo.getParticipations().isEmpty()) {
            for (AnnouncementMonitoringInfo.Participation participation : monitoringInfo.getParticipations()) {
                TCrossAnnouncementParticipant crossAnnouncementParticipant = new TCrossAnnouncementParticipant();
                DicParticipationTemplate dicParticipationTemplate = em.getReference(DicParticipationTemplate.class, participation.getTemplateId());
                crossAnnouncementParticipant.setDicParticipationTemplate(dicParticipationTemplate);
                TCrossAnnouncementParticipantId id = new TCrossAnnouncementParticipantId();
                id.setAnnouncement(announcement);
                DicParticipantCompany dicParticipantCompany = new DicParticipantCompany();
                dicParticipantCompany.setId(participation.getCompanyId());
                id.setDicParticipantCompany(dicParticipantCompany);
                crossAnnouncementParticipant.setId(id);
                em.persist(crossAnnouncementParticipant);
            }
        }
        try {
            if (announcement.isMonitoring() && !prevMonitoringStatus)
                new GoogleCalendarService(sysConfig).saveEventsToGoogle(announcement);
            else if (!announcement.isMonitoring() && prevMonitoringStatus)
                removeEventsFromGoogle(announcement);
        } catch (Exception e) {
            context.setRollbackOnly();
            throw new AnnouncementSaveException(e);
        }
    }

    @Override
    public long getAnnouncementsCount(AnnouncementFilter filter) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<TAnnouncement> root = criteriaQuery.from(TAnnouncement.class);
        criteriaQuery.select(criteriaBuilder.count(root));
        processFilter(criteriaBuilder, criteriaQuery, root, filter);
        return em.createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public List<TAnnouncement> findAnnouncements(AnnouncementFilter filter, int maxResults, int firstResult) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<TAnnouncement> criteriaQuery = criteriaBuilder.createQuery(TAnnouncement.class);
        Root<TAnnouncement> root = criteriaQuery.from(TAnnouncement.class);
        processFilter(criteriaBuilder, criteriaQuery, root, filter);
        if (filter.getSortColumn() != null) {
            List<Order> orderList = new ArrayList<>();
            switch (filter.getSortColumn()) {
                case AnnouncementNumber:
                    Path<String> numberPath = root.get(TAnnouncement_.number);
                    orderList.add(filter.isAsc() ? criteriaBuilder.asc(numberPath) : criteriaBuilder.desc(numberPath));
                    break;
                case AnnouncementName:
                    Path<String> namePath = root.get(TAnnouncement_.nameRu);
                    orderList.add(filter.isAsc() ? criteriaBuilder.asc(namePath) : criteriaBuilder.desc(namePath));
                    break;
                case AnnouncementStatus:
                    Path<String> statusPath = root.get(TAnnouncement_.announcementStatus).get(DicAnnouncementStatus_.nameRu);
                    orderList.add(filter.isAsc() ? criteriaBuilder.asc(statusPath) : criteriaBuilder.desc(statusPath));
                    break;
                case StartRecvTime:
                    Path<Date> startRecvPath = root.get(TAnnouncement_.startRecvTime);
                    orderList.add(filter.isAsc() ? criteriaBuilder.asc(startRecvPath) : criteriaBuilder.desc(startRecvPath));
                    break;
                case EndRecvTime:
                    Path<Date> endRecvPath = root.get(TAnnouncement_.endRecvTime);
                    orderList.add(filter.isAsc() ? criteriaBuilder.asc(endRecvPath) : criteriaBuilder.desc(endRecvPath));
                    break;
                case PublicationDate:
                    Path<Date> publicationDate = root.get(TAnnouncement_.publicationDate);
                    orderList.add(filter.isAsc() ? criteriaBuilder.asc(publicationDate) : criteriaBuilder.desc(publicationDate));
                    break;
            }
            criteriaQuery.orderBy(orderList);
        }
        return em.createQuery(criteriaQuery).setMaxResults(maxResults)
                .setFirstResult(firstResult)
                .getResultList();
    }

    @Override
    public double getAnnouncementAmount(Long announcementId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Double> query = criteriaBuilder.createQuery(Double.class);
        Root<TLotCrossAnnouncement> root = query.from(TLotCrossAnnouncement.class);
        query.select(criteriaBuilder.sum(root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.lot).get(TLot_.totalSum)));
        query.where(criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement).get(TAnnouncement_.id), announcementId));
        return em.createQuery(query).getSingleResult();
    }

    private void processFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Root<TAnnouncement> root, AnnouncementFilter filter) {
        List<Predicate> predicates = new ArrayList<>();
        if (filter.getAnnouncementNumber() != null)
            predicates.add(criteriaBuilder.equal(root.get(TAnnouncement_.number), filter.getAnnouncementNumber()));

        if (filter.getStartPublicationDate() != null)
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(TAnnouncement_.publicationDate), filter.getStartPublicationDate()));

        if (filter.getEndPublicationDate() != null)
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(TAnnouncement_.publicationDate), filter.getEndPublicationDate()));

        if (filter.getStartRecvDate() != null)
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(TAnnouncement_.endRecvTime), filter.getStartRecvDate()));

        if (filter.getEndRecvDate() != null)
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(TAnnouncement_.startRecvTime), filter.getStartRecvDate()));

        if (filter.getStatusId() != null)
            predicates.add(criteriaBuilder.equal(root.get(TAnnouncement_.announcementStatus).get(DicAnnouncementStatus_.id), filter.getStatusId()));

        if (filter.getName() != null) {
            String[] name = filter.getName().split(" ");
            List<Predicate> list = new ArrayList<>();
            for (String s : name) {
                list.add(criteriaBuilder.like(root.get(TAnnouncement_.nameRu), "%" + s + "%"));
            }
            predicates.add(criteriaBuilder.and(list.toArray(new Predicate[list.size()])));
        }

        if (filter.getCustomerId() != null) {
            ListJoin<TAnnouncement, TLotCrossAnnouncement> join = root.join(TAnnouncement_.lotCrossAnnouncements);
            predicates.add(criteriaBuilder.equal(join.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.lot).get(TLot_.customer).get(DicGovOrganization_.id), filter.getCustomerId()));
        }

        if (filter.getImplementerId() != null) {
            Subquery<TAnnouncement> subquery = criteriaQuery.subquery(TAnnouncement.class);
            Root<TImplementerLot> implementerLotRoot = subquery.from(TImplementerLot.class);
            subquery.select(implementerLotRoot.get(TImplementerLot_.lotCrossAnnouncement).get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement));
            subquery.where(criteriaBuilder.equal(implementerLotRoot.get(TImplementerLot_.implementer).get(DicImplementer_.id), filter.getImplementerId()));
            predicates.add(root.in(subquery));
        }

        if (filter.isMonitoring()) {
            predicates.add(criteriaBuilder.equal(root.get(TAnnouncement_.monitoring), Boolean.TRUE));
        }
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
    }

    @Override
    public TAnnouncement getAnnouncementByPortalId(long portalAnnouncementId, boolean initCollections) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<TAnnouncement> query = criteriaBuilder.createQuery(TAnnouncement.class);
        Root<TAnnouncement> root = query.from(TAnnouncement.class);
        if (initCollections)
            root.fetch(TAnnouncement_.lotCrossAnnouncements);
        query.where(criteriaBuilder.equal(root.get(TAnnouncement_.portalId), portalAnnouncementId));
        try {
            TAnnouncement retVal = em.createQuery(query).getSingleResult();
            if (initCollections)
                // Т.к. невозможно извлечь несколько подчиненных коллекций в запросе, то проинициализируем эту после извлечения объекта
                retVal.getCrossAnnouncementParticipants().size();
            return retVal;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public TAnnouncement getAnnouncementById(long announcementId, boolean initCollections) {
        Map<String, Object> hints = new HashMap<>();
        if (initCollections) {
            EntityGraph<TAnnouncement> entityGraph = em.createEntityGraph(TAnnouncement.class);
            entityGraph.addSubgraph(TAnnouncement_.lotCrossAnnouncements);
            hints.put("javax.persistence.loadgraph", entityGraph);
        }
        return em.find(TAnnouncement.class, announcementId, hints);
    }

    @EJB
    private AnnouncementSiteProcessorLogic siteProcessorLogic;

    @Override
    public void refreshInterestingAnnouncements() {
        siteProcessorLogic.refreshInterestingAnnouncements();
    }
}

package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.dbentities.TAnnouncementLoadQueue;
import kz.inessoft.egz.dbentities.TAnnouncementLoadQueue_;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejbapi.AnnouncementQueueInfo;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import java.util.Date;

/**
 * Created by alexey on 15.08.16.
 */
@Stateless(name = "AnnouncementQueueInfo")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AnnouncementQueueInfoBean implements AnnouncementQueueInfo {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Resource(lookup = "java:/jms/queue/AnnouncementsDownloadQueue")
    private Queue queue;

    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;

    @Override
    public boolean isAnnouncementLoading(long portalId) {
        return em.find(TAnnouncementLoadQueue.class, portalId) != null;
    }

    @Override
    public void queueAnnouncementLoading(long portalId) throws JMSException {
        if (!isAnnouncementLoading(portalId)) {
            TAnnouncementLoadQueue loadQueue = new TAnnouncementLoadQueue();
            loadQueue.setPortalId(portalId);
            loadQueue.setQueueTime(new Date());
            em.persist(loadQueue);

            Message message = jmsContext.createMessage();
            message.setLongProperty(JMSUtils.ANNOUNCEMENT_ID_PARAM_NAME, portalId);
            message.setStringProperty(JMSUtils.MESSAGE_TYPE_PROPERTY_NAME, JMSUtils.ANNOUNCEMENT_DOWNLOAD);
            JMSUtils.delayDeliveryForNight(message);

            jmsContext.createProducer().send(queue, message);
        }
    }

    @Override
    public void unregisterAnnouncementLoading(long portalId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaDelete<TAnnouncementLoadQueue> queryDelete = builder.createCriteriaDelete(TAnnouncementLoadQueue.class);
        Root<TAnnouncementLoadQueue> root = queryDelete.from(TAnnouncementLoadQueue.class);
        queryDelete.where(builder.equal(root.get(TAnnouncementLoadQueue_.portalId), portalId));
        em.createQuery(queryDelete).executeUpdate();
    }
}

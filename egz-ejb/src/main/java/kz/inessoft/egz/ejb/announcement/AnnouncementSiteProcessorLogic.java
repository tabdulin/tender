package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.ejb.JNDILookup;
import kz.inessoft.egz.ejbapi.AnnouncementSaveException;
import kz.inessoft.egz.ejb.announcement.http.SiteAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscussionsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteDocumentsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteLotsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteProposalsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteProtocolsInfo;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Local
public interface AnnouncementSiteProcessorLogic {
    String JNDI_NAME = "AnnouncementSiteProcessorLogicBean";
    String FULL_JNDI_NAME = JNDILookup.JNDINAME_PREFIX + JNDI_NAME;

    void saveAnnouncement(SiteAnnouncement siteAnnouncement, SiteLotsInfo lotsInfo, SiteDocumentsInfo documentsInfo,
                          SiteDiscussionsInfo siteDiscussionsInfo, SiteProposalsInfo siteProposalsInfo,
                          SiteProtocolsInfo protocolsInfo, DicParticipantCompany participantCompany) throws AnnouncementSaveException;

    void refreshMonitoredAnnouncements();

    List<Long> getMonitoredAnnouncementsPortalIds();

    void refreshInterestingAnnouncements();
}

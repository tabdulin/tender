package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus;
import kz.inessoft.egz.dbentities.DicAnnouncementStatus_;
import kz.inessoft.egz.dbentities.DicLotStatus_;
import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.DicTru_;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TAnnouncement_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement_;
import kz.inessoft.egz.dbentities.TLot_;
import kz.inessoft.egz.ejb.GoogleCalendarService;
import kz.inessoft.egz.ejb.announcement.discussions.DiscussionsSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.documents.DocumentSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.http.SiteAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscussionsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteDocumentsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteLotsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteProposalsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteProtocolsInfo;
import kz.inessoft.egz.ejb.announcement.lots.LotSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.myproposals.MyProposalsLogic;
import kz.inessoft.egz.ejb.announcement.proposals.ProposalsSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.protocols.ProtocolsSiteProcessorLogic;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.AnnouncementQueueInfo;
import kz.inessoft.egz.ejbapi.AnnouncementSaveException;
import kz.inessoft.egz.ejbapi.DicAnnouncementStatusLogic;
import kz.inessoft.egz.ejbapi.DicGovOrganizationLogic;
import kz.inessoft.egz.ejbapi.DicPurchaseModeLogic;
import kz.inessoft.egz.ejbapi.DicPurchaseObjectTypeLogic;
import kz.inessoft.egz.ejbapi.DicPurchaseTypeLogic;
import kz.inessoft.egz.ejbapi.DicTruLogic;
import kz.inessoft.egz.ejbapi.JiraService;
import kz.inessoft.egz.ejbapi.SysConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Stateless(name = AnnouncementSiteProcessorLogic.JNDI_NAME)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AnnouncementSiteProcessorLogicBean implements AnnouncementSiteProcessorLogic {
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(AnnouncementSiteProcessorLogicBean.class);

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicTruLogic truLogic;

    @EJB
    private ProposalsSiteProcessorLogic proposalsSiteProcessorLogic;

    @EJB
    private LotSiteProcessorLogic lotSiteProcessorLogic;

    @EJB
    private DocumentSiteProcessorLogic documentSiteProcessorLogic;

    @EJB
    private DiscussionsSiteProcessorLogic discussionsSiteProcessorLogic;

    @EJB
    private ProtocolsSiteProcessorLogic protocolsSiteProcessorLogic;

    @EJB
    private DicGovOrganizationLogic dicGovOrganizationLogic;

    @EJB
    private DicPurchaseModeLogic dicPurchaseModeLogic;

    @EJB
    private DicPurchaseObjectTypeLogic dicPurchaseObjectTypeLogic;

    @EJB
    private DicPurchaseTypeLogic dicPurchaseTypeLogic;

    @Resource
    private EJBContext context;

    @EJB
    private DicAnnouncementStatusLogic dicAnnouncementStatusLogic;

    @EJB
    private AnnouncementLogic announcementLogic;

    @EJB
    private JiraService jiraService;

    @EJB
    private SysConfig sysConfig;

    @EJB
    private MyProposalsLogic myProposalsLogic;

    @EJB
    private AnnouncementQueueInfo announcementQueueInfo;

    @EJB
    private AnnouncementSiteProcessorLogic announcementSiteProcessorLogic;

    public void saveAnnouncement(SiteAnnouncement siteAnnouncement, SiteLotsInfo lotsInfo, SiteDocumentsInfo documentsInfo,
                                 SiteDiscussionsInfo discussionsInfo, SiteProposalsInfo proposalsInfo,
                                 SiteProtocolsInfo protocolsInfo, DicParticipantCompany participantCompany) throws AnnouncementSaveException {
        TAnnouncement announcement = announcementLogic.getAnnouncementByPortalId(siteAnnouncement.getPortalId(), false);
        if (announcement == null)
            announcement = new TAnnouncement();
        else {
            announcement.setLoadTime(new Date());
        }
        fillAnnouncement(siteAnnouncement, announcement);

        announcement.setGeneralTab(siteAnnouncement.getGeneralTabHtml());
        if (discussionsInfo != null)
            announcement.setDiscussionsTab(discussionsInfo.getHtml());
        announcement.setDocumentationTab(documentsInfo.getHtml());
        if (proposalsInfo != null)
            announcement.setProposalsInfo(proposalsInfo.getHtml());
        if (protocolsInfo != null)
            announcement.setProtocolsTab(protocolsInfo.getHtml());
        announcement.setParticipantCompany(participantCompany);
        if (!announcement.isMonitoring() && myProposalsLogic.isAnnouncementHasMyProposals(siteAnnouncement.getPortalId()))
            // Т.е. объявление имеются организации, у которых это объявление есть в "Мои заявки"
            // по этому ставим его на мониторинг
            announcement.setMonitoring(true);

        if (announcement.getId() == null)
            em.persist(announcement);

        lotSiteProcessorLogic.saveLotsHtmls(announcement, lotsInfo.getHtmls());

        lotSiteProcessorLogic.saveLotsInfo(announcement, lotsInfo.getLots());
        documentSiteProcessorLogic.saveDocuments(announcement, documentsInfo.getSiteDocuments(), participantCompany);
        if (discussionsInfo != null)
            discussionsSiteProcessorLogic.saveDiscussions(announcement, discussionsInfo.getDiscussions());
        if (proposalsInfo != null)
            proposalsSiteProcessorLogic.saveProposals(announcement, proposalsInfo.getSiteProposals(), participantCompany);
        if (protocolsInfo != null)
            protocolsSiteProcessorLogic.saveProtocols(announcement, protocolsInfo.getProtocolFileInfos(), participantCompany);
        try {
            if (announcement.isMonitoring()) {
                // Сохранить события в гуглокалендаре, если объявление должно быть отмониторено
                new GoogleCalendarService(sysConfig).saveEventsToGoogle(announcement);
                // Сохранение в джире
                jiraService.createOrUpdateIssue(announcement.getPortalId());
            }
        } catch (Exception e) {
            context.setRollbackOnly();
            throw new AnnouncementSaveException(e);
        }
    }

    @Override
    public void refreshMonitoredAnnouncements() {
        List<Long> announcementsIds = announcementSiteProcessorLogic.getMonitoredAnnouncementsPortalIds();
        if (!announcementsIds.isEmpty()) {
            for (Long announcementsId : announcementsIds) {
                if (announcementQueueInfo.isAnnouncementLoading(announcementsId))
                    continue;

                try {
                    announcementQueueInfo.queueAnnouncementLoading(announcementsId);
                } catch (Throwable e) {
                    LOGGER.error("Error while refresh monitored annuncement {}. Skip this announcement.", announcementsId, e);
                }
            }
        }
    }

    @Override
    public List<Long> getMonitoredAnnouncementsPortalIds() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<TAnnouncement> root = query.from(TAnnouncement.class);
        query.select(root.get(TAnnouncement_.portalId));
        query.where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(TAnnouncement_.monitoring), Boolean.TRUE),
                        criteriaBuilder.equal(root.get(TAnnouncement_.announcementStatus).get(DicAnnouncementStatus_.finalStatus), Boolean.FALSE)
                )
        );

        List<Long> resultList = em.createQuery(query).getResultList();
        resultList.size();
        return resultList;
    }

    @Override
    public void refreshInterestingAnnouncements() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<TLotCrossAnnouncement> root = query.from(TLotCrossAnnouncement.class);
        Path<TLotCrossAnnouncementId> idPath = root.get(TLotCrossAnnouncement_.id);
        Path<TAnnouncement> announcementPath = idPath.get(TLotCrossAnnouncementId_.announcement);
        query.select(announcementPath.get(TAnnouncement_.portalId)).distinct(true);
        Path<DicAnnouncementStatus> statusPath = announcementPath.get(TAnnouncement_.announcementStatus);
        Date currentDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MONTH, -2);
        Date twoMonthAgo = cal.getTime();
        query.where(
                criteriaBuilder.or(
                        criteriaBuilder.and(
                                criteriaBuilder.equal(statusPath.get(DicAnnouncementStatus_.finalStatus), Boolean.FALSE),
                                criteriaBuilder.or(
                                        criteriaBuilder.equal(statusPath.get(DicAnnouncementStatus_.notUpdateUntilStartRecv), Boolean.FALSE),
                                        criteriaBuilder.and(
                                                criteriaBuilder.equal(statusPath.get(DicAnnouncementStatus_.notUpdateUntilStartRecv), Boolean.TRUE),
                                                criteriaBuilder.lessThan(announcementPath.get(TAnnouncement_.startRecvTime), currentDate)
                                        )
                                ),
                                criteriaBuilder.or(
                                        criteriaBuilder.equal(statusPath.get(DicAnnouncementStatus_.notUpdateUntilEndRecv), Boolean.FALSE),
                                        criteriaBuilder.and(
                                                criteriaBuilder.equal(statusPath.get(DicAnnouncementStatus_.notUpdateUntilEndRecv), Boolean.TRUE),
                                                criteriaBuilder.lessThan(announcementPath.get(TAnnouncement_.endRecvTime), currentDate)
                                        )
                                ),
                                idPath.get(TLotCrossAnnouncementId_.lot).get(TLot_.dicTru).get(DicTru_.id).in(truLogic.getInterestingTruIds())
                        ),
                        criteriaBuilder.and(
                                // так же обновим старые, уже завершенные объявления, у которых есть лоты "Закупка состоялась",
                                // но договора до сих пор нет, т.к. возможно что заказчик отказался от закупки уже после завершения заявки.
                                // К старым относим те объявления, у которых уже прошло 2 месяца после завершения приема заявлений
                                // Если по таким заявкам не обновить данные, то по ним будет постоянно производиться поиск контракта, которого нет и быть не может.
                                criteriaBuilder.equal(statusPath.get(DicAnnouncementStatus_.finalStatus), Boolean.TRUE),
                                criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.status).get(DicLotStatus_.contractStatus), Boolean.TRUE),
                                criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.hasContract), Boolean.FALSE),
                                criteriaBuilder.lessThan(announcementPath.get(TAnnouncement_.loadTime), twoMonthAgo),
                                idPath.get(TLotCrossAnnouncementId_.lot).get(TLot_.dicTru).get(DicTru_.id).in(truLogic.getInterestingTruIds())
                        )
                )
        );

        List<Long> portalIds = em.createQuery(query).getResultList();
        LOGGER.info("Selected {} announcements for update.", portalIds.size());
        try {
            if (!portalIds.isEmpty()) {
                for (Long announcementsId : portalIds) {
                    if (announcementQueueInfo.isAnnouncementLoading(announcementsId))
                        continue;
                    try {
                        announcementQueueInfo.queueAnnouncementLoading(announcementsId);
                    } catch (Throwable e) {
                        LOGGER.error("Error while refresh IT annuncement {}. Skip this announcement.", announcementsId, e);
                    }
                }
            }
        } finally {
            LOGGER.info("Refresh IT announcements done.");
        }
    }

    private void fillAnnouncement(SiteAnnouncement source, TAnnouncement destination) {
        destination.setNameRu(source.getName());
        destination.setOrganizatorBankRequisites(source.getOrganizatorBankRequisites());
        destination.setOrganizator(dicGovOrganizationLogic.getValueByBin(source.getOrganizatorRu().substring(0, 12),
                StringUtils.trim(source.getOrganizatorRu().substring(12)), source.getOrganizatorJurAddress()));
        destination.setAnnouncementStatus(dicAnnouncementStatusLogic.getValueByName(source.getAnnouncementStatusRu()));
        destination.setStartRecvTime(source.getStartRecvTime());
        destination.setEndRecvTime(source.getEndRecvTime());
        destination.setStartRecvAppendixTime(source.getStartRecvAppendixTime());
        destination.setEndRecvAppendixTime(source.getEndRecvAppendixTime());
        destination.setStartDiscussionTime(source.getStartDiscussionTime());
        destination.setEndDiscussionTime(source.getEndDiscussionTime());
        destination.setNumber(source.getNumber());
        destination.setOrganizatorEmail(source.getOrganizatorEmail());
        destination.setOrganizatorFio(source.getOrganizatorFio());
        destination.setOrganizatorJobPosition(source.getOrganizatorJobPosition());
        destination.setOrganizatorPhone(source.getOrganizatorPhone());
        destination.setPortalId(source.getPortalId());
        destination.setProperties(source.getProperties());
        destination.setPublicationDate(source.getPublicationDate());
        destination.setPurchaseMode(dicPurchaseModeLogic.getValueByName(source.getPurchaseModeRu()));
        destination.setPurchaseObjectType(dicPurchaseObjectTypeLogic.getValueByName(source.getPurchaseObjectTypeRu()));
        destination.setPurchaseType(dicPurchaseTypeLogic.getValueByName(source.getPurchaseTypeRu()));
    }
}

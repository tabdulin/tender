package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Created by alexey on 05.09.16.
 */
@Singleton
@Startup
public class AnnouncementsSheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementsSheduler.class);
    @EJB
    private AnnouncementSiteProcessorLogic announcementSiteProcessorLogic;

    @EJB
    private AnnouncementLogic announcementLogic;

    @Schedule(dayOfWeek = "1,2,3,4,5", hour = "10,13,16", minute = "30", persistent = false)
    public void startNewAnnouncementsSearching() {
        LOGGER.info("Starting scheduled process for collection new announcements");
        announcementLogic.loadAllAnnouncements();
    }

    @Schedule(dayOfWeek = "1,2,3,4,5", hour = "10", minute = "1", persistent = false)
    public void startRefreshMonitoredAnnouncements() {
        LOGGER.info("Starting scheduled process for refresh monitored announcements");
        announcementSiteProcessorLogic.refreshMonitoredAnnouncements();
    }

    @Schedule(dayOfWeek = "1,2,3,4,5", hour = "10", persistent = false)
    public void startRefreshIntrestingAnnouncements() {
        LOGGER.info("Starting scheduled process for refresh IT announcements");
        announcementSiteProcessorLogic.refreshInterestingAnnouncements();
    }
}

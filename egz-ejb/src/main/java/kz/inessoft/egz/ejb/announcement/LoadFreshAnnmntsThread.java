package kz.inessoft.egz.ejb.announcement;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.JNDILookup;
import kz.inessoft.egz.ejb.announcement.http.HTTPAnnouncementsIDIterator;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.AnnouncementQueueInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.util.Iterator;

/**
 * Created by alexey on 25.08.16.
 * Поток, производящий запрос свежих объявлений с типом "Услуга", с датой публикации не менее указанной в конструкторе
 */
public class LoadFreshAnnmntsThread extends Thread {
    private static boolean startedFlag = false;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoadFreshAnnmntsThread.class);

    @Override
    public void run() {
        if (startedFlag) {
            LOGGER.warn("Warning!!! Previous process for collection new announcements not finished yet.");
            return;
        }
        try {
            startedFlag = true;
            Iterator<Long> allAnnouncements = new HTTPAnnouncementsIDIterator();
            AnnouncementLogic annLogic = (AnnouncementLogic) JNDILookup.lookupLocal(JNDILookup.ANNOUNCEMENT_LOGIC_NAME);
            AnnouncementQueueInfo announcementQueueInfo = (AnnouncementQueueInfo) JNDILookup.lookupLocal(JNDILookup.ANNOUNCEMENT_QUEUE_INFO);
            while (allAnnouncements.hasNext()) {
                Long next = allAnnouncements.next();
                if (announcementQueueInfo.isAnnouncementLoading(next))
                    continue;

                TAnnouncement announcementByPortalId = annLogic.getAnnouncementByPortalId(next, false);
                if (announcementByPortalId != null)
                    continue;

                try {
                    announcementQueueInfo.queueAnnouncementLoading(next);
                } catch (Throwable e) {
                    LOGGER.error("Error while loading annuncement {}. Skip this announcement.", next, e);
                }
            }
        } catch (NamingException e) {
            LOGGER.error("Error while loading annuncements.", e);
        } finally {
            startedFlag = false;
        }
    }
}

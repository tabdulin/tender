package kz.inessoft.egz.ejb.announcement.comission;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteComissionMember;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 11.08.16.
 */
@Local
public interface TenderComissionSiteProcessorLogic {
    void saveComission(TAnnouncement announcement, List<SiteComissionMember> comissionMembers);
}

package kz.inessoft.egz.ejb.announcement.comission;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TComission;
import kz.inessoft.egz.ejbapi.DicComissionRoleLogic;
import kz.inessoft.egz.ejb.announcement.http.SiteComissionMember;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by alexey on 11.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TenderComissionSiteProcessorLogicBean implements TenderComissionSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicComissionRoleLogic dicComissionRoleLogic;

    @Override
    public void saveComission(TAnnouncement announcement, List<SiteComissionMember> comissionMembers) {
        em.createQuery("delete from TComission where announcement=:announcement")
                .setParameter("announcement", announcement)
                .executeUpdate();
        if (comissionMembers != null)
            for (SiteComissionMember comissionMember : comissionMembers) {
                TComission tComission = new TComission();
                tComission.setAnnouncement(announcement);
                tComission.setFio(comissionMember.getFio());
                tComission.setJobPosition(comissionMember.getJobPosition());
                tComission.setComissionRole(dicComissionRoleLogic.getValueByName(comissionMember.getRole()));
                em.persist(tComission);
            }
    }
}

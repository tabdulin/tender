package kz.inessoft.egz.ejb.announcement.discussions;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDiscussion;
import kz.inessoft.egz.dbentities.TDiscussionCrossAnnouncement;
import kz.inessoft.egz.ejbapi.DiscussionsLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DiscussionsLogicBean implements DiscussionsLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public List<TDiscussionCrossAnnouncement> getDiscussions(TAnnouncement announcement) {
        return em.createQuery("from TDiscussionCrossAnnouncement where id.announcement = :announcement")
                .setParameter("announcement", announcement)
                .getResultList();
    }

    @Override
    public TDiscussion getDiscussion(long id) {
        return em.find(TDiscussion.class, id);
    }
}

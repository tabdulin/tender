package kz.inessoft.egz.ejb.announcement.discussions;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscussionsInfo;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Local
public interface DiscussionsSiteProcessorLogic {
    void saveDiscussions(TAnnouncement announcement, List<SiteDiscussionsInfo.SiteDiscussion> discussions);
}

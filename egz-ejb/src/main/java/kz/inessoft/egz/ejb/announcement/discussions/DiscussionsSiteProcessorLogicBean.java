package kz.inessoft.egz.ejb.announcement.discussions;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDiscussion;
import kz.inessoft.egz.dbentities.TDiscussionCrossAnnouncement;
import kz.inessoft.egz.dbentities.TDiscussionCrossAnnouncementId;
import kz.inessoft.egz.dbentities.TDiscussionRequest;
import kz.inessoft.egz.dbentities.TDiscussionResponse;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscussionsInfo;
import kz.inessoft.egz.ejbapi.DicDiscussionRequestTypeLogic;
import kz.inessoft.egz.ejbapi.DicDiscussionResponseTypeLogic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DiscussionsSiteProcessorLogicBean implements DiscussionsSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicDiscussionRequestTypeLogic dicDiscussionRequestTypeLogic;

    @EJB
    private DicDiscussionResponseTypeLogic dicDiscussionResponseTypeLogic;

    @Override
    public void saveDiscussions(TAnnouncement announcement, List<SiteDiscussionsInfo.SiteDiscussion> discussions) {
        em.createQuery("delete from TDiscussionCrossAnnouncement where id.announcement = :announcement")
                .setParameter("announcement", announcement)
                .executeUpdate();
        Query query = em.createQuery("from TDiscussion where portalId = :portalId");
        for (SiteDiscussionsInfo.SiteDiscussion discussion : discussions) {
            TDiscussion tDiscussion;
            try {
                tDiscussion = (TDiscussion) query.setParameter("portalId", discussion.getPortalId())
                        .getSingleResult();
            } catch (NoResultException e) {
                tDiscussion = new TDiscussion();
            }
            tDiscussion.setSourceHtml(discussion.getSourceHtml());

            TDiscussionRequest request = new TDiscussionRequest();
            tDiscussion.setRequest(request);
            request.setAuthor(discussion.getRequest().getAuthor());
            request.setImplementer(discussion.getRequest().getImplementer());
            request.setRequestType(dicDiscussionRequestTypeLogic.getValueByName(discussion.getRequest().getRequestType()));
            request.setSubject(discussion.getRequest().getSubject());
            request.setText(discussion.getRequest().getText());
            request.setTime(discussion.getRequest().getTime());

            if (discussion.getResponce() != null) {
                TDiscussionResponse responce = new TDiscussionResponse();
                tDiscussion.setResponse(responce);
                responce.setTime(discussion.getResponce().getTime());
                responce.setAuthor(discussion.getResponce().getAuthor());
                responce.setResponseType(dicDiscussionResponseTypeLogic.getValueByName(discussion.getResponce().getDecision()));
                responce.setDescription(discussion.getResponce().getDescription());
                responce.setRejectCause(discussion.getResponce().getRejectCause());
                responce.setExplanation(discussion.getResponce().getExplanation());
            } else
                tDiscussion.setResponse(null);

            if (tDiscussion.getId() == null) {
                tDiscussion.setPortalId(discussion.getPortalId());
                em.persist(tDiscussion);
            }
            TDiscussionCrossAnnouncement crossAnnouncement = new TDiscussionCrossAnnouncement();
            TDiscussionCrossAnnouncementId id = new TDiscussionCrossAnnouncementId();
            id.setAnnouncement(announcement);
            id.setDiscussion(tDiscussion);
            crossAnnouncement.setId(id);
            em.persist(crossAnnouncement);
        }
    }
}

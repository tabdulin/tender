package kz.inessoft.egz.ejb.announcement.documents;

import kz.inessoft.egz.dbentities.DicTenderDocumentation_;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TAnnouncement_;
import kz.inessoft.egz.dbentities.TDocument;
import kz.inessoft.egz.dbentities.TDocumentCrossTPortalFile;
import kz.inessoft.egz.dbentities.TDocumentCrossTPortalFileId;
import kz.inessoft.egz.dbentities.TDocumentCrossTPortalFileId_;
import kz.inessoft.egz.dbentities.TDocumentCrossTPortalFile_;
import kz.inessoft.egz.dbentities.TDocument_;
import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.dbentities.TPortalFile_;
import kz.inessoft.egz.ejbapi.DocumentLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by alexey on 10.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DocumentLogicBean implements DocumentLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public List<TDocument> getDocumentsWithFiles(long announcementId) {
        Query query = em.createQuery("select e from TDocument e where e.announcement.id = :announcementId and e.portalFiles is not empty ");
        query.setParameter("announcementId", announcementId);
        List<TDocument> retVal = query.getResultList();
        retVal.size();
        return retVal;
    }

    @Override
    public TDocument getDocument(String documentTypeName, TAnnouncement announcement) {
        return (TDocument) em.createQuery("from TDocument where announcement = :announcement and documentation.nameRu = :nameRu")
                .setParameter("announcement", announcement)
                .setParameter("nameRu", documentTypeName)
                .getSingleResult();
    }

    @Override
    public TDocument getDocument(long id, boolean initCollection) {
        TDocument retVal = em.find(TDocument.class, id);
        if (initCollection) {
            retVal.getPortalFiles().size();
        }
        return retVal;
    }

    public List<TPortalFile> getJiraFiles(Long announcementId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TPortalFile> query = builder.createQuery(TPortalFile.class);
        Root<TDocumentCrossTPortalFile> root = query.from(TDocumentCrossTPortalFile.class);
        Path<TDocumentCrossTPortalFileId> idPath = root.get(TDocumentCrossTPortalFile_.id);
        query.select(idPath.get(TDocumentCrossTPortalFileId_.portalFile)).distinct(true);
        Path<TDocument> documentPath = idPath.get(TDocumentCrossTPortalFileId_.document);
        query.where(builder.and(
                builder.equal(documentPath.get(TDocument_.documentation).get(DicTenderDocumentation_.uploadToJira), Boolean.TRUE)),
                builder.equal(documentPath.get(TDocument_.announcement).get(TAnnouncement_.id), announcementId),
                builder.isNotNull(idPath.get(TDocumentCrossTPortalFileId_.portalFile).get(TPortalFile_.fileName))
        );
        return em.createQuery(query).getResultList();
    }
}

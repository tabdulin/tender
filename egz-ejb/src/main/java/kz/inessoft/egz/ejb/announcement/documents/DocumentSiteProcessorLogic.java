package kz.inessoft.egz.ejb.announcement.documents;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteDocumentsInfo;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Local
public interface DocumentSiteProcessorLogic {
    void saveDocuments(TAnnouncement announcement, List<SiteDocumentsInfo.SiteDocument> documents, DicParticipantCompany participantCompany);
}

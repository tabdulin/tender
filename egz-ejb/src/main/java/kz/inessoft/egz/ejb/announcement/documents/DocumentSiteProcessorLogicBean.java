package kz.inessoft.egz.ejb.announcement.documents;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDocument;
import kz.inessoft.egz.dbentities.TDocumentCrossTPortalFile;
import kz.inessoft.egz.dbentities.TDocumentCrossTPortalFileId;
import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.ejbapi.DicTenderDocumentationLogic;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import kz.inessoft.egz.ejb.announcement.http.ASiteFileInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteDocumentsInfo;
import kz.inessoft.egz.ejb.announcement.http.SiteFileRecieved;
import kz.inessoft.egz.ejb.announcement.http.SiteFileUploaded;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by alexey on 20.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DocumentSiteProcessorLogicBean implements DocumentSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicTenderDocumentationLogic dicTenderDocumentationLogic;

    @EJB
    private PortalFilesLogic portalFilesLogic;

    @Override
    public void saveDocuments(TAnnouncement announcement, List<SiteDocumentsInfo.SiteDocument> documents, DicParticipantCompany participantCompany) {
        em.createQuery("delete from TDocument where announcement=:announcement")
                .setParameter("announcement", announcement)
                .executeUpdate();
        for (SiteDocumentsInfo.SiteDocument document : documents) {
            TDocument tDocument = new TDocument();
            tDocument.setAnnouncement(announcement);
            tDocument.setRequired(document.isRequired());
            tDocument.setDocumentation(dicTenderDocumentationLogic.getValueByName(document.getName()));
            tDocument.setHtml(document.getSourceHtml());
            em.persist(tDocument);
            // На портале есть бага. Один и тот же файл можно прицепить много раз. Пример заявка 867817 - тех. спека.
            // Так же объявление 891801 и другие
            // По этому много раз заносить и тот же файл в базу не будем.
            Set<Long> processedFiles = new HashSet<>();
            for (ASiteFileInfo fileInfo : document.getPortalFiles()) {
                TPortalFile portalFile;
                if (fileInfo instanceof SiteFileUploaded)
                    portalFile = portalFilesLogic.queueLoadUploadedFile(fileInfo.getPortalFileId());
                else {
                    SiteFileRecieved fileRecieved = (SiteFileRecieved) fileInfo;
                    portalFile = portalFilesLogic.queueLoadRecievedFile(fileRecieved.getPortalFileId(), fileRecieved.getUrl(), participantCompany);
                }
                if (!processedFiles.contains(portalFile.getId())) {
                    processedFiles.add(portalFile.getId());
                    TDocumentCrossTPortalFile documentCrossTPortalFile = new TDocumentCrossTPortalFile();
                    TDocumentCrossTPortalFileId id = new TDocumentCrossTPortalFileId();
                    id.setDocument(tDocument);
                    id.setPortalFile(portalFile);
                    documentCrossTPortalFile.setId(id);
                    em.persist(documentCrossTPortalFile);
                }
            }
        }
    }
}

package kz.inessoft.egz.ejb.announcement.http;

/**
 * Created by alexey on 09.08.16.
 */
public abstract class ASiteFileInfo {
    private long portalFileId;

    public long getPortalFileId() {
        return portalFileId;
    }

    public void setPortalFileId(long portalFileId) {
        this.portalFileId = portalFileId;
    }
}

package kz.inessoft.egz.ejb.announcement.http;

/**
 * Created by alexey on 01.08.16.
 */
public class AnnouncementLoadException extends Exception {
    public AnnouncementLoadException(String message, Throwable cause) {
        super(message, cause);
    }

    public AnnouncementLoadException(Throwable cause) {
        super(cause);
    }

    public AnnouncementLoadException(String message) {
        super(message);
    }
}

package kz.inessoft.egz.ejb.announcement.http;

import kz.inessoft.egz.dbentities.ETProtocolType;

/**
 * Created by alexey on 09.08.16.
 */
public enum EProtocolType {
    DISCUSSION_PROTOCOL("Протокол обсуждения документации", ETProtocolType.DISCUSSION),
    OPENING_PROTOCOL("Протокол вскрытия", ETProtocolType.OPENING),
    PRELIMINARY_ACCES_PROTOCOL("Протокол предварительного допуска", ETProtocolType.PRELIMINARY_ACCES),
    ACCES_PROTOCOL("Протокол допуска", ETProtocolType.ACCES),
    RESULT_PROTOCOL("Протокол итогов", ETProtocolType.RESULT),
    RESULT_PROTOCOL_EXPERT_OPINION("Экспертное мнение", ETProtocolType.RESULT_EXPERT_OPINION);

    private String protocolName;
    private ETProtocolType dbType;

    EProtocolType(String protocolName, ETProtocolType dbType) {
        this.protocolName = protocolName;
        this.dbType = dbType;
    }

    public static EProtocolType getProtocolByName(String protocolName) {
        for (EProtocolType protocolType : EProtocolType.values()) {
            if (protocolType.protocolName.equalsIgnoreCase(protocolName))
                return protocolType;
        }
        return null;
    }

    public ETProtocolType getDBType() {
        return dbType;
    }
}

package kz.inessoft.egz.ejb.announcement.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.inessoft.egz.dbentities.IPortalUser;
import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static kz.inessoft.egz.ejb.announcement.http.EProtocolType.RESULT_PROTOCOL_EXPERT_OPINION;

/**
 * Created by alexey on 15.09.16.
 */
public class HTTPAnnouncementLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(HTTPAnnouncementLoader.class);
    private long announcementPortalId;
    private IPortalUser portalUser;
    private Response announceResponse;

    /**
     * @param announcementPortalId идентификатор объявления на сайте
     * @param portalUser           креденшиалы, под которыми будет производиться загрузка объявления
     */
    public HTTPAnnouncementLoader(long announcementPortalId, IPortalUser portalUser) throws AnnouncementLoadException {
        this.announcementPortalId = announcementPortalId;
        this.portalUser = portalUser;
    }

    /**
     * Загрузить объявление о конкурсе с сайта. Производится просто загрузка с сайта без сохранения в БД и т.п. Это все должен делать уже другой код
     *
     * @return
     */
    public SiteAnnouncement loadAnnouncement() throws AnnouncementLoadException {
        AHttpClient client = null;
        SiteAnnouncement retVal = new SiteAnnouncement();
        try {
            client = getClient();

            announceResponse = client.sendGetRequest(PortalUrlUtils.createAnnouncementUrl(announcementPortalId));
            retVal.setGeneralTabHtml(announceResponse.getContent());
            retVal.setName(getStringValue("//div[@class='panel-body']/div[@class='row']/div[@class='col-md-6' and position()=1]/div[@class='form-group' and position()=2]/div[@class='col-sm-7']/input[@type='text']", announceResponse));
            retVal.setPortalId(announcementPortalId);
            retVal.setNumber(getStringValue("//div[normalize-space(label)='Номер объявления']/div/input[@type='text']", announceResponse));
            retVal.setAnnouncementStatusRu(getStringValue("//div[normalize-space(label)='Статус объявления']/div/input[@type='text']", announceResponse));

            retVal.setStartRecvTime(getDate("//div[normalize-space(label)='Срок начала приема заявок' or normalize-space(label)='Предварительный срок начала приема заявок']/div/input[@type='text']", announceResponse));
            retVal.setEndRecvTime(getDate("//div[normalize-space(label)='Срок окончания приема заявок' or normalize-space(label)='Предварительный срок окончания приема заявок']/div/input[@type='text']", announceResponse));

            retVal.setStartRecvAppendixTime(getDate("//div[normalize-space(label)='Срок начала приема дополнения заявок']/div/input[@type='text']", announceResponse));
            retVal.setEndRecvAppendixTime(getDate("//div[normalize-space(label)='Срок окончания приема дополнения заявок']/div/input[@type='text']", announceResponse));

            retVal.setStartDiscussionTime(getDate("//div[normalize-space(label)='Срок начала обсуждения']/div/input[@type='text']", announceResponse));
            retVal.setEndDiscussionTime(getDate("//div[normalize-space(label)='Срок окончания обсуждения']/div/input[@type='text']", announceResponse));

            retVal.setPublicationDate(getDate("//div[normalize-space(label)='Дата публикации объявления']/div/input[@type='text']", announceResponse));
            retVal.setOrganizatorRu(getStringText("//tr[normalize-space(th)='Организатор']/td", announceResponse));
            retVal.setOrganizatorJurAddress(getStringText("//tr[normalize-space(th)='Юр. адрес организатора']/td", announceResponse));
            retVal.setProperties(getStringText("//tr[normalize-space(th)='Признаки']/td", announceResponse));
            retVal.setPurchaseModeRu(getStringText("//tr[normalize-space(th)='Способ проведения закупки']/td", announceResponse));
            retVal.setPurchaseObjectTypeRu(getStringText("//tr[normalize-space(th)='Вид предмета закупок']/td", announceResponse));
            retVal.setPurchaseTypeRu(getStringText("//tr[normalize-space(th)='Тип закупки']/td", announceResponse));
            retVal.setAmount(Double.valueOf(getStringText("//tr[normalize-space(th)='Сумма закупки']/td", announceResponse).replaceAll(" ", "")));
            retVal.setOrganizatorFio(getStringText("//tr[normalize-space(th)='ФИО представителя']/td", announceResponse));
            retVal.setOrganizatorJobPosition(getStringText("//tr[normalize-space(th)='Должность']/td", announceResponse));
            retVal.setOrganizatorPhone(getStringText("//tr[normalize-space(th)='Контактный телефон']/td", announceResponse));
            retVal.setOrganizatorEmail(getStringText("//tr[normalize-space(th)='E-Mail']/td", announceResponse));
            retVal.setOrganizatorBankRequisites(getStringText("//tr[normalize-space(th)='Банковские реквизиты для обеспечения заявки']/td", announceResponse));
            return retVal;
        } catch (Exception e) {
            LOGGER.error("Error while parcing announcement. Result html:\n {}", retVal.getGeneralTabHtml());
            throw new AnnouncementLoadException(e);
        } finally {
            if (client != null)
                HttpClientsPool.getInstance().freeClient(client);
        }
    }

    /**
     * Загрузить данные по лотам
     *
     * @return
     * @throws AnnouncementLoadException
     */
    public SiteLotsInfo loadLots() throws AnnouncementLoadException {
        AHttpClient client = null;
        try {
            client = getClient();
            SiteLotsInfo retVal = new SiteLotsInfo();
            int page = 1;
            while (true) {
                Response lotsResponse = client.sendGetRequest(PortalUrlUtils.createAnnouncementLotsUrl(announcementPortalId, page));
                retVal.getHtmls().add(lotsResponse.getContent());
                NodeList nodeList = lotsResponse.evaluateXPathNodeList("//table//a[@data-lot-id!='']/../..");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node row = nodeList.item(i);
                    Node link = XMLUtils.evaluateXPathNode("./td[2]/a/@data-lot-id", row);
                    long portalLotId = Long.valueOf(link.getNodeValue());
                    long planId = Long.valueOf(XMLUtils.evaluateXPathNode("./td[1]", row).getTextContent());
                    HttpPost httpPost = new HttpPost(PortalUrlUtils.createAnnouncementLotLoadAjaxUrl(announcementPortalId));
                    List<NameValuePair> parameters = new ArrayList<>();
                    parameters.add(new BasicNameValuePair("id", String.valueOf(portalLotId)));
                    httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                    Response lotResponse = client.sendPostRequest(httpPost);
                    SiteLotsInfo.SiteLot siteLot = new SiteLotsInfo.SiteLot();
                    siteLot.setPortalId(portalLotId);
                    siteLot.setPlanId(planId);
                    siteLot.setOriginalHtml(lotResponse.getContent());
                    siteLot.setNumber(getStringText("//tr[normalize-space(th)='Лот №']/td", lotResponse));
                    siteLot.setStatus(getStringText("//tr[normalize-space(th)='Статус лота']/td", lotResponse));
                    siteLot.setCustomerBin(getStringText("//tr[normalize-space(th)='БИН заказчика']/td", lotResponse));
                    siteLot.setCustomerName(getStringText("//tr[normalize-space(th)='Наименование заказчика']/td", lotResponse));
                    siteLot.setTruCode(getStringText("//tr[normalize-space(th)='Код ТРУ']/td", lotResponse));
                    siteLot.setTruName(getStringText("//tr[normalize-space(th)='Наименование ТРУ']/td", lotResponse));
                    siteLot.setShortCharacteristicRu(getStringText("//tr[normalize-space(th)='Краткая характеристика']/td", lotResponse));
                    siteLot.setAdditionalCharacteristicRu(getStringText("//tr[normalize-space(th)='Дополнительная характеристика']/td", lotResponse));
                    siteLot.setPaymentSourceRu(getStringText("//tr[normalize-space(th)='Источник финансирования']/td", lotResponse));
                    siteLot.setPriceForUnit(getDouble("//tr[normalize-space(th)='Цена за единицу']/td", lotResponse));
                    siteLot.setUnit(getStringText("//tr[normalize-space(th)='Еденица измерения' or normalize-space(th)='Единица измерения']/td", lotResponse));
                    siteLot.setCount(getDouble("//tr[normalize-space(th)='Количество']/td", lotResponse));
                    siteLot.setFirstYearSum(getDouble("//tr[normalize-space(th)='Сумма 1 год']/td", lotResponse));
                    siteLot.setSecondYearSum(getDouble("//tr[normalize-space(th)='Сумма 2 год']/td", lotResponse));
                    siteLot.setThirdYearSum(getDouble("//tr[normalize-space(th)='Сумма 3 год']/td", lotResponse));
                    siteLot.setTotalSum(getDouble("//tr[normalize-space(th)='Запланированная сумма']/td", lotResponse));
                    siteLot.setPrepayment(getDouble("//tr[normalize-space(th)='Размер авансового платежа %']/td", lotResponse));
                    siteLot.setPlacement(getStringText("//tr[normalize-space(th)='Место поставки товара, КАТО']/td", lotResponse)
                            .replaceAll("\n                  \n                  ", "\n")
                            .replaceAll("\n                  ", "\n"));
                    siteLot.setDeliveryTime(getStringText("//tr[normalize-space(th)='Срок поставки ТРУ']/td", lotResponse));
                    siteLot.setIncoterms(getStringText("//tr[normalize-space(th)='Условия поставки ИНКОТЕРМС']/td", lotResponse));
                    String ingeneering = getStringText("//tr[normalize-space(th)='Инжиниринговая услуга']/td", lotResponse);
                    if (ingeneering != null)
                        siteLot.setIngeneering(!"Нет".equalsIgnoreCase(ingeneering));
                    siteLot.setDemping(getStringText("//tr[normalize-space(th)='Признак демпинга']/td", lotResponse));
                    siteLot.setCustomerFIO(getStringText("//tr[normalize-space(th)='ФИО представителя']/td", lotResponse));
                    siteLot.setCustomerJobPosition(getStringText("//tr[normalize-space(th)='Должность']/td", lotResponse));
                    siteLot.setCustomerPhone(getStringText("//tr[normalize-space(th)='Контактный телефон']/td", lotResponse));
                    siteLot.setCustomerEmail(getStringText("//tr[normalize-space(th)='E-Mail']/td", lotResponse));
                    siteLot.setCustomerBankRequisites(getStringText("//tr[normalize-space(th)='Банковские реквизиты']/td", lotResponse));
                    retVal.getLots().add(siteLot);
                }
                Node node = lotsResponse.evaluateXPathNode("//ul[@class='pagination']//a[text()='" + (++page) + "']");
                if (node == null)
                    break;
            }
            return retVal;
        } catch (RequestException | XPathExpressionException | ParserConfigurationException | SAXException | IOException e) {
            throw new AnnouncementLoadException("Can't load lots list", e);
        } finally {
            if (client != null)
                HttpClientsPool.getInstance().freeClient(client);
        }
    }

    /**
     * Загрузить с сайта файлы с конкурсной документацией
     *
     * @return список загруженных файлов
     * @throws AnnouncementLoadException
     */
    public SiteDocumentsInfo downloadAnnouncementFiles() throws AnnouncementLoadException {
        AHttpClient client = null;
        try {
            SiteDocumentsInfo retVal = new SiteDocumentsInfo();
            client = getClient();
            Response docsResponse = client.sendGetRequest(PortalUrlUtils.createAnnouncementDocumentsUrl(announcementPortalId));
            retVal.setHtml(docsResponse.getContent());
            NodeList nodeList = docsResponse.evaluateXPathNodeList("//tr[normalize-space(th[1])='Наименование документа' and normalize-space(th[2])='Обязательность для поставщика']/../tr[position() > 1]");
            Pattern p = Pattern.compile("\\d+, *\\d+");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node tableRow = nodeList.item(i);
                SiteDocumentsInfo.SiteDocument document = new SiteDocumentsInfo.SiteDocument();
                document.setName(XMLUtils.evaluateXPathNode("./td[1]", tableRow).getTextContent().trim());
                document.setRequired("Да".equalsIgnoreCase(XMLUtils.evaluateXPathNode("./td[2]", tableRow).getTextContent()));
                retVal.getSiteDocuments().add(document);
                NodeList btn = XMLUtils.evaluateXPathNodeList("./td[3]/button/@onclick", tableRow);
                if (btn.getLength() > 0) {
                    String onclick = btn.item(0).getNodeValue();
                    Matcher matcher = p.matcher(onclick);
                    matcher.find();
                    String data = matcher.group();
                    long docTypePortalId = Long.valueOf(data.split(",")[1].trim());
                    Response response = client.sendGetRequest(PortalUrlUtils.createAnnouncementDocFilesList(announcementPortalId, docTypePortalId));
                    document.setSourceHtml(response.getContent());
                    NodeList fileLinks = response.evaluateXPathNodeList("//tr/td/a[contains(@href,'download_file')]/@href");
                    for (int j = 0; j < fileLinks.getLength(); j++) {
                        String url = fileLinks.item(j).getNodeValue();
                        document.getPortalFiles().add(getFileInfo(url));
                    }
                }
            }
            return retVal;
        } catch (Exception e) {
            throw new AnnouncementLoadException("Cannot download announcement: " + announcementPortalId, e);
        } finally {
            if (client != null)
                HttpClientsPool.getInstance().freeClient(client);
        }
    }

    /**
     * Загрузить с сайта файлы с обсуждениями документацией
     *
     * @throws AnnouncementLoadException
     */
    public SiteDiscussionsInfo downloadAnnouncementDiscussions() throws AnnouncementLoadException {
        AHttpClient client = null;
        try {
            if (announceResponse.evaluateXPathNodeList("//a[text()='Обсуждение положений документации']").getLength() > 0) {
                client = getClient();
                SiteDiscussionsInfo retVal = new SiteDiscussionsInfo();
                Response discussionsResponse = client.sendGetRequest(PortalUrlUtils.createAnnouncementDiscussionsUrl(announcementPortalId));
                retVal.setHtml(discussionsResponse.getContent());
                NodeList nodeList = discussionsResponse.evaluateXPathNodeList("//th[text()='№ запроса']/../../tr/td/a/@href");
                Pattern p = Pattern.compile("/actionShowDiscus/\\d+/\\d+");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node n = nodeList.item(i);
                    String url = n.getNodeValue();
                    Response response = client.sendGetRequest(url);
                    SiteDiscussionsInfo.SiteDiscussion discussion = new SiteDiscussionsInfo.SiteDiscussion();
                    discussion.setSourceHtml(response.getContent());
                    Matcher m = p.matcher(url);
                    m.find();
                    Long portalId = Long.valueOf(m.group().split("/")[3]);
                    discussion.setPortalId(portalId);
                    SiteDiscussionsInfo.SiteDiscussion.Request request = new SiteDiscussionsInfo.SiteDiscussion.Request();
                    discussion.setRequest(request);
                    request.setSubject(evaluateRequestField(response, "Тема сообщения"));
                    request.setRequestType(evaluateRequestField(response, "Тип сообщения"));
                    request.setImplementer(evaluateRequestField(response, "Поставщик"));
                    request.setAuthor(evaluateRequestField(response, "Представитель поставщика"));
                    request.setText(evaluateRequestField(response, "Текст сообщения"));
                    request.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(evaluateRequestField(response, "Дата и время отправки сообщения")));

                    Node node = response.evaluateXPathNode("//div[@id='editMessage']");
                    if (node != null) {
                        SiteDiscussionsInfo.SiteDiscussion.Responce responce = new SiteDiscussionsInfo.SiteDiscussion.Responce();
                        discussion.setResponce(responce);
                        responce.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(evaluateResponceField(node, "Дата:")));
                        responce.setAuthor(evaluateResponceField(node, "Автор:"));
                        responce.setDecision(evaluateResponceField(node, "Решение:"));
                        responce.setDescription(evaluateResponceField(node, "Описание внесения изменения"));
                        responce.setRejectCause(evaluateResponceField(node, "Причина отклонения"));
                        responce.setExplanation(evaluateResponceField(node, "Текст разъяснения"));
                    }
                    retVal.getDiscussions().add(discussion);
                }
                return retVal;
            } else return null;
        } catch (Exception e) {
            throw new AnnouncementLoadException("Discussions were not downloaded for annoncement: " + announcementPortalId, e);
        } finally {
            if (client != null)
                HttpClientsPool.getInstance().freeClient(client);
        }
    }

    private String evaluateRequestField(Response response, String fieldName) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        return response.evaluateXPathNode("//div[@class='panel-body']/div/div[@class='row']/div[normalize-space(strong)='" + fieldName + "']/../div[@class='col-md-9']").getTextContent().trim();
    }

    private String evaluateResponceField(Node node, String fieldName) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Node node1 = XMLUtils.evaluateXPathNode("./div[@class='row']/div[normalize-space(strong)='" + fieldName + "']/../div[@class='col-md-9']", node);
        if (node1 == null) return null;
        return node1.getTextContent().trim();
    }

    /**
     * Загрузить с сайта протоколы результатов тендера
     *
     * @throws AnnouncementLoadException
     */
    public SiteProtocolsInfo downloadAnnouncementProtocols() throws AnnouncementLoadException {
        // todo бывают проблемные случаи. К примеру есть проблемы с протоколом итогов по объявлению 834534. Там 2 файла на загрузку.
        // так же есть объявления, где несколько протоколов итогов, из них только один действителен. Надо доделать.
        SiteProtocolsInfo retVal = new SiteProtocolsInfo();
        try {
            // Протоколы
            AHttpClient client = getClient();
            try {
                Response protocolsResponse = client.sendGetRequest(PortalUrlUtils.createAnnouncementProtocolUrl(announcementPortalId));
                retVal.setHtml(protocolsResponse.getContent());
                NodeList nodeList = protocolsResponse.evaluateXPathNodeList("//div[@class='tab-content']//div[@class='panel panel-default']");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node panel = nodeList.item(i);
                    String section = XMLUtils.evaluateXPathNode("./div[@class='panel-heading']/b", panel).getTextContent();
                    EProtocolType protocolByName = EProtocolType.getProtocolByName(section);
                    if (protocolByName == null)
                        throw new AnnouncementLoadException("Unknown protocol type: " + section);

                    switch (protocolByName) {
                        case RESULT_PROTOCOL:
                            loadItogProtocols(protocolByName, retVal.getProtocolFileInfos(), panel);
                            break;
                        default:
                            loadProtocols(protocolByName, retVal.getProtocolFileInfos(), panel);
                    }
                }
            } finally {
                HttpClientsPool.getInstance().freeClient(client);
            }
            return retVal;
        } catch (SAXException | XPathExpressionException | ParserConfigurationException | IOException | RequestException e) {
            throw new AnnouncementLoadException("Unable to load protocols.", e);
        }
    }

    private void loadProtocols(EProtocolType protocolByName, List<SiteProtocolsInfo.SiteProtocolFileInfo> retVal, Node panel) throws AnnouncementLoadException {
        try {
            NodeList nodeList = XMLUtils.evaluateXPathNodeList(".//a/@href", panel);
            for (int i = 0; i < nodeList.getLength(); i++) {
                SiteProtocolsInfo.SiteProtocolFileInfo siteProtocolFileInfo = downloadProtocol(nodeList.item(i).getNodeValue(), protocolByName);
                if (siteProtocolFileInfo != null)
                    retVal.add(siteProtocolFileInfo);
            }
        } catch (Exception e) {
            throw new AnnouncementLoadException(e);
        }
    }

    private SiteProtocolsInfo.SiteProtocolFileInfo downloadProtocol(String protocolLink, EProtocolType protocolByName) throws IOException {
        if (protocolLink == null || !StringUtils.contains(protocolLink, "download_file")) {
            return null;
        }
        SiteProtocolsInfo.SiteProtocolFileInfo siteProtocolFileInfo = new SiteProtocolsInfo.SiteProtocolFileInfo();
        siteProtocolFileInfo.setPortalFile(getFileInfo(protocolLink));
        siteProtocolFileInfo.setProtocolType(protocolByName);
        return siteProtocolFileInfo;
    }

    private void loadItogProtocols(EProtocolType protocolByName, List<SiteProtocolsInfo.SiteProtocolFileInfo> retVal, Node panel) throws AnnouncementLoadException {
        try {
            // Загрузим актуальный протокол
            NodeList nodeList = XMLUtils.evaluateXPathNodeList(".//td[@class='green' and text()='Действительный']/../td/a/@href", panel);
            for (int i = 0; i < nodeList.getLength(); i++) {
                SiteProtocolsInfo.SiteProtocolFileInfo siteProtocolFileInfo = downloadProtocol(nodeList.item(i).getNodeValue(), protocolByName);
                if (siteProtocolFileInfo != null)
                    retVal.add(siteProtocolFileInfo);
            }

            // Загрузим экспертное мнение, если оно есть.
            nodeList = XMLUtils.evaluateXPathNodeList(".//th[normalize-space(text())='Экспертное мнение']/../../..//a/@href", panel);
            for (int i = 0; i < nodeList.getLength(); i++) {
                SiteProtocolsInfo.SiteProtocolFileInfo siteProtocolFileInfo = downloadProtocol(nodeList.item(i).getNodeValue(), RESULT_PROTOCOL_EXPERT_OPINION);
                if (siteProtocolFileInfo != null)
                    retVal.add(siteProtocolFileInfo);
            }
        } catch (Exception e) {
            throw new AnnouncementLoadException(e);
        }
    }

    /**
     * Вырезать из URL-а идентификатор файла на портале. Предполагается что URL имеет вид либо
     * https://v3bl.goszakup.gov.kz/files/download_file/2879138
     *
     * @param url входной URL
     * @return извлеченный идентификатор файла
     */
    private ASiteFileInfo getFileInfo(String url) {
        ASiteFileInfo retVal;
        if (url.contains("/download_file/")) {
            retVal = new SiteFileUploaded();
            Pattern pattern = Pattern.compile("/download_file/\\d+");
            Matcher matcher = pattern.matcher(url);
            matcher.find();
            retVal.setPortalFileId(Long.valueOf(matcher.group().replaceAll("/download_file/", "")));
        } else {
            Pattern pattern = Pattern.compile("/myapp/show_doc/\\d+/\\d+/\\d+");
            Matcher matcher = pattern.matcher(url);
            if (!matcher.find())
                throw new RuntimeException("Can't recognize url type " + url);
            SiteFileRecieved fileRecieved = new SiteFileRecieved();
            fileRecieved.setPortalFileId(Long.valueOf(matcher.group().replaceAll("/myapp/show_doc/", "").split("/")[2]));
            fileRecieved.setUrl(url);
            retVal = fileRecieved;
        }
        return retVal;
    }

    public SiteProposalsInfo downloadProposalDocuments() throws AnnouncementLoadException {
        AHttpClient client = null;
        try {

            // Заявки
            NodeList nodeList = announceResponse.evaluateXPathNodeList("//a[text()='Посмотреть заявки']/@href");
            if (nodeList.getLength() > 0) {
                client = getClient();
                String href = nodeList.item(0).getNodeValue();
                Response proposalsResponse = client.sendGetRequest(href);
                SiteProposalsInfo retVal = new SiteProposalsInfo();
                retVal.setHtml(proposalsResponse.getContent());
                NodeList rows = proposalsResponse.evaluateXPathNodeList("//div[@class='tab-content']//table/tr[position()>1]");
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                for (int i = 0; i < rows.getLength(); i++) {
                    Node row = rows.item(i);
                    SiteProposalsInfo.SiteProposal proposal = parseProposal(XMLUtils.evaluateXPathNode("./td[1]/a/@href", row).getNodeValue(), client);
                    proposal.setProposalTime(sf.parse(XMLUtils.evaluateXPathNode("./td[4]", row).getTextContent().trim()));
                    retVal.getSiteProposals().add(proposal);
                }
                return retVal;
            } else return null;
        } catch (IOException | XPathExpressionException | ParseException | SAXException | ParserConfigurationException | RequestException e) {
            throw new AnnouncementLoadException(e);
        } finally {
            if (client != null)
                HttpClientsPool.getInstance().freeClient(client);
        }
    }

    private SiteProposalsInfo.SiteProposal parseProposal(String url, AHttpClient client) throws RequestException, IOException, XPathExpressionException, ParserConfigurationException, SAXException, AnnouncementLoadException {
        SiteProposalsInfo.SiteProposal retVal = new SiteProposalsInfo.SiteProposal();
        Pattern p = Pattern.compile("/consideration/actionShowInfoSupplier/\\d+/\\d+");
        Matcher m = p.matcher(url);
        m.find();
        long portalId = Long.valueOf(m.group().replaceAll("/consideration/actionShowInfoSupplier/", "").split("/")[1]);
        retVal.setPortalId(portalId);
        // Основная информация
        Response response = client.sendGetRequest(url);
        retVal.setGeneralInfoTabHtml(response.getContent());
        retVal.setStatus(response.evaluateXPathNode("//div[@class='panel-body']//div[text()='Статус заявки']/following-sibling::div//input/@value").getNodeValue());
        retVal.setImplementerName(response.evaluateXPathNode("//div[@class='panel-body']//div[text()='Наименование поставщика']/following-sibling::div//input/@value").getNodeValue());
        retVal.setImplementerXin(response.evaluateXPathNode("//div[@class='panel-body']//div[text()='БИН (ИИН/ИНН/УНП)']/following-sibling::div//input/@value").getNodeValue());
        retVal.setImplementerAddress(response.evaluateXPathNode("//strong[text()='Адрес поставщика']/../..//input/@value").getNodeValue());
        retVal.setBankName(response.evaluateXPathNode("//strong[text()='Наименование банка']/../..//input/@value").getNodeValue());
        retVal.setIik(response.evaluateXPathNode("//strong[text()='ИИК']/../..//input/@value").getNodeValue());
        retVal.setBik(response.evaluateXPathNode("//strong[text()='БИК']/../..//input/@value").getNodeValue());
        retVal.setKbe(response.evaluateXPathNode("//strong[text()='КБе']/../..//input/@value").getNodeValue());
        retVal.setImplementerFIO(response.evaluateXPathNode("//strong[text()='Представитель поставщика']/../..//input/@value").getNodeValue());
        retVal.setPhone(response.evaluateXPathNode("//strong[text()='Контактный телефон']/../..//input/@value").getNodeValue());
        retVal.setJobPosition(response.evaluateXPathNode("//strong[text()='Должность']/../..//input/@value").getNodeValue());
        retVal.setConsorcium(response.evaluateXPathNode("//strong[text()='Признак консорциума']/../..//input/@checked") != null);

        // Лоты
        String lotsUrl = response.evaluateXPathNode("//a[text()='Лоты для участия в закупке поставщиком']/@href").getNodeValue();
        Response responseLots = client.sendGetRequest(lotsUrl);
        retVal.setLotsInfoTabHtml(responseLots.getContent());
        NodeList nodeList = responseLots.evaluateXPathNodeList("//table[@class='table table-bordered']//tr[position()>1]/td[1]/a");
        for (int i = 0; i < nodeList.getLength(); i++) {
            retVal.getLotNumbers().add(nodeList.item(i).getTextContent());
        }

        // Документация
        String docsUrl = response.evaluateXPathNode("//a[text()='Документация']/@href").getNodeValue();
        Response responceDocs = client.sendGetRequest(docsUrl);
        retVal.setDocsInfoTabHtml(responceDocs.getContent());

        NodeList lotsLinks = responceDocs.evaluateXPathNodeList("//a[@class='show_lot_docs']");
        for (int j = 0; j < lotsLinks.getLength(); j++) {
            Node docsLinkNode = lotsLinks.item(j);
            long lotId = Long.valueOf(docsLinkNode.getAttributes().getNamedItem("id").getNodeValue());
            String lotNumber = docsLinkNode.getTextContent().split("\n")[0].replace("Лот № ", "");
            HttpPost post = new HttpPost(PortalUrlUtils.createAnnouncementProposalLotDocumentsUrl(announcementPortalId, portalId, lotId));
            List<NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("show_doc", "Y"));
            parameters.add(new BasicNameValuePair("is_addition", "false"));
            post.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            String json = client.sendPostRequest(post).getContent();
            ObjectMapper mapper = new ObjectMapper();
            ProposalDocumentsLotResponce lotResponce = mapper.readValue(json, ProposalDocumentsLotResponce.class);
            if (lotResponce.getStatus() != 1) {
                throw new AnnouncementLoadException("Can not read proposal documents for announcement " + announcementPortalId +
                        " proposal " + portalId + " lotNumber " + lotNumber);
            }
            SiteProposalLotDocuments proposalLotDocuments = new SiteProposalLotDocuments();
            retVal.getProposalLotDocuments().add(proposalLotDocuments);
            proposalLotDocuments.setLotNumber(lotNumber);
            proposalLotDocuments.setHtml(lotResponce.getMessage());
            Document document = XMLUtils.getDocument("<div>" + lotResponce.getMessage() + "</div>");
            NodeList docs = XMLUtils.evaluateXPathNodeList("//div[@id='accordion2']/div", document);
            for (int i = 0; i < docs.getLength(); i++) {
                Node docNode = docs.item(i);
                String docName = XMLUtils.evaluateXPathNode(".//h4/a", docNode).getTextContent().trim();
                NodeList files = XMLUtils.evaluateXPathNodeList(".//tr/td[1]/a/@href", docNode);
                for (int k = 0; k < files.getLength(); k++) {
                    SiteProposalDocument proposalDocument = new SiteProposalDocument();
                    proposalDocument.setDocumentName(docName);
                    proposalDocument.setPortalFile(getFileInfo(files.item(k).getNodeValue()));
                    proposalLotDocuments.getProposalDocuments().add(proposalDocument);
                }
            }
        }
        return retVal;
    }

    private Date getDate(String xpath, Response response) throws ParseException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = response.evaluateXPathNodeList(xpath);
        if (nodeList.getLength() > 0) {
            String retVal = nodeList.item(0).getAttributes().getNamedItem("value").getNodeValue();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sf.parse(retVal);
        }
        return null;
    }

    private String getStringValue(String xpath, Response response) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = response.evaluateXPathNodeList(xpath);
        if (nodeList.getLength() > 0) {
            String retVal = nodeList.item(0).getAttributes().getNamedItem("value").getNodeValue();
            retVal = retVal.trim();
            return retVal.isEmpty() ? null : retVal;
        }
        return null;
    }

    private String getStringText(String xpath, Response response) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = response.evaluateXPathNodeList(xpath);
        if (nodeList.getLength() > 0) {
            String retVal = nodeList.item(0).getTextContent();
            retVal = retVal.trim();
            return retVal.isEmpty() ? null : retVal;
        }
        return null;
    }

    private Double getDouble(String xpath, Response response) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = response.evaluateXPathNodeList(xpath);
        if (nodeList.getLength() > 0) {
            String retVal = nodeList.item(0).getTextContent();
            retVal = StringUtils.trim(retVal);
            if (StringUtils.isEmpty(retVal))
                return null;
            return Double.valueOf(retVal.replaceAll(" ", ""));
        }
        return null;
    }

    private AHttpClient getClient() {
        if (portalUser == null)
            return HttpClientsPool.getInstance().getClient();
        else
            return HttpClientsPool.getInstance().getClient(portalUser);
    }
}

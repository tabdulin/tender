package kz.inessoft.egz.ejb.announcement.http;

import kz.inessoft.egz.dbentities.DicInterestingTru;
import kz.inessoft.egz.ejb.JNDILookup;
import kz.inessoft.egz.ejbapi.DicInterestingTruLogic;

import java.util.Iterator;
import java.util.List;

/**
 * Created by alexey on 16.09.16.
 */
public class HTTPAnnouncementsIDIterator implements Iterator<Long> {
    private List<DicInterestingTru> dicInterestingTrus;
    private int currentTRUIdx = 0;
    private HTTPAnnouncementsIDTRUIterator idtruIterator;

    public HTTPAnnouncementsIDIterator() {
        try {
            DicInterestingTruLogic logic = (DicInterestingTruLogic) JNDILookup.lookupLocal(JNDILookup.DIC_INTERESTING_TRU_LOGIC_NAME);
            dicInterestingTrus = logic.getInterestingTRUs();
            if (!dicInterestingTrus.isEmpty()) {
                idtruIterator = new HTTPAnnouncementsIDTRUIterator(dicInterestingTrus.get(currentTRUIdx));
            }
        } catch (Exception e) {
            throw new RuntimeException("Error while create announcement id iterator", e);
        }
    }

    @Override
    public boolean hasNext() {
        if (idtruIterator == null)
            return false;
        if (idtruIterator.hasNext())
            return true;

        if (++currentTRUIdx == dicInterestingTrus.size()) {
            idtruIterator = null;
            return false;
        }

        idtruIterator = new HTTPAnnouncementsIDTRUIterator(dicInterestingTrus.get(currentTRUIdx));
        return hasNext();
    }

    @Override
    public Long next() {
        return idtruIterator.next();
    }
}

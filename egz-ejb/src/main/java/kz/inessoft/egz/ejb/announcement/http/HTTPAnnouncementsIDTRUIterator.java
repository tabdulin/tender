package kz.inessoft.egz.ejb.announcement.http;

import kz.inessoft.egz.dbentities.DicInterestingTru;
import kz.inessoft.egz.ejb.JNDILookup;
import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejb.httpclient.AnonymousHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejbapi.DicInterestingTruLogic;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 13.01.17.
 */
public class HTTPAnnouncementsIDTRUIterator implements Iterator<Long> {
    private DicInterestingTru dicInterestingTru;
    private AnonymousHttpClient client;
    private boolean fetchSizeSetFlag = false;
    private Date startIterateDate;
    private Date startPeriod;
    private Iterator<String> truURLS;
    private String currentTRUUrl;
    private int currentPage = 1;
    private Iterator<Long> currentPageIds;
    private int count = 0;

    public HTTPAnnouncementsIDTRUIterator(DicInterestingTru dicInterestingTru) {
        this.dicInterestingTru = dicInterestingTru;
        client = HttpClientsPool.getInstance().getClient();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dicInterestingTru.getUpdateDate());
            calendar.add(Calendar.MONTH, -1);
            // На всякий пожарный будем перебирать начиная за месяц до последнего момента сбора информации
            startPeriod = calendar.getTime();
            LinkedList<String> urls = new LinkedList<>();
            processTRU(PortalUrlUtils.createTopKTRUListFilterURL(), urls);
            this.truURLS = urls.iterator();
            startIterateDate = new Date();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // Рекурсия. Собрать все ссылки на конечные КТРУ
    private void processTRU(String url, List<String> urls) throws RequestException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        String code = getTRUCodeFromURL(url);
        if (dicInterestingTru.getCode().startsWith(code) &&
                !dicInterestingTru.getCode().equals(code)) {
            NodeList nodeList = getTRULinks(url, client);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node link = nodeList.item(i);
                url = link.getAttributes().getNamedItem("href").getNodeValue();
                code = getTRUCodeFromURL(url);
                if (!dicInterestingTru.getCode().startsWith(code))
                    continue;
                // Ссылка на нужный корневой найдена. Надо пройти по ней и обработаботать все подкатегории до 6-го колена
                processTRU(url, urls);
                break;
            }
        } else {
            if (code.length() >= 8) // 8 потому что код задается по две цыфры через точку 6 цыфр + 2 точки = 8 символов
                urls.add(url);
            else {
                // Заглубляемся еще дальше...
                NodeList nodeList = getTRULinks(url, client);
                if (nodeList.getLength() == 0)
                    urls.add(url);
                else
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        Node link = nodeList.item(i);
                        processTRU(link.getAttributes().getNamedItem("href").getNodeValue(), urls);
                    }
            }
        }
    }

    private NodeList getTRULinks(String url, AnonymousHttpClient client) throws RequestException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Response response = client.sendGetRequest(url);
        return response.evaluateXPathNodeList("//table[@class='table table-bordered table-hover']//td/a");
    }

    private String getTRUCodeFromURL(String url) {
        String[] strings = url.split("/");
        if (strings.length == 8)
            // Значит что запрашивается корневой КТРУ
            return "";
        return strings[8];
    }

    /**
     * @return true если на странице были данные, false, если на странице данных не было и дальше итерировать их нет смысла
     * @throws RequestException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws XPathExpressionException
     * @throws IOException
     */
    private boolean initCurrentPageIds() throws RequestException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        if (!fetchSizeSetFlag) {
            // Установим так, что бы портал выдавал по 300 записей на страницу (что бы поменьше запросов подавать на сервер)
            HttpPost httpPost = new HttpPost(PortalUrlUtils.createKTRURowsURL());
            List<NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("count_record", String.valueOf(300)));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            client.sendPostRequest(httpPost);
            fetchSizeSetFlag = true;
        }
        List<Long> lst = new LinkedList<>();
        while (true) {
            Response response = client.sendGetRequest(PortalUrlUtils.buildKTRUFiterURL(currentTRUUrl, startPeriod, currentPage));
            NodeList nodeList = response.evaluateXPathNodeList("//table[@class='table table-bordered']//tr/td[position()=4]");
            if (nodeList.getLength() == 0) {
                System.out.println("For TRU " + currentTRUUrl + " retrieved " + count + " rows");
                count = 0;
                return false;
            }
            count += nodeList.getLength();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                String tru = XMLUtils.evaluateXPathNode("./p[position()=1]", node).getTextContent().trim();
                if (!tru.startsWith(dicInterestingTru.getCode()))
                    continue;
                String href = XMLUtils.evaluateXPathNode(".//a", node).getAttributes().getNamedItem("href").getNodeValue();
                lst.add(Long.valueOf(href.substring(href.lastIndexOf("/") + 1)));
            }
            if (!lst.isEmpty())
                break;
            else
                currentPage++;
        }
        currentPageIds = lst.iterator();
        return true;
    }

    @Override
    public boolean hasNext() {
        try {
            if (currentTRUUrl == null) {
                if (truURLS.hasNext()) {
                    currentTRUUrl = truURLS.next();
                    currentPage = 1;
                    currentPageIds = null;
                    return hasNext();
                }
            } else {
                if (currentPageIds == null) {
                    if (!initCurrentPageIds()) {
                        currentTRUUrl = null;
                    }
                    return hasNext();
                } else {
                    if (currentPageIds.hasNext())
                        return true;
                    else {
                        currentPage++;
                        currentPageIds = null;
                        return hasNext();
                    }
                }
            }
            DicInterestingTruLogic dicInterestingTruLogic = (DicInterestingTruLogic) JNDILookup.lookupLocal(JNDILookup.DIC_INTERESTING_TRU_LOGIC_NAME);
            dicInterestingTruLogic.setLastUpdatedDate(dicInterestingTru.getId(), startIterateDate);
            HttpClientsPool.getInstance().freeClient(client);
            return false;
        } catch (Exception e) {
            throw new RuntimeException("Can't iterate announcements.", e);
        }
    }

    @Override
    public Long next() {
        return currentPageIds.next();
    }
}

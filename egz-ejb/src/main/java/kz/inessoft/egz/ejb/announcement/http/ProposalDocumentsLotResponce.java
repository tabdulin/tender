package kz.inessoft.egz.ejb.announcement.http;

/**
 * Created by alexey on 25.05.17.
 */
public class ProposalDocumentsLotResponce {
    private long status;
    private String message;

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

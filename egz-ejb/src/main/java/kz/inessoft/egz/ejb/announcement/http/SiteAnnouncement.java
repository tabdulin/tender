package kz.inessoft.egz.ejb.announcement.http;

import java.util.Date;

/**
 * Created by alexey on 01.08.16.
 */
public class SiteAnnouncement {
    private Long portalId;
    private String number;
    private String name;
    private String announcementStatusRu;
    private Date publicationDate;
    private Date startRecvTime;
    private Date endRecvTime;
    private Date startRecvAppendixTime;
    private Date endRecvAppendixTime;
    private Date startDiscussionTime;
    private Date endDiscussionTime;
    private String purchaseModeRu;
    private String purchaseTypeRu;
    private String purchaseObjectTypeRu;
    private Double amount;
    private String properties;
    private String organizatorRu;
    private String organizatorJurAddress;
    private String organizatorFio;
    private String organizatorJobPosition;
    private String organizatorPhone;
    private String organizatorEmail;
    private String organizatorBankRequisites;
    private String generalTabHtml;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnouncementStatusRu() {
        return announcementStatusRu;
    }

    public void setAnnouncementStatusRu(String announcementStatusRu) {
        this.announcementStatusRu = announcementStatusRu;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getStartRecvTime() {
        return startRecvTime;
    }

    public void setStartRecvTime(Date startRecvTime) {
        this.startRecvTime = startRecvTime;
    }

    public Date getEndRecvTime() {
        return endRecvTime;
    }

    public void setEndRecvTime(Date endRecvTime) {
        this.endRecvTime = endRecvTime;
    }

    public Date getStartRecvAppendixTime() {
        return startRecvAppendixTime;
    }

    public void setStartRecvAppendixTime(Date startRecvAppendixTime) {
        this.startRecvAppendixTime = startRecvAppendixTime;
    }

    public Date getEndRecvAppendixTime() {
        return endRecvAppendixTime;
    }

    public void setEndRecvAppendixTime(Date endRecvAppendixTime) {
        this.endRecvAppendixTime = endRecvAppendixTime;
    }

    public Date getStartDiscussionTime() {
        return startDiscussionTime;
    }

    public void setStartDiscussionTime(Date startDiscussionTime) {
        this.startDiscussionTime = startDiscussionTime;
    }

    public Date getEndDiscussionTime() {
        return endDiscussionTime;
    }

    public void setEndDiscussionTime(Date endDiscussionTime) {
        this.endDiscussionTime = endDiscussionTime;
    }

    public String getPurchaseModeRu() {
        return purchaseModeRu;
    }

    public void setPurchaseModeRu(String purchaseModeRu) {
        this.purchaseModeRu = purchaseModeRu;
    }

    public String getPurchaseTypeRu() {
        return purchaseTypeRu;
    }

    public void setPurchaseTypeRu(String purchaseTypeRu) {
        this.purchaseTypeRu = purchaseTypeRu;
    }

    public String getPurchaseObjectTypeRu() {
        return purchaseObjectTypeRu;
    }

    public void setPurchaseObjectTypeRu(String purchaseObjectTypeRu) {
        this.purchaseObjectTypeRu = purchaseObjectTypeRu;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getOrganizatorRu() {
        return organizatorRu;
    }

    public void setOrganizatorRu(String organizatorRu) {
        this.organizatorRu = organizatorRu;
    }

    public String getOrganizatorJurAddress() {
        return organizatorJurAddress;
    }

    public void setOrganizatorJurAddress(String organizatorJurAddress) {
        this.organizatorJurAddress = organizatorJurAddress;
    }

    public String getOrganizatorFio() {
        return organizatorFio;
    }

    public void setOrganizatorFio(String organizatorFio) {
        this.organizatorFio = organizatorFio;
    }

    public String getOrganizatorJobPosition() {
        return organizatorJobPosition;
    }

    public void setOrganizatorJobPosition(String organizatorJobPosition) {
        this.organizatorJobPosition = organizatorJobPosition;
    }

    public String getOrganizatorPhone() {
        return organizatorPhone;
    }

    public void setOrganizatorPhone(String organizatorPhone) {
        this.organizatorPhone = organizatorPhone;
    }

    public String getOrganizatorEmail() {
        return organizatorEmail;
    }

    public void setOrganizatorEmail(String organizatorEmail) {
        this.organizatorEmail = organizatorEmail;
    }

    public String getOrganizatorBankRequisites() {
        return organizatorBankRequisites;
    }

    public void setOrganizatorBankRequisites(String organizatorBankRequisites) {
        this.organizatorBankRequisites = organizatorBankRequisites;
    }

    public String getGeneralTabHtml() {
        return generalTabHtml;
    }

    public void setGeneralTabHtml(String generalTabHtml) {
        this.generalTabHtml = generalTabHtml;
    }
}

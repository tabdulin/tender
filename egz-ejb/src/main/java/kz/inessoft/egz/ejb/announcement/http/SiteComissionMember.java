package kz.inessoft.egz.ejb.announcement.http;

/**
 * Created by alexey on 11.08.16.
 */
public class SiteComissionMember {
    private String fio;
    private String jobPosition;
    private String role;

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

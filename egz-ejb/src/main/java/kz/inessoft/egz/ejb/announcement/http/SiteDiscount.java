package kz.inessoft.egz.ejb.announcement.http;

/**
 * Created by alexey on 11.08.16.
 */
public class SiteDiscount {
    private String discountName;
    private String discountValue;

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }
}

package kz.inessoft.egz.ejb.announcement.http;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 19.09.16.
 */
public class SiteDiscussionsInfo {
    public static class SiteDiscussion {
        public static class Request {
            private String subject;
            private String requestType;
            private String implementer;
            private String author;
            private Date time;
            private String text;

            public String getSubject() {
                return subject;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public String getRequestType() {
                return requestType;
            }

            public void setRequestType(String requestType) {
                this.requestType = requestType;
            }

            public String getImplementer() {
                return implementer;
            }

            public void setImplementer(String implementer) {
                this.implementer = implementer;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public Date getTime() {
                return time;
            }

            public void setTime(Date time) {
                this.time = time;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }
        }

        public static class Responce {
            private Date time;
            private String author;
            private String decision;
            private String description;
            private String rejectCause;
            private String explanation;


            public Date getTime() {
                return time;
            }

            public void setTime(Date time) {
                this.time = time;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getDecision() {
                return decision;
            }

            public void setDecision(String decision) {
                this.decision = decision;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getRejectCause() {
                return rejectCause;
            }

            public void setRejectCause(String rejectCause) {
                this.rejectCause = rejectCause;
            }

            public String getExplanation() {
                return explanation;
            }

            public void setExplanation(String explanation) {
                this.explanation = explanation;
            }
        }

        private long portalId;
        private String sourceHtml;
        private Request request;
        private Responce responce;

        public long getPortalId() {
            return portalId;
        }

        public void setPortalId(long portalId) {
            this.portalId = portalId;
        }

        public String getSourceHtml() {
            return sourceHtml;
        }

        public void setSourceHtml(String sourceHtml) {
            this.sourceHtml = sourceHtml;
        }

        public Request getRequest() {
            return request;
        }

        public void setRequest(Request request) {
            this.request = request;
        }

        public Responce getResponce() {
            return responce;
        }

        public void setResponce(Responce responce) {
            this.responce = responce;
        }
    }

    private String html;
    private List<SiteDiscussion> discussions = new ArrayList<>();

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public List<SiteDiscussion> getDiscussions() {
        return discussions;
    }
}

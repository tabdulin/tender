package kz.inessoft.egz.ejb.announcement.http;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 19.09.16.
 */
public class SiteDocumentsInfo {
    public static class SiteDocument {
        private String name;
        private boolean required;
        private String sourceHtml;
        private List<ASiteFileInfo> portalFiles = new ArrayList<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isRequired() {
            return required;
        }

        public void setRequired(boolean required) {
            this.required = required;
        }

        public String getSourceHtml() {
            return sourceHtml;
        }

        public void setSourceHtml(String sourceHtml) {
            this.sourceHtml = sourceHtml;
        }

        public List<ASiteFileInfo> getPortalFiles() {
            return portalFiles;
        }
    }

    private String html;
    private List<SiteDocument> siteDocuments = new ArrayList<>();

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public List<SiteDocument> getSiteDocuments() {
        return siteDocuments;
    }
}

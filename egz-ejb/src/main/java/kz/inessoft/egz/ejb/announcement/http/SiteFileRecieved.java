package kz.inessoft.egz.ejb.announcement.http;

/**
 * Created by alexey on 20.09.16.
 */
public class SiteFileRecieved extends ASiteFileInfo {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

package kz.inessoft.egz.ejb.announcement.http;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 11.08.16.
 */
public class SiteImplementer {
    private String lotNumber;
    private String xin;
    private String name;
    private String requisites;
    private Date proposalTime;
    private Boolean accessGranted;
    private String deniedCause;
    private Double implementerPrice;
    private Double act26Price;
    private Double finalPrice;
    private boolean winnerFlag = false;
    private List<SiteDiscount> discounts = new ArrayList<>();

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getXin() {
        return xin;
    }

    public void setXin(String xin) {
        this.xin = xin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequisites() {
        return requisites;
    }

    public void setRequisites(String requisites) {
        this.requisites = requisites;
    }

    public Date getProposalTime() {
        return proposalTime;
    }

    public void setProposalTime(Date proposalTime) {
        this.proposalTime = proposalTime;
    }

    public Boolean getAccessGranted() {
        return accessGranted;
    }

    public void setAccessGranted(Boolean accessGranted) {
        this.accessGranted = accessGranted;
    }

    public String getDeniedCause() {
        return deniedCause;
    }

    public void setDeniedCause(String deniedCause) {
        this.deniedCause = deniedCause;
    }

    public Double getImplementerPrice() {
        return implementerPrice;
    }

    public void setImplementerPrice(Double implementerPrice) {
        this.implementerPrice = implementerPrice;
    }

    public Double getAct26Price() {
        return act26Price;
    }

    public void setAct26Price(Double act26Price) {
        this.act26Price = act26Price;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public boolean isWinnerFlag() {
        return winnerFlag;
    }

    public void setWinnerFlag(boolean winnerFlag) {
        this.winnerFlag = winnerFlag;
    }

    public List<SiteDiscount> getDiscounts() {
        return discounts;
    }
}

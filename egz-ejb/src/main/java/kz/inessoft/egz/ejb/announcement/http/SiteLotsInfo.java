package kz.inessoft.egz.ejb.announcement.http;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 19.09.16.
 */
public class SiteLotsInfo {
    public static class SiteLot {
        private long portalId;
        private long planId;
        private String number;
        private String status;
        private String customerBin;
        private String customerName;
        private String truCode;
        private String truName;
        private String shortCharacteristicRu;
        private String additionalCharacteristicRu;
        private String paymentSourceRu;
        private Double priceForUnit;
        private Double count;
        private String unit;
        private Double firstYearSum;
        private Double secondYearSum;
        private Double thirdYearSum;
        private Double totalSum;
        private Double prepayment;
        private String placement;
        private String deliveryTime;
        private String incoterms;
        private Boolean ingeneering;
        private String demping;
        private String customerFIO;
        private String customerJobPosition;
        private String customerPhone;
        private String customerEmail;
        private String customerBankRequisites;
        private String originalHtml;

        public long getPortalId() {
            return portalId;
        }

        public void setPortalId(long portalId) {
            this.portalId = portalId;
        }

        public long getPlanId() {
            return planId;
        }

        public void setPlanId(long planId) {
            this.planId = planId;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCustomerBin() {
            return customerBin;
        }

        public void setCustomerBin(String customerBin) {
            this.customerBin = customerBin;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getTruCode() {
            return truCode;
        }

        public void setTruCode(String truCode) {
            this.truCode = truCode;
        }

        public String getTruName() {
            return truName;
        }

        public void setTruName(String truName) {
            this.truName = truName;
        }

        public String getShortCharacteristicRu() {
            return shortCharacteristicRu;
        }

        public void setShortCharacteristicRu(String shortCharacteristicRu) {
            this.shortCharacteristicRu = shortCharacteristicRu;
        }

        public String getAdditionalCharacteristicRu() {
            return additionalCharacteristicRu;
        }

        public void setAdditionalCharacteristicRu(String additionalCharacteristicRu) {
            this.additionalCharacteristicRu = additionalCharacteristicRu;
        }

        public String getPaymentSourceRu() {
            return paymentSourceRu;
        }

        public void setPaymentSourceRu(String paymentSourceRu) {
            this.paymentSourceRu = paymentSourceRu;
        }

        public Double getPriceForUnit() {
            return priceForUnit;
        }

        public void setPriceForUnit(Double priceForUnit) {
            this.priceForUnit = priceForUnit;
        }

        public Double getCount() {
            return count;
        }

        public void setCount(Double count) {
            this.count = count;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public Double getFirstYearSum() {
            return firstYearSum;
        }

        public void setFirstYearSum(Double firstYearSum) {
            this.firstYearSum = firstYearSum;
        }

        public Double getSecondYearSum() {
            return secondYearSum;
        }

        public void setSecondYearSum(Double secondYearSum) {
            this.secondYearSum = secondYearSum;
        }

        public Double getThirdYearSum() {
            return thirdYearSum;
        }

        public void setThirdYearSum(Double thirdYearSum) {
            this.thirdYearSum = thirdYearSum;
        }

        public Double getTotalSum() {
            return totalSum;
        }

        public void setTotalSum(Double totalSum) {
            this.totalSum = totalSum;
        }

        public Double getPrepayment() {
            return prepayment;
        }

        public void setPrepayment(Double prepayment) {
            this.prepayment = prepayment;
        }

        public String getPlacement() {
            return placement;
        }

        public void setPlacement(String placement) {
            this.placement = placement;
        }

        public String getDeliveryTime() {
            return deliveryTime;
        }

        public void setDeliveryTime(String deliveryTime) {
            this.deliveryTime = deliveryTime;
        }

        public String getIncoterms() {
            return incoterms;
        }

        public void setIncoterms(String incoterms) {
            this.incoterms = incoterms;
        }

        public Boolean getIngeneering() {
            return ingeneering;
        }

        public void setIngeneering(Boolean ingeneering) {
            this.ingeneering = ingeneering;
        }

        public String getDemping() {
            return demping;
        }

        public void setDemping(String demping) {
            this.demping = demping;
        }

        public String getCustomerFIO() {
            return customerFIO;
        }

        public void setCustomerFIO(String customerFIO) {
            this.customerFIO = customerFIO;
        }

        public String getCustomerJobPosition() {
            return customerJobPosition;
        }

        public void setCustomerJobPosition(String customerJobPosition) {
            this.customerJobPosition = customerJobPosition;
        }

        public String getCustomerPhone() {
            return customerPhone;
        }

        public void setCustomerPhone(String customerPhone) {
            this.customerPhone = customerPhone;
        }

        public String getCustomerEmail() {
            return customerEmail;
        }

        public void setCustomerEmail(String customerEmail) {
            this.customerEmail = customerEmail;
        }

        public String getCustomerBankRequisites() {
            return customerBankRequisites;
        }

        public void setCustomerBankRequisites(String customerBankRequisites) {
            this.customerBankRequisites = customerBankRequisites;
        }

        public String getOriginalHtml() {
            return originalHtml;
        }

        public void setOriginalHtml(String originalHtml) {
            this.originalHtml = originalHtml;
        }
    }

    private List<String> htmls = new ArrayList<>();
    private List<SiteLot> lots = new ArrayList<>();

    public List<String> getHtmls() {
        return htmls;
    }

    public List<SiteLot> getLots() {
        return lots;
    }
}

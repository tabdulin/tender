package kz.inessoft.egz.ejb.announcement.http;

/**
 * Created by alexey on 19.09.16.
 */
public class SiteProposalDocument {
    private ASiteFileInfo portalFile;
    private String documentName;

    public ASiteFileInfo getPortalFile() {
        return portalFile;
    }

    public void setPortalFile(ASiteFileInfo portalFile) {
        this.portalFile = portalFile;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }
}

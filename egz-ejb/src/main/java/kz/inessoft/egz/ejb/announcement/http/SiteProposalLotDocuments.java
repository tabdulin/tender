package kz.inessoft.egz.ejb.announcement.http;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 25.05.17.
 */
public class SiteProposalLotDocuments {
    private String lotNumber;
    private String html;
    private List<SiteProposalDocument> proposalDocuments = new ArrayList<>();

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public List<SiteProposalDocument> getProposalDocuments() {
        return proposalDocuments;
    }
}

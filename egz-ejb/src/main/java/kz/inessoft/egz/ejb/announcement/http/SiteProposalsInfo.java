package kz.inessoft.egz.ejb.announcement.http;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 19.09.16.
 */
public class SiteProposalsInfo {
    public static class SiteProposal {
        private long portalId;
        private String status;
        private Date proposalTime;
        private String implementerXin;
        private String implementerName;
        private String implementerAddress;
        private String bankName;
        private String iik;
        private String bik;
        private String kbe;
        private String implementerFIO;
        private String phone;
        private String jobPosition;
        private boolean consorcium;
        private List<String> lotNumbers = new ArrayList<>();
        private List<SiteProposalLotDocuments> proposalLotDocuments = new ArrayList<>();
        private String generalInfoTabHtml;
        private String lotsInfoTabHtml;
        private String docsInfoTabHtml;

        public long getPortalId() {
            return portalId;
        }

        public void setPortalId(long portalId) {
            this.portalId = portalId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Date getProposalTime() {
            return proposalTime;
        }

        public void setProposalTime(Date proposalTime) {
            this.proposalTime = proposalTime;
        }

        public String getImplementerXin() {
            return implementerXin;
        }

        public void setImplementerXin(String implementerXin) {
            this.implementerXin = implementerXin;
        }

        public String getImplementerName() {
            return implementerName;
        }

        public void setImplementerName(String implementerName) {
            this.implementerName = implementerName;
        }

        public String getImplementerAddress() {
            return implementerAddress;
        }

        public void setImplementerAddress(String implementerAddress) {
            this.implementerAddress = implementerAddress;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getIik() {
            return iik;
        }

        public void setIik(String iik) {
            this.iik = iik;
        }

        public String getBik() {
            return bik;
        }

        public void setBik(String bik) {
            this.bik = bik;
        }

        public String getKbe() {
            return kbe;
        }

        public void setKbe(String kbe) {
            this.kbe = kbe;
        }

        public String getImplementerFIO() {
            return implementerFIO;
        }

        public void setImplementerFIO(String implementerFIO) {
            this.implementerFIO = implementerFIO;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getJobPosition() {
            return jobPosition;
        }

        public void setJobPosition(String jobPosition) {
            this.jobPosition = jobPosition;
        }

        public boolean isConsorcium() {
            return consorcium;
        }

        public void setConsorcium(boolean consorcium) {
            this.consorcium = consorcium;
        }

        public List<String> getLotNumbers() {
            return lotNumbers;
        }

        public List<SiteProposalLotDocuments> getProposalLotDocuments() {
            return proposalLotDocuments;
        }

        public String getGeneralInfoTabHtml() {
            return generalInfoTabHtml;
        }

        public void setGeneralInfoTabHtml(String generalInfoTabHtml) {
            this.generalInfoTabHtml = generalInfoTabHtml;
        }

        public String getLotsInfoTabHtml() {
            return lotsInfoTabHtml;
        }

        public void setLotsInfoTabHtml(String lotsInfoTabHtml) {
            this.lotsInfoTabHtml = lotsInfoTabHtml;
        }

        public String getDocsInfoTabHtml() {
            return docsInfoTabHtml;
        }

        public void setDocsInfoTabHtml(String docsInfoTabHtml) {
            this.docsInfoTabHtml = docsInfoTabHtml;
        }
    }

    private String html;
    private List<SiteProposal> siteProposals = new ArrayList<>();

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public List<SiteProposal> getSiteProposals() {
        return siteProposals;
    }
}

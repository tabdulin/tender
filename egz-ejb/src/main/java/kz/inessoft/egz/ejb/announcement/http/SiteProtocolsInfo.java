package kz.inessoft.egz.ejb.announcement.http;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 19.09.16.
 */
public class SiteProtocolsInfo {
    public static class SiteProtocolFileInfo {
        private EProtocolType protocolType;
        private ASiteFileInfo portalFile;

        public EProtocolType getProtocolType() {
            return protocolType;
        }

        public void setProtocolType(EProtocolType protocolType) {
            this.protocolType = protocolType;
        }

        public ASiteFileInfo getPortalFile() {
            return portalFile;
        }

        public void setPortalFile(ASiteFileInfo portalFile) {
            this.portalFile = portalFile;
        }
    }

    private String html;
    private List<SiteProtocolFileInfo> protocolFileInfos = new ArrayList<>();

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public List<SiteProtocolFileInfo> getProtocolFileInfos() {
        return protocolFileInfos;
    }
}

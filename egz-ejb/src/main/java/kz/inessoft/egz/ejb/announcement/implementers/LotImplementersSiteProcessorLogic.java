package kz.inessoft.egz.ejb.announcement.implementers;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteImplementer;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 11.08.16.
 */
@Local
public interface LotImplementersSiteProcessorLogic {
    void saveImplementersInfo(TAnnouncement announcement, List<SiteImplementer> siteImplementers);
}

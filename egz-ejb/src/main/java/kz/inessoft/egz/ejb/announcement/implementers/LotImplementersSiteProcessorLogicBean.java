package kz.inessoft.egz.ejb.announcement.implementers;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDiscount;
import kz.inessoft.egz.dbentities.TImplementerLot;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.ejbapi.DicConditionalDiscountLogic;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscount;
import kz.inessoft.egz.ejb.announcement.http.SiteImplementer;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 11.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class LotImplementersSiteProcessorLogicBean implements LotImplementersSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicImplementerLogic dicImplementerLogic;

    @EJB
    private DicConditionalDiscountLogic dicConditionalDiscountLogic;

    @Override
    public void saveImplementersInfo(TAnnouncement announcement, List<SiteImplementer> siteImplementers) {
        // Отберем из БД все лоты,которые упоминаются во входных данных, а так же удалим старые данные из БД
        Query qSelect = em.createQuery("from TLotCrossAnnouncement cross where cross.id.announcement=:announcement and cross.lotNumber=:number");
        Query qDelete = em.createQuery("delete from TImplementerLot where lotCrossAnnouncement=:lotCrossAnnouncement");
        Map<String, TLotCrossAnnouncement> cache = new HashMap<>();
        for (SiteImplementer siteImplementer : siteImplementers) {
            String lotNumber = siteImplementer.getLotNumber();
            if (cache.containsKey(lotNumber))
                continue;
            TLotCrossAnnouncement lotCrossAnnouncement = (TLotCrossAnnouncement) qSelect.setParameter("number", lotNumber)
                    .setParameter("announcement", announcement)
                    .getResultList()
                    .get(0);
            cache.put(lotNumber, lotCrossAnnouncement);
            qDelete.setParameter("lotCrossAnnouncement", lotCrossAnnouncement).executeUpdate();
        }

        for (SiteImplementer siteImplementer : siteImplementers) {
            TImplementerLot tImplementerLot = new TImplementerLot();
            tImplementerLot.setWinnerFlag(siteImplementer.isWinnerFlag());
            tImplementerLot.setAccessGranted(siteImplementer.getAccessGranted());
            if (Boolean.FALSE.equals(tImplementerLot.getAccessGranted()))
                tImplementerLot.setAccessDeniedCause(siteImplementer.getDeniedCause());
            tImplementerLot.setImplementer(dicImplementerLogic.getValueByXin(siteImplementer.getXin(), siteImplementer.getName()));
            tImplementerLot.setProposalTime(siteImplementer.getProposalTime());
            tImplementerLot.setRequestedPrice(siteImplementer.getImplementerPrice());
            tImplementerLot.setAct26Price(siteImplementer.getAct26Price());
            tImplementerLot.setFinalPrice(siteImplementer.getFinalPrice());
            tImplementerLot.setRequisites(siteImplementer.getRequisites());
            tImplementerLot.setLotCrossAnnouncement(cache.get(siteImplementer.getLotNumber()));
            em.persist(tImplementerLot);
            for (SiteDiscount siteDiscount : siteImplementer.getDiscounts()) {
                TDiscount discount = new TDiscount();
                try {
                    discount.setAmount(Double.valueOf(siteDiscount.getDiscountValue()));
                } catch (NumberFormatException e) {
                    discount.setStringAmount(siteDiscount.getDiscountValue());
                }
                discount.setImplementerLot(tImplementerLot);
                discount.setConditionalDiscount(dicConditionalDiscountLogic.getValueByName(siteDiscount.getDiscountName()));
                em.persist(discount);
            }
        }
    }
}

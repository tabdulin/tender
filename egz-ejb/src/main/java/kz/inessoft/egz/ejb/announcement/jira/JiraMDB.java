package kz.inessoft.egz.ejb.announcement.jira;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Attachment;
import com.atlassian.jira.rest.client.api.domain.BasicIssue;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.IssueFieldId;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.TransitionInput;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import kz.inessoft.egz.dbentities.JiraInfo;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.ejb.announcement.AnnouncementLoadMDB;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.DocumentLogic;
import kz.inessoft.egz.ejbapi.ESysParam;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import kz.inessoft.egz.ejbapi.SysConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexey on 28.01.17.
 */
@MessageDriven(mappedName = "java:/jms/queue/JiraQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/JiraQueue"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5")
})
public class JiraMDB implements MessageListener {
    static final String ANNOUNSEMENT_PORTAL_ID = "AnnouncementPortalId";

    private static final String TENDER_NUMBER = "customfield_12500";
    private static final String CUSTOMER = "customfield_12602";
    private static final String PORTAL = "customfield_12606";
    private static final String DOCUMENTATION = "customfield_12600";
    private static final String PUB_DATE = "customfield_12601";
    private static final String END_ADDITIONS_RECV = "customfield_12603";
    private static final String END_DISCUSSION_TIME = "customfield_12604";
    private static final String END_RECV_TIME = "customfield_12605";
    private static final String STATUS = "customfield_12607";
    private static final String COST = "customfield_12608";

    private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementLoadMDB.class);

    @Resource
    private MessageDrivenContext mdc;

    @EJB
    private AnnouncementLogic announcementLogic;

    @EJB
    private SysConfig sysConfig;

    @EJB
    private DocumentLogic documentLogic;

    @EJB
    private PortalFilesLogic portalFilesLogic;

    @Override
    public void onMessage(Message inMsg) {
        long portalAnnouncementId = 0;
        try {
            portalAnnouncementId = inMsg.getLongProperty(ANNOUNSEMENT_PORTAL_ID);
            TAnnouncement announcement = announcementLogic.getAnnouncementByPortalId(portalAnnouncementId, false);
            double amount = announcementLogic.getAnnouncementAmount(announcement.getId());
            List<TPortalFile> jiraFiles = documentLogic.getJiraFiles(announcement.getId());
            if (announcement.getJiraInfo() == null) {
                registerToJira(announcement, amount, jiraFiles);
            } else {
                checkAndUpdate(announcement, amount, jiraFiles);
            }
        } catch (Exception e) {
            LOGGER.error("Error while JIRA process announcement with portal id {}", portalAnnouncementId, e);
            mdc.setRollbackOnly();
        }
    }

    private void registerToJira(TAnnouncement announcement, double amount, List<TPortalFile> files) {
        JiraRestClient restClient = null;
        try {
            restClient = getRestClient();
            IssueInput issueInput = getIssueInput(announcement, amount);
            IssueRestClient issueClient = restClient.getIssueClient();
            BasicIssue basicIssue = issueClient.createIssue(issueInput).get();
            JiraInfo jiraInfo = new JiraInfo();
            announcement.setJiraInfo(jiraInfo);
            jiraInfo.setId(basicIssue.getId());
            jiraInfo.setKey(basicIssue.getKey());

            processStatusAndAttachments(announcement, files, issueClient);
        } catch (URISyntaxException | InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        } finally {
            if (restClient != null)
                try {
                    restClient.close();
                } catch (IOException e) {
                    LOGGER.error("Error while close jira client", e);
                }
        }
    }

    private void checkAndUpdate(TAnnouncement announcement, double amount, List<TPortalFile> files) {
        JiraRestClient restClient = null;
        try {
            restClient = getRestClient();
            IssueRestClient issueClient = restClient.getIssueClient();
            IssueInput issueInput = getIssueInput(announcement, amount);
            issueClient.updateIssue(announcement.getJiraInfo().getKey(), issueInput).get();

            processStatusAndAttachments(announcement, files, issueClient);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (restClient != null)
                try {
                    restClient.close();
                } catch (IOException e) {
                    LOGGER.error("Error while close jira restClient", e);
                }
        }
    }

    /**
     * Т.к. этот метод вызывается когда в джире уже зарегистрирован issue, то он не должен выкидывать исключений. Иначе произойдет
     * откат транзакции, изменения не сядут в БД, а в Jira останется зареганный issue. Лучше пусть не поменяется статус, либо не прикрепятся
     * аттачменты. Но вообще тут еще надо подумать...
     * @param announcement
     * @param files
     * @param issueClient
     */
    private void processStatusAndAttachments(TAnnouncement announcement, List<TPortalFile> files, IssueRestClient issueClient) {
        try {
            Issue issue = issueClient.getIssue(announcement.getJiraInfo().getKey()).get();
            try {
                setStatus(issue, announcement, issueClient);
            } catch (Exception e) {
                LOGGER.error("Error while set status for issue for announcement {}", announcement.getPortalId(), e);
            }

            try {
                List<TPortalFile> tmpFiles = new LinkedList<>();
                List<Attachment> toRemove = new LinkedList<>();
                tmpFiles.addAll(files);
                for (Attachment attachment : issue.getAttachments()) {
                    boolean found = false;
                    List<TPortalFile> tmpFiles1 = new LinkedList<>();
                    tmpFiles1.addAll(tmpFiles);
                    for (TPortalFile tmpFile : tmpFiles1) {
                        if (tmpFile.getFileName().equals(attachment.getFilename()) && tmpFile.getFileSize() == attachment.getSize()) {
                            found = true;
                            tmpFiles.remove(tmpFile);
                        }
                    }
                    if (!found)
                        toRemove.add(attachment);
                }
                if (!tmpFiles.isEmpty()) {
                    // т.к. на портале гз. часто крепят один и тот же файл несколько раз к каждому лоту, то из оставшегося
                    // списка уберем дублирующиеся по имени и размеру файлы, оставив только одну копию
                    List<TPortalFile> result = new LinkedList<>();
                    for (TPortalFile tmpFile : tmpFiles) {
                        boolean foundFlag = false;
                        for (TPortalFile portalFile : result) {
                            if (portalFile.getFileName().equals(tmpFile.getFileName()) && portalFile.getFileSize() == tmpFile.getFileSize()) {
                                foundFlag = true;
                                break;
                            }
                        }
                        if (!foundFlag)
                            result.add(tmpFile);
                    }
                    for (TPortalFile portalFile : result)
                        portalFilesLogic.processFileContent(portalFile.getId(),
                                contentStream -> issueClient.addAttachment(issue.getAttachmentsUri(), contentStream, portalFile.getFileName()).get());
                }
            } catch (Exception e) {
                LOGGER.error("Error while upload attachements to jira for announcement {}", announcement.getPortalId(), e);
            }
        } catch (Exception e) {
            LOGGER.error("Error while get issue from jira for announcement {}", announcement.getPortalId(), e);
        }
    }

    private void setStatus(Issue issue, TAnnouncement announcement, IssueRestClient issueClient) throws InterruptedException, ExecutionException {
        // Двинем статус если возможно.
        // Сначала проверим какой текущий статус, возможно он уже актуальный и двигать его не надо.
        if (!announcement.getAnnouncementStatus().getNameRu().equals(issue.getStatus().getDescription())) {
            for (Transition next : issueClient.getTransitions(issue).get()) {
                if (next.getName().equals(announcement.getAnnouncementStatus().getNameRu())) {
                    issueClient.transition(issue, new TransitionInput(next.getId()));
                    break;
                }
            }
        }
    }

    private IssueInput getIssueInput(TAnnouncement announcement, double amount) {
        String portalUrl = PortalUrlUtils.createAnnouncementUrl(announcement.getPortalId());
        IssueInputBuilder builder = new IssueInputBuilder()
                .setProjectKey(sysConfig.getSysParamValue(ESysParam.JIRA_PROJECT))
                .setIssueTypeId(Long.valueOf(sysConfig.getSysParamValue(ESysParam.JIRA_ISSUE_TYPE_ID)))
                .setSummary(getSummary(announcement))
                .setFieldValue(TENDER_NUMBER, announcement.getNumber())
                .setFieldValue(CUSTOMER, getOrganizator(announcement))
                .setFieldValue(PORTAL, portalUrl)
                .setFieldValue(DOCUMENTATION, "https://tenders.inessoft.kz/egz/files.xhtml?announcePortalId=" + announcement.getPortalId())
                .setFieldValue(PUB_DATE, getDate(announcement.getPublicationDate()))
                .setFieldValue(END_RECV_TIME, getDate(announcement.getEndRecvTime()))
                .setFieldValue(STATUS, announcement.getAnnouncementStatus().getNameRu())
                .setFieldValue(COST, amount);
        if (announcement.getEndRecvAppendixTime() != null)
            builder.setFieldValue(END_ADDITIONS_RECV, getDate(announcement.getEndRecvAppendixTime()));
        if (announcement.getEndDiscussionTime() != null)
            builder.setFieldValue(END_DISCUSSION_TIME, getDate(announcement.getEndDiscussionTime()));
        String sb = announcement.getNameRu() + "\n\n";
        for (TLotCrossAnnouncement lotCrossAnnouncement : announcement.getLotCrossAnnouncements()) {
            sb += "Номер лота: " + lotCrossAnnouncement.getLotNumber() + "\n" +
                    "Название лота (ТРУ): " + lotCrossAnnouncement.getId().getLot().getDicTru().getNameRu() + "\n" +
                    "Краткая характеристика: " + lotCrossAnnouncement.getId().getLot().getShortCharacteristicRu() + "\n" +
                    "Сумма лота: " + getFormatedAmount(lotCrossAnnouncement.getId().getLot().getTotalSum()) + "\n\n";
        }
        builder.setFieldInput(new FieldInput(IssueFieldId.DESCRIPTION_FIELD, sb));
        return builder.build();
    }

    private JiraRestClient getRestClient() throws URISyntaxException {
        AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        return factory.createWithBasicHttpAuthentication(
                new URI(sysConfig.getSysParamValue(ESysParam.JIRA_URL)),
                sysConfig.getSysParamValue(ESysParam.JIRA_LOGIN),
                sysConfig.getSysParamValue(ESysParam.JIRA_PASSWORD)
        );
    }

    private String getDate(Date date) {
        if (date == null)
            return null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        return sdf.format(date);
    }

    private String getFormatedAmount(double amount) {
        return new DecimalFormat("#.00").format(amount);
    }

    private String getSummary(TAnnouncement announcement) {
        if (announcement.getShortNameRu().length() < 255)
            return announcement.getShortNameRu();
        else
            return announcement.getShortNameRu().substring(0, 254);
    }

    private String getOrganizator(TAnnouncement announcement) {
        if (announcement.getOrganizator().getNameRu().length() < 255)
            return announcement.getOrganizator().getNameRu();
        else
            return announcement.getOrganizator().getNameRu().substring(0, 254);
    }
}

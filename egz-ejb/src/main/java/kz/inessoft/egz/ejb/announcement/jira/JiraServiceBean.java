package kz.inessoft.egz.ejb.announcement.jira;

import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.announcement.AnnouncementSiteProcessorLogic;
import kz.inessoft.egz.ejbapi.JiraService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.jms.*;
import java.util.List;

/**
 * Created by alexey on 27.01.17.
 */
@Singleton
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class JiraServiceBean implements JiraService {
    public static final Logger LOGGER = LoggerFactory.getLogger(JiraServiceBean.class);

    @Resource(lookup = "java:/jms/queue/JiraQueue")
    private Queue queue;

    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;

    @EJB
    private AnnouncementSiteProcessorLogic siteProcessorLogic;

    @Override
    public void createOrUpdateIssue(long announcementPortalId) throws JMSException {
        Message message = jmsContext.createMessage();
        message.setStringProperty(JMSUtils.MESSAGE_TYPE_PROPERTY_NAME, JMSUtils.JIRA_MESSAGE);
        message.setLongProperty(JiraMDB.ANNOUNSEMENT_PORTAL_ID, announcementPortalId);
        jmsContext.createProducer().send(queue, message);
    }

    @Override
    public void registerMonitored() throws JMSException {
        List<Long> portalIds = siteProcessorLogic.getMonitoredAnnouncementsPortalIds();
        for (Long portalId : portalIds) {
            createOrUpdateIssue(portalId);
        }
    }
}

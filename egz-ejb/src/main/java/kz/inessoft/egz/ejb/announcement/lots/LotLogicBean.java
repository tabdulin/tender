package kz.inessoft.egz.ejb.announcement.lots;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus_;
import kz.inessoft.egz.dbentities.DicGovOrganization_;
import kz.inessoft.egz.dbentities.DicImplementer_;
import kz.inessoft.egz.dbentities.DicLotStatus_;
import kz.inessoft.egz.dbentities.DicTru_;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TAnnouncementLotsTabHtml;
import kz.inessoft.egz.dbentities.TAnnouncementLotsTabHtml_;
import kz.inessoft.egz.dbentities.TAnnouncement_;
import kz.inessoft.egz.dbentities.TImplementerLot;
import kz.inessoft.egz.dbentities.TImplementerLot_;
import kz.inessoft.egz.dbentities.TLot;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement_;
import kz.inessoft.egz.dbentities.TLot_;
import kz.inessoft.egz.ejbapi.LotFilter;
import kz.inessoft.egz.ejbapi.LotLogic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class LotLogicBean implements LotLogic {
    private static final Logger LOGGER = LoggerFactory.getLogger(LotLogicBean.class);

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    public int getLotsListPagesCount(long announcemenId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<TAnnouncementLotsTabHtml> root = query.from(TAnnouncementLotsTabHtml.class);
        query.select(builder.count(root));
        query.where(builder.equal(
                root.get(TAnnouncementLotsTabHtml_.announcement).get(TAnnouncement_.id), announcemenId
        ));
        return em.createQuery(query).getSingleResult().intValue();
    }

    @Override
    public String getLotsListHtml(long announcemenId, int pageNumber) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<TAnnouncementLotsTabHtml> root = query.from(TAnnouncementLotsTabHtml.class);
        query.select(root.get(TAnnouncementLotsTabHtml_.html));
        query.where(builder.and(
                builder.equal(root.get(TAnnouncementLotsTabHtml_.announcement).get(TAnnouncement_.id), announcemenId),
                builder.equal(root.get(TAnnouncementLotsTabHtml_.pageNumber), pageNumber)
        ));
        return em.createQuery(query).getSingleResult();
    }

    @Override
    public List<TLotCrossAnnouncement> getLots(LotFilter filter, Integer maxResults, Integer firstResult) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TLotCrossAnnouncement> criteriaQuery = builder.createQuery(TLotCrossAnnouncement.class);
        Root<TLotCrossAnnouncement> root = criteriaQuery.from(TLotCrossAnnouncement.class);

        Path<TAnnouncement> announcementPath = root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement);
        Path<Date> publicationDatePath = announcementPath.get(TAnnouncement_.publicationDate);
        Path<TLot> lotPath = root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.lot);

        setFilter(filter, builder, criteriaQuery, root);

        if (filter.getOrderByList() != null) {
            List<Order> orderList = new LinkedList<>();
            for (LotFilter.OrderBy orderBy : filter.getOrderByList()) {
                switch (orderBy.getOrderByColumn()) {
                    case PublicationDate:
                        orderList.add(orderBy.isAsc() ? builder.asc(publicationDatePath) : builder.desc(publicationDatePath));
                        break;
                    case Price:
                        Path<Double> pricePath = lotPath.get(TLot_.priceForUnit);
                        orderList.add(orderBy.isAsc() ? builder.asc(pricePath) : builder.desc(pricePath));
                        break;
                    case Customer:
                        Path<String> nameRuPath = lotPath.get(TLot_.customer).get(DicGovOrganization_.nameRu);
                        orderList.add(orderBy.isAsc() ? builder.asc(nameRuPath) : builder.desc(nameRuPath));
                        break;
                    case AnnoucementNumber:
                        Path<String> numberPath = announcementPath.get(TAnnouncement_.number);
                        orderList.add(orderBy.isAsc() ? builder.asc(numberPath) : builder.desc(numberPath));
                        break;
                    case AnnouncementName:
                        Path<String> namePath = announcementPath.get(TAnnouncement_.nameRu);
                        orderList.add(orderBy.isAsc() ? builder.asc(namePath) : builder.desc(namePath));
                        break;
                    case LotNumber:
                        Path<String> lotNumberPath = root.get(TLotCrossAnnouncement_.lotNumber);
                        orderList.add(orderBy.isAsc() ? builder.asc(lotNumberPath) : builder.desc(lotNumberPath));
                        break;
                    case LotStatus:
                        Path<String> lotStatusPath = root.get(TLotCrossAnnouncement_.status).get(DicLotStatus_.nameRu);
                        orderList.add(orderBy.isAsc() ? builder.asc(lotStatusPath) : builder.desc(lotStatusPath));
                        break;
                    case AnnouncementStatus:
                        Path<String> announcementStatusNamePath = announcementPath.get(TAnnouncement_.announcementStatus).get(DicAnnouncementStatus_.nameRu);
                        orderList.add(orderBy.isAsc() ? builder.asc(announcementStatusNamePath) : builder.desc(announcementStatusNamePath));
                        break;
                    case StartRecvTime:
                        Path<Date> startRecvPath = announcementPath.get(TAnnouncement_.startRecvTime);
                        orderList.add(orderBy.isAsc() ? builder.asc(startRecvPath) : builder.desc(startRecvPath));
                        break;
                    case EndRecvTime:
                        Path<Date> endRecvPath = announcementPath.get(TAnnouncement_.endRecvTime);
                        orderList.add(orderBy.isAsc() ? builder.asc(endRecvPath) : builder.desc(endRecvPath));
                        break;
                    case ShortCharacteristic:
                        Path<String> shortCharacteristicPath = lotPath.get(TLot_.shortCharacteristicRu);
                        orderList.add(orderBy.isAsc() ? builder.asc(shortCharacteristicPath) : builder.desc(shortCharacteristicPath));
                        break;
                    case AdditionalCharacteristic:
                        Path<String> additionalCharacteristicPath = lotPath.get(TLot_.additionalCharacteristicRu);
                        orderList.add(orderBy.isAsc() ? builder.asc(additionalCharacteristicPath) : builder.desc(additionalCharacteristicPath));
                        break;
                }
                criteriaQuery.orderBy(orderList);
            }
        }
        TypedQuery<TLotCrossAnnouncement> query = em.createQuery(criteriaQuery);
        if (maxResults != null)
            query.setMaxResults(maxResults);
        if (firstResult != null)
            query.setFirstResult(firstResult);
        return query.getResultList();
    }

    @Override
    public long countLots(LotFilter filter) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<TLotCrossAnnouncement> root = criteriaQuery.from(TLotCrossAnnouncement.class);
        criteriaQuery.select(criteriaBuilder.count(root));
        setFilter(filter, criteriaBuilder, criteriaQuery, root);
        return em.createQuery(criteriaQuery).getSingleResult();
    }

    private void setFilter(LotFilter filter, CriteriaBuilder criteriaBuilder,
                           CriteriaQuery criteriaQuery, Root<TLotCrossAnnouncement> root) {
        Path<TAnnouncement> announcementPath = root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement);
        Path<Date> publicationDatePath = announcementPath.get(TAnnouncement_.publicationDate);
        Path<TLot> lotPath = root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.lot);
        List<Predicate> predicates = new LinkedList<>();
        if (filter.getNotBefore() != null) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(publicationDatePath, filter.getNotBefore()));
        }

        if (filter.getTruCodesMask() != null && !filter.getTruCodesMask().isEmpty()) {
            List<Predicate> likes = new LinkedList<>();
            for (String like : filter.getTruCodesMask()) {
                likes.add(criteriaBuilder.like(lotPath.get(TLot_.dicTru).get(DicTru_.code), like + "%"));
            }
            predicates.add(criteriaBuilder.or(likes.toArray(new Predicate[likes.size()])));
        }

        if (filter.getStatuses() != null && !filter.getStatuses().isEmpty()) {
            predicates.add(announcementPath.get(TAnnouncement_.announcementStatus).in(filter.getStatuses()));
        }

        if (filter.getImplementerId() != null) {
            Subquery<TLotCrossAnnouncement> subquery = criteriaQuery.subquery(TLotCrossAnnouncement.class);
            Root<TImplementerLot> implementerLotRoot = subquery.from(TImplementerLot.class);
            subquery.select(implementerLotRoot.get(TImplementerLot_.lotCrossAnnouncement));
            subquery.where(criteriaBuilder.equal(implementerLotRoot.get(TImplementerLot_.implementer).get(DicImplementer_.id), filter.getImplementerId()));
            predicates.add(root.in(subquery));
        }

        if (filter.getCustomerId() != null) {
            predicates.add(criteriaBuilder.equal(lotPath.get(TLot_.customer).get(DicGovOrganization_.id), filter.getCustomerId()));
        }

        if (filter.getLotNumber() != null) {
            predicates.add(criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.lotNumber), filter.getLotNumber()));
        }

        if (filter.getLotStatusId() != null) {
            predicates.add(criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.status).get(DicLotStatus_.id), filter.getLotStatusId()));
        }

        if (filter.getKeywords() != null && !filter.getKeywords().isEmpty()) {
            List<Predicate> likesShortCh = new LinkedList<>();
            List<Predicate> likesAddCh = new LinkedList<>();
            List<Predicate> likesAnnNameCh = new LinkedList<>();
            for (String keyword : filter.getKeywords()) {
                likesShortCh.add(criteriaBuilder.like(lotPath.get(TLot_.shortCharacteristicRu), "%" + keyword + "%"));
                likesAddCh.add(criteriaBuilder.like(lotPath.get(TLot_.additionalCharacteristicRu), "%" + keyword + "%"));
                likesAnnNameCh.add(criteriaBuilder.like(announcementPath.get(TAnnouncement_.nameRu), "%" + keyword + "%"));
            }
            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.and(likesShortCh.toArray(new Predicate[likesShortCh.size()])),
                    criteriaBuilder.and(likesAddCh.toArray(new Predicate[likesAddCh.size()])),
                    criteriaBuilder.and(likesAnnNameCh.toArray(new Predicate[likesAnnNameCh.size()]))
            ));
        }

        if (filter.isMonitoring()) {
            predicates.add(criteriaBuilder.equal(announcementPath.get(TAnnouncement_.monitoring), Boolean.TRUE));
        }

        criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
    }

    @Override
    public List<TLotCrossAnnouncement> getLotsByAnnouncement(Long announcementId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<TLotCrossAnnouncement> criteriaQuery = criteriaBuilder.createQuery(TLotCrossAnnouncement.class);
        Root<TLotCrossAnnouncement> root = criteriaQuery.from(TLotCrossAnnouncement.class);
        criteriaQuery.where(criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement).get(TAnnouncement_.id), announcementId));
        return em.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public TLotCrossAnnouncement getLotById(Long announcementId, Long lotId) {
        TLotCrossAnnouncementId id = new TLotCrossAnnouncementId();
        id.setAnnouncement(em.getReference(TAnnouncement.class, announcementId));
        id.setLot(em.getReference(TLot.class, lotId));
        return em.find(TLotCrossAnnouncement.class, id);
    }

    @Override
    public TLotCrossAnnouncement getLotByNumber(TAnnouncement announcement, String lotNumber) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<TLotCrossAnnouncement> criteriaQuery = criteriaBuilder.createQuery(TLotCrossAnnouncement.class);
        Root<TLotCrossAnnouncement> root = criteriaQuery.from(TLotCrossAnnouncement.class);
        criteriaQuery.where(criteriaBuilder.and(
                criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement), announcement),
                criteriaBuilder.equal(root.get(TLotCrossAnnouncement_.lotNumber), lotNumber)
        ));
        return em.createQuery(criteriaQuery).getSingleResult();
    }

    public void setLotHasContract(long planUnitId, String announcementNumber) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaUpdate<TLotCrossAnnouncement> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(TLotCrossAnnouncement.class);

        Subquery<TAnnouncement> announcementSubquery = criteriaUpdate.subquery(TAnnouncement.class);
        Root<TAnnouncement> announcementRoot = announcementSubquery.from(TAnnouncement.class);
        announcementSubquery.select(announcementRoot);
        announcementSubquery.where(criteriaBuilder.equal(announcementRoot.get(TAnnouncement_.number), announcementNumber));

        Subquery<TLot> lotSubquery = criteriaUpdate.subquery(TLot.class);
        Root<TLot> lotRoot = lotSubquery.from(TLot.class);
        lotSubquery.select(lotRoot);
        lotSubquery.where(criteriaBuilder.equal(lotRoot.get(TLot_.planUnitId), planUnitId));

        Root<TLotCrossAnnouncement> root = criteriaUpdate.from(TLotCrossAnnouncement.class);
        criteriaUpdate.set(root.get(TLotCrossAnnouncement_.hasContract), true);
        Path<TLotCrossAnnouncementId> idPath = root.get(TLotCrossAnnouncement_.id);
        criteriaUpdate.where(
                idPath.get(TLotCrossAnnouncementId_.announcement).in(announcementSubquery),
                idPath.get(TLotCrossAnnouncementId_.lot).in(lotSubquery)
        );
        int i = em.createQuery(criteriaUpdate).executeUpdate();
        if (i == 0)
            LOGGER.warn("Set lot has contract failed. There is not found lot for announcement with number {} and planUnit {} in DB", announcementNumber, planUnitId);
    }
}

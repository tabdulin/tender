package kz.inessoft.egz.ejb.announcement.lots;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteLotsInfo;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Local
public interface LotSiteProcessorLogic {
    void saveLotsHtmls(TAnnouncement announcement, List<String> htmls);

    void saveLotsInfo(TAnnouncement announcement, List<SiteLotsInfo.SiteLot> siteLots);
}

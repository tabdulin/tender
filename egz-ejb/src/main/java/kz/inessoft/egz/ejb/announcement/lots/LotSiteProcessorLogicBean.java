package kz.inessoft.egz.ejb.announcement.lots;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TAnnouncementLotsTabHtml;
import kz.inessoft.egz.dbentities.TAnnouncementLotsTabHtml_;
import kz.inessoft.egz.dbentities.TLot;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement_;
import kz.inessoft.egz.dbentities.TLotPlacement;
import kz.inessoft.egz.dbentities.TLot_;
import kz.inessoft.egz.ejb.announcement.http.SiteLotsInfo;
import kz.inessoft.egz.ejbapi.DicGovOrganizationLogic;
import kz.inessoft.egz.ejbapi.DicLotStatusLogic;
import kz.inessoft.egz.ejbapi.DicTruLogic;
import kz.inessoft.egz.ejbapi.DicUnitLogic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class LotSiteProcessorLogicBean implements LotSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicGovOrganizationLogic dicGovOrganizationLogic;

    @EJB
    private DicTruLogic dicTruLogic;

    @EJB
    private DicLotStatusLogic dicLotStatusLogic;

    @EJB
    private DicUnitLogic dicUnitLogic;

    public void saveLotsHtmls(TAnnouncement announcement, List<String> htmls) {
        if (announcement.getId() != null) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaDelete<TAnnouncementLotsTabHtml> delete = builder.createCriteriaDelete(TAnnouncementLotsTabHtml.class);
            Root<TAnnouncementLotsTabHtml> root = delete.from(TAnnouncementLotsTabHtml.class);
            delete.where(builder.equal(root.get(TAnnouncementLotsTabHtml_.announcement), announcement));
            em.createQuery(delete).executeUpdate();
        }

        int i = 1;
        for (String html : htmls) {
            TAnnouncementLotsTabHtml lotsTabHtml = new TAnnouncementLotsTabHtml();
            lotsTabHtml.setHtml(html);
            lotsTabHtml.setAnnouncement(announcement);
            lotsTabHtml.setPageNumber(i++);
            em.persist(lotsTabHtml);
        }
    }

    @Override
    public void saveLotsInfo(TAnnouncement announcement, List<SiteLotsInfo.SiteLot> siteLots) {
        if (announcement.getLotCrossAnnouncements() == null)
            announcement.setLotCrossAnnouncements(new ArrayList<>());

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TLotCrossAnnouncement> queryTLotCrossAnnouncement = builder.createQuery(TLotCrossAnnouncement.class);
        Root<TLotCrossAnnouncement> root = queryTLotCrossAnnouncement.from(TLotCrossAnnouncement.class);
        ParameterExpression<String> lotNumberParameter = builder.parameter(String.class);
        queryTLotCrossAnnouncement.where(builder.and(
                builder.equal(root.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement), announcement),
                builder.equal(root.get(TLotCrossAnnouncement_.lotNumber),lotNumberParameter)
        ));
        TypedQuery<TLotCrossAnnouncement> query = em.createQuery(queryTLotCrossAnnouncement);

        for (SiteLotsInfo.SiteLot siteLot : siteLots) {
            TLot lot = getLotByPortalId(siteLot.getPortalId());
            if (lot == null) {
                lot = new TLot();
            } else {
                em.createQuery("delete from TLotPlacement where lot=:lot")
                        .setParameter("lot", lot)
                        .executeUpdate();
            }

            lot.setPortalId(siteLot.getPortalId());
            lot.setPlanUnitId(siteLot.getPlanId());
            lot.setCustomer(dicGovOrganizationLogic.getValueByBin(siteLot.getCustomerBin(), siteLot.getCustomerName(), null));
            lot.setDicTru(dicTruLogic.getTruByCode(siteLot.getTruCode(), siteLot.getTruName()));
            lot.setShortCharacteristicRu(siteLot.getShortCharacteristicRu());
            lot.setAdditionalCharacteristicRu(siteLot.getAdditionalCharacteristicRu());
            lot.setPaymentSourceRu(siteLot.getPaymentSourceRu());
            lot.setPriceForUnit(siteLot.getPriceForUnit());
            lot.setUnit(dicUnitLogic.getValueByName(siteLot.getUnit()));
            lot.setCount(siteLot.getCount());
            lot.setFirstYearSum(siteLot.getFirstYearSum());
            lot.setSecondYearSum(siteLot.getSecondYearSum() == null ? 0 : siteLot.getSecondYearSum());
            lot.setThirdYearSum(siteLot.getThirdYearSum() == null ? 0 : siteLot.getThirdYearSum());
            lot.setTotalSum(siteLot.getTotalSum());
            lot.setPrepayment(siteLot.getPrepayment());
            lot.setDeliveryTime(siteLot.getDeliveryTime());
            lot.setIncoterms(siteLot.getIncoterms());
            lot.setIngeneering(siteLot.getIngeneering());
            lot.setDemping(siteLot.getDemping());
            lot.setCustomerFIO(siteLot.getCustomerFIO());
            lot.setCustomerJobPosition(siteLot.getCustomerJobPosition());
            lot.setCustomerPhone(siteLot.getCustomerPhone());
            lot.setCustomerEmail(siteLot.getCustomerEmail());
            lot.setCustomerBankRequisites(siteLot.getCustomerBankRequisites());
            if (lot.getId() == null)
                em.persist(lot);

            TLotCrossAnnouncement lotCrossAnnouncement = new TLotCrossAnnouncement();
            if (announcement.getId() != null) {
                try {
                    TLotCrossAnnouncement lotCrossAnnouncementTmp = query.setParameter(lotNumberParameter, siteLot.getNumber()).getSingleResult();
                    if (lot.getId() == null || lotCrossAnnouncementTmp.getId().getLot().getId().longValue() != lot.getId()) {
                        em.remove(lotCrossAnnouncementTmp);
                        em.flush();
//                        announcement.getLotCrossAnnouncements().remove(lotCrossAnnouncementTmp);
                    } else
                        lotCrossAnnouncement = lotCrossAnnouncementTmp;
                } catch (NoResultException ignored) {
                }
            }
            lotCrossAnnouncement.setLotNumber(siteLot.getNumber());
            lotCrossAnnouncement.setOriginalHtml(siteLot.getOriginalHtml());
            lotCrossAnnouncement.setStatus(dicLotStatusLogic.getValueByName(siteLot.getStatus()));

            if (lotCrossAnnouncement.getId() == null) {
                TLotCrossAnnouncementId id = new TLotCrossAnnouncementId();
                id.setLot(lot);
                id.setAnnouncement(announcement);
                lotCrossAnnouncement.setId(id);
                em.persist(lotCrossAnnouncement);
                announcement.getLotCrossAnnouncements().add(lotCrossAnnouncement);
            } else
                lotCrossAnnouncement.getId().setLot(lot);

            String[] placements = siteLot.getPlacement().split("\\n");
            for (int i = 0; i < placements.length; i += 2) {
                int firstSpaceIdx = placements[i].indexOf(" ");
                String kato = placements[i].substring(0, firstSpaceIdx - 1);
                String addr = placements[i].substring(firstSpaceIdx + 1);
                TLotPlacement placement = new TLotPlacement();
                placement.setLot(lot);
                placement.setKato(kato);
                placement.setAddress(addr);
                placement.setVolume(Double.valueOf(placements[i + 1].substring("Количество, объем: ".length())));
                em.persist(placement);
            }
        }
        em.flush();
    }

    private TLot getLotByPortalId(Long portalId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TLot> query = builder.createQuery(TLot.class);
        Root<TLot> root = query.from(TLot.class);
        query.where(builder.equal(root.get(TLot_.portalId), portalId));
        try {
            return em.createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}

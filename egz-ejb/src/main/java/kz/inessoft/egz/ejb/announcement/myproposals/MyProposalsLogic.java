package kz.inessoft.egz.ejb.announcement.myproposals;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.ejb.announcement.myproposals.http.SiteMyProposal;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 03.02.17.
 */
@Local
public interface MyProposalsLogic {
    void updateMyProposals(List<SiteMyProposal> siteProposals, DicParticipantCompany participantCompany);

    DicParticipantCompany getParticipantForAnnLoad(long announcementPortalId);

    boolean isAnnouncementHasMyProposals(long announcementPortalId);
}

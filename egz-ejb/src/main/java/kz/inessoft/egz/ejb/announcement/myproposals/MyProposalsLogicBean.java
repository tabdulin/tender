package kz.inessoft.egz.ejb.announcement.myproposals;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.DicParticipantCompany_;
import kz.inessoft.egz.dbentities.DicProposalStatus_;
import kz.inessoft.egz.dbentities.TMyProposal;
import kz.inessoft.egz.dbentities.TMyProposal_;
import kz.inessoft.egz.ejb.announcement.myproposals.http.SiteMyProposal;
import kz.inessoft.egz.ejbapi.DicProposalStatusLogic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.List;

/**
 * Created by alexey on 03.02.17.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MyProposalsLogicBean implements MyProposalsLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicProposalStatusLogic proposalStatusLogic;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void updateMyProposals(List<SiteMyProposal> siteProposals, DicParticipantCompany participantCompany) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaDelete<TMyProposal> criteriaDelete = builder.createCriteriaDelete(TMyProposal.class);
        Root<TMyProposal> root = criteriaDelete.from(TMyProposal.class);
        criteriaDelete.where(builder.equal(root.get(TMyProposal_.dicParticipantCompany), participantCompany));
        em.createQuery(criteriaDelete).executeUpdate();

        for (SiteMyProposal siteProposal : siteProposals) {
            TMyProposal myProposal = new TMyProposal();
            myProposal.setAnnouncementPortalId(siteProposal.getAnnouncementPortalId());
            myProposal.setPortalId(siteProposal.getPortalId());
            myProposal.setDicParticipantCompany(participantCompany);
            myProposal.setProposalNumber(siteProposal.getNumber());
            myProposal.setHtml(siteProposal.getSrcHtml());
            myProposal.setDicProposalStatus(proposalStatusLogic.getValueByName(siteProposal.getProposalStatus()));
            em.persist(myProposal);
        }
    }

    @Override
    public DicParticipantCompany getParticipantForAnnLoad(long announcementPortalId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<DicParticipantCompany> query = builder.createQuery(DicParticipantCompany.class);
        Root<TMyProposal> root = query.from(TMyProposal.class);
        query.select(root.get(TMyProposal_.dicParticipantCompany));

        Subquery<Long> subquery = query.subquery(Long.class);
        Root<TMyProposal> subRoot = subquery.from(TMyProposal.class);
        subquery.select(subRoot.get(TMyProposal_.proposalNumber));
        subquery.where(
                builder.and(
                        builder.equal(subRoot.get(TMyProposal_.dicProposalStatus).get(DicProposalStatus_.skipOnAnnouncementLoad), true),
                        builder.equal(subRoot.get(TMyProposal_.announcementPortalId), announcementPortalId),
                        builder.equal(subRoot.get(TMyProposal_.dicParticipantCompany), root.get(TMyProposal_.dicParticipantCompany))
                )
        );

        query.where(
                builder.and(
                        builder.equal(root.get(TMyProposal_.announcementPortalId), announcementPortalId),
                        builder.not(builder.in(root.get(TMyProposal_.proposalNumber)).value(subquery))
                ));
        query.orderBy(builder.asc(root.get(TMyProposal_.dicParticipantCompany).get(DicParticipantCompany_.loadAnnouncementPriority)));

        List<DicParticipantCompany> resultList = em.createQuery(query).getResultList();
        if (resultList.isEmpty())
            return null;
        return resultList.get(0);
    }

    @Override
    public boolean isAnnouncementHasMyProposals(long announcementPortalId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Boolean> query = builder.createQuery(Boolean.class);
        Root<TMyProposal> root = query.from(TMyProposal.class);
        query.select(builder.greaterThan(builder.count(root), 0L));
        query.where(builder.equal(root.get(TMyProposal_.announcementPortalId), announcementPortalId));
        return em.createQuery(query).getSingleResult();
    }
}

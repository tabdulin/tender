package kz.inessoft.egz.ejb.announcement.myproposals;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejb.announcement.myproposals.http.SiteMyProposal;
import kz.inessoft.egz.ejb.httpclient.AuthorizedHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 03.02.17.
 */
@Singleton
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MyProposalsSheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyProposalsSheduler.class);

    @EJB
    private DicParticipantCompanyLogic participantCompanyLogic;

    @EJB
    private MyProposalsLogic proposalsLogic;

    @Schedule(dayOfWeek = "1,2,3,4,5", hour = "9,14", minute = "30", persistent = false)
    public void processMyProposals() {
        List<DicParticipantCompany> dicParticipantCompanies = participantCompanyLogic.getDicParticipantCompanies();
        dicParticipantCompanies.forEach(this::processCompany);
    }

    private void processCompany(DicParticipantCompany participantCompany) {
        AuthorizedHttpClient client = HttpClientsPool.getInstance().getClient(participantCompany);
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            String url = PortalUrlUtils.createMyPropposalsUrl();
            List<SiteMyProposal> proposals = new LinkedList<>();
            while (true) {
                Response response = client.sendGetRequest(url);
                NodeList nodeList = response.evaluateXPathNodeList("//table[@class='table table-bordered']//tr/td/..");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    SiteMyProposal proposal = new SiteMyProposal();
                    Node row = nodeList.item(i);
                    StringWriter sw = new StringWriter();
                    t.transform(new DOMSource(row), new StreamResult(sw));
                    String html = sw.toString();
                    proposal.setSrcHtml(html);
                    Node link = XMLUtils.evaluateXPathNode("./td[position()=1]/a", row);
                    String proposalUrl = link.getAttributes().getNamedItem("href").getNodeValue();
                    String[] parts = proposalUrl.split("/");
                    proposal.setPortalId(Long.valueOf(parts[parts.length - 1]));
                    proposal.setNumber(Long.valueOf(link.getTextContent()));
                    String announcementUrl = XMLUtils.evaluateXPathNode("./td[position()=2]/a/@href", row).getNodeValue();
                    parts = announcementUrl.split("/");
                    proposal.setAnnouncementPortalId(Long.valueOf(parts[parts.length - 1]));
                    proposal.setProposalStatus(XMLUtils.evaluateXPathNode("./td[position()=8]", row).getTextContent());
                    proposals.add(proposal);
                }

                Node node = response.evaluateXPathNode("//ul[@class='pagination']/li/a[text()='>']");
                if (node == null)
                    break;
                else
                    url = node.getAttributes().getNamedItem("href").getNodeValue();
            }
            proposalsLogic.updateMyProposals(proposals, participantCompany);
        } catch (Exception e) {
            LOGGER.error("Error while update my proposals", e);
        } finally {
            HttpClientsPool.getInstance().freeClient(client);
        }
    }
}

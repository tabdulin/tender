package kz.inessoft.egz.ejb.announcement.myproposals.http;

/**
 * Created by alexey on 03.02.17.
 */
public class SiteMyProposal {
    private long number;
    private long portalId;
    private long announcementPortalId;
    private String proposalStatus;
    private String srcHtml;

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public long getPortalId() {
        return portalId;
    }

    public void setPortalId(long portalId) {
        this.portalId = portalId;
    }

    public long getAnnouncementPortalId() {
        return announcementPortalId;
    }

    public void setAnnouncementPortalId(long announcementPortalId) {
        this.announcementPortalId = announcementPortalId;
    }

    public String getProposalStatus() {
        return proposalStatus;
    }

    public void setProposalStatus(String proposalStatus) {
        this.proposalStatus = proposalStatus;
    }

    public String getSrcHtml() {
        return srcHtml;
    }

    public void setSrcHtml(String srcHtml) {
        this.srcHtml = srcHtml;
    }
}

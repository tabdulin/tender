package kz.inessoft.egz.ejb.announcement.proposals;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteProposalsInfo;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Local
public interface ProposalsSiteProcessorLogic {
    void saveProposals(TAnnouncement announcement, List<SiteProposalsInfo.SiteProposal> proposalList,
                       DicParticipantCompany participantCompany);
}

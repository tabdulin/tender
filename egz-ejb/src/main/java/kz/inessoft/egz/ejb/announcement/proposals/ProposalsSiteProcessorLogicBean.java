package kz.inessoft.egz.ejb.announcement.proposals;

import kz.inessoft.egz.dbentities.DicImplementer;
import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDocument;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.dbentities.TLotProposal;
import kz.inessoft.egz.dbentities.TLotProposalId;
import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.dbentities.TProposal;
import kz.inessoft.egz.dbentities.TProposalDocument;
import kz.inessoft.egz.dbentities.TProposalId;
import kz.inessoft.egz.dbentities.TProposalId_;
import kz.inessoft.egz.dbentities.TProposal_;
import kz.inessoft.egz.ejb.announcement.http.SiteFileRecieved;
import kz.inessoft.egz.ejb.announcement.http.SiteFileUploaded;
import kz.inessoft.egz.ejb.announcement.http.SiteProposalDocument;
import kz.inessoft.egz.ejb.announcement.http.SiteProposalLotDocuments;
import kz.inessoft.egz.ejb.announcement.http.SiteProposalsInfo;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import kz.inessoft.egz.ejbapi.DicProposalStatusLogic;
import kz.inessoft.egz.ejbapi.DocumentLogic;
import kz.inessoft.egz.ejbapi.LotLogic;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 20.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProposalsSiteProcessorLogicBean implements ProposalsSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicImplementerLogic implementerLogic;

    @EJB
    private DicProposalStatusLogic proposalStatusLogic;

    @EJB
    private DocumentLogic documentLogic;

    @EJB
    private PortalFilesLogic portalFilesLogic;

    @EJB
    private LotLogic lotLogic;

    @Override
    public void saveProposals(TAnnouncement announcement, List<SiteProposalsInfo.SiteProposal> proposalList,
                              DicParticipantCompany participantCompany) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TProposal> query = builder.createQuery(TProposal.class);
        Root<TProposal> root = query.from(TProposal.class);
        query.where(builder.equal(root.get(TProposal_.id).get(TProposalId_.announcement), announcement));
        List<TProposal> proposals = em.createQuery(query).getResultList();
        Map<Long, TProposal> proposalCache = new HashMap<>();
        for (TProposal proposal : proposals) {
            proposalCache.put(proposal.getId().getImplementer().getId(), proposal);
        }
        for (SiteProposalsInfo.SiteProposal siteProposal : proposalList) {
            DicImplementer implementer = implementerLogic.getValueByXin(siteProposal.getImplementerXin(), siteProposal.getImplementerName());
            TProposal proposal = null;
            if (implementer.getId() != null) {
                proposal = proposalCache.remove(implementer.getId());
            }
            if (proposal == null) {
                proposal = new TProposal();
            }

            proposal.setPortalId(siteProposal.getPortalId());
            proposal.setProposalTime(siteProposal.getProposalTime());
            proposal.setProposalStatus(proposalStatusLogic.getValueByName(siteProposal.getStatus()));
            proposal.setAddress(siteProposal.getImplementerAddress());
            proposal.setBankName(siteProposal.getBankName());
            proposal.setBik(siteProposal.getBik());
            proposal.setConsorcium(siteProposal.isConsorcium());
            proposal.setDocsTabHtml(siteProposal.getDocsInfoTabHtml());
            proposal.setFio(siteProposal.getImplementerFIO());
            proposal.setGeneralTabHtml(siteProposal.getGeneralInfoTabHtml());
            proposal.setIik(siteProposal.getIik());
            proposal.setJobPosition(siteProposal.getJobPosition());
            proposal.setKbe(siteProposal.getKbe());
            proposal.setLotsTabHtml(siteProposal.getLotsInfoTabHtml());
            proposal.setPhone(siteProposal.getPhone());

            if (proposal.getId() == null) {
                TProposalId id = new TProposalId();
                id.setAnnouncement(announcement);
                id.setImplementer(implementer);
                proposal.setId(id);
                em.persist(proposal);
            }

            em.createQuery("delete from TLotProposal where id.proposal = :proposal")
                    .setParameter("proposal", proposal)
                    .executeUpdate();
            List<SiteProposalLotDocuments> proposalDocuments = new ArrayList<>(siteProposal.getProposalLotDocuments());
            for (String s : siteProposal.getLotNumbers()) {
                TLotCrossAnnouncement lotByNumber = lotLogic.getLotByNumber(announcement, s);
                TLotProposal lotProposal = new TLotProposal();
                TLotProposalId id = new TLotProposalId();
                id.setProposal(proposal);
                id.setLotId(lotByNumber.getId().getLot().getId());
                lotProposal.setId(id);

                List<TProposalDocument> documentList = new ArrayList<>();
                // поиск и сохранение документов по этому лоту в заявке
                for (int i = 0; i < proposalDocuments.size(); i++) {
                    SiteProposalLotDocuments proposalLotDocuments = proposalDocuments.get(i);
                    // поиск осуществляется сравнением портального ID лота
                    if (lotByNumber.getLotNumber().equals(proposalLotDocuments.getLotNumber())) {
                        lotProposal.setHtml(proposalLotDocuments.getHtml());
                        for (SiteProposalDocument siteProposalDocument : proposalLotDocuments.getProposalDocuments()) {
                            TPortalFile file;
                            if (siteProposalDocument.getPortalFile() instanceof SiteFileUploaded) {
                                file = portalFilesLogic.queueLoadUploadedFile(siteProposalDocument.getPortalFile().getPortalFileId());
                            } else {
                                SiteFileRecieved fileRecieved = (SiteFileRecieved) siteProposalDocument.getPortalFile();
                                file = portalFilesLogic.queueLoadRecievedFile(fileRecieved.getPortalFileId(), fileRecieved.getUrl(), participantCompany);
                            }
                            TDocument document = documentLogic.getDocument(siteProposalDocument.getDocumentName(), announcement);
                            TProposalDocument proposalDocument = new TProposalDocument();
                            proposalDocument.setPortalFile(file);
                            proposalDocument.setDocument(document);
                            proposalDocument.setLotProposal(lotProposal);
                            documentList.add(proposalDocument);
                        }
                        proposalDocuments.remove(i);
                        break;
                    }
                }
                em.persist(lotProposal);
                for (TProposalDocument proposalDocument : documentList) {
                    em.persist(proposalDocument);
                }
            }
            // все документы по заявке должны разойтись по своим лотам.
            if (!proposalDocuments.isEmpty())
                throw new RuntimeException("Undefined proposal documents for announcement portal " + announcement.getPortalId());
        }

        // Оставшиеся в кэше предложения отсутствовали на странице. удалим их из БД
        for (TProposal tProposal : proposalCache.values()) {
            em.remove(tProposal);
        }
    }
}

package kz.inessoft.egz.ejb.announcement.proposals;

import kz.inessoft.egz.dbentities.DicImplementer;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TProposal;
import kz.inessoft.egz.dbentities.TProposalDocument;
import kz.inessoft.egz.dbentities.TProposalId;
import kz.inessoft.egz.ejbapi.TProposalsLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TProposalsLogicBean implements TProposalsLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public List<TProposal> getProposals(TAnnouncement announcement) {
        List<TProposal> retVal = em.createQuery("from TProposal where id.announcement = :announcement")
                .setParameter("announcement", announcement)
                .getResultList();
        retVal.size();
        return retVal;
    }

    @Override
    public TProposal getProposal(TAnnouncement announcement, long implementerId) {
        TProposalId id = new TProposalId();
        id.setAnnouncement(announcement);
        id.setImplementer(em.getReference(DicImplementer.class, implementerId));
        return em.find(TProposal.class, id);
    }

    @Override
    public TProposalDocument getProposalDocument(long id) {
        return em.find(TProposalDocument.class, id);
    }

    @Override
    public List<TProposalDocument> getProposalDocuments(TAnnouncement announcement, long implementerId) {
        List<TProposalDocument> retVal = em.createQuery("from TProposalDocument where lotProposal.id.proposal.id.announcement = :announcement and lotProposal.id.proposal.id.implementer.id = :implementerId")
                .setParameter("announcement", announcement)
                .setParameter("implementerId", implementerId)
                .getResultList();
        retVal.size();
        return retVal;
    }

    @Override
    public List<TProposalDocument> getProposalDocuments(long tDocumentId, long implementerId) {
        List<TProposalDocument> retVal = em.createQuery("from TProposalDocument where document.id = :tDocumentId and lotProposal.id.proposal.id.implementer.id = :implementerId")
                .setParameter("tDocumentId", tDocumentId)
                .setParameter("implementerId", implementerId)
                .getResultList();
        retVal.size();
        return retVal;
    }

    @Override
    public boolean hasProposals(TAnnouncement announcement) {
        Long cnt = (Long) em.createQuery("select count(*) from TProposal where id.announcement = :announcement")
                .setParameter("announcement", announcement)
                .getSingleResult();
        return cnt > 0;
    }
}

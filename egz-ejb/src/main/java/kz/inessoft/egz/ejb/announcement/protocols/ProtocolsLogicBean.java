package kz.inessoft.egz.ejb.announcement.protocols;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TProtocol;
import kz.inessoft.egz.ejbapi.ProtocolsLogic;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProtocolsLogicBean implements ProtocolsLogic {
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ProtocolsLogicBean.class);

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public List<TProtocol> getProtocols(TAnnouncement announcement) {
        List<TProtocol> retVal = em.createQuery("from TProtocol where announcement = :announcement")
                .setParameter("announcement", announcement)
                .getResultList();
        return retVal;
    }

    @Override
    public TProtocol getProtocol(long id) {
        return em.find(TProtocol.class, id);
    }
}

package kz.inessoft.egz.ejb.announcement.protocols;

import kz.inessoft.egz.dbentities.ETProtocolType;
import kz.inessoft.egz.dbentities.TPortalFile_;
import kz.inessoft.egz.dbentities.TProtocol;
import kz.inessoft.egz.dbentities.TProtocol_;
import kz.inessoft.egz.ejb.announcement.comission.TenderComissionSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.implementers.LotImplementersSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.protocols.parsers.ParserBuilder;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import kz.inessoft.egz.ejbapi.ProtocolsException;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by alexey on 21.10.16.
 */
@Singleton
@Startup
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProtocolsParser {
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ProtocolsParser.class);
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;


    @EJB
    private TenderComissionSiteProcessorLogic tenderComissionSiteProcessorLogic;

    @EJB
    private LotImplementersSiteProcessorLogic lotImplementersSiteProcessorLogic;

    @EJB
    private PortalFilesLogic portalFilesLogic;

    @Schedule(hour = "*", minute = "30", persistent = false)
    public void parseProtocols() throws ProtocolsException {
        LOGGER.info("Start parsing fresh protocols");
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<TProtocol> root = query.from(TProtocol.class);
        query.select(root.get(TProtocol_.id));
        query.where(builder.and(
                builder.equal(root.get(TProtocol_.parsed), Boolean.FALSE),
                root.get(TProtocol_.portalFile).get(TPortalFile_.fileName).isNotNull(),
                builder.equal(root.get(TProtocol_.type), ETProtocolType.RESULT)
        ));

        List<Long> ids = em.createQuery(query).getResultList();
        for (Long id : ids) {
            TProtocol protocol = em.find(TProtocol.class, id);
            Long annPortalId = protocol.getAnnouncement().getPortalId();
            LOGGER.info("Parsing protocol {} for announcement {}", protocol.getPortalFile().getPortalId(), annPortalId);
            new ParserBuilder().setEm(em)
                    .setLotImplementersSiteProcessorLogic(lotImplementersSiteProcessorLogic)
                    .setTenderComissionSiteProcessorLogic(tenderComissionSiteProcessorLogic)
                    .setPortalFilesLogic(portalFilesLogic)
                    .setProtocol(protocol)
                    .build().parse();
            protocol.setParsed(true);
            // Сбросить сессию в базу и очистить, что бы не возникло OOM, т.к. в одной сессии будет куча файлв с данными.
            em.flush();
            em.clear();
        }
    }
}

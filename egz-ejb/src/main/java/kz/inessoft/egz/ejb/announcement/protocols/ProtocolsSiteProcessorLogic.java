package kz.inessoft.egz.ejb.announcement.protocols;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.announcement.http.SiteProtocolsInfo;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.10.16.
 */
@Local
public interface ProtocolsSiteProcessorLogic {
    void saveProtocols(TAnnouncement announcement, List<SiteProtocolsInfo.SiteProtocolFileInfo> siteProtocols, DicParticipantCompany participantCompany);
}

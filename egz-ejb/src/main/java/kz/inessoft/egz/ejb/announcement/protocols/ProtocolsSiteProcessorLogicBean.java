package kz.inessoft.egz.ejb.announcement.protocols;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.ETProtocolType;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TComission;
import kz.inessoft.egz.dbentities.TComission_;
import kz.inessoft.egz.dbentities.TImplementerLot;
import kz.inessoft.egz.dbentities.TImplementerLot_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement_;
import kz.inessoft.egz.dbentities.TPortalRecievedFile;
import kz.inessoft.egz.dbentities.TPortalUploadedFile;
import kz.inessoft.egz.dbentities.TProtocol;
import kz.inessoft.egz.dbentities.TProtocol_;
import kz.inessoft.egz.ejb.announcement.http.SiteFileRecieved;
import kz.inessoft.egz.ejb.announcement.http.SiteFileUploaded;
import kz.inessoft.egz.ejb.announcement.http.SiteProtocolsInfo;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 20.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProtocolsSiteProcessorLogicBean implements ProtocolsSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private PortalFilesLogic portalFilesLogic;

    @Override
    public void saveProtocols(TAnnouncement announcement, List<SiteProtocolsInfo.SiteProtocolFileInfo> siteProtocols, DicParticipantCompany participantCompany) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TProtocol> query = builder.createQuery(TProtocol.class);
        Root<TProtocol> root = query.from(TProtocol.class);
        query.where(builder.equal(root.get(TProtocol_.announcement), announcement));
        List<TProtocol> protocols = em.createQuery(query).getResultList();
        Map<Long, TProtocol> protocolsUploadedMap = new HashMap<>();
        Map<Long, TProtocol> protocolsRecievedMap = new HashMap<>();
        for (TProtocol protocol : protocols) {
            if (protocol.getPortalFile() instanceof TPortalUploadedFile)
                protocolsUploadedMap.put(protocol.getPortalFile().getPortalId(), protocol);
            else if (protocol.getPortalFile() instanceof TPortalRecievedFile)
                protocolsRecievedMap.put(protocol.getPortalFile().getPortalId(), protocol);
            else
                throw new IllegalArgumentException("Can't detect protocol type");
        }
        for (SiteProtocolsInfo.SiteProtocolFileInfo siteProtocol : siteProtocols) {
            TProtocol protocol;
            if (siteProtocol.getPortalFile() instanceof SiteFileUploaded) {
                protocol = protocolsUploadedMap.remove(siteProtocol.getPortalFile().getPortalFileId());
                if (protocol == null) {
                    protocol = new TProtocol();
                    protocol.setAnnouncement(announcement);
                    protocol.setPortalFile(portalFilesLogic.queueLoadUploadedFile(siteProtocol.getPortalFile().getPortalFileId()));
                } else if (protocol.getPortalFile().getPortalId() != siteProtocol.getPortalFile().getPortalFileId()) {
                    protocol.setParsed(false);
                    protocol.setPortalFile(portalFilesLogic.queueLoadUploadedFile(siteProtocol.getPortalFile().getPortalFileId()));
                }
            } else if (siteProtocol.getPortalFile() instanceof SiteFileRecieved) {
                SiteFileRecieved portalFile = (SiteFileRecieved) siteProtocol.getPortalFile();
                protocol = protocolsRecievedMap.remove(siteProtocol.getPortalFile().getPortalFileId());
                if (protocol == null) {
                    protocol = new TProtocol();
                    protocol.setAnnouncement(announcement);
                    protocol.setPortalFile(portalFilesLogic.queueLoadRecievedFile(portalFile.getPortalFileId(), portalFile.getUrl(), participantCompany));
                } else if (protocol.getPortalFile().getPortalId() != siteProtocol.getPortalFile().getPortalFileId()) {
                    protocol.setParsed(false);
                    protocol.setPortalFile(portalFilesLogic.queueLoadRecievedFile(portalFile.getPortalFileId(), portalFile.getUrl(), participantCompany));
                }
            } else
                throw new IllegalArgumentException("Can't detect protocol type");

            protocol.setType(siteProtocol.getProtocolType().getDBType());
            if (protocol.getId() == null)
                em.persist(protocol);
        }

        // Оставшиеся протоколы в мапе лишние. Их не было на странице и по этому надо удалить из БД
        for (TProtocol protocol : protocolsRecievedMap.values()) {
            em.remove(protocol);
            clearResultProtocolData(protocol);

        }
        for (TProtocol protocol : protocolsUploadedMap.values()) {
            em.remove(protocol);
            clearResultProtocolData(protocol);
        }
    }

    /**
     * При удалении не действительного протокола итогов надо так же удалить распарсенные из него данные. Метод эту операцию
     * @param protocol
     */
    private void clearResultProtocolData(TProtocol protocol) {
        if (protocol.getType() != ETProtocolType.RESULT)
            return;

        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaDelete<TComission> deleteTComission = builder.createCriteriaDelete(TComission.class);
        Root<TComission> rootTComission = deleteTComission.from(TComission.class);
        deleteTComission.where(builder.equal(rootTComission.get(TComission_.announcement), protocol.getAnnouncement()));
        em.createQuery(deleteTComission).executeUpdate();

        CriteriaDelete<TImplementerLot> deleteTImplementerLot = builder.createCriteriaDelete(TImplementerLot.class);
        Root<TImplementerLot> rootTImplementerLot = deleteTImplementerLot.from(TImplementerLot.class);
        deleteTImplementerLot.where(builder.equal(rootTImplementerLot.get(TImplementerLot_.lotCrossAnnouncement).get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement), protocol.getAnnouncement()));
        em.createQuery(deleteTImplementerLot).executeUpdate();
    }
}

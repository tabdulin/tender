package kz.inessoft.egz.ejb.announcement.protocols.parsers;

import kz.inessoft.egz.dbentities.TProtocol;
import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejb.announcement.comission.TenderComissionSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.http.SiteComissionMember;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscount;
import kz.inessoft.egz.ejb.announcement.http.SiteImplementer;
import kz.inessoft.egz.ejb.announcement.implementers.LotImplementersSiteProcessorLogic;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.persistence.EntityManager;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by alexey on 10.01.17.
 */
public class HTMLProtocolParser implements IProtocolParser {
    private class LotWrapper {
        private Node lot;
        private int firstIdx;
        private int lastIdx;
        private Node implementersTable;
        private Node qualifiedImplementersTable;
        private Node declinedImplementersTable;
        private Node approvedImplementersTable;
        private Node discountsTable;
        private Node pricesTable;
        private Node winnerInfo;

        public LotWrapper(Node lot, int firstIdx) {
            this.lot = lot;
            this.firstIdx = firstIdx;
        }

        public Node getLot() {
            return lot;
        }

        public int getFirstIdx() {
            return firstIdx;
        }

        public int getLastIdx() {
            return lastIdx;
        }

        public void setLastIdx(int lastIdx) {
            this.lastIdx = lastIdx;
        }

        public Node getImplementersTable() {
            return implementersTable;
        }

        public void setImplementersTable(Node implementersTable) {
            this.implementersTable = implementersTable;
        }

        public Node getQualifiedImplementersTable() {
            return qualifiedImplementersTable;
        }

        public void setQualifiedImplementersTable(Node qualifiedImplementersTable) {
            this.qualifiedImplementersTable = qualifiedImplementersTable;
        }

        public Node getDeclinedImplementersTable() {
            return declinedImplementersTable;
        }

        public void setDeclinedImplementersTable(Node declinedImplementersTable) {
            this.declinedImplementersTable = declinedImplementersTable;
        }

        public Node getApprovedImplementersTable() {
            return approvedImplementersTable;
        }

        public void setApprovedImplementersTable(Node approvedImplementersTable) {
            this.approvedImplementersTable = approvedImplementersTable;
        }

        public Node getDiscountsTable() {
            return discountsTable;
        }

        public void setDiscountsTable(Node discountsTable) {
            this.discountsTable = discountsTable;
        }

        public Node getPricesTable() {
            return pricesTable;
        }

        public void setPricesTable(Node pricesTable) {
            this.pricesTable = pricesTable;
        }

        public Node getWinnerInfo() {
            return winnerInfo;
        }

        public void setWinnerInfo(Node winnerInfo) {
            this.winnerInfo = winnerInfo;
        }
    }

    private EntityManager em;
    private TenderComissionSiteProcessorLogic tenderComissionSiteProcessorLogic;
    private LotImplementersSiteProcessorLogic lotImplementersSiteProcessorLogic;
    private PortalFilesLogic portalFilesLogic;
    private TProtocol protocol;
    private Document document;

    public HTMLProtocolParser(EntityManager em, TenderComissionSiteProcessorLogic tenderComissionSiteProcessorLogic,
                              LotImplementersSiteProcessorLogic lotImplementersSiteProcessorLogic,
                              PortalFilesLogic portalFilesLogic, TProtocol protocol) {
        this.em = em;
        this.tenderComissionSiteProcessorLogic = tenderComissionSiteProcessorLogic;
        this.lotImplementersSiteProcessorLogic = lotImplementersSiteProcessorLogic;
        this.portalFilesLogic = portalFilesLogic;
        this.protocol = protocol;
    }

    @Override
    public void parse() {
        try {
            portalFilesLogic.processFileContent(protocol.getPortalFile().getId(), contentStream -> {
                String html = IOUtils.toString(contentStream, "UTF8");
                document = XMLUtils.getDocument(html);
                List<SiteComissionMember> comissionMembers = parseComission();
                List<SiteImplementer> concursImplementers = parseLots();
                em.flush();
                tenderComissionSiteProcessorLogic.saveComission(protocol.getAnnouncement(), comissionMembers);
                lotImplementersSiteProcessorLogic.saveImplementersInfo(protocol.getAnnouncement(), concursImplementers);
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<SiteComissionMember> parseComission() throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = XMLUtils.evaluateXPathNodeList("//table/caption[text()='Состав конкурсной комиссии:']/../tr[position()>1]", document);
        List<SiteComissionMember> retVal = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            SiteComissionMember member = new SiteComissionMember();
            member.setFio(XMLUtils.evaluateXPathNode("./td[position()=2]", node).getTextContent().toUpperCase());
            member.setJobPosition(XMLUtils.evaluateXPathNode("./td[position()=3]", node).getTextContent());
            member.setRole(XMLUtils.evaluateXPathNode("./td[position()=4]", node).getTextContent());
            retVal.add(member);
        }
        return retVal;
    }

    private List<SiteImplementer> parseLots() throws ParserConfigurationException, SAXException, XPathExpressionException, IOException, ParseException {
        List<SiteImplementer> retVal = new ArrayList<>();
        NodeList nodeList = XMLUtils.evaluateXPathNodeList("//p[starts-with(normalize-space(text()), '№ лота')]", document);
        List<LotWrapper> wrappers = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            LotWrapper wrapper = new LotWrapper(item, getNodeIndex(item));
            wrappers.add(wrapper);
        }
        Collections.sort(wrappers, (o1, o2) -> o1.getFirstIdx() - o2.getFirstIdx());
        for (int i = 0; i < wrappers.size() - 1; i++) {
            LotWrapper wrapper = wrappers.get(i);
            wrapper.setLastIdx(wrappers.get(i + 1).getFirstIdx() - 1);
        }

        nodeList = XMLUtils.evaluateXPathNodeList("//table/caption[starts-with(normalize-space(text()), 'Информация о представленных заявках на участие в конкурсе (лоте):')]/..", document);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            findWrapper(wrappers, getNodeIndex(node)).setImplementersTable(node);
        }

        nodeList = XMLUtils.evaluateXPathNodeList("//table/caption[starts-with(normalize-space(text()), 'Информация о приведенных в соответствие с квалификационными требованиями и требованиями конкурсной документации заявках на участие в конкурсе:')]/..", document);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            findWrapper(wrappers, getNodeIndex(node)).setQualifiedImplementersTable(node);
        }

        nodeList = XMLUtils.evaluateXPathNodeList("//table/caption[starts-with(normalize-space(text()), 'Отклоненные заявки на участие в конкурсе:')]/..", document);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            findWrapper(wrappers, getNodeIndex(node)).setDeclinedImplementersTable(node);
        }

        nodeList = XMLUtils.evaluateXPathNodeList("//table/caption[starts-with(normalize-space(text()), 'Допущенные заявки на участие в конкурсе:')]/..", document);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            findWrapper(wrappers, getNodeIndex(node)).setApprovedImplementersTable(node);
        }

        nodeList = XMLUtils.evaluateXPathNodeList("//table/caption[starts-with(normalize-space(text()), 'Информация о результатах применения относительного значения критериев, влияющих на конкурсное ценовое предложение:')]/..", document);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            findWrapper(wrappers, getNodeIndex(node)).setDiscountsTable(node);
        }

        nodeList = XMLUtils.evaluateXPathNodeList("//table/caption[starts-with(normalize-space(text()), 'Расчет условных цен участников конкурса:')]/..", document);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            findWrapper(wrappers, getNodeIndex(node)).setPricesTable(node);
        }

        nodeList = XMLUtils.evaluateXPathNodeList("//p[starts-with(normalize-space(text()), 'Победитель по лоту №')]", document);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            findWrapper(wrappers, getNodeIndex(node)).setWinnerInfo(node);
        }

        for (LotWrapper wrapper : wrappers) {
            retVal.addAll(parseLot(wrapper));
        }

        return retVal;
    }

    private List<SiteImplementer> parseLot(LotWrapper wrapper) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException, ParseException {
        String lotNumber = XMLUtils.evaluateXPathNode("./b[position()=1]", wrapper.getLot()).getTextContent();
        List<SiteImplementer> retVal = parseLotImplementers(wrapper.getImplementersTable(), wrapper.getQualifiedImplementersTable(), lotNumber);
        parseDeniedImplementers(wrapper.getDeclinedImplementersTable(), retVal);
        parseGrantedImplementers(wrapper.getApprovedImplementersTable(), retVal);
        parseDiscounts(wrapper.getDiscountsTable(), retVal);
        parsePrices(wrapper.getPricesTable(), retVal);
        parseWinner(wrapper.getWinnerInfo(), retVal);
        return retVal;
    }

    private List<SiteImplementer> parseLotImplementers(Node implementersTable, Node qualifiedImplementersTable, String lotNumber) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException, ParseException {
        List<SiteImplementer> retVal = new ArrayList<>();
        NodeList nodeList = XMLUtils.evaluateXPathNodeList(".//tr/td[position()=3 and text()!='']/..", implementersTable);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            SiteImplementer implementer = new SiteImplementer();
            implementer.setName(XMLUtils.evaluateXPathNode("./td[position()=2]", node).getTextContent());
            implementer.setXin(XMLUtils.evaluateXPathNode("./td[position()=3]", node).getTextContent());
            Node node1 = XMLUtils.evaluateXPathNode("./tr/td[position()=3 and text()='" + implementer.getXin() + "']/../td[4]", qualifiedImplementersTable);
            if (node1 != null) {
                String textContent = node1.getTextContent();
                while (textContent.contains("\n "))
                    textContent = textContent.replaceAll("\\n ", "\n");
                implementer.setRequisites(textContent);
            }
            implementer.setProposalTime(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS").parse(XMLUtils.evaluateXPathNode("./td[position()=4]", node).getTextContent()));
            implementer.setLotNumber(lotNumber);
            retVal.add(implementer);
        }
        return retVal;
    }

    private void parseDeniedImplementers(Node declinedImplementersTable, List<SiteImplementer> implementers) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = XMLUtils.evaluateXPathNodeList(".//tr[position()>1]", declinedImplementersTable);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            String xin = XMLUtils.evaluateXPathNode("./td[position()=3]", node).getTextContent().trim();
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    implementer.setAccessGranted(false);
                    implementer.setDeniedCause(XMLUtils.evaluateXPathNode("./td[position()=4]", node).getTextContent().trim());
                    break;
                }
            }
        }
    }

    private void parseGrantedImplementers(Node grantedImplementersTable, List<SiteImplementer> implementers) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = XMLUtils.evaluateXPathNodeList(".//tr[position()>1]", grantedImplementersTable);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            String xin = XMLUtils.evaluateXPathNode("./td[position()=3]", node).getTextContent().trim();
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    implementer.setAccessGranted(true);
                    break;
                }
            }
        }
    }

    private void parseDiscounts(Node discountsTable, List<SiteImplementer> implementers) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = XMLUtils.evaluateXPathNodeList(".//tr[position()=2]/th", discountsTable);
        List<String> discountNames = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            discountNames.add(nodeList.item(i).getTextContent());
        }
        nodeList = XMLUtils.evaluateXPathNodeList(".//tr[position()>3]", discountsTable);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            String xin = XMLUtils.evaluateXPathNode("./td[position()=3]", node).getTextContent();
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    NodeList nodeList1 = XMLUtils.evaluateXPathNodeList("./td[position()>3]", node);
                    for (int j = 0; j < nodeList1.getLength(); j++) {
                        String strValue = nodeList1.item(j).getTextContent();
                        if (!strValue.isEmpty() && !"0".equals(strValue)) {
                            SiteDiscount discount = new SiteDiscount();
                            implementer.getDiscounts().add(discount);
                            discount.setDiscountName(discountNames.get(j));
                            discount.setDiscountValue(strValue);
                        }
                    }
                    break;
                }
            }
        }
    }

    private void parsePrices(Node pricesTable, List<SiteImplementer> implementers) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        NodeList nodeList = XMLUtils.evaluateXPathNodeList(".//tr[position()>2]", pricesTable);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            String xin = XMLUtils.evaluateXPathNode("./td[position()=3]", node).getTextContent().trim();
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    implementer.setImplementerPrice(Double.valueOf(XMLUtils.evaluateXPathNode("./td[position()=5]", node).getTextContent().trim().replaceAll(" ", "")));
                    String tmp = XMLUtils.evaluateXPathNode("./td[position()=6]", node).getTextContent().trim().replaceAll(" ", "");
                    if (!StringUtils.isEmpty(tmp))
                        implementer.setAct26Price(Double.valueOf(tmp));
                    implementer.setFinalPrice(Double.valueOf(XMLUtils.evaluateXPathNode("./td[position()=8]", node).getTextContent().trim().replaceAll(" ", "")));
                    break;
                }
            }
        }
    }

    private void parseWinner(Node winnerNode, List<SiteImplementer> implementers) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        if (winnerNode == null)
            return;
        String name = XMLUtils.evaluateXPathNode("./b[position()=2]", winnerNode).getTextContent();
        if (name != null)
            // В некоторых протоколах победитель почему то не указан. Например конкурс 91582
            for (SiteImplementer implementer : implementers) {
                if (name.equals(implementer.getName())) {
                    implementer.setWinnerFlag(true);
                }
            }
    }


    private int getNodeIndex(Node node) {
        int index;
        Node sibling;

        index = 0;
        while ((sibling = node.getPreviousSibling()) != null) {
            node = sibling;
            ++index;
        }

        return index;
    }

    private LotWrapper findWrapper(List<LotWrapper> wrappers, int idx) {
        for (LotWrapper wrapper : wrappers) {
            if (idx > wrapper.getFirstIdx() && (idx <= wrapper.getLastIdx() || wrapper.getLastIdx() == 0))
                return wrapper;
        }
        return null;
    }
}

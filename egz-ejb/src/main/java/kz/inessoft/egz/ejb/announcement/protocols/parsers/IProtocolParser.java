package kz.inessoft.egz.ejb.announcement.protocols.parsers;

/**
 * Created by alexey on 10.01.17.
 */
public interface IProtocolParser {
    void parse();
}

package kz.inessoft.egz.ejb.announcement.protocols.parsers;

import kz.inessoft.egz.dbentities.TProtocol;
import kz.inessoft.egz.ejb.announcement.comission.TenderComissionSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.http.SiteComissionMember;
import kz.inessoft.egz.ejb.announcement.http.SiteDiscount;
import kz.inessoft.egz.ejb.announcement.http.SiteImplementer;
import kz.inessoft.egz.ejb.announcement.implementers.LotImplementersSiteProcessorLogic;
import kz.inessoft.egz.ejb.pdf.Document;
import kz.inessoft.egz.ejb.pdf.IPDFElement;
import kz.inessoft.egz.ejb.pdf.PDFParser;
import kz.inessoft.egz.ejb.pdf.Page;
import kz.inessoft.egz.ejb.pdf.Table;
import kz.inessoft.egz.ejb.pdf.TableCell;
import kz.inessoft.egz.ejb.pdf.TableRow;
import kz.inessoft.egz.ejb.pdf.Text;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by alexey on 10.01.17.
 */
public class PDFProtocolParser implements IProtocolParser {
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(PDFProtocolParser.class);

    /**
     * Класс позволяет проверить не идут ли в итераторе подряд 2 и более таблиц без текстовок между ними. Если так, то склеить эти
     * таблицы в одну и вернуть результат. Итератор при этом переместить на последний элемент таблицы.
     */
    private abstract class ATableGlue {
        private Table firstElement;
        private ListIterator<IPDFElement> elementIterator;

        /**
         * @param firstElement    текущий элемент итератора (не хочется в коде двигать итератор на один шаг назад,
         *                        а получить текущий элемент итератора невозможно)
         * @param elementIterator
         */
        public ATableGlue(Table firstElement, ListIterator<IPDFElement> elementIterator) {
            this.firstElement = firstElement;
            this.elementIterator = elementIterator;
        }

        public Table checkAngGlue() {
            Table retVal = new Table();
            retVal.getRows().addAll(firstElement.getRows());
            while (elementIterator.hasNext()) {
                IPDFElement element = elementIterator.next();
                if (!(element instanceof Table)) {
                    // Если следующий элемент - не таблица, то прекращаем цикл откатив итератор на шаг назад, что бы он
                    // указывал на последний элемент таблицы
                    elementIterator.previous();
                    break;
                }
                Table nextTable = (Table) element;

                // Следующий элемент - таблица. Отлично, значит надо строки этой таблицы объединить с тем что уже есть.
                // Этим будет заниматься конкретная реализация
                processTable(retVal, nextTable);
            }
            return retVal;
        }

        protected abstract void processTable(Table targetTable, Table nextTable);
    }

    /**
     * Класс, реализующий "умное" склеивание. Таблица разбивается только когда она переходит с одной страницы на
     * другую и надо убедиться что первая строка таблицы на следующей странице не является продолжением последней
     * строки на предыдущей таблице. Т.е. ячейка таблицы слишком высокая, и она не поместилась на одной странице.
     * Предполагаем что первый столбец таблицы - номер по порядку. Если в следующей таблице в первой ячейке номер
     * не указан вообще, либо идентичен предыдущему - считаем что это была одна строка, и объединяем ее.
     */
    private class UsualGlue extends ATableGlue {

        UsualGlue(Table firstElement, ListIterator<IPDFElement> elementIterator) {
            super(firstElement, elementIterator);
        }

        @Override
        protected void processTable(Table targetTable, Table nextTable) {
            TableRow lastTableRow = targetTable.getRows().get(targetTable.getRows().size() - 1);
            TableRow firstTableRow = nextTable.getRows().get(0);
            String number = lastTableRow.getCells().get(0).getText().replaceAll(" ", "");
            String fNumber = firstTableRow.getCells().get(0).getText().replaceAll(" ", "");
            if (fNumber.isEmpty() || number.equals(fNumber)) {
                // Тогда первую строку новой таблицы склеим с предыдущей строкой предыдущей таблицы. Естественно предполагаем
                // что количество ячеек в таблицах идентично.
                for (int i = 1; i < lastTableRow.getCells().size(); i++) {
                    TableCell tableCell = lastTableRow.getCells().get(i);
                    TableCell newTableCell = firstTableRow.getCells().get(i);
                    lastTableRow.getCells().set(i, new TableCell(tableCell.getText() + " " + newTableCell.getText()));
                }
                for (int i = 1; i < nextTable.getRows().size(); i++) {
                    targetTable.getRows().add(nextTable.getRows().get(i));
                }
            } else
                // В противном случае - на новой странице новая строка, просто добавляем все строки в конец таблицы
                targetTable.getRows().addAll(nextTable.getRows());
        }
    }

    /**
     * Табличный клей для таблиц с ценовыми предложениями. Дело в том что есть случаи (например заявка 835511), когда
     * в первом столбце тупо не указывается порядковый номер. По этому эти таблицы будем проверять на соответствие БИН-ов
     * на предыдущей и следующей странице. Если в первой строке следующей страницы указан другой БИН, чем в последней
     * строке предыдущей, значит это новая строка. Если БИН не указан - значит это продолжение строки
     */
    private class PriceTableGlue extends ATableGlue {
        /**
         * @param firstElement    текущий элемент итератора (не хочется в коде двигать итератор на один шаг назад,
         *                        а получить текущий элемент итератора невозможно)
         * @param elementIterator
         */
        PriceTableGlue(Table firstElement, ListIterator<IPDFElement> elementIterator) {
            super(firstElement, elementIterator);
        }

        @Override
        protected void processTable(Table targetTable, Table nextTable) {
            TableRow lastTableRow = targetTable.getRows().get(targetTable.getRows().size() - 1);
            TableRow firstTableRow = nextTable.getRows().get(0);
            String fBin = firstTableRow.getCells().get(2).getText().replaceAll(" ", "");
            if (fBin.isEmpty()) {
                // Тогда первую строку новой таблицы склеим с предыдущей строкой предыдущей таблицы. Естественно предполагаем
                // что количество ячеек в таблицах идентично.
                for (int i = 1; i < lastTableRow.getCells().size(); i++) {
                    TableCell tableCell = lastTableRow.getCells().get(i);
                    TableCell newTableCell = firstTableRow.getCells().get(i);
                    lastTableRow.getCells().set(i, new TableCell(tableCell.getText() + " " + newTableCell.getText()));
                }
                for (int i = 1; i < nextTable.getRows().size(); i++) {
                    targetTable.getRows().add(nextTable.getRows().get(i));
                }
            } else
                // В противном случае - на новой странице новая строка, просто добавляем все строки в конец таблицы
                targetTable.getRows().addAll(nextTable.getRows());
        }
    }

    private EntityManager em;
    private TenderComissionSiteProcessorLogic tenderComissionSiteProcessorLogic;
    private LotImplementersSiteProcessorLogic lotImplementersSiteProcessorLogic;
    private PortalFilesLogic portalFilesLogic;
    private TProtocol protocol;

    public PDFProtocolParser(EntityManager em, TenderComissionSiteProcessorLogic tenderComissionSiteProcessorLogic,
                             LotImplementersSiteProcessorLogic lotImplementersSiteProcessorLogic,
                             PortalFilesLogic portalFilesLogic, TProtocol protocol) {
        this.em = em;
        this.tenderComissionSiteProcessorLogic = tenderComissionSiteProcessorLogic;
        this.lotImplementersSiteProcessorLogic = lotImplementersSiteProcessorLogic;
        this.portalFilesLogic = portalFilesLogic;
        this.protocol = protocol;
    }

    @Override
    public void parse() {
        // Предполагается что протокол штатно идет в PDF-формате
        try {
            portalFilesLogic.processFileContent(protocol.getPortalFile().getId(),
                    contentStream -> {
                        try {
                            PDDocument document = PDDocument.load(contentStream);
                            Document parsedDocument = PDFParser.parsePDF(document);
                            document.close();
                            // Уберем разбиение на страницы.
                            List<IPDFElement> elements = new LinkedList<>();
                            for (Page page : parsedDocument.getPages()) {
                                elements.addAll(page.getElements());
                            }

                            List<SiteComissionMember> comissionMembers = null;
                            List<SiteImplementer> currentLotImplementers = null;
                            List<SiteImplementer> concursImplementers = new ArrayList<>();
                            String currentLotNumber = null;
                            // Начнем перебирать элементы
                            ListIterator<IPDFElement> it = elements.listIterator();
                            IPDFElement next = null;
                            while (it.hasNext()) {
                                if (next == null) next = it.next();
                                if (next instanceof Text) {
                                    String txt = ((Text) next).getText();
                                    if ("Состав конкурсной комиссии:".equals(txt)) {
                                        comissionMembers = parseComission(it);
                                    } else if ("№ лота ".equals(txt)) {
                                        currentLotNumber = ((Text) it.next()).getText();
                                    } else if (txt != null && (txt.startsWith("Информация о представленных заявках на участие в конкурсе (лоте):")) ||
                                            txt.startsWith("Информация о представленных заявках на участие в конкурсе:")) {
                                        currentLotImplementers = parseImplementers(it, currentLotNumber);
                                        concursImplementers.addAll(currentLotImplementers);
                                    } else if (txt != null && txt.startsWith("Отклоненные заявки на участие в конкурсе:")) {
                                        parseDeniedImplementers(it, currentLotNumber, currentLotImplementers);
                                    } else if (txt != null && (txt.startsWith("Следующие заявки на участие в конкурсе были допущены:") ||
                                            txt.startsWith("Допущенные заявки на участие в конкурсе:"))) {
                                        parseGrantedImplementers(it, currentLotNumber, currentLotImplementers);
                                    } else if ("участие в данном конкурсе:".equals(txt) ||
                                            "влияющих на конкурсное ценовое предложение:".equals(txt)) {
                                        parseDiscounts(it, currentLotNumber, currentLotImplementers);
                                    } else if ("Расчет условных цен участников конкурса:".equals(txt)) {
                                        parsePrices(it, currentLotNumber, currentLotImplementers);
                                    } else if ("Победитель по лоту № ".equals(txt)) {
                                        parseWinner(it, currentLotNumber, currentLotImplementers);
                                    }
                                }
                                next = it.next();
                            }
                            em.flush();
                            tenderComissionSiteProcessorLogic.saveComission(protocol.getAnnouncement(), comissionMembers);
                            lotImplementersSiteProcessorLogic.saveImplementersInfo(protocol.getAnnouncement(), concursImplementers);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Распарсить таблицу с членами конкурсной комиссии. После завершения итератор указывает на последний элемент таблицы в документе.
     *
     * @param elementIterator
     * @return
     */
    private List<SiteComissionMember> parseComission(ListIterator<IPDFElement> elementIterator) {
        // По структуре документа следом должна идти таблица, если так, то переходим на ее парсинг
        IPDFElement element = elementIterator.next();
        if (!(element instanceof Table)) {
            // В противном случае выдаем сообщение об ошибке в лог, и продолжаем парсинг
            LOGGER.error("Can't parse comission members for announcement {}", protocol.getAnnouncement().getPortalId());
            return null;
        }
        List<SiteComissionMember> retVal = new ArrayList<>();
        Table table = new UsualGlue((Table) element, elementIterator).checkAngGlue();
        for (int i = 1; i < table.getRows().size(); i++) {
            TableRow tableRow = table.getRows().get(i);
            if (tableRow.isEmpty())
                continue;
            SiteComissionMember member = new SiteComissionMember();
            member.setFio(tableRow.getCells().get(1).getText().toUpperCase());
            member.setJobPosition(tableRow.getCells().get(2).getText());
            member.setRole(tableRow.getCells().get(3).getText());
            retVal.add(member);
        }
        return retVal;
    }


    private List<SiteImplementer> parseImplementers(ListIterator<IPDFElement> elementIterator, String lotNumber) throws ParseException {
        // По структуре документа следом должна идти таблица, если так, то переходим на ее парсинг
        IPDFElement element = elementIterator.next();
        if (!(element instanceof Table)) {
            // В противном случае выдаем сообщение об ошибке в лог, и продолжаем парсинг
            LOGGER.error("Can't parse implementer list for announcement {} lot {}", protocol.getAnnouncement().getPortalId(), lotNumber);
            return null;
        }
        List<SiteImplementer> retVal = new ArrayList<>();
        Table table = new UsualGlue((Table) element, elementIterator).checkAngGlue();
        for (int i = 1; i < table.getRows().size(); i++) {
            TableRow tableRow = table.getRows().get(i);
            if (tableRow.isEmpty())
                continue;
            SiteImplementer implementer = new SiteImplementer();
            implementer.setName(tableRow.getCells().get(1).getText());
            implementer.setXin(tableRow.getCells().get(2).getText().replaceAll(" ", ""));
            if (tableRow.getCells().size() >= 5) {
                implementer.setRequisites(tableRow.getCells().get(3).getText());
                implementer.setProposalTime(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS").parse(tableRow.getCells().get(4).getText()));
            } else
                implementer.setProposalTime(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS").parse(tableRow.getCells().get(3).getText()));
            implementer.setLotNumber(lotNumber);
            retVal.add(implementer);
        }
        return retVal;
    }

    private void parseDeniedImplementers(ListIterator<IPDFElement> elementIterator,
                                         String lotNumber, List<SiteImplementer> implementers) {
        // По структуре документа следом должна идти таблица, если так, то переходим на ее парсинг
        IPDFElement element = elementIterator.next();
        if (!(element instanceof Table)) {
            // В противном случае выдаем сообщение об ошибке в лог, и продолжаем парсинг
            LOGGER.error("Can't parse access denied implementer list for announcement {} lot {}", protocol.getAnnouncement().getPortalId(), lotNumber);
            return;
        }
        Table table = new UsualGlue((Table) element, elementIterator).checkAngGlue();
        for (int i = 1; i < table.getRows().size(); i++) {
            TableRow tableRow = table.getRows().get(i);
            if (tableRow.isEmpty())
                continue;
            String xin = tableRow.getCells().get(2).getText().replaceAll(" ", "");
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    implementer.setAccessGranted(false);
                    implementer.setDeniedCause(tableRow.getCells().get(3).getText());
                    break;
                }
            }
        }
    }

    private void parseGrantedImplementers(ListIterator<IPDFElement> elementIterator,
                                          String lotNumber, List<SiteImplementer> implementers) {
        // По структуре документа следом должна идти таблица, если так, то переходим на ее парсинг
        IPDFElement element = elementIterator.next();
        if (!(element instanceof Table)) {
            // В противном случае выдаем сообщение об ошибке в лог, и продолжаем парсинг
            LOGGER.error("Can't parse access granted implementer list for announcement {} lot {}", protocol.getAnnouncement().getPortalId(), lotNumber);
            return;
        }
        Table table = new UsualGlue((Table) element, elementIterator).checkAngGlue();
        for (int i = 1; i < table.getRows().size(); i++) {
            TableRow tableRow = table.getRows().get(i);
            if (tableRow.isEmpty())
                continue;
            String xin = tableRow.getCells().get(2).getText().replaceAll(" ", "");
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    implementer.setAccessGranted(true);
                    break;
                }
            }
        }
    }

    private void parseDiscounts(ListIterator<IPDFElement> elementIterator,
                                String lotNumber, List<SiteImplementer> implementers) {
        // По структуре документа следом должна идти таблица, если так, то переходим на ее парсинг
        IPDFElement element = elementIterator.next();
        if (!(element instanceof Table)) {
            // В противном случае выдаем сообщение об ошибке в лог, и продолжаем парсинг
            LOGGER.error("Can't parse discounts for announcement {} lot {}", protocol.getAnnouncement().getPortalId(), lotNumber);
            return;
        }
        Table table = new UsualGlue((Table) element, elementIterator).checkAngGlue();
        List<String> discountNames = new ArrayList<>(9);
        for (int i = 3; i < table.getRows().get(1).getCells().size(); i++) {
            TableCell cell = table.getRows().get(1).getCells().get(i);
            discountNames.add(cell.getText());
        }
        for (int i = 2; i < table.getRows().size(); i++) {
            TableRow tableRow = table.getRows().get(i);
            String xin = tableRow.getCells().get(2).getText().replaceAll(" ", "");
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    for (int j = 3; j < tableRow.getCells().size(); j++) {
                        String strValue = tableRow.getCells().get(j).getText();
                        if (!strValue.isEmpty() && !"0".equals(strValue)) {
                            SiteDiscount discount = new SiteDiscount();
                            implementer.getDiscounts().add(discount);
                            discount.setDiscountName(discountNames.get(j - 3));
                            discount.setDiscountValue(strValue);
                        }
                    }
                    break;
                }
            }
        }
    }

    private void parsePrices(ListIterator<IPDFElement> elementIterator,
                             String lotNumber, List<SiteImplementer> implementers) {
        // По структуре документа следом должна идти таблица, если так, то переходим на ее парсинг
        IPDFElement element = elementIterator.next();
        if (!(element instanceof Table)) {
            // В противном случае возвращаем итератор на шаг назад, выдаем сообщение об ошибке в лог, и продолжаем парсинг
            LOGGER.error("Can't parse prices for announcement {} lot {}", protocol.getAnnouncement().getPortalId(), lotNumber);
            return;
        }
        Table table = new PriceTableGlue((Table) element, elementIterator).checkAngGlue();
        for (int i = 2; i < table.getRows().size(); i++) {
            TableRow tableRow = table.getRows().get(i);
            if (tableRow.isEmpty())
                continue;
            String xin = tableRow.getCells().get(2).getText().replaceAll(" ", "");
            for (SiteImplementer implementer : implementers) {
                if (implementer.getXin().equals(xin)) {
                    implementer.setImplementerPrice(Double.valueOf(tableRow.getCells().get(4).getText().replaceAll(" ", "")));
                    String tmp = tableRow.getCells().get(5).getText().replaceAll(" ", "");
                    if (!StringUtils.isEmpty(tmp))
                        implementer.setAct26Price(Double.valueOf(tmp));
                    implementer.setFinalPrice(Double.valueOf(tableRow.getCells().get(7).getText().replaceAll(" ", "")));
                    break;
                }
            }
        }
    }

    private void parseWinner(ListIterator<IPDFElement> elementIterator, String lotNumber, List<SiteImplementer> implementers) {
        String lNumber = ((Text) elementIterator.next()).getText();
        if (lNumber.equals(lotNumber)) {
            String text = ((Text) elementIterator.next()).getText();
            if (" закупки: ".equals(text)) {
                String name = null;
                while (true) {
                    String txt = ((Text) elementIterator.next()).getText();
                    if (".".equals(txt))
                        break;
                    if (name == null)
                        name = txt;
                    else
                        name += " " + txt;
                }
                if (name != null)
                    // В некоторых протоколах победитель почему то не указан. Например конкурс 91582
                    for (SiteImplementer implementer : implementers) {
                        if (name.equals(implementer.getName())) {
                            implementer.setWinnerFlag(true);
                        }
                    }
            }
        }
    }
}

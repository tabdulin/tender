package kz.inessoft.egz.ejb.announcement.protocols.parsers;

import kz.inessoft.egz.dbentities.TProtocol;
import kz.inessoft.egz.ejb.announcement.comission.TenderComissionSiteProcessorLogic;
import kz.inessoft.egz.ejb.announcement.implementers.LotImplementersSiteProcessorLogic;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;

import javax.persistence.EntityManager;

/**
 * Created by alexey on 10.01.17.
 */
public class ParserBuilder {
    private EntityManager em;
    private TenderComissionSiteProcessorLogic tenderComissionSiteProcessorLogic;
    private LotImplementersSiteProcessorLogic lotImplementersSiteProcessorLogic;
    private PortalFilesLogic portalFilesLogic;
    private TProtocol protocol;

    public ParserBuilder setEm(EntityManager em) {
        this.em = em;
        return this;
    }

    public ParserBuilder setTenderComissionSiteProcessorLogic(TenderComissionSiteProcessorLogic tenderComissionSiteProcessorLogic) {
        this.tenderComissionSiteProcessorLogic = tenderComissionSiteProcessorLogic;
        return this;
    }

    public ParserBuilder setLotImplementersSiteProcessorLogic(LotImplementersSiteProcessorLogic lotImplementersSiteProcessorLogic) {
        this.lotImplementersSiteProcessorLogic = lotImplementersSiteProcessorLogic;
        return this;
    }

    public ParserBuilder setPortalFilesLogic(PortalFilesLogic portalFilesLogic) {
        this.portalFilesLogic = portalFilesLogic;
        return this;
    }

    public ParserBuilder setProtocol(TProtocol protocol) {
        this.protocol = protocol;
        return this;
    }

    public IProtocolParser build() {
        if ("\"text/html\"".equals(protocol.getPortalFile().getMimeType()))
            return new HTMLProtocolParser(em, tenderComissionSiteProcessorLogic, lotImplementersSiteProcessorLogic, portalFilesLogic, protocol);
        else if ("\"application/pdf\"".equals(protocol.getPortalFile().getMimeType()))
            return new PDFProtocolParser(em, tenderComissionSiteProcessorLogic, lotImplementersSiteProcessorLogic, portalFilesLogic, protocol);
        else
            throw new RuntimeException("Unknown mime type " + protocol.getPortalFile().getMimeType() + " for protocol " + protocol.getPortalFile().getPortalId());
    }
}

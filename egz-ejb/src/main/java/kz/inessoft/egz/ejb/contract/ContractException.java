package kz.inessoft.egz.ejb.contract;

import javax.ejb.ApplicationException;

/**
 * Created by alexey on 17.10.16.
 */
@ApplicationException(rollback = true)
public class ContractException extends Exception {
    public ContractException(Throwable cause) {
        super(cause);
    }
}

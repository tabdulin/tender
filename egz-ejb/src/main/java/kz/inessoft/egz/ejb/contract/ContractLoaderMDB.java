package kz.inessoft.egz.ejb.contract;

import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.contract.http.HTTPContractLoader;
import kz.inessoft.egz.ejb.contract.http.SiteContract;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created by alexey on 13.10.16.
 */
@MessageDriven(mappedName = "java:/jms/queue/AnnouncementsDownloadQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/ContractsDownloadQueue"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")
})
public class ContractLoaderMDB implements MessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractLoaderMDB.class);

    @Resource
    private MessageDrivenContext mdc;

    @EJB
    private ContractSiteProcessorLogic contractSiteProcessorLogic;

    @EJB
    private DicParticipantCompanyLogic companyLogic;

    @Override
    public void onMessage(Message msg) {
        Long portalId = null;
        try {
            portalId = msg.getLongProperty(JMSUtils.CONTRACT_ID_PARAM_NAME);
            LOGGER.info("Loading contract with portal ID {}", portalId);

            HTTPContractLoader loader = new HTTPContractLoader(portalId);
            SiteContract siteContract = loader.loadContract();
            contractSiteProcessorLogic.saveSiteContract(siteContract);
            contractSiteProcessorLogic.removeFromQueue(portalId);
            LOGGER.info("Contract {} loaded", portalId);
        } catch (JMSException | IOException | ParseException | SAXException | ParserConfigurationException | RequestException | XPathExpressionException e) {
            LOGGER.error("Can't to process contract {}", portalId, e);
            mdc.setRollbackOnly();
        }
    }
}

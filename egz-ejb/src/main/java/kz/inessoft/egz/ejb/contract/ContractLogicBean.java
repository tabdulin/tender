package kz.inessoft.egz.ejb.contract;

import kz.inessoft.egz.dbentities.CGeneralInfo;
import kz.inessoft.egz.dbentities.CGeneralInfoAnnouncementInfo;
import kz.inessoft.egz.dbentities.CGeneralInfoCustomerInfo;
import kz.inessoft.egz.dbentities.CGeneralInfoImplementerInfo;
import kz.inessoft.egz.dbentities.CGeneralInfoReasonDocumentInfo;
import kz.inessoft.egz.dbentities.CGeneralInfoShordDescription;
import kz.inessoft.egz.dbentities.CLoadQueue;
import kz.inessoft.egz.dbentities.CObject;
import kz.inessoft.egz.dbentities.CObjectAdditionalCharacteristic;
import kz.inessoft.egz.dbentities.CObjectName;
import kz.inessoft.egz.dbentities.CObjectShortDescription;
import kz.inessoft.egz.dbentities.CPaymentSpecific;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.contract.http.SiteContract;
import kz.inessoft.egz.ejb.contract.http.SiteContractCustomer;
import kz.inessoft.egz.ejb.contract.http.SiteContractImplementer;
import kz.inessoft.egz.ejb.contract.http.SiteContractItem;
import kz.inessoft.egz.ejb.contract.http.SiteContractItemSpecific;
import kz.inessoft.egz.ejbapi.DicContractBudjetTypeLogic;
import kz.inessoft.egz.ejbapi.DicContractConclusionTypeLogic;
import kz.inessoft.egz.ejbapi.DicContractObjectPurchaseTypeLogic;
import kz.inessoft.egz.ejbapi.DicContractObjectSpecificLogic;
import kz.inessoft.egz.ejbapi.DicContractPurchaseTypeLogic;
import kz.inessoft.egz.ejbapi.DicContractStatusLogic;
import kz.inessoft.egz.ejbapi.DicContractTypeLogic;
import kz.inessoft.egz.ejbapi.DicCurrencyLogic;
import kz.inessoft.egz.ejbapi.DicGovOrganizationLogic;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import kz.inessoft.egz.ejbapi.DicPurchaseModeLogic;
import kz.inessoft.egz.ejbapi.DicPurchaseObjectTypeLogic;
import kz.inessoft.egz.ejbapi.DicTruLogic;
import kz.inessoft.egz.ejbapi.DicUnitLogic;
import kz.inessoft.egz.ejbapi.LotLogic;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 14.10.16.
 * Специфики контракта есть например в договоре https://v3bl.goszakup.gov.kz/ru/egzcontract/cpublic/units/1590351
 */
@Stateless(name = "ContractLogicBean")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ContractLogicBean implements ContractSiteProcessorLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private DicContractTypeLogic contractTypeLogic;

    @EJB
    private DicPurchaseObjectTypeLogic purchaseObjectTypeLogic;

    @EJB
    private DicContractConclusionTypeLogic contractConclusionTypeLogic;

    @EJB
    private DicContractStatusLogic contractStatusLogic;

    @EJB
    private DicContractPurchaseTypeLogic contractPurchaseTypeLogic;

    @EJB
    private DicContractBudjetTypeLogic contractBudjetTypeLogic;

    @EJB
    private DicPurchaseModeLogic purchaseModeLogic;

    @EJB
    private DicCurrencyLogic currencyLogic;

    @EJB
    private DicGovOrganizationLogic dicGovOrganizationLogic;

    @EJB
    private DicImplementerLogic implementerLogic;

    @EJB
    private DicTruLogic truLogic;

    @EJB
    private DicUnitLogic dicUnitLogic;

    @EJB
    private DicContractObjectPurchaseTypeLogic contractObjectPurchaseTypeLogic;

    @EJB
    private DicContractObjectSpecificLogic contractObjectSpecificLogic;

    @EJB
    private LotLogic lotLogic;

    @Resource(lookup = "java:/jms/queue/ContractsDownloadQueue")
    private Queue queue;

    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;


    @Override
    public void saveSiteContract(SiteContract siteContract) {
        CGeneralInfo contract;
        try {
            contract = (CGeneralInfo) em.createQuery("from CGeneralInfo where portalId = :portalId")
                    .setParameter("portalId", siteContract.getPortalId())
                    .getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            contract = new CGeneralInfo();
        }
        contract.setPortalId(siteContract.getPortalId());
        contract.setLoadTime(siteContract.getLoadTime());
        contract.setGeneralTabHtml(siteContract.getGeneralTabHtml());
        contract.setObjectsTabHtml(siteContract.getObjectsTabHtml());
        contract.setCustomerTabHtml(siteContract.getCustomerTabHtml());
        contract.setTreasuryTabHtml(siteContract.getTreasuryTabHtml());
        contract.setAgreementTabHtml(siteContract.getAgreementTabHtml());
        contract.setContractType(contractTypeLogic.getValueByName(siteContract.getContractType()));
        contract.setReestrNumber(siteContract.getReestrNumber());
        contract.setNumber(siteContract.getNumber());
        contract.setMainContractReestrNumber(siteContract.getMainContractReestrNumber());
        contract.setMainContractPortalId(siteContract.getMainContractPortalId());
        CGeneralInfoAnnouncementInfo announcementInfo = new CGeneralInfoAnnouncementInfo();
        announcementInfo.setDocName(siteContract.getAnnouncementDocName());
        announcementInfo.setDocNumber(siteContract.getAnnouncementDocNumber());
        announcementInfo.setDocDate(siteContract.getAnnouncementDocDate());
        contract.setAnnouncementInfo(announcementInfo);
        contract.setConclusionDate(siteContract.getConclusionDate());
        contract.setCreationTime(siteContract.getCreationTime());
        contract.setLastChangedTime(siteContract.getLastChangedTime());
        CGeneralInfoShordDescription shordDescription = new CGeneralInfoShordDescription();
        shordDescription.setShortDescriptionKk(siteContract.getShortDescriptionKk());
        shordDescription.setShortDescriptionRu(siteContract.getShortDescriptionRu());
        contract.setShordDescription(shordDescription);
        contract.setPurchaseObjectType(purchaseObjectTypeLogic.getValueByName(siteContract.getPurchaseObjectType()));
        contract.setConclusionType(contractConclusionTypeLogic.getValueByName(siteContract.getConclusionType()));
        contract.setContractStatus(contractStatusLogic.getValueByName(siteContract.getContractStatus()));
        contract.setContractPurchaseType(contractPurchaseTypeLogic.getValueByName(siteContract.getContractPurchaseType()));
        contract.setFinancialYear(siteContract.getFinancialYear());
        contract.setBudjetType(contractBudjetTypeLogic.getValueByName(siteContract.getBudjetType()));
        contract.setFinancialSource(siteContract.getFinancialSource());
        contract.setPlanedPurchaseMode(purchaseModeLogic.getValueByName(siteContract.getPlanedPurchaseMode()));
        contract.setRealPurchaseMode(purchaseModeLogic.getValueByName(siteContract.getRealPurchaseMode()));
        contract.setPlanedSum(siteContract.getPlanedSum());
        contract.setResultContractSum(siteContract.getResultContractSum());
        contract.setTotalContractSum(siteContract.getTotalContractSum());
        contract.setRealContractSum(siteContract.getRealContractSum());
        contract.setCurrency(currencyLogic.getValueByCode(siteContract.getCurrency()));
        contract.setRate(siteContract.getRate());
        contract.setContractTime(siteContract.getContractTime());
        contract.setPlannedExecutionDate(siteContract.getPlannedExecutionDate());
        contract.setRealExecutionDate(siteContract.getRealExecutionDate());
        contract.setExecutionMarkDate(siteContract.getExecutionMarkDate());
        CGeneralInfoReasonDocumentInfo reasonDocumentInfo = new CGeneralInfoReasonDocumentInfo();
        reasonDocumentInfo.setDocNameKk(siteContract.getDocNameKk());
        reasonDocumentInfo.setDocNameRu(siteContract.getDocNameRu());
        reasonDocumentInfo.setDocNumber(siteContract.getDocNumber());
        reasonDocumentInfo.setDocDate(siteContract.getDocDate());
        contract.setReasonDocumentInfo(reasonDocumentInfo);
        SiteContractCustomer customer = siteContract.getCustomer();
        CGeneralInfoCustomerInfo customerInfo = new CGeneralInfoCustomerInfo();
        customerInfo.setCustomer(dicGovOrganizationLogic.getValueByBin(customer.getBin(), customer.getNameRu(), null));
        customerInfo.setNameKk(customer.getNameKk());
        customerInfo.setNameRu(customer.getNameRu());
        customerInfo.setBin(customer.getBin());
        customerInfo.setRnn(customer.getRnn());
        contract.setCustomerInfo(customerInfo);

        SiteContractImplementer implementer = siteContract.getImplementer();
        CGeneralInfoImplementerInfo implementerInfo = new CGeneralInfoImplementerInfo();
        String xin = implementer.getBin() == null ? implementer.getIin() : implementer.getBin();
        implementerInfo.setImplementer(implementerLogic.getValueByXin(xin, implementer.getNameRu()));
        implementerInfo.setNameKk(implementer.getNameKk());
        implementerInfo.setNameRu(implementer.getNameRu());
        implementerInfo.setBin(implementer.getBin());
        implementerInfo.setIin(implementer.getIin());
        implementerInfo.setRnn(implementer.getRnn());
        implementerInfo.setVatInfo(implementer.getVatInfo());
        contract.setImplementerInfo(implementerInfo);

        if (contract.getId() == null)
            em.persist(contract);

        processContractItems(siteContract.getContractItems(), contract);
    }

    @Override
    public void queueContractForLoad(Long portalId) throws ContractException {
        try {
            CLoadQueue loadQueue = em.find(CLoadQueue.class, portalId);
            if (loadQueue == null) {
                loadQueue = new CLoadQueue();
                loadQueue.setPortalId(portalId);
                loadQueue.setQueueTime(new Date());
                em.persist(loadQueue);

                Message message = jmsContext.createMessage();
                message.setLongProperty(JMSUtils.CONTRACT_ID_PARAM_NAME, portalId);
                message.setStringProperty(JMSUtils.MESSAGE_TYPE_PROPERTY_NAME, JMSUtils.CONTRACT_DOWNLOAD);
                JMSUtils.delayDeliveryForNight(message);

                jmsContext.createProducer().send(queue, message);
            }
        } catch (JMSException e) {
            throw new ContractException(e);
        }
    }

    @Override
    public void removeFromQueue(Long portalId) {
        em.createQuery("delete from CLoadQueue  where portalId = :portalId")
                .setParameter("portalId", portalId)
                .executeUpdate();
    }

    @Override
    public List<String> getInterestingAnnouncementNumbersWithoutContracts() {
        return em.createQuery("select distinct id.announcement.number from TLotCrossAnnouncement " +
                "where id.lot.dicTru.id in :truIds and hasContract = false and status.contractStatus = true")
                .setParameter("truIds", truLogic.getInterestingTruIds())
                .getResultList();
    }

    private void processContractItems(List<SiteContractItem> items, CGeneralInfo contract) {
        em.createQuery("delete from CObject where generalInfo = :generalInfo")
                .setParameter("generalInfo", contract)
                .executeUpdate();

        for (SiteContractItem item : items) {
            CObject contractItem = new CObject();
            contractItem.setPortalId(item.getPortalId());
            contractItem.setGeneralInfo(contract);
            contractItem.setOriginalHtml(item.getOriginalHtml());
            contractItem.setLotPortalId(item.getLotPortalId());
            contractItem.setPlanId(item.getPlanId());
            CObjectName name = new CObjectName();
            name.setNameKk(item.getItemNameKk());
            name.setNameRu(item.getItemNameRu());
            contractItem.setName(name);
            contractItem.setYear(item.getYear());
            contractItem.setTru(truLogic.getTruByCode(item.getTru(), item.getShortCharacteristicRu()));
            contractItem.setPurchaseObjectType(purchaseObjectTypeLogic.getValueByName(item.getPurchaseObjectType()));
            CObjectShortDescription shortDescription = new CObjectShortDescription();
            shortDescription.setShortDescriptionKk(item.getShortDescriptionKk());
            shortDescription.setShortDescriptionRu(item.getShortCharacteristicRu());
            contractItem.setShortDescription(shortDescription);
            CObjectAdditionalCharacteristic additionalCharacteristic = new CObjectAdditionalCharacteristic();
            additionalCharacteristic.setAdditionalCharacteristicKk(item.getAdditionalCharacteristicKk());
            additionalCharacteristic.setAdditionalCharacteristicRu(item.getAdditionalCharacteristicRu());
            contractItem.setAdditionalCharacteristic(additionalCharacteristic);
            contractItem.setPriceWithoutVAT(item.getPriceWithoutVAT());
            contractItem.setPriceWithVAT(item.getPriceWithVAT());
            contractItem.setValue(item.getValue());
            contractItem.setDicUnit(dicUnitLogic.getValueByName(item.getUnitName()));
            contractItem.setContractSumWithoutVAT(item.getContractSumWithoutVAT());
            contractItem.setContractVATSum(item.getContractVATSum());
            contractItem.setContractTotalSum(item.getContractTotalSum());
            contractItem.setRealContractSum(item.getRealContractSum());
            contractItem.setPlacement(item.getPlacement());
            contractItem.setPaymentSource(item.getPaymentSource());
            String month = item.getPlannedBuyMonth();
            if (month.equalsIgnoreCase("Январь"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.January);
            else if (month.equalsIgnoreCase("Февраль"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.February);
            else if (month.equalsIgnoreCase("Март"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.Mart);
            else if (month.equalsIgnoreCase("Апрель"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.April);
            else if (month.equalsIgnoreCase("Май"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.May);
            else if (month.equalsIgnoreCase("Июнь"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.June);
            else if (month.equalsIgnoreCase("Июль"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.July);
            else if (month.equalsIgnoreCase("Август"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.August);
            else if (month.equalsIgnoreCase("Сентябрь"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.September);
            else if (month.equalsIgnoreCase("Октябрь"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.October);
            else if (month.equalsIgnoreCase("Ноябрь"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.November);
            else if (month.equalsIgnoreCase("Декабрь"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.December);
            else if (month.equalsIgnoreCase("Прошлый год"))
                contractItem.setPlannedBuyMonth(CObject.EBuyMonth.LastYear);
            else
                throw new RuntimeException("Unknown month: " + month);
            contractItem.setPlanedExecutionDate(item.getPlannedExecutionDate());
            contractItem.setRealExecutionDate(item.getRealExecutionDate());
            contractItem.setMarkExecutionDate(item.getMarkExecutionDate());
            contractItem.setContractObjectPurchaseType(contractObjectPurchaseTypeLogic.getValueByName(item.getContractObjectPurchaseType()));
            em.persist(contractItem);

            for (SiteContractItemSpecific siteContractItemSpecific : item.getItemSpecificList()) {
                CPaymentSpecific paymentSpecific = new CPaymentSpecific();
                paymentSpecific.setCObject(contractItem);
                paymentSpecific.setContractObjectSpecific(contractObjectSpecificLogic.getValueByName(siteContractItemSpecific.getContractObjectSpecific()));
                paymentSpecific.setFinancialYear(siteContractItemSpecific.getFinancialYear());
                paymentSpecific.setSum(siteContractItemSpecific.getSum());
                em.persist(paymentSpecific);
            }
            if (contract.getAnnouncementInfo() != null &&
                    "Объявление о государственных закупках".equals(contract.getAnnouncementInfo().getDocName()))
                lotLogic.setLotHasContract(contractItem.getPlanId(), contract.getAnnouncementInfo().getDocNumber());
        }
    }
}

package kz.inessoft.egz.ejb.contract;

import kz.inessoft.egz.ejb.contract.http.SiteContract;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 14.10.16.
 */
@Local
public interface ContractSiteProcessorLogic {
    void saveSiteContract(SiteContract siteContract);

    void queueContractForLoad(Long portalId) throws ContractException;

    void removeFromQueue(Long portalId);

    List<String> getInterestingAnnouncementNumbersWithoutContracts();
}

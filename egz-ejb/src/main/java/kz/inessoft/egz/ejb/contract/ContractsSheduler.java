package kz.inessoft.egz.ejb.contract;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Created by alexey on 17.10.16.
 */
@Singleton
@Startup
public class ContractsSheduler {
    private volatile boolean started = false;

    @Schedule(dayOfWeek = "1,2,3,4,5", hour = "11,15", minute = "30", persistent = false)
    public void startLoadAllContracts() {
        if (!started) {
            started = true;
            new LoadAllContractsThread().start();
        }
    }
}

package kz.inessoft.egz.ejb.contract;

import kz.inessoft.egz.dbentities.DicContractObjectPurchaseType;
import kz.inessoft.egz.ejb.ASimpleDictionaryDAO;
import kz.inessoft.egz.ejbapi.DicContractObjectPurchaseTypeLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 14.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicContractObjectPurchaseTypeLogicBean extends ASimpleDictionaryDAO<DicContractObjectPurchaseType> implements DicContractObjectPurchaseTypeLogic {
    @Override
    protected Class<DicContractObjectPurchaseType> getEntityClass() {
        return DicContractObjectPurchaseType.class;
    }
}

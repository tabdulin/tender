package kz.inessoft.egz.ejb.contract;

import kz.inessoft.egz.dbentities.DicContractObjectSpecific;
import kz.inessoft.egz.ejb.ASimpleDictionaryDAO;
import kz.inessoft.egz.ejbapi.DicContractObjectSpecificLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 14.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicContractObjectSpecificLogicBean extends ASimpleDictionaryDAO<DicContractObjectSpecific> implements DicContractObjectSpecificLogic {
    @Override
    protected Class<DicContractObjectSpecific> getEntityClass() {
        return DicContractObjectSpecific.class;
    }
}

package kz.inessoft.egz.ejb.contract;

import kz.inessoft.egz.dbentities.DicContractStatus;
import kz.inessoft.egz.ejb.ASimpleDictionaryDAO;
import kz.inessoft.egz.ejbapi.DicContractStatusLogic;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * Created by alexey on 14.10.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DicContractStatusLogicBean extends ASimpleDictionaryDAO<DicContractStatus> implements DicContractStatusLogic {
    @Override
    protected Class<DicContractStatus> getEntityClass() {
        return DicContractStatus.class;
    }
}

package kz.inessoft.egz.ejb.contract;

import kz.inessoft.egz.ejb.JNDILookup;
import kz.inessoft.egz.ejb.contract.http.HTTPContractsIterator;
import kz.inessoft.egz.ejb.httpclient.AnonymousHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import org.xml.sax.SAXException;

import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

/**
 * Created by alexey on 17.10.16.
 */
public class LoadAllContractsThread extends Thread {
    @Override
    public void run() {
        AnonymousHttpClient client = null;
        try {
            client = HttpClientsPool.getInstance().getClient();
            ContractSiteProcessorLogic contractSiteProcessorLogic = (ContractSiteProcessorLogic) JNDILookup.lookupLocal(JNDILookup.CONTRACTS_LOGIC);
            HTTPContractsIterator contractsIterator = new HTTPContractsIterator(client, contractSiteProcessorLogic);
            while (contractsIterator.hasNext()) {
                Long portalId = contractsIterator.next();
                contractSiteProcessorLogic.queueContractForLoad(portalId);
            }
        } catch (RequestException | NamingException | IOException | ParserConfigurationException | SAXException | XPathExpressionException | ContractException e) {
            e.printStackTrace();
        } finally {
            if (client != null)
                HttpClientsPool.getInstance().freeClient(client);
        }
    }
}

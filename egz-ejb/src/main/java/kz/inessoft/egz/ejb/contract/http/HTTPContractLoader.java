package kz.inessoft.egz.ejb.contract.http;

import kz.inessoft.egz.ejb.httpclient.AnonymousHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 13.10.16.
 */
public class HTTPContractLoader {
    private long contractPortalId;

    public HTTPContractLoader(long contractPortalId) {
        this.contractPortalId = contractPortalId;
    }

    public SiteContract loadContract() throws RequestException, IOException, XPathExpressionException, ParserConfigurationException, SAXException, ParseException {
        AnonymousHttpClient client = HttpClientsPool.getInstance().getClient();
        try {
            Response response = client.sendGetRequest(PortalUrlUtils.createContractGeneralInfoURL(contractPortalId));
            SiteContract retVal = new SiteContract();
            retVal.setPortalId(contractPortalId);
            retVal.setGeneralTabHtml(response.getContent());
            retVal.setContractType(getValueFromTable(response, 1, "Тип"));
            retVal.setReestrNumber(getValueFromTable(response, 1, "Номер договора в реестре договоров"));
            retVal.setNumber(getValueFromTable(response, 1, "Номер договора"));
            Node node = getNodeFromTable(response, 1, "Номер основного договора в реестре договоров");
            if (node != null) {
                node = XMLUtils.evaluateXPathNode("./a", node);
                retVal.setMainContractReestrNumber(node.getTextContent());
                String href = node.getAttributes().getNamedItem("href").getNodeValue();
                retVal.setMainContractPortalId(Long.valueOf(href.substring(href.lastIndexOf('/') + 1)));
            }
            retVal.setAnnouncementDocName(getValueFromTable(response, 1, "Наименование документа объявления о проведении государственных закупок"));
            retVal.setAnnouncementDocNumber(getValueFromTable(response, 1, "Номер объявления о проведении государственных закупок"));
            String date = getValueFromTable(response, 1, "Дата объявления о проведении государственных закупок");
            SimpleDateFormat formatShort = new SimpleDateFormat("yyyy-MM-dd");
            if (date != null)
                retVal.setAnnouncementDocDate(formatShort.parse(date));
            String contractDate = getValueFromTable(response, 1, "Дата заключения договора");
            if (contractDate != null)
                retVal.setConclusionDate(formatShort.parse(contractDate));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            retVal.setCreationTime(dateFormat.parse(getValueFromTable(response, 1, "Дата создания договора")));
            retVal.setLastChangedTime(dateFormat.parse(getValueFromTable(response, 1, "Дата последнего изменения")));
            retVal.setShortDescriptionKk(getValueFromTable(response, 1, "Краткое содержание договора на казахском языке"));
            retVal.setShortDescriptionRu(getValueFromTable(response, 1, "Краткое соержание договора на русском языке"));
            retVal.setPurchaseObjectType(getValueFromTable(response, 1, "Вид предмета закупок"));
            retVal.setConclusionType(getValueFromTable(response, 1, "Форма заключения договора"));
            retVal.setContractStatus(getValueFromTable(response, 1, "Статус договора"));
            retVal.setContractPurchaseType(getValueFromTable(response, 2, "Тип закупки"));
            retVal.setFinancialYear(Integer.valueOf(getValueFromTable(response, 2, "Финансовый год")));
            retVal.setBudjetType(getValueFromTable(response, 2, "Вид бюджета"));
            retVal.setFinancialSource(getValueFromTable(response, 2, "Источник финансирования"));
            retVal.setPlanedPurchaseMode(getValueFromTable(response, 2, "Планируемый способ осуществления закупки"));
            retVal.setRealPurchaseMode(getValueFromTable(response, 2, "Фактический способ осуществления закупки"));
            retVal.setPlanedSum(Double.valueOf(getValueFromTable(response, 2, "Общая плановая сумма договора").replaceAll(" ", "")));
            retVal.setResultContractSum(Double.valueOf(getValueFromTable(response, 2, "Общая сумма договора по итогам закупки").replaceAll(" ", "")));
            retVal.setTotalContractSum(Double.valueOf(getValueFromTable(response, 2, "Общая итоговая сумма договора").replaceAll(" ", "")));
            String total = getValueFromTable(response, 2, "Общая фактическая сумма договора");
            if (total != null) {
                retVal.setRealContractSum(Double.valueOf(total.replaceAll(" ", "")));
            }
            retVal.setCurrency(getValueFromTable(response, 2, "Валюта договора"));
            retVal.setRate(Double.valueOf(getValueFromTable(response, 2, "Курс").replaceAll(" ", "")));
            retVal.setContractTime(formatShort.parse(getValueFromTable(response, 2, "Срок действия договора")));
            String plannedDate = getValueFromTable(response, 2, "Планируемая дата исполнения");
            if (plannedDate != null)
                retVal.setPlannedExecutionDate(formatShort.parse(plannedDate));
            String factDate = getValueFromTable(response, 2, "Фактическая дата исполнения");
            if (factDate != null) {
                retVal.setRealExecutionDate(formatShort.parse(factDate));
            }
            String markDate = getValueFromTable(response, 2, "Дата проставления отметки исполнения договора");
            if (markDate != null) {
                retVal.setExecutionMarkDate(formatShort.parse(markDate));
            }
            retVal.setDocNameKk(getValueFromTable(response, 2, "Наименование документа на государственном языке"));
            retVal.setDocNameRu(getValueFromTable(response, 2, "Наименование документа на русском языке"));
            retVal.setDocNumber(getValueFromTable(response, 2, "Номер"));
            String data = getValueFromTable(response, 2, "Дата");
            if (data != null)
                retVal.setDocDate(formatShort.parse(data));

            loadUnits(client, retVal);

            loadCustomer(client, retVal);

            if (response.evaluateXPathNode("//li/a[text()='Казначейство']") != null)
                retVal.setTreasuryTabHtml(client.sendGetRequest(PortalUrlUtils.createContractTreasureURL(contractPortalId)).getContent());

            retVal.setAgreementTabHtml(client.sendGetRequest(PortalUrlUtils.createContractAgreementURL(contractPortalId)).getContent());

            retVal.setLoadTime(new Date());
            return retVal;
        } finally {
            HttpClientsPool.getInstance().freeClient(client);
        }
    }

    private void loadUnits(AnonymousHttpClient client, SiteContract contract) throws RequestException, ParserConfigurationException, SAXException, XPathExpressionException, IOException, ParseException {
        Response response = client.sendGetRequest(PortalUrlUtils.createContractUnitsURL(contractPortalId));
        contract.setObjectsTabHtml(response.getContent());
        NodeList nodeList = response.evaluateXPathNodeList("//h3[text()='Предметы договора']/..//tr[position()>1]");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node row = nodeList.item(i);
            String planId = XMLUtils.evaluateXPathNode("./td[3]", row).getTextContent();
            Node link = XMLUtils.evaluateXPathNode("./td[2]/a", row);
            long portalId = Long.valueOf(link.getTextContent());
            HttpPost request = new HttpPost(PortalUrlUtils.HOST_URL + "/ru/egzcontract/cpublic/loadunit");
            List<NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("pid", String.valueOf(contractPortalId)));
            parameters.add(new BasicNameValuePair("unit_id", String.valueOf(portalId)));
            request.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            Response unitResponce = client.sendPostRequest(request);
            SiteContractItem contractItem = new SiteContractItem();
            contractItem.setPortalId(portalId);
            contractItem.setPlanId(Long.valueOf(planId));
            contractItem.setOriginalHtml(unitResponce.getContent());
            String lotId = getValueFromUnitTable(unitResponce, "ИД лота в закупке");
            if (lotId != null)
                contractItem.setLotPortalId(Long.valueOf(lotId));
            contractItem.setItemNameKk(getValueFromUnitTable(unitResponce, "Наименование закупаемых товаров, работ или услуг (на государственном языке)"));
            contractItem.setItemNameRu(getValueFromUnitTable(unitResponce, "Наименование закупаемых товаров, работ или услуг (на русском языке)"));
            contractItem.setYear(Integer.valueOf(getValueFromUnitTable(unitResponce, "Год")));
            String tru = getValueFromUnitTable(unitResponce, "СТРУ");
            contractItem.setTru(tru != null ? tru : getValueFromUnitTable(unitResponce, "КТРУ"));
            contractItem.setPurchaseObjectType(getValueFromUnitTable(unitResponce, "Вид предмета закупок"));
            contractItem.setShortDescriptionKk(getValueFromUnitTable(unitResponce, "Краткая характеристика (на государственном языке)"));
            contractItem.setShortCharacteristicRu(getValueFromUnitTable(unitResponce, "Краткая характеристика (на русском языке)"));
            contractItem.setAdditionalCharacteristicKk(getValueFromUnitTable(unitResponce, "Дополнительная характеристика (на государственном языке)"));
            contractItem.setAdditionalCharacteristicRu(getValueFromUnitTable(unitResponce, "Дополнительная характеристика (на русском языке)"));
            String price = getValueFromUnitTable(unitResponce, "Цена за единицу (без учета НДС)").replaceAll("тенге", " ").trim();
            contractItem.setPriceWithoutVAT(Double.valueOf(price.replaceAll(" ", "")));
            contractItem.setPriceWithVAT(Double.valueOf(getValueFromUnitTable(unitResponce, "Цена за единицу (с учетом НДС)").replaceAll(" ", "")));
            contractItem.setValue(Double.valueOf(getValueFromUnitTable(unitResponce, "Количество, объем").replaceAll(" ", "")));
            contractItem.setUnitName(getValueFromUnitTable(unitResponce, "Единица измерения"));
            String sum = getValueFromUnitTable(unitResponce, "Сумма по предмету договора (без учета НДС)").replaceAll("тенге", "").trim();
            contractItem.setContractSumWithoutVAT(Double.valueOf(sum.replaceAll(" ", "")));
            sum = getValueFromUnitTable(unitResponce, "Сумма НДС по предмету договора").replaceAll("тенге", "").trim();
            contractItem.setContractVATSum(Double.valueOf(sum.replaceAll(" ", "")));
            sum = getValueFromUnitTable(unitResponce, "Сумма по предмету договора (с учетом НДС)").replaceAll("тенге", "").trim();
            contractItem.setContractTotalSum(Double.valueOf(sum.replaceAll(" ", "")));
            sum = getValueFromUnitTable(unitResponce, "Сумма предмета договора (лот) исполненная, фактическая");
            if (sum != null) {
                sum = sum.replaceAll("тенге", "").trim();
                contractItem.setRealContractSum(Double.valueOf(sum.replaceAll(" ", "")));
            }
            contractItem.setPlacement(getValueFromUnitTable(unitResponce, "Место поставки, адрес"));
            contractItem.setPaymentSource(getValueFromUnitTable(unitResponce, "Источник финансирования"));
            contractItem.setPlannedBuyMonth(getValueFromUnitTable(unitResponce, "Планируемый срок закупки (месяц)"));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String plannedDate = getValueFromUnitTable(unitResponce, "Планируемый срок исполнения лота");
            if (plannedDate != null)
                contractItem.setPlannedExecutionDate(dateFormat.parse(plannedDate));
            String realDate = getValueFromUnitTable(unitResponce, "Фактический срок исполнения лота");
            if (realDate != null) {
                contractItem.setRealExecutionDate(dateFormat.parse(realDate));
            }

            String markDate = getValueFromUnitTable(unitResponce, "Дата установки признака исполнения лота");
            if (markDate != null) {
                contractItem.setMarkExecutionDate(dateFormat.parse(markDate));
            }

            contractItem.setContractObjectPurchaseType(getValueFromUnitTable(unitResponce, "Тип закупки"));
            contract.getContractItems().add(contractItem);

            NodeList nodeList1 = unitResponce.evaluateXPathNodeList("/div[@id='spec_sum_data']/table//tr[position() > 1]");
            for (int j = 0; j < nodeList1.getLength(); j++) {
                Node node = nodeList1.item(j);
                SiteContractItemSpecific itemSpecific = new SiteContractItemSpecific();
                itemSpecific.setContractObjectSpecific(XMLUtils.evaluateXPathNode("./td[1]", node).getTextContent().trim());
                itemSpecific.setFinancialYear(Integer.valueOf(XMLUtils.evaluateXPathNode("./td[2]", node).getTextContent()));
                itemSpecific.setSum(Double.valueOf(XMLUtils.evaluateXPathNode("./td[3]", node).getTextContent().replaceAll(" ", "")));
                contractItem.getItemSpecificList().add(itemSpecific);
            }
        }
    }

    private String getValueFromUnitTable(Response response, String header) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Node node = response.evaluateXPathNode("//table/tr[normalize-space(td)='" + header + "']/td[2]");
        if (node == null)
            return null;
        String retVal = node.getTextContent().trim();
        if (retVal.isEmpty()) return null;
        return retVal;
    }

    private void loadCustomer(AnonymousHttpClient client, SiteContract contract) throws RequestException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Response response = client.sendGetRequest(PortalUrlUtils.createContractCustomerURL(contractPortalId));
        contract.setCustomerTabHtml(response.getContent());
        SiteContractCustomer customer = new SiteContractCustomer();
        customer.setNameKk(getDataFromCustomerTable(response, "Наименование заказчика (на государственном языке)"));
        customer.setNameRu(getDataFromCustomerTable(response, "Наименование заказчика (на русском языке)"));
        customer.setBin(getDataFromCustomerTable(response, "БИН"));
        customer.setRnn(getDataFromCustomerTable(response, "РНН"));
        contract.setCustomer(customer);

        SiteContractImplementer implementer = new SiteContractImplementer();
        implementer.setNameKk(getDataFromImplementerTable(response, "Наименование поставщика (на государственном языке)"));
        implementer.setNameRu(getDataFromImplementerTable(response, "Наименование поставщика (на русском языке)"));
        implementer.setBin(getDataFromImplementerTable(response, "БИН"));
        implementer.setIin(getDataFromImplementerTable(response, "ИИН"));
        implementer.setRnn(getDataFromImplementerTable(response, "РНН"));
        implementer.setVatInfo(getDataFromImplementerTable(response, "Плательщик НДС"));
        contract.setImplementer(implementer);
    }

    private String getDataFromCustomerTable(Response response, String header) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Node node = response.evaluateXPathNode("//div[normalize-space(h3)='Заказчик']/table//tr[normalize-space(td)='" + header + "']/td[2]");
        if (node != null) {
            String retVal = node.getTextContent().trim();
            if (!retVal.isEmpty())
                return retVal;
        }
        return null;
    }

    private String getDataFromImplementerTable(Response response, String header) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Node node = response.evaluateXPathNode("//div[normalize-space(h3)='Поставщик']/table//tr[normalize-space(td)='" + header + "']/td[2]");
        if (node != null) {
            String retVal = node.getTextContent().trim();
            if (!retVal.isEmpty())
                return retVal;
        }
        return null;
    }

    private String getValueFromTable(Response response, int tableNumber, String header) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Node node = getNodeFromTable(response, tableNumber, header);
        if (node == null) return null;
        String textContent = node.getTextContent().trim();
        if (textContent.isEmpty())
            return null;
        return textContent;
    }

    private Node getNodeFromTable(Response response, int tableNumber, String header) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        return response.evaluateXPathNode("//div[@id='home']/div[" + tableNumber + "]/table/tr[normalize-space(td)='" + header + "']/td[2]");
    }
}

package kz.inessoft.egz.ejb.contract.http;

import kz.inessoft.egz.ejb.contract.ContractSiteProcessorLogic;
import kz.inessoft.egz.ejb.httpclient.AnonymousHttpClient;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 17.10.16.
 */
public class HTTPContractsIterator implements Iterator<Long> {
    private AnonymousHttpClient client;
    private List<String> contractNumbers;
    private int currentNumberIdx = 0;
    private Iterator<Long> delegator = Collections.emptyIterator();

    /**
     * @param client                     AuthorizedHttpClient, через который будут запрашиваться страницы. Обратить внимание что итератор его не закрывает, т.о. возвращать его в пул должен вызывающий класс
     * @param contractSiteProcessorLogic Через этот бин запрашиваются номера завершенных объявлений, которые еще не имеют договоров
     * @throws RequestException
     * @throws UnsupportedEncodingException
     */
    public HTTPContractsIterator(AnonymousHttpClient client, ContractSiteProcessorLogic contractSiteProcessorLogic) throws RequestException, IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        this.client = client;
        contractNumbers = contractSiteProcessorLogic.getInterestingAnnouncementNumbersWithoutContracts();
    }

    @Override
    public boolean hasNext() {
        if (delegator.hasNext())
            return true;

        if (currentNumberIdx == contractNumbers.size())
            return false;

        try {
            while (!delegator.hasNext()) {
                String number = contractNumbers.get(currentNumberIdx++);
                HttpPost request = new HttpPost(PortalUrlUtils.createContractsListURL(null));
                List<NameValuePair> parameters = new ArrayList<>();
                parameters.add(new BasicNameValuePair("filter[buy_id]", number));
                parameters.add(new BasicNameValuePair("filter_search", "Искать"));
                request.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                Response response = client.sendPostRequest(request);
                NodeList nodeList = response.evaluateXPathNodeList("//table[@class='table table-lot']//tr[position()>1]");
                List<Long> lst = new LinkedList<>();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node item = nodeList.item(i);
                    lst.add(Long.valueOf(XMLUtils.evaluateXPathNode("./td[1]", item).getTextContent()));
                }
                delegator = lst.iterator();
                if (!delegator.hasNext() && currentNumberIdx == contractNumbers.size())
                    return false;
            }
            return true;
        } catch (RequestException | SAXException | IOException | XPathExpressionException | ParserConfigurationException e) {
            throw new RuntimeException("Unable to get contracts list", e);
        }
    }

    @Override
    public Long next() {
        return delegator.next();
    }
}

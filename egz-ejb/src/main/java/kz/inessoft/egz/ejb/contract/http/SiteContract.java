package kz.inessoft.egz.ejb.contract.http;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 13.10.16.
 */
public class SiteContract {
    private long portalId;

    private Date loadTime;

    /**
     * Содержимое вкладки "Общее"
     */
    private String generalTabHtml;

    /**
     * Содержимое вкладки "Предметы договора"
     */
    private String objectsTabHtml;

    /**
     * Содержимое вкладки "Заказчик и поставщик"
     */
    private String customerTabHtml;

    /**
     * Содержимое вкладки "Казначейство"
     */
    private String treasuryTabHtml;

    /**
     * Содержимое вкладки "Договор и согласование"
     */
    private String agreementTabHtml;

    /**
     * Тип
     */
    private String contractType;

    /**
     * Номер договора в реестре договоров
     */
    private String reestrNumber;

    /**
     * Номер договора
     */
    private String number;

    /**
     * Номер основного договора в реестре договоров
     */
    private String mainContractReestrNumber;
    private long mainContractPortalId;

    /**
     * Наименование документа объявления о проведении государственных закупок
     */
    private String announcementDocName;

    /**
     * Номер объявления о проведении государственных закупок
     */
    private String announcementDocNumber;

    /**
     * Дата объявления о проведении государственных закупок
     */
    private Date announcementDocDate;

    /**
     * Дата заключения договора
     */
    private Date conclusionDate;

    /**
     * Дата создания договора
     */
    private Date creationTime;

    /**
     * Дата последнего изменения
     */
    private Date lastChangedTime;

    /**
     * Краткое содержание договора на казахском языке
     */
    private String shortDescriptionKk;

    /**
     * Краткое соержание договора на русском языке
     */
    private String shortDescriptionRu;

    /**
     * Вид предмета закупок
     */
    private String purchaseObjectType;

    /**
     * Форма заключения договора
     */
    private String conclusionType;

    /**
     * Статус договора
     */
    private String contractStatus;

    /**
     *
     Тип закупки
     */
    private String contractPurchaseType;

    /**
     * Финансовый год
     */
    private Integer financialYear;

    /**
     * Вид бюджета
     */
    private String budjetType;

    /**
     * Источник финансирования
     */
    private String financialSource;

    /**
     * Планируемый способ осуществления закупки
     */
    private String planedPurchaseMode;

    /**
     * Фактический способ осуществления закупки
     */
    private String realPurchaseMode;

    /**
     * Общая плановая сумма договора
     */
    private Double planedSum;

    /**
     * Общая сумма договора по итогам закупки
     */
    private Double resultContractSum;

    /**
     * Общая итоговая сумма договора
     */
    private Double totalContractSum;

    /**
     * Общая фактическая сумма договора
     */
    private Double realContractSum;

    /**
     * Валюта договора
     */
    private String currency;

    /**
     * Курс
     */
    private Double rate;

    /**
     * Срок действия договора
     */
    private Date contractTime;

    /**
     * Планируемая дата исполнения
     */
    private Date plannedExecutionDate;

    /**
     * Фактическая дата исполнения
     */
    private Date realExecutionDate;

    /**
     * Дата проставления отметки исполнения договора
     */
    private Date executionMarkDate;

    /**
     * Реквизиты документа, подтверждающего основание заключения договора
     * Наименование документа на государственном языке
     */
    private String docNameKk;

    /**
     * Реквизиты документа, подтверждающего основание заключения договора
     * Наименование документа на русском языке
     */
    private String docNameRu;

    /**
     * Реквизиты документа, подтверждающего основание заключения договора
     * Номер
     */
    private String docNumber;

    /**
     * Реквизиты документа, подтверждающего основание заключения договора
     * Дата
     */
    private Date docDate;

    /**
     * Заказчик
     */
    private SiteContractCustomer customer;

    /**
     * Поставщик
     */
    private SiteContractImplementer implementer;

    /**
     * Предметы договора
     */
    private List<SiteContractItem> contractItems = new ArrayList<>();

    public long getPortalId() {
        return portalId;
    }

    public void setPortalId(long portalId) {
        this.portalId = portalId;
    }

    public Date getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(Date loadTime) {
        this.loadTime = loadTime;
    }

    public String getGeneralTabHtml() {
        return generalTabHtml;
    }

    public void setGeneralTabHtml(String generalTabHtml) {
        this.generalTabHtml = generalTabHtml;
    }

    public String getObjectsTabHtml() {
        return objectsTabHtml;
    }

    public void setObjectsTabHtml(String objectsTabHtml) {
        this.objectsTabHtml = objectsTabHtml;
    }

    public String getCustomerTabHtml() {
        return customerTabHtml;
    }

    public void setCustomerTabHtml(String customerTabHtml) {
        this.customerTabHtml = customerTabHtml;
    }

    public String getTreasuryTabHtml() {
        return treasuryTabHtml;
    }

    public void setTreasuryTabHtml(String treasuryTabHtml) {
        this.treasuryTabHtml = treasuryTabHtml;
    }

    public String getAgreementTabHtml() {
        return agreementTabHtml;
    }

    public void setAgreementTabHtml(String agreementTabHtml) {
        this.agreementTabHtml = agreementTabHtml;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getReestrNumber() {
        return reestrNumber;
    }

    public void setReestrNumber(String reestrNumber) {
        this.reestrNumber = reestrNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMainContractReestrNumber() {
        return mainContractReestrNumber;
    }

    public void setMainContractReestrNumber(String mainContractReestrNumber) {
        this.mainContractReestrNumber = mainContractReestrNumber;
    }

    public long getMainContractPortalId() {
        return mainContractPortalId;
    }

    public void setMainContractPortalId(long mainContractPortalId) {
        this.mainContractPortalId = mainContractPortalId;
    }

    public String getAnnouncementDocName() {
        return announcementDocName;
    }

    public void setAnnouncementDocName(String announcementDocName) {
        this.announcementDocName = announcementDocName;
    }

    public String getAnnouncementDocNumber() {
        return announcementDocNumber;
    }

    public void setAnnouncementDocNumber(String announcementDocNumber) {
        this.announcementDocNumber = announcementDocNumber;
    }

    public Date getAnnouncementDocDate() {
        return announcementDocDate;
    }

    public void setAnnouncementDocDate(Date announcementDocDate) {
        this.announcementDocDate = announcementDocDate;
    }

    public Date getConclusionDate() {
        return conclusionDate;
    }

    public void setConclusionDate(Date conclusionDate) {
        this.conclusionDate = conclusionDate;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getLastChangedTime() {
        return lastChangedTime;
    }

    public void setLastChangedTime(Date lastChangedTime) {
        this.lastChangedTime = lastChangedTime;
    }

    public String getShortDescriptionKk() {
        return shortDescriptionKk;
    }

    public void setShortDescriptionKk(String shortDescriptionKk) {
        this.shortDescriptionKk = shortDescriptionKk;
    }

    public String getShortDescriptionRu() {
        return shortDescriptionRu;
    }

    public void setShortDescriptionRu(String shortDescriptionRu) {
        this.shortDescriptionRu = shortDescriptionRu;
    }

    public String getPurchaseObjectType() {
        return purchaseObjectType;
    }

    public void setPurchaseObjectType(String purchaseObjectType) {
        this.purchaseObjectType = purchaseObjectType;
    }

    public String getConclusionType() {
        return conclusionType;
    }

    public void setConclusionType(String conclusionType) {
        this.conclusionType = conclusionType;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getContractPurchaseType() {
        return contractPurchaseType;
    }

    public void setContractPurchaseType(String contractPurchaseType) {
        this.contractPurchaseType = contractPurchaseType;
    }

    public Integer getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(Integer financialYear) {
        this.financialYear = financialYear;
    }

    public String getBudjetType() {
        return budjetType;
    }

    public void setBudjetType(String budjetType) {
        this.budjetType = budjetType;
    }

    public String getFinancialSource() {
        return financialSource;
    }

    public void setFinancialSource(String financialSource) {
        this.financialSource = financialSource;
    }

    public String getPlanedPurchaseMode() {
        return planedPurchaseMode;
    }

    public void setPlanedPurchaseMode(String planedPurchaseMode) {
        this.planedPurchaseMode = planedPurchaseMode;
    }

    public String getRealPurchaseMode() {
        return realPurchaseMode;
    }

    public void setRealPurchaseMode(String realPurchaseMode) {
        this.realPurchaseMode = realPurchaseMode;
    }

    public Double getPlanedSum() {
        return planedSum;
    }

    public void setPlanedSum(Double planedSum) {
        this.planedSum = planedSum;
    }

    public Double getResultContractSum() {
        return resultContractSum;
    }

    public void setResultContractSum(Double resultContractSum) {
        this.resultContractSum = resultContractSum;
    }

    public Double getTotalContractSum() {
        return totalContractSum;
    }

    public void setTotalContractSum(Double totalContractSum) {
        this.totalContractSum = totalContractSum;
    }

    public Double getRealContractSum() {
        return realContractSum;
    }

    public void setRealContractSum(Double realContractSum) {
        this.realContractSum = realContractSum;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Date getContractTime() {
        return contractTime;
    }

    public void setContractTime(Date contractTime) {
        this.contractTime = contractTime;
    }

    public Date getPlannedExecutionDate() {
        return plannedExecutionDate;
    }

    public void setPlannedExecutionDate(Date plannedExecutionDate) {
        this.plannedExecutionDate = plannedExecutionDate;
    }

    public Date getRealExecutionDate() {
        return realExecutionDate;
    }

    public void setRealExecutionDate(Date realExecutionDate) {
        this.realExecutionDate = realExecutionDate;
    }

    public Date getExecutionMarkDate() {
        return executionMarkDate;
    }

    public void setExecutionMarkDate(Date executionMarkDate) {
        this.executionMarkDate = executionMarkDate;
    }

    public String getDocNameKk() {
        return docNameKk;
    }

    public void setDocNameKk(String docNameKk) {
        this.docNameKk = docNameKk;
    }

    public String getDocNameRu() {
        return docNameRu;
    }

    public void setDocNameRu(String docNameRu) {
        this.docNameRu = docNameRu;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    public SiteContractCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(SiteContractCustomer customer) {
        this.customer = customer;
    }

    public SiteContractImplementer getImplementer() {
        return implementer;
    }

    public void setImplementer(SiteContractImplementer implementer) {
        this.implementer = implementer;
    }

    public List<SiteContractItem> getContractItems() {
        return contractItems;
    }
}

package kz.inessoft.egz.ejb.contract.http;

/**
 * Created by alexey on 13.10.16.
 */
public class SiteContractCustomer {
    /**
     *
     Наименование заказчика (на государственном языке)
     */
    private String nameKk;

    /**
     * Наименование заказчика (на русском языке)
     */
    private String nameRu;

    /**
     * БИН
     */
    private String bin;

    /**
     * РНН
     */
    private String rnn;

    public String getNameKk() {
        return nameKk;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getRnn() {
        return rnn;
    }

    public void setRnn(String rnn) {
        this.rnn = rnn;
    }
}

package kz.inessoft.egz.ejb.contract.http;

/**
 * Created by alexey on 13.10.16.
 */
public class SiteContractImplementer {
    /**
     * Наименование поставщика (на государственном языке)
     */
    private String nameKk;

    /**
     * Наименование поставщика (на русском языке)
     */
    private String nameRu;

    /**
     * БИН
     */
    private String bin;

    /**
     * ИИН
     */
    private String iin;

    /**
     * РНН
     */
    private String rnn;

    /**
     * Плательщик НДС
     */
    private String vatInfo;

    public String getNameKk() {
        return nameKk;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }

    public String getRnn() {
        return rnn;
    }

    public void setRnn(String rnn) {
        this.rnn = rnn;
    }

    public String getVatInfo() {
        return vatInfo;
    }

    public void setVatInfo(String vatInfo) {
        this.vatInfo = vatInfo;
    }
}

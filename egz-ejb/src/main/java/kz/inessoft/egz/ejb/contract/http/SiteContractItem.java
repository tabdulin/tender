package kz.inessoft.egz.ejb.contract.http;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 13.10.16.
 */
public class SiteContractItem {
    private long portalId;

    private String originalHtml;

    /**
     * ИД пункта плана
     */
    private Long planId;

    /**
     * ИД лота в закупке
     */
    private Long lotPortalId;

    /**
     * Наименование закупаемых товаров, работ или услуг (на государственном языке)
     */
    private String itemNameKk;

    /**
     * Наименование закупаемых товаров, работ или услуг (на русском языке)
     */
    private String itemNameRu;

    /**
     * Год
     */
    private int year;

    /**
     * КТРУ
     */
    private String tru;

    /**
     * Вид предмета закупок
     */
    private String purchaseObjectType;

    /**
     * Краткая характеристика (на государственном языке)
     */
    private String shortDescriptionKk;

    /**
     * Краткая характеристика (на русском языке)
     */
    private String shortCharacteristicRu;

    /**
     * Дополнительная характеристика (на государственном языке)
     */
    private String additionalCharacteristicKk;

    /**
     * Дополнительная характеристика (на русском языке)
     */
    private String additionalCharacteristicRu;

    /**
     * Цена за единицу (без учета НДС)
     */
    private Double priceWithoutVAT;

    /**
     * Цена за единицу (с учетом НДС)
     */
    private Double priceWithVAT;

    /**
     * Количество, объем
     */
    private Double value;

    /**
     * Единица измерения
     */
    private String unitName;

    /**
     * Сумма по предмету договора (без учета НДС)
     */
    private Double contractSumWithoutVAT;

    /**
     * Сумма НДС по предмету договора
     */
    private Double contractVATSum;

    /**
     * Сумма по предмету договора (с учетом НДС)
     */
    private Double contractTotalSum;

    /**
     * Сумма предмета договора (лот) исполненная, фактическая
     */
    private Double realContractSum;

    /**
     * Место поставки, адрес
     */
    private String placement;

    /**
     * Источник финансирования
     */
    private String paymentSource;

    /**
     * Планируемый срок закупки (месяц)
     */
    private String plannedBuyMonth;

    /**
     * Планируемый срок исполнения лота
     */
    private Date plannedExecutionDate;

    /**
     * Фактический срок исполнения лота
     */
    private Date realExecutionDate;

    /**
     * Дата установки признака исполнения лота
     */
    private Date markExecutionDate;

    /**
     * Тип закупки
     */
    private String contractObjectPurchaseType;

    private List<SiteContractItemSpecific> itemSpecificList = new ArrayList<>();

    public long getPortalId() {
        return portalId;
    }

    public void setPortalId(long portalId) {
        this.portalId = portalId;
    }

    public String getOriginalHtml() {
        return originalHtml;
    }

    public void setOriginalHtml(String originalHtml) {
        this.originalHtml = originalHtml;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getLotPortalId() {
        return lotPortalId;
    }

    public void setLotPortalId(Long lotPortalId) {
        this.lotPortalId = lotPortalId;
    }

    public String getItemNameKk() {
        return itemNameKk;
    }

    public void setItemNameKk(String itemNameKk) {
        this.itemNameKk = itemNameKk;
    }

    public String getItemNameRu() {
        return itemNameRu;
    }

    public void setItemNameRu(String itemNameRu) {
        this.itemNameRu = itemNameRu;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTru() {
        return tru;
    }

    public void setTru(String tru) {
        this.tru = tru;
    }

    public String getPurchaseObjectType() {
        return purchaseObjectType;
    }

    public void setPurchaseObjectType(String purchaseObjectType) {
        this.purchaseObjectType = purchaseObjectType;
    }

    public String getShortDescriptionKk() {
        return shortDescriptionKk;
    }

    public void setShortDescriptionKk(String shortDescriptionKk) {
        this.shortDescriptionKk = shortDescriptionKk;
    }

    public String getShortCharacteristicRu() {
        return shortCharacteristicRu;
    }

    public void setShortCharacteristicRu(String shortCharacteristicRu) {
        this.shortCharacteristicRu = shortCharacteristicRu;
    }

    public String getAdditionalCharacteristicKk() {
        return additionalCharacteristicKk;
    }

    public void setAdditionalCharacteristicKk(String additionalCharacteristicKk) {
        this.additionalCharacteristicKk = additionalCharacteristicKk;
    }

    public String getAdditionalCharacteristicRu() {
        return additionalCharacteristicRu;
    }

    public void setAdditionalCharacteristicRu(String additionalCharacteristicRu) {
        this.additionalCharacteristicRu = additionalCharacteristicRu;
    }

    public Double getPriceWithoutVAT() {
        return priceWithoutVAT;
    }

    public void setPriceWithoutVAT(Double priceWithoutVAT) {
        this.priceWithoutVAT = priceWithoutVAT;
    }

    public Double getPriceWithVAT() {
        return priceWithVAT;
    }

    public void setPriceWithVAT(Double priceWithVAT) {
        this.priceWithVAT = priceWithVAT;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Double getContractSumWithoutVAT() {
        return contractSumWithoutVAT;
    }

    public void setContractSumWithoutVAT(Double contractSumWithoutVAT) {
        this.contractSumWithoutVAT = contractSumWithoutVAT;
    }

    public Double getContractVATSum() {
        return contractVATSum;
    }

    public void setContractVATSum(Double contractVATSum) {
        this.contractVATSum = contractVATSum;
    }

    public Double getContractTotalSum() {
        return contractTotalSum;
    }

    public void setContractTotalSum(Double contractTotalSum) {
        this.contractTotalSum = contractTotalSum;
    }

    public Double getRealContractSum() {
        return realContractSum;
    }

    public void setRealContractSum(Double realContractSum) {
        this.realContractSum = realContractSum;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public String getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(String paymentSource) {
        this.paymentSource = paymentSource;
    }

    public String getPlannedBuyMonth() {
        return plannedBuyMonth;
    }

    public void setPlannedBuyMonth(String plannedBuyMonth) {
        this.plannedBuyMonth = plannedBuyMonth;
    }

    public Date getPlannedExecutionDate() {
        return plannedExecutionDate;
    }

    public void setPlannedExecutionDate(Date plannedExecutionDate) {
        this.plannedExecutionDate = plannedExecutionDate;
    }

    public Date getRealExecutionDate() {
        return realExecutionDate;
    }

    public void setRealExecutionDate(Date realExecutionDate) {
        this.realExecutionDate = realExecutionDate;
    }

    public Date getMarkExecutionDate() {
        return markExecutionDate;
    }

    public void setMarkExecutionDate(Date markExecutionDate) {
        this.markExecutionDate = markExecutionDate;
    }

    public String getContractObjectPurchaseType() {
        return contractObjectPurchaseType;
    }

    public void setContractObjectPurchaseType(String contractObjectPurchaseType) {
        this.contractObjectPurchaseType = contractObjectPurchaseType;
    }

    public List<SiteContractItemSpecific> getItemSpecificList() {
        return itemSpecificList;
    }
}

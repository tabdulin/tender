package kz.inessoft.egz.ejb.contract.http;

/**
 * Created by alexey on 14.10.16.
 */
public class SiteContractItemSpecific {
    private String contractObjectSpecific;
    private int financialYear;
    private Double sum;

    public String getContractObjectSpecific() {
        return contractObjectSpecific;
    }

    public void setContractObjectSpecific(String contractObjectSpecific) {
        this.contractObjectSpecific = contractObjectSpecific;
    }

    public int getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(int financialYear) {
        this.financialYear = financialYear;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }
}

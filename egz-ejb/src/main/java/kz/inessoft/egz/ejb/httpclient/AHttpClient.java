package kz.inessoft.egz.ejb.httpclient;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * Created by alexey on 18.10.16.
 */
public abstract class AHttpClient {
    public static final Logger LOGGER = LoggerFactory.getLogger(AHttpClient.class);
    //    private static final long MIN_DELAY = 2500;
//    private static final long MAX_DELAY = 5000;
    private CloseableHttpClient client;
//    private long lastRequestTime;

    protected CloseableHttpClient getClient() {
        if (client == null) {
            client = ClosableHttpClientFactory.getInstance().createClient();
        }
        return client;
    }

    public CloseableHttpResponse openUrl(HttpGet httpGet) throws RequestException {
        try {
            return HttpUtils.makeRequest(httpGet, getClient());
        } catch (IOException | InterruptedException e) {
            throw new RequestException("Can't send get request", e);
        }
    }

    public abstract Response sendPostRequest(HttpPost request) throws RequestException;

    public abstract Response sendGetRequest(String url) throws RequestException;

    protected abstract Logger getLogger();

    protected Response executeRequest(HttpRequestBase request, IResponceChecker responceChecker) throws Exception {
        while (true) {
//            long currTime = System.currentTimeMillis();
//            long currentDelay = currTime - lastRequestTime;
//            if (currentDelay < MIN_DELAY) {
//                long newDelay = MIN_DELAY + (long) (Math.random() * (MAX_DELAY - MIN_DELAY)) - currentDelay;
//                LOGGER.info("Sleep for {} mseconds", newDelay);
//                Thread.sleep(newDelay);
//            }
            Response response;
            while (true) {
                try {
                    HttpUtils.ResponceWrapper responceWrapper = HttpUtils.makeExtendedRequest(request, getClient());
                    response = new Response(request, responceWrapper.getCloseableHttpResponse(), responceWrapper.getContext());
                    break;
                } catch (SSLException | SocketTimeoutException | SocketException e) {
                    // Если произошла ошибка соединения покажем сообщение, поспим 5 сек и повторим запрос
                    // todo Надо бы это покрасивее сделать. Выкидывать исключение наверх, там обрабатывать, отключать нерабочий прокси и т.д.
                    LOGGER.error("Connection error while make request. Repeating request");
                }
            }
//            lastRequestTime = System.currentTimeMillis();
            if (!responceChecker.isResponceCorrect(response)) {
                // Если ошибка 4XX или 5XXX то уснуть на минуту
                int pause = response.getStatus().getStatusCode() == 503 ? 5 : 60;
                getLogger().warn("Server returned status {} for URI {}. Sleep for {} seconds", response.getStatus(), request.getURI().toString(), pause);
                Thread.sleep(pause * 1000);
            } else
                return response;
        }
    }
}

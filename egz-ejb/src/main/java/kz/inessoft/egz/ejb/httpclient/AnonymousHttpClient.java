package kz.inessoft.egz.ejb.httpclient;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by alexey on 18.10.16.
 */
public class AnonymousHttpClient extends AHttpClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnonymousHttpClient.class);

    @Override
    public Response sendPostRequest(HttpPost request) throws RequestException {
        try {
            return executeRequest(request, new DefaultResponceChecker());
        } catch (Exception e) {
            throw new RequestException("Can't post request", e);
        }
    }

    @Override
    public Response sendGetRequest(String url) throws RequestException {
        try {
            HttpGet httpGet = new HttpGet(url);
            return executeRequest(httpGet, new DefaultResponceChecker());
        } catch (Exception e) {
            throw new RequestException("Can't get request", e);
        }
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}

package kz.inessoft.egz.ejb.httpclient;

import org.apache.http.Header;

/**
 * Created by alexey on 24.10.16.
 * Этот класс скажет что ответ корректен в случае, когда сервер вернул 403-ю ошибку, но содержимое страницы не является заглушкой. Т.е. это значит что нас не забанили, а просто сессия сдохла и надо перелогиниться
 */
public class AuthResponceChecker extends DefaultResponceChecker {
    @Override
    public boolean isResponceCorrect(Response response) throws Exception {
        int statusCode = response.getStatus().getStatusCode();
        Header contentType = response.getFirstHeader("Content-Type");
        return statusCode == 403 && contentType != null &&
                ("text/html; charset=utf-8".equals(contentType.getValue()) || "text/html".equals(contentType.getValue())) &&
                response.evaluateXPathNodeList("//ul/li/a[@href='https://v3bl.goszakup.gov.kz/ru/user/login']").getLength() != 0 ||
                super.isResponceCorrect(response);
    }
}

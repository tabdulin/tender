package kz.inessoft.egz.ejb.httpclient;

import kz.inessoft.egz.dbentities.IPortalUser;
import kz.inessoft.egz.ejb.sign.SignUtils;
import kz.inessoft.egz.ejb.sign.SignXMLInfo;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 15.09.16.
 * Внимание! Этот метод openUrl(HttpGet httpGet) не проверяет залогинен ли пользователь. По этому если критически важно что бы пользователь был залогинен, необходимо перед
 * вызовом этого метода вызвать checkLogin. Связано с тем что надо вернуть объект класса CloseableHttpResponse с сырым, не вычитанным входным потоком
 * А проверить авторизацию можно только вычитав поток.
 */
public class AuthorizedHttpClient extends AHttpClient {
    public static final Logger LOGGER = LoggerFactory.getLogger(AuthorizedHttpClient.class);
    private static final String LOGIN_XML_TEMPLATE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><root><key>${key}</key></root>";
    private IPortalUser portalUser;

    AuthorizedHttpClient(IPortalUser portalUser) {
        this.portalUser = portalUser;
    }

    public Response sendGetRequest(String url) throws RequestException {
        try {
            HttpGet httpGet = new HttpGet(url);
            return checkLoginAndExecute(httpGet);
        } catch (Exception e) {
            throw new RequestException("Can't send get request", e);
        }
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    public void checkLogin() throws RequestException {
        sendGetRequest("https://v3bl.goszakup.gov.kz/ru/cabinet/profile");
    }

    public Response sendPostRequest(HttpPost request) throws RequestException {
        try {
            return checkLoginAndExecute(request);
        } catch (Exception e) {
            throw new RequestException("Can't post request", e);
        }
    }

    private Response checkLoginAndExecute(HttpRequestBase request) throws Exception {
        while (true) {
            Response response = executeRequest(request, new AuthResponceChecker());
            Header contentType = response.getFirstHeader("Content-Type");
            if (contentType != null &&
                    ("text/html; charset=utf-8".equals(contentType.getValue()) || "text/html".equals(contentType.getValue())) &&
                    response.evaluateXPathNodeList("//ul/li/a[@href='https://v3bl.goszakup.gov.kz/ru/user/login']").getLength() != 0) {
                LOGGER.warn("User logged out. Authentication...");
                login();
                continue;
            }
            return response;
        }
    }

    IPortalUser getPortalUser() {
        return portalUser;
    }

    private void login() throws Exception {
        // Запрос токена на портале
        HttpPost httpPost = new HttpPost("https://v3bl.goszakup.gov.kz/ru/user/sendkey/kz");
        Response response = executeRequest(httpPost, new DefaultResponceChecker());
        String id = response.getContent();

        // Подготовка и подписание XML
        String xmlToSign = LOGIN_XML_TEMPLATE.replace("${key}", id);
        SignXMLInfo signXMLInfo = SignUtils.signXML(xmlToSign, portalUser.getEdsPrivateKey(), portalUser.getEdsPassword());

        // Отправка подписанной XML и сертификата пользователя
        HttpPost postRequest = new HttpPost("https://v3bl.goszakup.gov.kz/user/sendsign/kz");
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("sign", signXMLInfo.getSignedXML()));
        nvps.add(new BasicNameValuePair("key", signXMLInfo.getBase64Certificate()));
        postRequest.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
        response = executeRequest(postRequest, new DefaultResponceChecker());
        if (!"<script>document.location.href = 'https://v3bl.goszakup.gov.kz/ru/user/auth_confirm'</script>".equals(response.getContent())) {
            // Если сервер вернул не тот ответ - значит что-то пошло не так. Ошибка авторизации
            throw new RequestException("Can't login to portal. Server doesn't accept this EDS. Response status " +
                    response.getStatus());
        }

        // Передача пароля пользователя
        postRequest = new HttpPost("https://v3bl.goszakup.gov.kz/ru/user/auth_confirm");
        nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("password", getPortalUser().getPortalPassword()));
        postRequest.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
        response = executeRequest(postRequest, new DefaultResponceChecker());

        // В ответ сервер должен вернуть редирект на профиль
        Header header = response.getFirstHeader(HttpHeaders.LOCATION);
        if (header == null || !"https://v3bl.goszakup.gov.kz/ru/cabinet/profile".equals(header.getValue()))
            throw new RequestException("Can't login to portal. Server doesn't accept user password. Response status " +
                    response.getStatus());
    }
}

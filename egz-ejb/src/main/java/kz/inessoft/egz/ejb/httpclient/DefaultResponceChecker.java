package kz.inessoft.egz.ejb.httpclient;

/**
 * Created by alexey on 24.10.16.
 */
public class DefaultResponceChecker implements IResponceChecker {
    @Override
    public boolean isResponceCorrect(Response response) throws Exception {
        int statusCode = response.getStatus().getStatusCode();
        return !(statusCode >= 400 && statusCode <= 599);
    }
}

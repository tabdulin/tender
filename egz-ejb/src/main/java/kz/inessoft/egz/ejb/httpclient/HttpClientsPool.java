package kz.inessoft.egz.ejb.httpclient;

import kz.inessoft.egz.dbentities.IPortalUser;
import org.apache.commons.pool2.BaseKeyedPooledObjectFactory;
import org.apache.commons.pool2.KeyedObjectPool;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericKeyedObjectPool;

/**
 * Created by alexey on 15.09.16.
 */
public class HttpClientsPool {
    private class Factory extends BaseKeyedPooledObjectFactory<IPortalUser, AuthorizedHttpClient> {

        @Override
        public AuthorizedHttpClient create(IPortalUser portalUser) throws Exception {
            return new AuthorizedHttpClient(portalUser);
        }

        @Override
        public PooledObject<AuthorizedHttpClient> wrap(AuthorizedHttpClient httpClient) {
            return new DefaultPooledObject<>(httpClient);
        }
    }

    private static HttpClientsPool instance;
    private KeyedObjectPool<IPortalUser, AuthorizedHttpClient> objectPool = new GenericKeyedObjectPool<>(new Factory());

    private HttpClientsPool() {
    }

    public static HttpClientsPool getInstance() {
        if (instance == null) {
            instance = new HttpClientsPool();
        }
        return instance;
    }

    public AuthorizedHttpClient getClient(IPortalUser portalUser) {
        try {
            return objectPool.borrowObject(portalUser);
        } catch (Exception e) {
            throw new RuntimeException("Can't create new authorizedHttpClient", e);
        }
    }

    public AnonymousHttpClient getClient() {
        // Анонимных клиентов кэшировать больше не будем.
        return new AnonymousHttpClient();
    }

    public void freeClient(AHttpClient client) {
        try {
            if (client instanceof AuthorizedHttpClient) {
                AuthorizedHttpClient authorizedHttpClient = (AuthorizedHttpClient) client;
                objectPool.returnObject(authorizedHttpClient.getPortalUser(), authorizedHttpClient);
            }

            if (client instanceof AnonymousHttpClient)
                client.getClient().close();
        } catch (Exception e) {
            throw new RuntimeException("Can't return httpClient to the pool", e);
        }
    }
}

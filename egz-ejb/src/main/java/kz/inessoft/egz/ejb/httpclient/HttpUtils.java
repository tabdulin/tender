package kz.inessoft.egz.ejb.httpclient;

import kz.inessoft.egz.dbentities.SystemProxy;
import kz.inessoft.egz.ejb.JNDILookup;
import kz.inessoft.egz.ejbapi.ESysParam;
import kz.inessoft.egz.ejbapi.IContentStreamProcessor;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import kz.inessoft.egz.ejbapi.SysConfig;
import org.apache.http.HttpHost;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.execchain.RequestAbortedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import javax.net.ssl.SSLException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by alexey on 10.10.16.
 */
public class HttpUtils {
    private static class RequestInfo {
        private long time;
        private HttpRequestBase request;

        public RequestInfo(HttpRequestBase request) {
            this.request = request;
            this.time = System.currentTimeMillis();
        }

        public long getTime() {
            return time;
        }

        public HttpRequestBase getRequest() {
            return request;
        }
    }

    public interface IResponceProcessor {
        void processResponce(CloseableHttpResponse response) throws Exception;
    }

    public static class ResponceWrapper {
        private CloseableHttpResponse closeableHttpResponse;
        private HttpClientContext context;

        public ResponceWrapper(CloseableHttpResponse closeableHttpResponse, HttpClientContext context) {
            this.closeableHttpResponse = closeableHttpResponse;
            this.context = context;
        }

        public CloseableHttpResponse getCloseableHttpResponse() {
            return closeableHttpResponse;
        }

        public HttpClientContext getContext() {
            return context;
        }
    }

    static {
        new Timer(true).schedule(new TimerTask() {
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();
                LOGGER.info("Aborting long requests");
                synchronized (REQUEST_INFOS) {
                    for (RequestInfo requestInfo : REQUEST_INFOS) {
                        if ((currentTime - requestInfo.getTime()) >= (10 * 60 * 1000)) {
                            LOGGER.warn("Aborting long request {}", requestInfo.getRequest().getURI());
                            requestInfo.getRequest().abort();
                        } else
                            break;
                    }
                }
            }
        }, 60 * 1000, 60 * 1000);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtils.class);
    private static final int MS_STEP = 5000;
    private static final int MIN_FIRST_DELAY = 15000;
    private static final List<RequestInfo> REQUEST_INFOS = new LinkedList<>();

    public static CloseableHttpResponse makeRequest(HttpRequestBase request, CloseableHttpClient client) throws IOException, InterruptedException {
        return makeExtendedRequest(request, client).getCloseableHttpResponse();
    }

    public static ResponceWrapper makeExtendedRequest(HttpRequestBase request, CloseableHttpClient client) throws IOException, InterruptedException {
        LOGGER.info("Requesting {}", request.getURI().toString());

        // Проверка что прошлый запрос был достаточно давно, и если прошло мало времени - усыпить поток на время.
        int delayCount = 0;
        while (true) {
            HttpClientContext context;
            CloseableHttpResponse response;
            while (true) {
                context = new HttpClientContext();
                RequestConfig.Builder builder = RequestConfig.custom()
                        .setSocketTimeout(60000)
                        .setConnectionRequestTimeout(10 * 60 * 1000)
                        .setConnectTimeout(20000);
                configureProxy(builder);
                request.setConfig(builder.build());
                RequestInfo requestInfo = new RequestInfo(request);
                synchronized (REQUEST_INFOS) {
                    REQUEST_INFOS.add(requestInfo);
                }
                try {
                    response = client.execute(request, context);
                    break;
                } catch (InternalError | SSLException | ConnectTimeoutException | SocketTimeoutException | SocketException | RequestAbortedException | NoHttpResponseException e) {
                    // Если произошла ошибка соединения покажем сообщение, поспим 5 сек и повторим запрос
                    // todo Надо бы это покрасивее сделать. Выкидывать исключение наверх, там обрабатывать, отключать нерабочий прокси и т.д.
                    LOGGER.error("Connection error while make request. Repeating request");
                    request.reset();
                } finally {
                    synchronized (REQUEST_INFOS) {
                        REQUEST_INFOS.remove(requestInfo);
                    }
                }
            }
            List<URI> redirectLocations = context.getRedirectLocations();
            if (redirectLocations != null && !redirectLocations.isEmpty() &&
                    redirectLocations.get(redirectLocations.size() - 1).toString().endsWith("/ru/captcha")) {
                // Обнаружено перенаправление на капчу. Обработаем его.
                response.close();
                // Первая и вторая задержки должна быть минимум 15 секунд, т.к. практически установлено, что бан снимается либо через 15 сек. либо через 30,
                // остальные случайные возрастающие через 5. сек.
                delayCount++;
                int sleepRnd;
                if (delayCount <= 2)
                    sleepRnd = (int) (MIN_FIRST_DELAY + MS_STEP * Math.random());
                else
                    sleepRnd = (int) (MS_STEP * (delayCount - 2) * Math.random());
                LOGGER.warn("Portal requested captha for URI {}. Sleep for {} m.second and renew request...", request.getURI(), sleepRnd);
                Thread.sleep(sleepRnd);
                continue;
            }
            LOGGER.info("Request for URI {} completed", request.getURI().toString());
            return new ResponceWrapper(response, context);
        }
    }

    private static void configureProxy(RequestConfig.Builder builder) {
        try {
            SysConfig sysConfig;
            sysConfig = (SysConfig) JNDILookup.lookupLocal(JNDILookup.SYS_CONFIG_NAME);
            String proxyType = sysConfig.getSysParamValue(ESysParam.PROXY_TYPE);
            if (proxyType == null || "NONE".equals(proxyType))
                return;
            String proxyHost;
            Integer proxyPort;
            if ("RAND".equals(proxyType)) {
                ProxyLogic proxyLogic = (ProxyLogic) JNDILookup.lookupLocal(JNDILookup.PROXY_LOGIC);
                SystemProxy randomProxy = proxyLogic.getRandomProxy();
                proxyHost = randomProxy.getId().getProxyHost();
                proxyPort = randomProxy.getId().getProxyPort();
            } else {
                proxyHost = sysConfig.getSysParamValue(ESysParam.PROXY_HOST);
                proxyPort = Integer.valueOf(sysConfig.getSysParamValue(ESysParam.PROXY_PORT));
            }
            LOGGER.info("Making socket with proxy {}:{}", proxyHost, proxyPort);
            builder.setProxy(new HttpHost(proxyHost, proxyPort, "http"));
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    public static void downloadFileAndProcess(long portalFileId, IResponceProcessor responceProcessor) throws Exception {
        CloseableHttpClient client = ClosableHttpClientFactory.getInstance().createClient();
        HttpGet httpGet = new HttpGet(PortalUrlUtils.createDownloadFileUrl(portalFileId));
        CloseableHttpResponse response = null;
        while (true)
            try {
                response = HttpUtils.makeRequest(httpGet, client);
                responceProcessor.processResponce(response);
                break;
            } catch (SSLException | SocketTimeoutException e) {
                LOGGER.error("Error while download file. Repeat request.");
            } finally {
                try {
                    if (response != null)
                        response.close();
                } catch (IOException e1) {
                    LOGGER.error("Error while close stream.", e1);
                }
            }
    }

    public static void downloadFileAndProcess(long portalFileId, IContentStreamProcessor streamProcessor) throws Exception {
        downloadFileAndProcess(portalFileId,
                (IResponceProcessor) response -> streamProcessor.processContent(response.getEntity().getContent()));
    }
}

package kz.inessoft.egz.ejb.httpclient;

/**
 * Created by alexey on 24.10.16.
 */
public interface IResponceChecker {
    boolean isResponceCorrect(Response response) throws Exception;
}

package kz.inessoft.egz.ejb.httpclient;

import kz.inessoft.egz.dbentities.SystemProxy;

import javax.ejb.Local;

/**
 * Created by alexey on 17.01.17.
 */
@Local
public interface ProxyLogic {
    SystemProxy getRandomProxy();
}

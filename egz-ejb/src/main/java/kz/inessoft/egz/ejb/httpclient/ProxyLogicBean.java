package kz.inessoft.egz.ejb.httpclient;

import kz.inessoft.egz.dbentities.SystemProxy;
import kz.inessoft.egz.dbentities.SystemProxyId_;
import kz.inessoft.egz.dbentities.SystemProxy_;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by alexey on 17.01.17.
 */
@Singleton(name = "ProxyLogic")
public class ProxyLogicBean implements ProxyLogic {
    private List<SystemProxy> proxyList;
    private Long lastLoadTime;
    private static final long MAX_TIME = 5 * 1000;

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;


    @Override
    public SystemProxy getRandomProxy() {
        if (lastLoadTime == null || System.currentTimeMillis() - lastLoadTime > MAX_TIME) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<SystemProxy> query = builder.createQuery(SystemProxy.class);
            Root<SystemProxy> root = query.from(SystemProxy.class);
            query.where(builder.and(
                    builder.equal(root.get(SystemProxy_.id).get(SystemProxyId_.proxyType), "HTTP"),
                    builder.equal(root.get(SystemProxy_.available), Boolean.TRUE))
            );
            proxyList = em.createQuery(query).getResultList();
            lastLoadTime = System.currentTimeMillis();
        }
        return proxyList.get((int) (Math.random() * proxyList.size()));
    }
}

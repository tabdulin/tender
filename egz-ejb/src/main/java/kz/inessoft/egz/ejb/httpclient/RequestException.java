package kz.inessoft.egz.ejb.httpclient;

/**
 * Created by alexey on 16.09.16.
 */
public class RequestException extends Exception {
    public RequestException(String message) {
        super(message);
    }

    public RequestException(String message, Throwable cause) {
        super(message, cause);
    }
}

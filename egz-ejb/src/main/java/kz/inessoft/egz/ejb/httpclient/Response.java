package kz.inessoft.egz.ejb.httpclient;

import kz.inessoft.egz.ejb.XMLUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 15.09.16.
 */
public class Response {
    private HttpClientContext context;
    private HttpRequestBase request;
    private byte[] byteContent;
    private String contentHtml;
    private StatusLine status;
    private org.w3c.dom.Document document;
    private Map<String, List<Header>> headersMap = new HashMap<>();

    public Response(HttpRequestBase request, CloseableHttpResponse httpResponse, HttpClientContext context) throws IOException {
        this.context = context;
        this.request = request;
        try {
            status = httpResponse.getStatusLine();
            for (Header header : httpResponse.getAllHeaders()) {
                List<Header> lst = headersMap.get(header.getName());
                if (lst == null) {
                    lst = new ArrayList<>();
                    headersMap.put(header.getName(), lst);
                }
                lst.add(header);
            }
            byteContent = IOUtils.toByteArray(httpResponse.getEntity().getContent());
        } finally {
            httpResponse.close();
        }
    }

    public StatusLine getStatus() {
        return status;
    }

    public Header getFirstHeader(String headerName) {
        List<Header> headers = headersMap.get(headerName);
        if (headers == null) return null;
        return headers.get(0);
    }

    public String getContent() throws IOException {
        if (contentHtml == null) {
            contentHtml = new String(byteContent, "UTF-8");
        }
        return contentHtml;
    }

    private Document getDocument() throws IOException, SAXException, ParserConfigurationException {
        if (document == null) {
            document = XMLUtils.getDocument(getContent());
        }
        return document;
    }

    public NodeList evaluateXPathNodeList(String xpath) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        return XMLUtils.evaluateXPathNodeList(xpath, getDocument());
    }

    public Node evaluateXPathNode(String xpath) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        return XMLUtils.evaluateXPathNode(xpath, getDocument());
    }

    public URI getRealRequestURI() {
        List<URI> redirectLocations = context.getRedirectLocations();
        if (redirectLocations != null && !redirectLocations.isEmpty())
            return redirectLocations.get(redirectLocations.size() - 1);
        return request.getURI();
    }
}

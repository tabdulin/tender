package kz.inessoft.egz.ejb.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 27.07.16.
 */
public class Document {
    private List<Page> pages = new ArrayList<Page>();

    public List<Page> getPages() {
        return pages;
    }
}

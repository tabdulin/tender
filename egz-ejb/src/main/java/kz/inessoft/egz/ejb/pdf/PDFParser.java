package kz.inessoft.egz.ejb.pdf;

import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alexey on 27.07.16.
 */
public class PDFParser {
    private static class PDFText {
        private Point2D.Float textPoint;
        private String text;

        PDFText(Point2D.Float textPoint, String text) {
            this.textPoint = textPoint;
            this.text = text;
        }

        Point2D.Float getTextPoint() {
            return textPoint;
        }

        String getText() {
            return text;
        }
    }

    private static class PDFTableCell {
        private Rectangle2D.Float rectangle;
        private String text;

        PDFTableCell(Rectangle2D.Float rectangle, String text) {
            this.rectangle = rectangle;
            this.text = text;
        }

        Rectangle2D.Float getRectangle() {
            return rectangle;
        }

        String getText() {
            return text;
        }
    }

    private static class TableInfo {
        private Table table;
        private Rectangle2D.Float rectangle;

        TableInfo(Rectangle2D.Float rectangle) {
            this.table = new Table();
            this.rectangle = rectangle;
        }

        Table getTable() {
            return table;
        }

        Rectangle2D.Float getRectangle() {
            return rectangle;
        }
    }

    private enum EIntersection {
        /**
         * Если 2-ой прямоугольник по Y-оси пересекается с первым на 90% и больше
         */
        RIGHT_OR_INNER,
        /**
         * Если 2-ой прямоугольник по Y-оси пересекается с 1-м меньше чем на 90%, либо отстоит от первого вниз не более чем на 0.01
         */
        BOTTOM,

        /**
         * Если прямоугольники не пересекаются и находятся в отдалении друг от друга (больше чем на 0.01)
         */
        NONE
    }


    public static Document parsePDF(PDDocument document) throws IOException {
        PDPageTree pages = document.getPages();
        Document retVal = new Document();
        for (PDPage page : pages) {
            List<PDFText> texts = new ArrayList<>();
            List<Rectangle2D.Float> rectangles = new ArrayList<>();
            List<COSBase> arguments = new ArrayList<>();
            PDFStreamParser parser = new PDFStreamParser(page);
            Object token = parser.parseNextToken();
            PDFTextStripperByArea stripperByArea = new PDFTextStripperByArea();
            stripperByArea.setSortByPosition(true);
            Point2D.Float textPosition = null;
            while (token != null) {
                if (token instanceof COSObject) {
                    arguments.add(((COSObject) token).getObject());
                } else if (token instanceof Operator) {
                    if ("Td".equals(((Operator) token).getName())) {
                        COSNumber x = (COSNumber) arguments.get(0);
                        COSNumber y = (COSNumber) arguments.get(1);
                        textPosition = new Point2D.Float(x.floatValue(), y.floatValue());
                    } else if ("Tj".equals(((Operator) token).getName())) {
                        texts.add(new PDFText(textPosition, new String(((COSString) arguments.get(0)).getBytes(), "UTF16")));
                    } else if ("re".equals(((Operator) token).getName())) {
                        COSNumber x = (COSNumber) arguments.get(0);
                        COSNumber y = (COSNumber) arguments.get(1);
                        COSNumber w = (COSNumber) arguments.get(2);
                        COSNumber h = (COSNumber) arguments.get(3);

                        float x1 = x.floatValue();
                        float y1 = y.floatValue();
                        float w1 = w.floatValue();
                        float h1 = h.floatValue();
                        if (h1 < 0) {
                            y1 = y1 + h1;
                            h1 = -h1;
                        }

                        if (w1 < 0) {
                            x1 = x1 + w1;
                            w1 = -w1;
                        }
                        rectangles.add(new Rectangle2D.Float(x1, y1, w1, h1));
                    }
                    arguments = new ArrayList<>();
                } else {
                    arguments.add((COSBase) token);
                }
                token = parser.parseNextToken();
            }

            // Тут начинается самый секс...
            // На оптимальность алгоритма не претендую.
            // Отсортируем текстовки в порядке их появления на странице (сверху вниз). Начало координат - левый нижний угол
            Collections.sort(texts, (o1, o2) -> {
                if (o1.getTextPoint().getY() < o2.getTextPoint().getY())
                    return 1;
                if (o1.getTextPoint().getY() > o2.getTextPoint().getY())
                    return -1;
                if (o1.getTextPoint().getX() > o2.getTextPoint().getX())
                    return 1;
                if (o1.getTextPoint().getX() < o2.getTextPoint().getX())
                    return -1;
                return 0;
            });

            // Отсортируем прямоугольники в порядке их появления на странице
            Collections.sort(rectangles, (o1, o2) -> {
                if (o1.getY() < o2.getY())
                    return 1;
                if (o1.getY() > o2.getY())
                    return -1;
                if (o1.getX() > o2.getX())
                    return 1;
                if (o1.getX() < o2.getX())
                    return -1;
                return 0;
            });

            // Переберем все прямоугольники, найдем находящийся в них текст, и эти текстовки перенесем в новую коллекцию ячеек.
            // Координатой ячейки будет левый верхний угол
            List<PDFTableCell> tableCells = new ArrayList<>();
            for (Rectangle2D.Float rectangle : rectangles) {
                List<PDFText> selectedTexts = texts.stream().filter(text -> rectangle.contains(text.getTextPoint())).collect(Collectors.toList());
                texts.removeAll(selectedTexts);
                StringBuilder buf = new StringBuilder();
                for (PDFText selectedText : selectedTexts) {
                    if (buf.length() != 0)
                        buf.append(" ");
                    buf = buf.append(selectedText.getText());
                }
                tableCells.add(new PDFTableCell(rectangle, buf.toString()));
            }

            // Переберем все ячейки и объединим их в таблицы
            List<TableInfo> tableInfos = new ArrayList<>();
            TableInfo tableInfo = null;
            for (PDFTableCell tableCell : tableCells) {
                EIntersection intersection;
                if (tableInfo == null)
                    intersection = EIntersection.NONE;
                else
                    intersection = checkInterSection(tableInfo.getRectangle(), tableCell.getRectangle());
                switch (intersection) {
                    case RIGHT_OR_INNER:
                        // Ячейка уже входит в текущую таблицу, значит строка уже создана. Добавим в конец ячейку
                        List<TableRow> rows = tableInfo.getTable().getRows();
                        rows.get(rows.size() - 1).getCells().add(new TableCell(tableCell.getText()));
                        tableInfo.rectangle = (Rectangle2D.Float) tableInfo.getRectangle().createUnion(tableCell.getRectangle());
                        break;
                    case BOTTOM:
                        // Ячейка не входит в текущие границы таблицы, но прилегает к ней снизу. Значит таблицу надо расширить
                        // и добавить в нее строку
                        TableRow row = new TableRow();
                        tableInfo.getTable().getRows().add(row);
                        row.getCells().add(new TableCell(tableCell.getText()));
                        tableInfo.rectangle = (Rectangle2D.Float) tableInfo.getRectangle().createUnion(tableCell.getRectangle());
                        break;
                    case NONE:
                        // В противном случае имеем дело с отдельной таблицей. Надо ее создавать.
                        tableInfo = new TableInfo(tableCell.rectangle);
                        tableInfos.add(tableInfo);
                        row = new TableRow();
                        tableInfo.getTable().getRows().add(row);
                        row.getCells().add(new TableCell(tableCell.getText()));
                }
            }
            Page page1 = new Page();
            retVal.getPages().add(page1);
            // Добавим в страницу текстовки и таблицы в порядке их следования в документе
            while (!tableInfos.isEmpty() || !texts.isEmpty()) {
                if (tableInfos.isEmpty()) {
                    PDFText text = texts.remove(0);
                    page1.getElements().add(new Text(text.getText()));
                    continue;
                }
                if (texts.isEmpty()) {
                    TableInfo table = tableInfos.remove(0);
                    page1.getElements().add(table.getTable());
                    continue;
                }
                PDFText text = texts.get(0);
                TableInfo table = tableInfos.get(0);
                if (text.getTextPoint().getY() > table.getRectangle().getY() + table.getRectangle().getHeight()) {
                    page1.getElements().add(new Text(text.getText()));
                    texts.remove(0);
                } else {
                    page1.getElements().add(table.getTable());
                    tableInfos.remove(0);
                }
            }
        }
        return retVal;
    }

    private static EIntersection checkInterSection(Rectangle2D r1, Rectangle2D r2) {
        final double error = 0.01;
        double x1 = r1.getX();
        double y1 = r1.getY();
        double w1 = r1.getWidth();
        double h1 = r1.getHeight();
        double x2 = r2.getX();
        double y2 = r2.getY();
        double w2 = r2.getWidth();
        double h2 = r2.getHeight();

        // Проверка на RIGHT_OR_INNER
        double yBot = Math.max(y1, y2);
        double yTop = Math.min(y1 + h1, y2 + h2);
        double height = yTop - yBot;
        if (height >= 0.9 * h2)
            // По Y пересекаются на 90% и больше
            return EIntersection.RIGHT_OR_INNER;
        else if (height >= 0 || Math.abs(height) <= error)
            // Пересекаются по Y на небольшую величину, либо стоят очень близко друг к другу по вертикали
            return EIntersection.BOTTOM;
        else
            // Не пересекаются
            return EIntersection.NONE;
    }

    public static void main(String[] args) {
        try {
            PDDocument document = PDDocument.load(new File("/home/alexey/work/InesSoft/tender/materials/pdfbox/files/buy_pi_0310_164121571.pdf"));
            parsePDF(document);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package kz.inessoft.egz.ejb.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 27.07.16.
 */
public class Page {
    private List<IPDFElement> elements = new ArrayList<IPDFElement>();

    public List<IPDFElement> getElements() {
        return elements;
    }
}

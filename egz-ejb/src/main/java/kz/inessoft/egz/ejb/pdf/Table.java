package kz.inessoft.egz.ejb.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 27.07.16.
 */
public class Table implements IPDFElement {
    private List<TableRow> rows = new ArrayList<TableRow>();

    public List<TableRow> getRows() {
        return rows;
    }
}

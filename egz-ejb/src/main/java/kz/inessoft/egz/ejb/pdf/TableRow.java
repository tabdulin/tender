package kz.inessoft.egz.ejb.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 27.07.16.
 */
public class TableRow {
    private List<TableCell> cells = new ArrayList<TableCell>();

    public List<TableCell> getCells() {
        return cells;
    }

    public boolean isEmpty() {
        for (TableCell cell : cells) {
            if (cell.getText() != null && !cell.getText().trim().isEmpty())
                return false;
        }
        return true;
    }
}

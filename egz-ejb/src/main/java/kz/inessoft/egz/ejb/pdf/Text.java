package kz.inessoft.egz.ejb.pdf;

/**
 * Created by alexey on 27.07.16.
 */
public class Text implements IPDFElement {
    private String text;

    public Text(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}

package kz.inessoft.egz.ejb.portalfiles;

import kz.inessoft.egz.ejbapi.PortalFilesLogic;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Created by alexey on 23.09.16.
 */
@Singleton
@Startup
public class FilesService {
    @EJB
    private PortalFilesLogic portalFilesLogic;

    @Schedule(dayOfWeek = "1,2,3,4,5", hour = "9,10,11,12,13,14,15,16,17", minute = "*", persistent = false)
    public void startLoadFiles() {
        portalFilesLogic.queueNextPortionUnloadedFiles();
    }
}

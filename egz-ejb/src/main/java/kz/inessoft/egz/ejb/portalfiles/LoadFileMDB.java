package kz.inessoft.egz.ejb.portalfiles;

import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.dbentities.TPortalRecievedFile;
import kz.inessoft.egz.dbentities.TPortalUploadedFile;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.httpclient.AuthorizedHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.HttpUtils;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.net.ssl.SSLException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.sql.SQLException;

/**
 * Created by alexey on 13.02.17.
 */
@MessageDriven(mappedName = "java:/jms/queue/PortalFileDownloadQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/PortalFileDownloadQueue"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5")
})
public class LoadFileMDB implements MessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoadFileMDB.class);
    @EJB
    private PortalFilesLogic portalFilesLogic;
    @Resource
    private MessageDrivenContext mdc;

    @Override
    public void onMessage(Message inMsg) {
        TPortalFile portalFile = null;
        try {
            Long fileId = inMsg.getLongProperty(JMSUtils.FILE_ID);
            portalFile = portalFilesLogic.getPortalFile(fileId);
            LOGGER.info("Downloading file {} with portalId {}", fileId, portalFile.getPortalId());
            if (portalFile instanceof TPortalUploadedFile) {
                loadUploadedFile((TPortalUploadedFile) portalFile);
            } else if (portalFile instanceof TPortalRecievedFile) {
                loadRecievedFile((TPortalRecievedFile) portalFile);
            } else
                LOGGER.error("Unknown file type " + fileId);
        } catch (Exception e) {
            LOGGER.error("Error while downloading file {}, portalId {}", portalFile == null ? null : portalFile.getId(),
                    portalFile == null ? null : portalFile.getPortalId(), e);
            mdc.setRollbackOnly();
        }
    }


    private void loadUploadedFile(TPortalUploadedFile uploadedFile) throws Exception {
        HttpUtils.downloadFileAndProcess(uploadedFile.getPortalId(),
                (HttpUtils.IResponceProcessor) response -> saveFile(response, uploadedFile, false));
    }

    private void loadRecievedFile(TPortalRecievedFile recievedFile) throws IOException, RequestException, SQLException {
        AuthorizedHttpClient client = HttpClientsPool.getInstance().getClient(recievedFile.getParticipantCompany());
        CloseableHttpResponse response = null;
        try {
            while (true) {
                try {
                    client.checkLogin();
                    response = client.openUrl(new HttpGet(recievedFile.getUrl()));
                    saveFile(response, recievedFile, true);
                    break;
                } catch (SSLException | SocketTimeoutException e) {
                    LOGGER.error("Error while download file. Repeat request.");
                } finally {
                    try {
                        if (response != null)
                            response.close();
                    } catch (IOException e1) {
                        LOGGER.error("Error while close stream.", e1);
                    }
                }
            }
        } finally {
            HttpClientsPool.getInstance().freeClient(client);
        }
    }

    private void saveFile(CloseableHttpResponse response, TPortalFile portalFile, boolean save403) throws IOException, SQLException {
        if (response.getStatusLine().getStatusCode() != 200) {
            if (!save403 || response.getStatusLine().getStatusCode() != 403) {
                throw new RuntimeException("Can't download uploaded file " + portalFile.getPortalId() + ". Status " + response.getStatusLine());
            }
        }
        HttpEntity entity = response.getEntity();
        String mimeType = entity.getContentType().getValue();
        long contentLength = entity.getContentLength();
        Header contentDisposition = response.getFirstHeader("Content-Disposition");
        String fileName;
        if (contentDisposition != null) {
            String disposition = new String(contentDisposition.getValue().getBytes("ISO-8859-1"), "UTF8");
            int idx = disposition.indexOf("filename=");
            fileName = disposition.substring(idx + 9);
        } else {
            fileName = "recieved_" + portalFile.getPortalId();
            if (mimeType.contains("text/html"))
                fileName += ".html";
        }
        if (fileName.startsWith("\""))
            fileName = fileName.substring(1);
        if (fileName.endsWith("\""))
            fileName = fileName.substring(0, fileName.length() - 1);
        InputStream content = entity.getContent();
        int length = portalFilesLogic.saveFile(portalFile.getId(), fileName, mimeType, contentLength, content);
        if (length != contentLength)
            LOGGER.warn("Wrong content length for file {}", portalFile.getPortalId());
    }
}

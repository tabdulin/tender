package kz.inessoft.egz.ejb.portalfiles;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.dbentities.TPortalFile_;
import kz.inessoft.egz.dbentities.TPortalRecievedFile;
import kz.inessoft.egz.dbentities.TPortalRecievedFile_;
import kz.inessoft.egz.dbentities.TPortalUploadedFile;
import kz.inessoft.egz.dbentities.TPortalUploadedFile_;
import kz.inessoft.egz.ejb.DBUtils;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejbapi.IContentStreamProcessor;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Stateless(name = "PortalFiles")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PortalFilesLogicBean implements PortalFilesLogic {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortalFilesLogicBean.class);
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;
    @Resource(lookup = "java:/jms/queue/PortalFileDownloadQueue")
    private Queue queue;
    @Resource
    private EJBContext context;
    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;

    @Override
    public TPortalUploadedFile queueLoadUploadedFile(long portalId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TPortalUploadedFile> query = builder.createQuery(TPortalUploadedFile.class);
        Root<TPortalUploadedFile> root = query.from(TPortalUploadedFile.class);
        query.where(builder.equal(root.get(TPortalUploadedFile_.portalId), portalId));
        List<TPortalUploadedFile> resultList = em.createQuery(query).getResultList();
        TPortalUploadedFile uploadedFile;
        if (resultList.size() == 0) {
            uploadedFile = new TPortalUploadedFile();
            uploadedFile.setPortalId(portalId);
            em.persist(uploadedFile);
        } else
            uploadedFile = resultList.get(0);
        return uploadedFile;
    }

    @Override
    public TPortalRecievedFile queueLoadRecievedFile(long portalId, String url, DicParticipantCompany participantCompany) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TPortalRecievedFile> query = builder.createQuery(TPortalRecievedFile.class);
        Root<TPortalRecievedFile> root = query.from(TPortalRecievedFile.class);
        query.where(builder.equal(root.get(TPortalRecievedFile_.portalId), portalId));
        List<TPortalRecievedFile> resultList = em.createQuery(query).getResultList();
        TPortalRecievedFile recievedFile;
        if (resultList.size() == 0) {
            recievedFile = new TPortalRecievedFile();
            recievedFile.setPortalId(portalId);
            recievedFile.setParticipantCompany(participantCompany);
            recievedFile.setUrl(url);
            em.persist(recievedFile);
        } else
            recievedFile = resultList.get(0);
        return recievedFile;
    }

    @Override
    public int saveFile(long fileId, String fileName, String mimeType, long fileSize, InputStream contentStream) throws SQLException, IOException {
        TPortalFile portalFile = em.find(TPortalFile.class, fileId);
        portalFile.setFileName(fileName);
        portalFile.setMimeType(mimeType);
        portalFile.setFileSize(fileSize);

        DBUtils.SaveDataInfo saveDataInfo = DBUtils.createBlob(em, contentStream);
        Connection connection = DBUtils.getConnection(em);
        PreparedStatement pstmt = connection.prepareStatement("UPDATE t_portal_file SET file_content=? WHERE id=?");
        pstmt.setLong(2, fileId);
        pstmt.setLong(1, saveDataInfo.getOid());
        pstmt.executeUpdate();
        return saveDataInfo.getDataLength();
    }

    @Override
    public void queueNextPortionUnloadedFiles() {
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

            CriteriaQuery<Long> queryCnt = criteriaBuilder.createQuery(Long.class);
            Root<TPortalFile> rootCnt = queryCnt.from(TPortalFile.class);
            queryCnt.select(criteriaBuilder.count(rootCnt.get(TPortalFile_.id)));
            queryCnt.where(
                    criteriaBuilder.and(
                            rootCnt.get(TPortalFile_.fileName).isNull(),
                            criteriaBuilder.equal(rootCnt.get(TPortalFile_.fileSize), 1)

                    )
            );
            Long count = em.createQuery(queryCnt).getSingleResult();
            if (count > 5) {
                LOGGER.warn("Warning!!! Previous process for downloading files not finished yet.");
            } else {
                CriteriaQuery<TPortalFile> query = criteriaBuilder.createQuery(TPortalFile.class);
                Root<TPortalFile> root = query.from(TPortalFile.class);
                query.where(
                        criteriaBuilder.and(
                                root.get(TPortalFile_.fileName).isNull(),
                                criteriaBuilder.notEqual(root.get(TPortalFile_.fileSize), 1)
                        )
                );

                List<TPortalFile> retVal = em.createQuery(query).setMaxResults(100).getResultList();
                if (retVal.size() == 0) {
                    LOGGER.info("There is no files for downloading.");
                    return;
                }

                for (TPortalFile portalFile : retVal) {
                    portalFile.setFileSize(1);
                    Message message = jmsContext.createMessage();
                    message.setLongProperty(JMSUtils.FILE_ID, portalFile.getId());
                    message.setStringProperty(JMSUtils.MESSAGE_TYPE_PROPERTY_NAME, JMSUtils.FILE_DOWNLOAD);
                    jmsContext.createProducer().send(queue, message);
                }
                LOGGER.info("Queued {} files for download.", retVal.size());
            }
        } catch (JMSException e) {
            LOGGER.error("Error while queue unloaded files", e);
            context.setRollbackOnly();
        }
    }

    @Override
    public TPortalFile getPortalFile(long fileId) {
        return em.find(TPortalFile.class, fileId);
    }

    @Override
    public void processFileContent(long fileId, IContentStreamProcessor streamProcessor) throws Exception {
        final Exception[] ex = new Exception[1];
        Connection connection = DBUtils.getConnection(em);
        PreparedStatement pstmt = connection.prepareStatement("SELECT file_content FROM t_portal_file WHERE id=?");
        pstmt.setLong(1, fileId);
        ResultSet rst = pstmt.executeQuery();
        rst.next();
        try {
            streamProcessor.processContent(rst.getBlob(1).getBinaryStream());
        } catch (Exception e) {
            ex[0] = e;
        }
    }

    @Override
    public void writeFileContent(long fileId, OutputStream outputStream) throws Exception {
        processFileContent(fileId, contentStream -> IOUtils.copy(contentStream, outputStream));
    }
}

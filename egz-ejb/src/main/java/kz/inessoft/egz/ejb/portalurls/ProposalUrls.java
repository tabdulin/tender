package kz.inessoft.egz.ejb.portalurls;

import kz.inessoft.egz.ejbapi.PortalUrlUtils;

/**
 * Created by alexey on 21.02.17.
 */
public class ProposalUrls {
    private ProposalUrls() {
    }

    public static String createProposalUrl(long announcementPortalId) {
        return PortalUrlUtils.BASE_URL + "/application/create/" + announcementPortalId;
    }

    public static String getAjaxCreateProposalURL(long announcementPortalId) {
        return PortalUrlUtils.BASE_URL + "/application/ajax_create_application/" + announcementPortalId;
    }

    public static String getStep2ProposalURL(long announcementPortalId, long proposalPortalId) {
        return PortalUrlUtils.BASE_URL + "/application/lots/" + announcementPortalId + "/" + proposalPortalId;
    }

    public static String getAjaxSelectLotsURL(long announcementPortalId, long proposalPortalId) {
        return PortalUrlUtils.BASE_URL + "/application/ajax_add_lots/" + announcementPortalId + "/" + proposalPortalId;
    }

    public static String getAjaxDocsNextPageURL(long announcementPortalId, long proposalPortalId) {
        return PortalUrlUtils.BASE_URL + "/application/ajax_docs_next/" + announcementPortalId + "/" + proposalPortalId;
    }

    public static String getAjaxSavetLotsURL(long announcementPortalId, long proposalPortalId) {
        return PortalUrlUtils.BASE_URL + "/application/ajax_lots_next/" + announcementPortalId + "/" + proposalPortalId;
    }

    public static String getDocsURL(long announcementPortalId, long proposalPortalId) {
        return PortalUrlUtils.BASE_URL + "/application/docs/" + announcementPortalId + "/" + proposalPortalId;
    }

    public static String getGenerateDocURL(long announcementPortalId, long proposalPortalId, long documentTypeId) {
        return PortalUrlUtils.BASE_URL + "/application/show_doc/" + announcementPortalId + "/" + proposalPortalId + "/" + documentTypeId;
    }

    public static String getCheckByHashURL() {
        return PortalUrlUtils.HOST_URL + "/files/check_by_hash/";
    }

    public static String getCheckSignStatusURL() {
        return PortalUrlUtils.HOST_URL + "/files/check_status/";
    }
}

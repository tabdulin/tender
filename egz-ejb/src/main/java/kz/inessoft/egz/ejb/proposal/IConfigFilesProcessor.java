package kz.inessoft.egz.ejb.proposal;

/**
 * Created by alexey on 23.02.17.
 */
public interface IConfigFilesProcessor {
    void processFile(String fileName, byte[] data) throws Exception;
}

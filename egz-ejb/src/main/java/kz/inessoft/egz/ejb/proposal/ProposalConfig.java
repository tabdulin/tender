package kz.inessoft.egz.ejb.proposal;

import java.util.List;

/**
 * Created by alexey on 21.07.16.
 */
public class ProposalConfig {
    public static class Service {
        private String name;
        private String startYear;
        private String startMonth;
        private String endYear;
        private String endMonth;
        private String price;
        private String country;
        private String customerUIN;
        private String customerName;
        private String customerAddress;
        private String documentName;
        private String documentNumber;
        private String documentDate;
        private String documentFile;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStartYear() {
            return startYear;
        }

        public void setStartYear(String startYear) {
            this.startYear = startYear;
        }

        public String getStartMonth() {
            return startMonth;
        }

        public void setStartMonth(String startMonth) {
            this.startMonth = startMonth;
        }

        public String getEndYear() {
            return endYear;
        }

        public void setEndYear(String endYear) {
            this.endYear = endYear;
        }

        public String getEndMonth() {
            return endMonth;
        }

        public void setEndMonth(String endMonth) {
            this.endMonth = endMonth;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCustomerUIN() {
            return customerUIN;
        }

        public void setCustomerUIN(String customerUIN) {
            this.customerUIN = customerUIN;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerAddress() {
            return customerAddress;
        }

        public void setCustomerAddress(String customerAddress) {
            this.customerAddress = customerAddress;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getDocumentNumber() {
            return documentNumber;
        }

        public void setDocumentNumber(String documentNumber) {
            this.documentNumber = documentNumber;
        }

        public String getDocumentDate() {
            return documentDate;
        }

        public void setDocumentDate(String documentDate) {
            this.documentDate = documentDate;
        }

        public String getDocumentFile() {
            return documentFile;
        }

        public void setDocumentFile(String documentFile) {
            this.documentFile = documentFile;
        }
    }

    public static class Hardware {
        private String name;
        private String amount;
        private String condition;
        private String rented;
        private String owner;
        private String documentFile;
        private String documentName;
        private String documentNumber;
        private String documentDate;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public String getRented() {
            return rented;
        }

        public void setRented(String rented) {
            this.rented = rented;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getDocumentNumber() {
            return documentNumber;
        }

        public void setDocumentNumber(String documentNumber) {
            this.documentNumber = documentNumber;
        }

        public String getDocumentDate() {
            return documentDate;
        }

        public void setDocumentDate(String documentDate) {
            this.documentDate = documentDate;
        }

        public String getDocumentFile() {
            return documentFile;
        }

        public void setDocumentFile(String documentFile) {
            this.documentFile = documentFile;
        }
    }

    public static class Employee {
        private String iin;
        private String name;
        private String country;
        private String experience;
        private String qualification;
        private String category;
        private String documentFile;

        public String getIin() {
            return iin;
        }

        public void setIin(String iin) {
            this.iin = iin;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getExperience() {
            return experience;
        }

        public void setExperience(String experience) {
            this.experience = experience;
        }

        public String getQualification() {
            return qualification;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getDocumentFile() {
            return documentFile;
        }

        public void setDocumentFile(String documentFile) {
            this.documentFile = documentFile;
        }
    }

    private String address;
    private String iik;
    private Boolean consortium = false;
    private String lot;
    private List<Service> application6services;
    private List<Hardware> application6hardware;
    private List<Employee> application6employees;
    private List<String> vatPayerDocuments;
    private List<String> taxDebtDocuments;
    private List<String> certificatesDocuments;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIik() {
        return iik;
    }

    public void setIik(String iik) {
        this.iik = iik;
    }

    public Boolean getConsortium() {
        return consortium;
    }

    public void setConsortium(Boolean consortium) {
        this.consortium = consortium;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public List<Service> getApplication6services() {
        return application6services;
    }

    public void setApplication6services(List<Service> application6services) {
        this.application6services = application6services;
    }

    public List<Hardware> getApplication6hardware() {
        return application6hardware;
    }

    public void setApplication6hardware(List<Hardware> application6hardware) {
        this.application6hardware = application6hardware;
    }

    public List<Employee> getApplication6employees() {
        return application6employees;
    }

    public void setApplication6employees(List<Employee> application6employees) {
        this.application6employees = application6employees;
    }

    public List<String> getVatPayerDocuments() {
        return vatPayerDocuments;
    }

    public void setVatPayerDocuments(List<String> vatPayerDocuments) {
        this.vatPayerDocuments = vatPayerDocuments;
    }

    public List<String> getTaxDebtDocuments() {
        return taxDebtDocuments;
    }

    public void setTaxDebtDocuments(List<String> taxDebtDocuments) {
        this.taxDebtDocuments = taxDebtDocuments;
    }

    public List<String> getCertificatesDocuments() {
        return certificatesDocuments;
    }

    public void setCertificatesDocuments(List<String> certificatesDocuments) {
        this.certificatesDocuments = certificatesDocuments;
    }
}

package kz.inessoft.egz.ejb.proposal;

import kz.inessoft.egz.ejbapi.IContentStreamProcessor;

import javax.ejb.Local;

/**
 * Created by alexey on 23.02.17.
 */
@Local
public interface ProposalConfigLogic {
    ProposalConfig getConfig(long prInfoId);

    void processConfigStream(long prInfoId, String fileName, IContentStreamProcessor contentStreamProcessor) throws Exception;

    void processConfigFilesFromDirectory(long prInfoId, String directory, IConfigFilesProcessor filesProcessor) throws Exception;
}

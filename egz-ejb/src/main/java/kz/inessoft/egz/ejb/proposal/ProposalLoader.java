package kz.inessoft.egz.ejb.proposal;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Talgat.Abdulin
 */
// TODO: think about abstracting methods by loading types
class ProposalLoader {
    private static final Logger logger = LoggerFactory.getLogger(ProposalLoader.class);
    private XSSFWorkbook wb;
    private ProposalConfig proposalConfig;

    public ProposalLoader(XSSFWorkbook wb) {
        this.wb = wb;
    }

    public ProposalConfig loadProposalConfig() {
        proposalConfig = new ProposalConfig();
        loadAnnouncement();
        loadRequisites();
        loadServices();
        loadEmployees();
        loadHardware();
        loadCertificates();
        return proposalConfig;
    }

    private void loadAnnouncement() {
        Sheet sheet = wb.getSheet("Заявка");
        for (Row row : sheet) {
            String key = getValue(row, 0);
            String value = getValue(row, 1);
            switch (key) {
                case "Номер лота": {
                    proposalConfig.setLot(value);
                    break;
                }
            }
        }
    }

    private void loadRequisites() {
        Sheet sheet = wb.getSheet("Реквизиты");
        for (Row row : sheet) {
            String requisite = getValue(row, 0);
            String value = getValue(row, 1);
            switch (requisite) {
                case "Юридический адрес": {
                    proposalConfig.setAddress(value);
                    break;
                }
                case "ИИК": {
                    proposalConfig.setIik(value);
                    break;
                }
                case "Признак консорциума": {
                    proposalConfig.setConsortium(StringUtils.equalsIgnoreCase("да", value));
                    break;
                }
            }
        }
    }

    private void loadServices() {
        Sheet sheet = wb.getSheet("Услуги");
        Map<String, Integer> columns = new HashMap<>();
        for (Cell header : sheet.getRow(0)) {
            columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
        }

        List<ProposalConfig.Service> services = new LinkedList<>();
        proposalConfig.setApplication6services(services);
        for (Row row : sheet) {
            if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                continue;
            }

            ProposalConfig.Service service = new ProposalConfig.Service();
            columns.keySet().stream().forEach((column) -> {
                String value = getValue(row, columns.get(column));
                switch (column) {
                    case "Наименование услуги": {
                        service.setName(value);
                        break;
                    }
                    case "С": {
                        String[] date = StringUtils.split(value, "-");
                        service.setStartMonth(getMonth(date[1]));
                        service.setStartYear(date[2]);
                        break;
                    }
                    case "До": {
                        String[] date = StringUtils.split(value, "-");
                        service.setEndMonth(getMonth(date[1]));
                        service.setEndYear(date[2]);
                        break;
                    }
                    case "Стоимость договора (тенге)": {
                        service.setPrice(value);
                        break;
                    }
                    case "Страна": {
                        service.setCountry(value);
                        break;
                    }
                    case "Наименование Заказчика": {
                        service.setCustomerName(value);
                        break;
                    }
                    case "Адрес Заказчика": {
                        service.setCustomerAddress(value);
                        break;
                    }
                    case "Подтверждающий документ": {
                        service.setDocumentName(value);
                        break;
                    }
                    case "Номер подтверждющего документа": {
                        service.setDocumentNumber(value);
                        break;
                    }
                    case "Дата подтверждающего документа": {
                        service.setDocumentDate(value);
                        break;
                    }
                    case "Название файла в папке application-6-services": {
                        service.setDocumentFile(value);
                        break;
                    }
                }
            });
            services.add(service);
        }
    }

    private void loadEmployees() {
        Sheet sheet = wb.getSheet("Сотрудники");
        Map<String, Integer> columns = new HashMap<>();
        for (Cell header : sheet.getRow(0)) {
            columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
        }

        List<ProposalConfig.Employee> employees = new LinkedList<>();
        proposalConfig.setApplication6employees(employees);
        for (Row row : sheet) {
            if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                continue;
            }

            final ProposalConfig.Employee employee = new ProposalConfig.Employee();
            columns.keySet().stream().forEach((column) -> {
                String value = getValue(row, columns.get(column));
                switch (column) {
                    case "ИИН": {
                        employee.setIin(value);
                        break;
                    }
                    case "Страна": {
                        employee.setCountry(value);
                        break;
                    }
                    case "ФИО": {
                        employee.setName(value);
                        break;
                    }
                    case "Стаж работы в сфере оказания услуг": {
                        employee.setExperience(value);
                        break;
                    }
                    case "Квалификация (специальность) по диплому, свидетельству и др. документам об образ": {
                        employee.setQualification(value);
                        break;
                    }
                    case "Категория, разряд, класс по специальности": {
                        employee.setCategory(value);
                        break;
                    }
                    case "Название файла в папке application-6-employees": {
                        employee.setDocumentFile(value);
                        break;
                    }
                }
            });

            employees.add(employee);
        }
    }

    private void loadHardware() {
        Sheet sheet = wb.getSheet("Оборудование");
        Map<String, Integer> columns = new HashMap<>();
        for (Cell header : sheet.getRow(0)) {
            columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
        }

        List<ProposalConfig.Hardware> hardwares = new LinkedList<>();
        proposalConfig.setApplication6hardware(hardwares);
        for (Row row : sheet) {
            if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                continue;
            }

            final ProposalConfig.Hardware hardware = new ProposalConfig.Hardware();
            columns.keySet().stream().forEach((column) -> {
                String value = getValue(row, columns.get(column));
                switch (column) {
                    case "Наименование оборудования (механизмов, машин)": {
                        hardware.setName(value);
                        break;
                    }
                    case "Количество имеющихся единиц": {
                        hardware.setAmount(value);
                        break;
                    }
                    case "Состояние (новое, хорошее, плохое)": {
                        hardware.setCondition(value);
                        break;
                    }
                    case "Собственное / Арендованное": {
                        hardware.setRented(value);
                        break;
                    }
                    case "У кого арендовано": {
                        hardware.setOwner(value);
                        break;
                    }
                    case "Наименование подтверждающего документа": {
                        hardware.setDocumentName(value);
                        break;
                    }
                    case "Номер подтверждающего документа": {
                        hardware.setDocumentNumber(value);
                        break;
                    }
                    case "Дата подтверждающего документа": {
                        hardware.setDocumentDate(value);
                        break;
                    }
                    case "Название файла папке application-6-hardware": {
                        hardware.setDocumentFile(value);
                        break;
                    }
                }
            });

            hardwares.add(hardware);
        }
    }

    private void loadCertificates() {
        Sheet sheet = wb.getSheet("Сертификаты компании");
        Map<String, Integer> columns = new HashMap<>();
        for (Cell header : sheet.getRow(0)) {
            columns.put(getValue(sheet.getRow(0), header.getColumnIndex()), header.getColumnIndex());
        }

        proposalConfig.setCertificatesDocuments(new LinkedList<>());
        for (Row row : sheet) {
            if (!StringUtils.equalsIgnoreCase("да", getValue(row, columns.get("Конкурс")))) {
                continue;
            }

            columns.keySet().stream().forEach((column) -> {
                String value = getValue(row, columns.get(column));
                switch (column) {
                    case "Название файла в папке application-certificates": {
                        proposalConfig.getCertificatesDocuments().add(value);
                        break;
                    }
                }
            });
        }
    }

    private String getValue(Row row, int column) {
        String value;
        switch (row.getCell(column).getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
                value = StringUtils.removeEnd(Double.toString(row.getCell(column).getNumericCellValue()), ".0");
                break;
            default:
                value = StringUtils.trim(row.getCell(column).getStringCellValue());
                break;
        }

        return StringUtils.removeEnd(value, "\0x160");
    }

    private String getMonth(String month) {
        return Integer.toString(Integer.parseInt(month));
    }
}

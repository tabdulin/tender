package kz.inessoft.egz.ejb.proposal;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.DBUtils;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.proposal.mdbs.MakeProposalMDB;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.IContentStreamProcessor;
import kz.inessoft.egz.ejbapi.ProposalLogic;
import kz.inessoft.egz.ejbapi.ProposalLogicException;
import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Message;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by alexey on 01.11.16.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProposalLogicBean implements ProposalLogic, ProposalConfigLogic {
    private abstract class ZipConfigProcessor {
        private Long prInfoId;

        public ZipConfigProcessor(Long prInfoId) {
            this.prInfoId = prInfoId;
        }

        void processConfig() throws Exception {
            Connection connection = DBUtils.getConnection(em);
            PreparedStatement pstmt = connection.prepareStatement("SELECT proposal_data FROM pr_info WHERE id=?");
            pstmt.setLong(1, prInfoId);
            ResultSet rst = pstmt.executeQuery();
            rst.next();
            Blob blob = rst.getBlob(1);
            ZipInputStream zipInputStream = new ZipInputStream(blob.getBinaryStream());
            try {
                processStream(zipInputStream);
            } finally {
                rst.close();
                pstmt.close();
            }
        }

        abstract void processStream(ZipInputStream zipInputStream) throws Exception;
    }

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private AnnouncementLogic announcementLogic;

    @Resource
    private EJBContext context;

    @Resource(lookup = "java:/jms/queue/MakeProposalQueue")
    private Queue queue;

    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;


    @Override
    public void makeProposal(long participantId, long announcementId, InputStream zipData) throws ProposalLogicException {
        try {
            TAnnouncement announcement = em.getReference(TAnnouncement.class, announcementId);
            DicParticipantCompany participantCompany = em.getReference(DicParticipantCompany.class, participantId);
            final PrInfo prInfo = new PrInfo();
            prInfo.setAnnouncement(announcement);
            prInfo.setParticipantCompany(participantCompany);
            prInfo.setCreateDate(new Date());
            prInfo.setFinishedFlag(false);
            DBUtils.SaveDataInfo blob = DBUtils.createBlob(em, zipData);
            prInfo.setProposalData(blob.getOid());
            em.persist(prInfo);

            Message message = jmsContext.createMessage();
            message.setLongProperty(JMSUtils.PROPOSAL_ID, prInfo.getId());
            message.setStringProperty(JMSUtils.MESSAGE_TYPE_PROPERTY_NAME, JMSUtils.MAKE_PROPOSAL_MESSAGE);
            message.setStringProperty(JMSUtils.STEP_PROPERTY, MakeProposalMDB.STEP);

            jmsContext.createProducer().send(queue, message);
        } catch (Exception e) {
            context.setRollbackOnly();
            throw new ProposalLogicException("Error while start proposal", e);
        }
    }

    @Override
    public ProposalConfig getConfig(long prInfoId) {
        final XSSFWorkbook[] wb = new XSSFWorkbook[1];
        try {
            processConfigStream(prInfoId, "config.xlsx", contentStream -> wb[0] = new XSSFWorkbook(contentStream));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return new ProposalLoader(wb[0]).loadProposalConfig();
    }

    public void processConfigFilesFromDirectory(long prInfoId, String directory, IConfigFilesProcessor filesProcessor) throws Exception {
        new ZipConfigProcessor(prInfoId) {
            @Override
            void processStream(ZipInputStream zipInputStream) throws Exception {
                String tmpDir = directory;
                if (!tmpDir.endsWith("/"))
                    tmpDir += "/";
                ZipEntry nextEntry;
                    while ((nextEntry = zipInputStream.getNextEntry()) != null) {
                        if (nextEntry.getName().startsWith(tmpDir) && !nextEntry.isDirectory()) {
                            filesProcessor.processFile(nextEntry.getName().substring(tmpDir.length()), IOUtils.toByteArray(zipInputStream));
                        }
                    }

                }
        }.processConfig();
    }

    public void processConfigStream(long prInfoId, String fileName, IContentStreamProcessor contentStreamProcessor) throws Exception {
        new ZipConfigProcessor(prInfoId) {
            @Override
            void processStream(ZipInputStream zipInputStream) throws Exception {
                ZipEntry nextEntry;
                while ((nextEntry = zipInputStream.getNextEntry()) != null) {
                    if (fileName.equals(nextEntry.getName())) {
                        contentStreamProcessor.processContent(zipInputStream);
                        return;
                    }
                }
            }
        }.processConfig();
    }
}

package kz.inessoft.egz.ejb.proposal.mdbs;

import kz.inessoft.egz.ejb.JMSUtils;

import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * Created by alexey on 21.02.17.
 */
public abstract class ABaseMDB implements MessageListener {
    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    protected JMSContext jmsContext;

    protected Message makeMessage(Message originalMsg) throws JMSException {
        Message message = jmsContext.createMessage();
        message.setLongProperty(JMSUtils.PROPOSAL_ID, originalMsg.getLongProperty(JMSUtils.PROPOSAL_ID));
        message.setStringProperty(JMSUtils.MESSAGE_TYPE_PROPERTY_NAME, JMSUtils.MAKE_PROPOSAL_MESSAGE);
        message.setStringProperty(JMSUtils.STEP_PROPERTY, getNextStep());
        return message;
    }

    protected abstract String getNextStep();
}

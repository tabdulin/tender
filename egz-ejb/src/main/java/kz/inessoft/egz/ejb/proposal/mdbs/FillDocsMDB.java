package kz.inessoft.egz.ejb.proposal.mdbs;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.httpclient.AuthorizedHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.portalurls.ProposalUrls;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejb.proposal.mdbs.appprocessors.AppProcessorBuilder;
import kz.inessoft.egz.ejb.proposal.mdbs.appprocessors.ApplicationProcessor;
import kz.inessoft.egz.ejb.proposal.mdbs.appprocessors.UnknownAppTypeException;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.nio.charset.Charset;
import java.util.LinkedList;

/**
 * Created by alexey on 21.02.17.
 */
@MessageDriven(mappedName = "java:/jms/queue/AnnouncementsDownloadQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MakeProposalQueue"),
        @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = JMSUtils.STEP_PROPERTY + "='" + FillDocsMDB.STEP + "'"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5")
})
public class FillDocsMDB implements MessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(FillDocsMDB.class);
    static final String STEP = "FillDocs";

    @Resource
    private MessageDrivenContext mdc;

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private ProposalConfigLogic proposalConfigLogic;

    @Resource(lookup = "java:/jms/queue/DLQ")
    private Queue dlq;

    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;


    @Override
    public void onMessage(Message msg) {
        try {
            Long id = msg.getLongProperty(JMSUtils.PROPOSAL_ID);
            PrInfo prInfo = em.find(PrInfo.class, id);
            TAnnouncement announcement = prInfo.getAnnouncement();
            DicParticipantCompany participantCompany = prInfo.getParticipantCompany();
            AuthorizedHttpClient client = HttpClientsPool.getInstance().getClient(participantCompany);
            String docName = null;
            try {
                prInfo.setCurrentStepDescription("Заполнение документов");
                prInfo.setErrorMessage(null);

                String docsURL = ProposalUrls.getDocsURL(announcement.getPortalId(), prInfo.getPortalId());
                Response response = client.sendGetRequest(docsURL);
                NodeList nodeList = response.evaluateXPathNodeList("//table//tr/td[normalize-space(text())='Обязателен']/../td/a");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node item = nodeList.item(i);
                    docName = item.getTextContent().trim();
                    String url = item.getAttributes().getNamedItem("href").getNodeValue();

                    // JTidy обрезает пустые span, которые не имеют атрибутов "id" или "name". Можно конечно его пропатчить, но не хочется его трогать.
                    // по этому тут этот костыль, по нахождению span-а и выяснению его класса.
                    int idx = response.getContent().indexOf(url);
                    String substr = response.getContent().substring(0, idx);
                    int lastSpanIdx = substr.lastIndexOf("<span");
                    String spanTag = substr.substring(lastSpanIdx);
                    if (spanTag.contains("glyphicon-remove-circle")) {
                        // Если у spana есть класс glyphicon-remove-circle, то значит документ еще не заполнен
                        ApplicationProcessor applicationProcessor = new AppProcessorBuilder(docName, client,
                                participantCompany, url, prInfo, proposalConfigLogic).build();
                        applicationProcessor.process();
                    }
                }
                // POST по нажатию на Далее на странице со списком документов
                HttpPost httpPost = new HttpPost(ProposalUrls.getAjaxDocsNextPageURL(announcement.getPortalId(), prInfo.getPortalId()));
                LinkedList<NameValuePair> parameters = new LinkedList<>();
                parameters.add(new BasicNameValuePair("next", "1"));
                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                client.sendPostRequest(httpPost);
                prInfo.setFinishedFlag(true);
                prInfo.setCurrentStepDescription("Заявка заполнена. Заполните цену и отправьте. " +
                        ProposalUrls.createProposalUrl(announcement.getPortalId()) + "/" + prInfo.getPortalId());
            } catch (UnknownAppTypeException e) {
                jmsContext.createProducer().send(dlq, msg);
                prInfo.setErrorMessage("Неизвестный тип документа \"" + docName + "\".");
            } finally {
                HttpClientsPool.getInstance().freeClient(client);
            }
        } catch (Exception e) {
            mdc.setRollbackOnly();
            LOGGER.error("Error while fill document for proposal", e);
        }
    }
}

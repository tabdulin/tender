package kz.inessoft.egz.ejb.proposal.mdbs;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.httpclient.AuthorizedHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.portalurls.ProposalUrls;
import kz.inessoft.egz.ejb.proposal.ProposalConfig;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Message;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 21.02.17.
 */
@MessageDriven(mappedName = "java:/jms/queue/AnnouncementsDownloadQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MakeProposalQueue"),
        @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = JMSUtils.STEP_PROPERTY + "='" + MakeProposalMDB.STEP + "'"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5")
})
public class MakeProposalMDB extends ABaseMDB {
    private static final Logger LOGGER = LoggerFactory.getLogger(MakeProposalMDB.class);
    public static final String STEP = "Start";

    @Resource
    private MessageDrivenContext mdc;

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private ProposalConfigLogic configLogic;

    @Resource(lookup = "java:/jms/queue/MakeProposalQueue")
    private Queue queue;

    @Resource(lookup = "java:/jms/queue/DLQ")
    private Queue dlq;

    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;

    @Override
    public void onMessage(Message msg) {
        try {
            Long id = msg.getLongProperty(JMSUtils.PROPOSAL_ID);
            PrInfo prInfo = em.find(PrInfo.class, id);
            TAnnouncement announcement = prInfo.getAnnouncement();
            DicParticipantCompany participantCompany = prInfo.getParticipantCompany();
            AuthorizedHttpClient client = HttpClientsPool.getInstance().getClient(participantCompany);
            ProposalConfig config = configLogic.getConfig(id);
            try {
                prInfo.setCurrentStepDescription("Создание заявки");

                String proposalUrl = ProposalUrls.createProposalUrl(announcement.getPortalId());
                Response response = client.sendGetRequest(proposalUrl);
                if (response.evaluateXPathNode("//div[label='Юридический адрес']") == null) {
                    prInfo.setErrorMessage("Заявка по этому объявлению уже существует");
                }
                // Проверим, предлагает ли портал нам ИИК и адрес для выбора, которые указаны в настройках
                NodeList nodeList = response.evaluateXPathNodeList("//select[@name='subject_address']/option");
                Long addressPortalId = null;
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node item = nodeList.item(i);
                    if (item.getTextContent().trim().equals(config.getAddress())) {
                        addressPortalId = Long.valueOf(item.getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    }
                }
                if (addressPortalId == null)
                    prInfo.setErrorMessage("Выбранный в профиле адрес не найден на портале");

                nodeList = response.evaluateXPathNodeList("//select[@id='iik']/option");
                Long iikPortalId = null;
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node item = nodeList.item(i);
                    if (item.getTextContent().trim().equals(config.getIik())) {
                        iikPortalId = Long.valueOf(item.getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    }
                }
                if (iikPortalId == null)
                    prInfo.setErrorMessage("Выбранный в профиле ИИК не найден на портале");

                if (prInfo.getErrorMessage() != null) {
                    jmsContext.createProducer().send(dlq, msg);
                    return;
                }

                HttpPost httpPost = new HttpPost(ProposalUrls.getAjaxCreateProposalURL(announcement.getPortalId()));
                List<NameValuePair> parameters = new ArrayList<>();
                parameters.add(new BasicNameValuePair("subject_address", String.valueOf(addressPortalId)));
                parameters.add(new BasicNameValuePair("iik", String.valueOf(iikPortalId)));
                if (config.getConsortium())
                    parameters.add(new BasicNameValuePair("consortium", "1"));
                else
                    parameters.add(new BasicNameValuePair("consortium", "0"));

                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                client.sendPostRequest(httpPost);
                response = client.sendGetRequest(proposalUrl);
                String realUrl = response.getRealRequestURI().toString();
                String[] strings = realUrl.split("/");
                prInfo.setPortalId(Long.valueOf(strings[strings.length - 1]));

                Message message = makeMessage(msg);
                jmsContext.createProducer().send(queue, message);
            } finally {
                HttpClientsPool.getInstance().freeClient(client);
            }
        } catch (Exception e) {
            LOGGER.error("Error while create proposal", e);
            mdc.setRollbackOnly();
        }
    }

    @Override
    protected String getNextStep() {
        return SelectLotsMDB.STEP;
    }
}

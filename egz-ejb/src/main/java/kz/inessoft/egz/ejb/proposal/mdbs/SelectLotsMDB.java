package kz.inessoft.egz.ejb.proposal.mdbs;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejb.JMSUtils;
import kz.inessoft.egz.ejb.httpclient.AuthorizedHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.portalurls.ProposalUrls;
import kz.inessoft.egz.ejb.proposal.ProposalConfig;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Message;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 21.02.17.
 */
@MessageDriven(mappedName = "java:/jms/queue/AnnouncementsDownloadQueue", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MakeProposalQueue"),
        @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = JMSUtils.STEP_PROPERTY + "='" + SelectLotsMDB.STEP + "'"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5")
})
public class SelectLotsMDB extends ABaseMDB {
    static final String STEP = "SelectLots";
    private static final Logger LOGGER = LoggerFactory.getLogger(MakeProposalMDB.class);
    @Resource
    private MessageDrivenContext mdc;

    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @EJB
    private ProposalConfigLogic configLogic;

    @Resource(lookup = "java:/jms/queue/MakeProposalQueue")
    private Queue queue;

    @Resource(lookup = "java:/jms/queue/DLQ")
    private Queue dlq;

    @Inject
    @JMSConnectionFactory("java:/JmsXA")
    private JMSContext jmsContext;

    @Override
    protected String getNextStep() {
        return FillDocsMDB.STEP;
    }

    @Override
    public void onMessage(Message msg) {
        try {
            Long id = msg.getLongProperty(JMSUtils.PROPOSAL_ID);
            PrInfo prInfo = em.find(PrInfo.class, id);
            TAnnouncement announcement = prInfo.getAnnouncement();
            DicParticipantCompany participantCompany = prInfo.getParticipantCompany();
            AuthorizedHttpClient client = HttpClientsPool.getInstance().getClient(participantCompany);
            ProposalConfig config = configLogic.getConfig(id);
            try {
                prInfo.setCurrentStepDescription("Выбор лотов");
                prInfo.setErrorMessage(null);

                String proposalLotsUrl = ProposalUrls.getStep2ProposalURL(announcement.getPortalId(), prInfo.getPortalId());
                Response response = client.sendGetRequest(proposalLotsUrl);

                Node node = response.evaluateXPathNode("//div[@id='selected_lots']/table");
                if (node == null) {
                    node = response.evaluateXPathNode("//div[@id='select_lots']/table/tbody/tr/td[position()=2 and normalize-space(text())='" + config.getLot() + "']/../td[1]/input");
                    if (node != null) {
                        long lotId = Long.valueOf(node.getAttributes().getNamedItem("value").getNodeValue());
                        HttpPost httpPost = new HttpPost(ProposalUrls.getAjaxSelectLotsURL(announcement.getPortalId(), prInfo.getPortalId()));
                        List<NameValuePair> parameters = new ArrayList<>();
                        parameters.add(new BasicNameValuePair("selectLots[]", String.valueOf(lotId)));
                        httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                        client.sendPostRequest(httpPost);
                    } else {
                        LOGGER.error("Lot with number {} not found in announcement {}", config.getLot(), announcement.getPortalId());
                        prInfo.setErrorMessage("Лот с номером " + config.getLot() + " не найден в объявлении");
                        jmsContext.createProducer().send(dlq, msg);
                        return;
                    }
                } else
                    LOGGER.warn("Lots were already selected");

                HttpPost httpPost = new HttpPost(ProposalUrls.getAjaxSavetLotsURL(announcement.getPortalId(), prInfo.getPortalId()));
                List<NameValuePair> parameters = new ArrayList<>();
                parameters.add(new BasicNameValuePair("next", "1"));
                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                client.sendPostRequest(httpPost);

                Message message = makeMessage(msg);
                jmsContext.createProducer().send(queue, message);
            } finally {
                HttpClientsPool.getInstance().freeClient(client);
            }
        } catch (Exception e) {
            mdc.setRollbackOnly();
        }
    }
}

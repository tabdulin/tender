package kz.inessoft.egz.ejb.proposal.mdbs;

import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.portalurls.ProposalUrls;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejb.sign.HashData;
import kz.inessoft.egz.ejb.sign.SignUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 24.02.17.
 */
public class UploadFilesUtil {
    public static String uploadAndSignFile(Response response, AHttpClient client, PrInfo prInfo, String fileName, byte[] data) throws Exception {
        String formUploadFileUrl = response.evaluateXPathNode("//form[@class='form_upload panel panel-default']").getAttributes().getNamedItem("action").getNodeValue();
        // Перед загрузкой файла надо вызвать этот адрес, передав туда хэши файла
        HttpPost httpPost = new HttpPost(ProposalUrls.getCheckByHashURL());
        LinkedList<NameValuePair> parameters = new LinkedList<>();
        HashData hash = SignUtils.getHash(data);
        parameters.add(new BasicNameValuePair("md5", hash.getMd5Hash()));
        parameters.add(new BasicNameValuePair("sha256", hash.getSha256Hash()));
        httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
        Response response1 = client.sendPostRequest(httpPost);
        String signature = SignUtils.signBinaryData(data, prInfo.getParticipantCompany().getEdsPrivateKey(), prInfo.getParticipantCompany().getEdsPassword());
        httpPost = new HttpPost(formUploadFileUrl);
        if ("true".equals(response1.getContent())) {
            // Если портал вернул true, это значит что на портале уже есть такие файлы с такими хэшами.
            // Значит передавать сам файл на портал не нужно. Достаточно передать только его эцп, название, размер.
            parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("md5", hash.getMd5Hash()));
            parameters.add(new BasicNameValuePair("sha256", hash.getSha256Hash()));
            parameters.add(new BasicNameValuePair("file_name", fileName));
            parameters.add(new BasicNameValuePair("file_size", String.valueOf(data.length)));
            parameters.add(new BasicNameValuePair("signature", signature));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
        } else if ("false".equals(response1.getContent())) {
            // Если портал вернул false, значит там такого файла еще нету, и надо передать все атрибуты файла вместе с ЭЦП
            httpPost.setEntity(MultipartEntityBuilder.create()
                    .addBinaryBody("userfile", data, ContentType.APPLICATION_OCTET_STREAM, fileName)
                    .addTextBody("uid", "")
                    .addTextBody("signature", signature)
                    .addTextBody("file_name", fileName)
                    .addTextBody("file_size", String.valueOf(data.length))
                    .addTextBody("md5", "")
                    .addTextBody("sha256", "")
                    .build());
        } else
            throw new Exception("Unknown responce from server " + response1.getContent());
        // Передаем запрос
        response1 = client.sendPostRequest(httpPost);
        // В ответ портал должен вернуть JSON с данными по файлу
        JSONObject jsonObject = new JSONObject(response1.getContent());
        int error = jsonObject.getInt("error");
        if (error != 0)
            // В JSON-е выставлен флаг ошибки. Что-то пошло не так...
            throw new Exception("Server returned error for upload file: " + response1.getContent());
        // Узнаем ID загруженного файла
        String fileId = jsonObject.getString("file_id");
        while (true) {
            // Периодически будем проверять статус валидации эцп на портале.
            httpPost = new HttpPost(ProposalUrls.getCheckSignStatusURL());
            parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("file_id_ar[0]", fileId));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            response1 = client.sendPostRequest(httpPost);
            String content = response1.getContent();
            if (content == null || content.isEmpty())
                throw new Exception("Server does't recieve signature: " + response1.getContent());
            jsonObject = new JSONObject(content);
            int finalStatus = jsonObject.getJSONObject(fileId).getInt("final");
            if (finalStatus == 1)
                // Валидация эцп прошла успешно. Заканчиваем цикл
                break;
            Thread.sleep(5000);
        }
        return fileId;
    }

    public static void uploadAndSignFiles(Response response, AHttpClient client, PrInfo prInfo, ProposalConfigLogic proposalConfigLogic, String filesDirectory) throws Exception {
        List<String> files = new LinkedList<>();
        proposalConfigLogic.processConfigFilesFromDirectory(prInfo.getId(), filesDirectory, (fileName, data) -> {
            String fileId = uploadAndSignFile(response, client, prInfo, fileName, data);
            files.add(fileId);
        });
        List<NameValuePair> params = new LinkedList<>();
        params.add(new BasicNameValuePair("send_form", ""));
        saveFiles(response, client, files, params);
    }

    public static void saveFiles(Response response, AHttpClient client, List<String> files, List<NameValuePair> additionalParams) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException, RequestException {
        // Переходим на страницу загрузки файлов ТС по лоту
        String dataFileGroup = response.evaluateXPathNode("//div[@class='form_uploading_block panel panel-default']").getAttributes().getNamedItem("data-file-group-id").getNodeValue();
        String formSaveFilesUrl = response.evaluateXPathNode("//form[@id='attached_files']").getAttributes().getNamedItem("action").getNodeValue();
        // Сохранение загруженных файлов (эмуляция нажатия на "Сохранить")
        HttpPost httpPost = new HttpPost(formSaveFilesUrl);
        List<NameValuePair> params = new LinkedList<>();
        params.addAll(additionalParams);
        for (String file : files) {
            params.add(new BasicNameValuePair("userfile[" + dataFileGroup + "][]", file));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        client.sendPostRequest(httpPost);
    }
}

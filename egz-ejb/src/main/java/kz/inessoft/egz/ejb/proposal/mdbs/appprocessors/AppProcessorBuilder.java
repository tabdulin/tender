package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;

/**
 * Created by alexey on 21.02.17.
 */
public class AppProcessorBuilder {
    private String description;
    private AHttpClient client;
    private DicParticipantCompany participantCompany;
    private String url;
    private PrInfo prInfo;
    private ProposalConfigLogic proposalConfigLogic;

    public AppProcessorBuilder(String description, AHttpClient client, DicParticipantCompany participantCompany,
                               String url, PrInfo prInfo, ProposalConfigLogic proposalConfigLogic) {
        this.description = description;
        this.client = client;
        this.participantCompany = participantCompany;
        this.url = url;
        this.prInfo = prInfo;
        this.proposalConfigLogic = proposalConfigLogic;
    }

    public ApplicationProcessor build() throws UnknownAppTypeException {
        EAppType appType = EAppType.getTypeByDescription(description);
        if (appType == null)
            throw new UnknownAppTypeException();
        switch (appType) {
            case BankGuarantee:
                return new Application8BankGuaranteeProcessor(client, url, prInfo, proposalConfigLogic);
            case BankReference:
                return new Application9BankReferenceProcessor(client, url, prInfo, proposalConfigLogic);
            case Certificates:
                return new CertificatesProcessor(client, url, prInfo, proposalConfigLogic);
            case KonkursAgreement:
                return new Application4KonkursAgreementProcessor(client, participantCompany, url, prInfo);
            case LotList:
                return new Application1LotsListProcessor(client, participantCompany, url, prInfo);
            case Qualification:
                return new Application6QualificationProcessor(client, url, proposalConfigLogic, prInfo);
            case TaxDebt:
                return new TaxDebtProcessor();
            case TechSpec:
                return new Application2TSProcessor(client, url, prInfo, proposalConfigLogic);
            case VatPayer:
                return new VatPayerProcessor(client, url, prInfo, proposalConfigLogic);
            default:
                throw new UnknownAppTypeException();
        }
    }
}

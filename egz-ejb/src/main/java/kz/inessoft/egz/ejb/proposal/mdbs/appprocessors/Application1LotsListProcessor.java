package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpUtils;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.portalurls.ProposalUrls;
import kz.inessoft.egz.ejb.sign.SignUtils;
import kz.inessoft.egz.ejbapi.IContentStreamProcessor;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 22.07.16.
 */
class Application1LotsListProcessor implements ApplicationProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application1LotsListProcessor.class);
    private AHttpClient client;
    private String url;
    private PrInfo prInfo;
    private DicParticipantCompany participantCompany;

    Application1LotsListProcessor(AHttpClient client, DicParticipantCompany participantCompany, String url, PrInfo prInfo) {
        this.client = client;
        this.url = url;
        this.prInfo = prInfo;
        this.participantCompany = participantCompany;
    }

    public void process() throws ProcessApplicationException {
        try {
            Response response = client.sendGetRequest(url);
            Node node = response.evaluateXPathNode("//input[@value='Сформировать документ']");
            String generateDocURL = ProposalUrls.getGenerateDocURL(prInfo.getAnnouncement().getPortalId(), prInfo.getPortalId(), 3);
            if (node != null) {
                HttpPost httpPost = new HttpPost(generateDocURL);
                List<NameValuePair> parameters = new LinkedList<>();
                parameters.add(new BasicNameValuePair("generate", "Сформировать документ"));
                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                response = client.sendPostRequest(httpPost);
            } else {
                LOGGER.warn("Document already formed");
            }

            node = response.evaluateXPathNode("//div[@class='add_signature_block']/button[@data-form-id='file_sign_form']");
            if (node != null) {
                Long fileId = Long.valueOf(node.getAttributes().getNamedItem("data-file-identifier").getNodeValue());
                final byte[][] bytes = new byte[1][1];
                HttpUtils.downloadFileAndProcess(fileId,
                        (IContentStreamProcessor) contentStream -> bytes[0] = IOUtils.toByteArray(contentStream));

                String signData = SignUtils.signBinaryData(bytes[0], participantCompany.getEdsPrivateKey(), participantCompany.getEdsPassword());
                HttpPost httpPost = new HttpPost(generateDocURL);
                List<NameValuePair> parameters = new LinkedList<>();
                parameters.add(new BasicNameValuePair("userfile[3]", fileId.toString()));
                parameters.add(new BasicNameValuePair("save_form", ""));
                parameters.add(new BasicNameValuePair("signature[" + fileId.toString() + "]", signData));
                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                response = client.sendPostRequest(httpPost);
                if (response.getStatus().getStatusCode() == 302)
                    response = client.sendGetRequest(response.getFirstHeader("Location").getValue());
            } else {
                LOGGER.warn("Document possibly already signed");
            }

            if (response.evaluateXPathNode("//td[@class='show_signatures']") == null) {
                throw new ProcessApplicationException("Не удалось подписать список лотов. Подпись не найдена.");
            }
        } catch (ProcessApplicationException e) {
          throw e;
        } catch (Exception e) {
            throw new ProcessApplicationException("Не удалось подписать список лотов.", e);
        }
    }
}

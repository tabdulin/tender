package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.proposal.ProposalConfig;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejb.proposal.mdbs.UploadFilesUtil;
import org.w3c.dom.Node;

/**
 * Created by alexey on 22.07.16.
 */
public class Application2TSProcessor implements ApplicationProcessor {
    private AHttpClient client;
    private String url;
    private PrInfo prInfo;
    private ProposalConfig proposalConfig;
    private ProposalConfigLogic proposalConfigLogic;

    Application2TSProcessor(AHttpClient client, String url, PrInfo prInfo, ProposalConfigLogic proposalConfigLogic) {
        this.client = client;
        this.url = url;
        this.prInfo = prInfo;
        this.proposalConfigLogic = proposalConfigLogic;
        this.proposalConfig = proposalConfigLogic.getConfig(prInfo.getId());
    }

    public void process() throws ProcessApplicationException {
        try {
            // Заходим на список лотов
            Response response = client.sendGetRequest(url);
            // Находим URL на заполнение документов нужного нам лота
            Node node = response.evaluateXPathNode("//td[text()='" + proposalConfig.getLot() + "']/../td[last()]/a");
            String href = node.getAttributes().getNamedItem("href").getNodeValue();
            // Переходим на страницу загрузки файлов ТС по лоту
            response = client.sendGetRequest(href);
            UploadFilesUtil.uploadAndSignFiles(response, client, prInfo, proposalConfigLogic, "announcement/application-2");
        } catch (Exception e) {
            throw new ProcessApplicationException("Не удалось подписать тех. спеку.", e);
        }
    }
}

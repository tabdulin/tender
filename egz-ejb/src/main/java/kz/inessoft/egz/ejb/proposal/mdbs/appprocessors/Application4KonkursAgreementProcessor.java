package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpUtils;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.portalurls.ProposalUrls;
import kz.inessoft.egz.ejb.sign.SignUtils;
import kz.inessoft.egz.ejbapi.IContentStreamProcessor;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 22.07.16.
 */
public class Application4KonkursAgreementProcessor implements ApplicationProcessor {
    private static Logger LOGGER = LoggerFactory.getLogger(Application4KonkursAgreementProcessor.class);

    private AHttpClient client;
    private String url;
    private PrInfo prInfo;
    private DicParticipantCompany participantCompany;

    public Application4KonkursAgreementProcessor(AHttpClient client, DicParticipantCompany participantCompany, String url, PrInfo prInfo) {
        this.client = client;
        this.url = url;
        this.participantCompany = participantCompany;
        this.prInfo = prInfo;
    }

    public void process() throws ProcessApplicationException {
        try {
            Response response = client.sendGetRequest(url);
            Node node = response.evaluateXPathNode("//input[@value='Принять соглашение']");
            String generateDocURL = ProposalUrls.getGenerateDocURL(prInfo.getAnnouncement().getPortalId(), prInfo.getPortalId(), 5);
            if (node != null) {
                HttpPost httpPost = new HttpPost(generateDocURL);
                List<NameValuePair> parameters = new LinkedList<>();
                parameters.add(new BasicNameValuePair("generate", "Принять соглашение"));
                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                response = client.sendPostRequest(httpPost);
            } else {
                LOGGER.warn("Document already formed");
            }

            node = response.evaluateXPathNode("//div[@class='add_signature_block']/button[@data-form-id='file_sign_form']");
            if (node != null) {
                Long fileId = Long.valueOf(node.getAttributes().getNamedItem("data-file-identifier").getNodeValue());
                final byte[][] bytes = new byte[1][1];
                HttpUtils.downloadFileAndProcess(fileId,
                        (IContentStreamProcessor) contentStream -> bytes[0] = IOUtils.toByteArray(contentStream));

                String signData = SignUtils.signBinaryData(bytes[0], participantCompany.getEdsPrivateKey(), participantCompany.getEdsPassword());
                HttpPost httpPost = new HttpPost(generateDocURL);
                List<NameValuePair> parameters = new LinkedList<>();
                parameters.add(new BasicNameValuePair("userfile[5]", fileId.toString()));
                parameters.add(new BasicNameValuePair("save_form", ""));
                parameters.add(new BasicNameValuePair("signature[" + fileId.toString() + "]", signData));
                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
                response = client.sendPostRequest(httpPost);
                if (response.getStatus().getStatusCode() == 302)
                    response = client.sendGetRequest(response.getFirstHeader("Location").getValue());
            } else {
                LOGGER.warn("Document possibly already signed");
            }

            if (response.evaluateXPathNode("//td[@class='show_signatures']") == null) {
                throw new ProcessApplicationException("Не удалось подписать соглашение на участие в конкурсе. Подпись не найдена.");
            }

        } catch (ProcessApplicationException e) {
            throw e;
        } catch (Exception e) {
            throw new ProcessApplicationException("Не удалось подписать соглашение на участие в конкурсе.", e);
        }
//        $(".panel-heading").$(byText("Приложение 4 (Соглашение об участии в конкурсе)")).shouldBe(visible);
//        if ($(byValue("Принять соглашение")).isDisplayed()) {
//            $(byValue("Принять соглашение")).shouldBe(Condition.visible).click();
//            SelenideUtils.waitPageLoadComplete();
//            $(byText("Подписать")).shouldBe(Condition.visible).click();
//            $(byText("Файлы успешно прикреплены")).shouldBe(Condition.exist);
//        } else {
//            logger.info("Application 4 has been already formed");
//        }
//
//        $(byText("Вернуться в список документов")).click();
    }
}

package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpUtils;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.proposal.ProposalConfig;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejb.proposal.mdbs.UploadFilesUtil;
import kz.inessoft.egz.ejb.sign.SignUtils;
import kz.inessoft.egz.ejbapi.IContentStreamProcessor;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 22.07.16.
 */
public class Application6QualificationProcessor implements ApplicationProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application6QualificationProcessor.class);
    private AHttpClient client;
    private String url;
    private ProposalConfig proposalConfig;
    private ProposalConfigLogic proposalConfigLogic;
    private PrInfo prInfo;
    private String lotDataUrl;

    public Application6QualificationProcessor(AHttpClient client, String url, ProposalConfigLogic proposalConfigLogic, PrInfo prInfo) {
        this.client = client;
        this.url = url;
        this.proposalConfigLogic = proposalConfigLogic;
        this.proposalConfig = proposalConfigLogic.getConfig(prInfo.getId());
        this.prInfo = prInfo;
    }

    public void process() throws ProcessApplicationException {
        try {

            // Заходим на список лотов
            Response response = client.sendGetRequest(url);
            // Находим URL на заполнение документов нужного нам лота
            Node node = response.evaluateXPathNode("//td[text()='" + proposalConfig.getLot() + "']/../td[last()]/a");
            lotDataUrl = PortalUrlUtils.HOST_URL + node.getAttributes().getNamedItem("href").getNodeValue();

            fillServices();
            fillHardware();
            fillStaff();

            List<String> files = new LinkedList<>();
            for (ProposalConfig.Employee employee : proposalConfig.getApplication6employees()) {
                proposalConfigLogic.processConfigStream(prInfo.getId(),
                        "company/application-6-employees/" + employee.getDocumentFile(),
                        contentStream -> files.add(UploadFilesUtil.uploadAndSignFile(response, client, prInfo, employee.getDocumentFile(), IOUtils.toByteArray(contentStream))));
            }

            for (ProposalConfig.Hardware hardware : proposalConfig.getApplication6hardware()) {
                proposalConfigLogic.processConfigStream(prInfo.getId(),
                        "company/application-6-hardware/" + hardware.getDocumentFile(),
                        contentStream -> files.add(UploadFilesUtil.uploadAndSignFile(response, client, prInfo, hardware.getDocumentFile(), IOUtils.toByteArray(contentStream))));
            }

            for (ProposalConfig.Service service : proposalConfig.getApplication6services()) {
                proposalConfigLogic.processConfigStream(prInfo.getId(),
                        "company/application-6-services/" + service.getDocumentFile(),
                        contentStream -> files.add(UploadFilesUtil.uploadAndSignFile(response, client, prInfo, service.getDocumentFile(), IOUtils.toByteArray(contentStream))));
            }
            List<NameValuePair> params = new LinkedList<>();
            params.add(new BasicNameValuePair("btn", "docSaveFile"));
            UploadFilesUtil.saveFiles(response, client, files, params);

            // Сгенерировать PDF
            HttpPost httpPost = new HttpPost(url);
            LinkedList<NameValuePair> parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("btn", "docGen"));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            client.sendPostRequest(httpPost);
            Response response1 = client.sendGetRequest(url);
            node = response1.evaluateXPathNode("//form[@id='formBtnDocGen']//button[@class='btn btn-success btn-add-signature']");
            Long fileId = Long.valueOf(node.getAttributes().getNamedItem("data-file-identifier").getNodeValue());
            final byte[][] bytes = new byte[1][1];
            HttpUtils.downloadFileAndProcess(fileId,
                    (IContentStreamProcessor) contentStream -> bytes[0] = IOUtils.toByteArray(contentStream));

            String signData = SignUtils.signBinaryData(bytes[0], prInfo.getParticipantCompany().getEdsPrivateKey(),
                    prInfo.getParticipantCompany().getEdsPassword());
            httpPost = new HttpPost(lotDataUrl);
            parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("btn", "docSave"));
            parameters.add(new BasicNameValuePair("signature[" + fileId + "]", signData));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            client.sendPostRequest(httpPost);
        } catch (Exception e) {
            throw new ProcessApplicationException("Не удалось заполнить квалификационные требования.", e);
        }


//        $(byText("Сведения о квалификации при закупках услуг")).shouldBe(visible);
//
//        if ($(byText("Удалить приложение")).is(visible)) {
//            logger.info("Application 6 is already filled");
//            $(byText("Вернуться в список документов")).click();
//            return;
//        }
//
//        $$("tr a.doc_show").stream().forEach((link) -> {
//            String lot = link.closest("tr").findAll("td").get(1).text();
//            if (StringUtils.isNotEmpty(proposal.getLot())
//                    && !StringUtils.equalsIgnoreCase(proposal.getLot(), lot)) {
//                return;
//            }
//
//            link.click();
//            try {
//                fillServices(proposal);
//                fillHardware(proposal);
//                fillStaff(proposal);
//            } catch (Exception e) {
//                logger.error("Application 6 was not formed for lot: {}", lot, e);
//            }
//
//            $(withText("Вернуться")).click();
//        });
//
//        $(byText("Сведения о квалификации при закупках услуг")).shouldBe(visible);
//        proposal.getApplication6employees().stream()
//                .forEach((employee) -> PortalUtils.upload(employee.getDocumentFile()));
//        proposal.getApplication6hardware().stream()
//                .forEach((hardware) -> PortalUtils.upload(hardware.getDocumentFile()));
//        proposal.getApplication6services().stream()
//                .forEach((s) -> PortalUtils.upload(s.getDocumentFile()));
//
//        $$(byName("lot_id[]")).stream().forEach((input) -> {
//            String lot = input.closest("tr").findAll("td").get(1).text();
//            if (StringUtils.isNotEmpty(proposal.getLot())
//                    && !StringUtils.equalsIgnoreCase(proposal.getLot(), lot)) {
//                return;
//            }
//
//            input.setSelected(true);
//        });
//
//        $(byText("Сформировать приложение")).click();
//        $(byText("Подписать")).shouldBe(visible).click();
//        $(byText("Документ успешно сформирован и подписан")).shouldBe(Condition.visible);
//
//        $(byText("Вернуться в список документов")).click();
    }
//
    private void fillServices() throws RequestException, IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        String servicesUrl = lotDataUrl + "/wrk/new";
        Response response = client.sendGetRequest(servicesUrl);
        for (ProposalConfig.Service service : proposalConfig.getApplication6services()) {
            Node node = response.evaluateXPathNode("//select[@name='country_code']/option[text()='" + service.getCountry() + "']");
            HttpPost httpPost = new HttpPost(servicesUrl);
            List<NameValuePair> parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("work", service.getName()));
            parameters.add(new BasicNameValuePair("y1", service.getStartYear()));
            parameters.add(new BasicNameValuePair("m1", service.getStartMonth()));
            parameters.add(new BasicNameValuePair("y2", service.getEndYear()));
            parameters.add(new BasicNameValuePair("m2", service.getEndMonth()));
            parameters.add(new BasicNameValuePair("pay", service.getPrice()));
            parameters.add(new BasicNameValuePair("country_code", node.getAttributes().getNamedItem("value").getNodeValue()));
            parameters.add(new BasicNameValuePair("place", service.getCustomerAddress()));
            parameters.add(new BasicNameValuePair("doc_type_name", service.getDocumentName()));
            parameters.add(new BasicNameValuePair("doc_num", service.getDocumentNumber()));
            parameters.add(new BasicNameValuePair("doc_date", service.getDocumentDate()));
            parameters.add(new BasicNameValuePair("iin", service.getCustomerUIN()));
            parameters.add(new BasicNameValuePair("name", service.getCustomerName()));
            parameters.add(new BasicNameValuePair("btn", "save"));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            client.sendPostRequest(httpPost);
        }
//        $(".nav-tabs").$(byText("Объем оказанных поставщиком услуг")).click();
//        $(".nav-tabs .active").$(byText("Объем оказанных поставщиком услуг")).shouldBe(visible);
//
//        for (Proposal.Service service : proposal.getApplication6services()) {
//            $$(".btn").filterBy(visible).filterBy(withSubElement(cssSelector(".glyphicon-plus"))).first().click();
//            $("#work").val(service.getName());
//            $("#y1").val(service.getStartYear());
//            $(byName("m1")).selectOptionByValue(service.getStartMonth());
//            $("#y2").val(service.getStartYear());
//            $(byName("m2")).selectOptionByValue(service.getStartMonth());
//            $("#pay").val(service.getPrice());
//            $(byName("country_code")).selectOption(service.getCountry());
//            $("#place").val(service.getCustomerAddress());
//            $("#doc_type_name").val(service.getDocumentName());
//            $("#doc_num").val(service.getDocumentNumber());
//            $("#doc_date").val(service.getDocumentDate());
//            $("#iin").val(service.getCustomerUIN());
//            $("#name").val(service.getCustomerName());
//
//            $(byText("Сохранить")).click();
//            SelenideUtils.waitPageLoadComplete();
//            $(withText("Вернуться")).click();
//        }
//
//        $(".nav-tabs .active").$(byText("Объем оказанных поставщиком услуг")).shouldBe(visible);
    }

    private void fillHardware() throws UnsupportedEncodingException, RequestException {
        String hwUrl = lotDataUrl + "/mch/new";
        for (ProposalConfig.Hardware hardware : proposalConfig.getApplication6hardware()) {
            HttpPost httpPost = new HttpPost(hwUrl);
            List<NameValuePair> parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("name", hardware.getName()));
            parameters.add(new BasicNameValuePair("cnt", hardware.getAmount()));
            parameters.add(new BasicNameValuePair("state", hardware.getCondition()));
            parameters.add(new BasicNameValuePair("is_rent", "Арендованное".equalsIgnoreCase(hardware.getRented()) ? "1" : "0"));
            parameters.add(new BasicNameValuePair("rent_name", hardware.getOwner()));
            parameters.add(new BasicNameValuePair("doc_type_name", hardware.getDocumentName()));
            parameters.add(new BasicNameValuePair("doc_num", hardware.getDocumentNumber()));
            parameters.add(new BasicNameValuePair("doc_date", hardware.getDocumentDate()));
            parameters.add(new BasicNameValuePair("btn", "save"));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            client.sendPostRequest(httpPost);
        }
//        $(".nav-tabs").$(byText("Cведения о наличии оборудования")).click();
//        $(".nav-tabs .active").$(byText("Cведения о наличии оборудования")).shouldBe(visible);
//        for (Proposal.Hardware hardware : proposal.getApplication6hardware()) {
//            $$(".btn").filterBy(visible).filter(withSubElement(cssSelector(".glyphicon-plus"))).first().click();
//            $(".panel-heading").$(byText("Cведения о наличии оборудования")).shouldBe(visible);
//            $("#name").val(hardware.getName());
//            $("#cnt").val(hardware.getAmount());
//            $("#state").val(hardware.getCondition());
//            $(byName("is_rent")).selectRadio(StringUtils.equals("Арендованное", hardware.getRented()) ? "1" : "0");
//            $("#rent_name").val(hardware.getOwner());
//            $("#doc_type_name").val(hardware.getDocumentName());
//            $("#doc_num").val(hardware.getDocumentNumber());
//            $("#doc_date").val(hardware.getDocumentDate());
//            $(byText("Сохранить")).click();
//            SelenideUtils.waitPageLoadComplete();
//            $(withText("Вернуться")).click();
//        }
//
//        $(".nav-tabs .active").$(byText("Cведения о наличии оборудования")).shouldBe(visible);
    }

    private void fillStaff() throws IOException, RequestException, ParserConfigurationException, SAXException, XPathExpressionException {
        String staffUrl = lotDataUrl + "/spc/new";
        HttpPost httpPost = new HttpPost(staffUrl);
        List<NameValuePair> parameters = new LinkedList<>();
        parameters.add(new BasicNameValuePair("nokz", "0"));
        parameters.add(new BasicNameValuePair("iin", ""));
        parameters.add(new BasicNameValuePair("btn", "nokz"));
        parameters.add(new BasicNameValuePair("staj", ""));
        parameters.add(new BasicNameValuePair("dipl", ""));
        parameters.add(new BasicNameValuePair("kateg", ""));
        httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
        Response response = client.sendPostRequest(httpPost);

        for (ProposalConfig.Employee employee : proposalConfig.getApplication6employees()) {
            httpPost = new HttpPost(staffUrl);
            Node node = response.evaluateXPathNode("//select[@name='country_code']/option[text()='" + employee.getCountry() + "']");
            parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("nokz", "1"));
            parameters.add(new BasicNameValuePair("country_code", node.getAttributes().getNamedItem("value").getNodeValue()));
            parameters.add(new BasicNameValuePair("fio", employee.getName()));
            parameters.add(new BasicNameValuePair("staj", employee.getExperience()));
            parameters.add(new BasicNameValuePair("dipl", employee.getQualification()));
            parameters.add(new BasicNameValuePair("kateg", employee.getCategory()));
            parameters.add(new BasicNameValuePair("btn", "save"));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            client.sendPostRequest(httpPost);
        }
//        $(".nav-tabs").$(byText("Cведения о квалифицированных работников")).click();
//        $(".nav-tabs .active").$(byText("Cведения о квалифицированных работников")).shouldBe(visible);
//
//        for (Proposal.Employee employee : proposal.getApplication6employees()) {
//            $$(".btn").filterBy(visible).filterBy(withSubElement(cssSelector(".glyphicon-plus"))).first().click();
//            $(".panel-heading").$(byText("Cведения о квалифицированных работников")).shouldBe(visible);
//            /*
//            // TODO: make iin processing (actually it't not neccessary)
//            if (StringUtils.isNotEmpty(employee.getIin())) {
//                $("#iin").val(employee.getIin());
//                $(byValue("find")).click();
//            }
//            */
//
//            $(byValue("nokz")).click();
//            $(byName("country_code")).shouldBe(visible).selectOption(employee.getCountry());
//            $("#fio").val(employee.getName());
//            $("#staj").val(employee.getExperience());
//            $("#dipl").val(employee.getQualification());
//            $("#kateg").val(employee.getCategory());
//            $(byText("Сохранить")).click();
//            SelenideUtils.waitPageLoadComplete();
//            $(withText("Вернуться")).click();
//        }
//
//        $(".nav-tabs .active").$(byText("Cведения о квалифицированных работников")).shouldBe(visible);
    }
}

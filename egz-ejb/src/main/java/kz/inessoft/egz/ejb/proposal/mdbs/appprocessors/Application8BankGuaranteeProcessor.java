package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.proposal.ProposalConfig;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejb.proposal.mdbs.UploadFilesUtil;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Node;

import java.util.LinkedList;

/**
 * Пока добавляется только электронная копия платежного поручения. Электронная банковская гарантия пока не реализовано
 * todo Сделать электронную банковскую гарантию (если это вообще надо)
 */
public class Application8BankGuaranteeProcessor implements ApplicationProcessor {
    private AHttpClient client;
    private String url;
    private PrInfo prInfo;
    private ProposalConfig proposalConfig;
    private ProposalConfigLogic proposalConfigLogic;

    public Application8BankGuaranteeProcessor(AHttpClient client, String url, PrInfo prInfo, ProposalConfigLogic proposalConfigLogic) {
        this.client = client;
        this.url = url;
        this.prInfo = prInfo;
        this.proposalConfig = proposalConfigLogic.getConfig(prInfo.getId());
        this.proposalConfigLogic = proposalConfigLogic;
    }

    public void process() throws ProcessApplicationException {
        try {
            // Заходим на список лотов
            Response response = client.sendGetRequest(url);
            // Находим URL на заполнение документов нужного нам лота
            Node node = response.evaluateXPathNode("//td[text()='" + proposalConfig.getLot() + "']/../td[last()]/a");
            String href = node.getAttributes().getNamedItem("href").getNodeValue();
            // Переходим на страницу с одновременным постом типа документа Платёжное поручение / банковская гарантия(электронная копия). Минус один запрос
            HttpPost httpPost = new HttpPost(href);
            LinkedList<NameValuePair> parameters = new LinkedList<>();
            parameters.add(new BasicNameValuePair("typeDoc", "2"));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            response = client.sendPostRequest(httpPost);
            UploadFilesUtil.uploadAndSignFiles(response, client, prInfo, proposalConfigLogic, "announcement/application-8");
        } catch (Exception e) {
            throw new ProcessApplicationException("Не удалось добавить банковские гарантии в заявку.", e);
        }
//        $(byText("Добавление обеспечения заявки")).shouldBe(visible);
//        $$("tr a.btn").stream().forEach((button) -> {
//            String lot = button.closest("tr").findAll("td").get(0).text();
//            if (!StringUtils.isEmpty(proposal.getLot()) && !StringUtils.equalsIgnoreCase(lot, proposal.getLot())) {
//                return;
//            }
//
//            button.shouldBe(visible).click();
//            $(".panel-heading").$(byText("Добавление обеспечения заявки")).shouldBe(visible);
//            $(byName("typeDoc")).selectOption("Платёжное поручение / банковская гарантия(электронная копия)");
//
//            PortalUtils.upload(proposal.getApplication8documents());
//            $(byText("Назад")).click();
//        });
//
//        $(byText("Вернуться в заявку")).shouldBe(visible).click();
    }
}

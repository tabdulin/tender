package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejb.proposal.mdbs.UploadFilesUtil;

/**
 * Created by alexey on 25.07.16.
 */
public class Application9BankReferenceProcessor implements ApplicationProcessor {
    private AHttpClient client;
    private String url;
    private PrInfo prInfo;
    private ProposalConfigLogic proposalConfigLogic;

    public Application9BankReferenceProcessor(AHttpClient client, String url, PrInfo prInfo, ProposalConfigLogic proposalConfigLogic) {
        this.client = client;
        this.url = url;
        this.prInfo = prInfo;
        this.proposalConfigLogic = proposalConfigLogic;
    }

    public void process() throws ProcessApplicationException {
        try {
            Response response = client.sendGetRequest(url);
            UploadFilesUtil.uploadAndSignFiles(response, client, prInfo, proposalConfigLogic, "anouncement/application-9");
        } catch (Exception e) {
            throw new ProcessApplicationException("Не удалось добавить справку банка об отсутсвии задолженности.", e);
        }
//        $(".panel-heading").$(byText("Приложение 9 (Справка банка об отсутствии задолженности)")).shouldBe(visible);
//        PortalUtils.upload(proposal.getApplication9documents());
//        $(byText("Вернуться в список документов")).shouldBe(visible).click();
    }
}

package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

/**
 * Created by alexey on 21.02.17.
 */
public interface ApplicationProcessor {
    // TODO: think about correct exception processing
    void process() throws ProcessApplicationException;
}

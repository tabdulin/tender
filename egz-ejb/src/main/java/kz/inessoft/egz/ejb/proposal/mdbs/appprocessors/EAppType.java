package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

/**
 * Created by alexey on 21.02.17.
 */
public enum EAppType {
    LotList("Приложение 1 (Перечень лотов)"),
    TechSpec("Приложение 2 (Техническая спецификация) либо Проектно - сметная документация/Технико - экономическое обоснование"),
    KonkursAgreement("Приложение 4 (Соглашение об участии в конкурсе)"),
    Qualification("Приложение 6 (Сведения о квалификации Поставщика при закупках услуг)"),
    BankGuarantee("Приложение 8 (Обеспечение заявки, либо гарантийный денежный взнос)"),
    BankReference("Приложение 9 (Справка банка об отсутствии задолженности)"),
    TaxDebt("Сведения об отсутствии налоговой задолженности"),
    Certificates("Свидетельства, сертификаты, дипломы и другие документы"),
    VatPayer("Свидетельство о постановке на учет по НДС");

    private String description;

    EAppType(String description) {
        this.description = description;
    }

    public static EAppType getTypeByDescription(String description) {
        for (EAppType type : EAppType.values()) {
            if (type.description.equals(description))
                return type;
        }
        return null;
    }
}

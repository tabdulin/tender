package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

/**
 * Created by alexey on 21.02.17.
 */
public class ProcessApplicationException extends Exception {
    public ProcessApplicationException(String message) {
        super(message);
    }

    public ProcessApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}

package kz.inessoft.egz.ejb.proposal.mdbs.appprocessors;

import kz.inessoft.egz.dbentities.PrInfo;
import kz.inessoft.egz.ejb.httpclient.AHttpClient;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejb.proposal.ProposalConfigLogic;
import kz.inessoft.egz.ejb.proposal.mdbs.UploadFilesUtil;

/**
 * Created by alexey on 25.07.16.
 */
public class VatPayerProcessor implements ApplicationProcessor {
    private AHttpClient client;
    private String url;
    private PrInfo prInfo;
    private ProposalConfigLogic proposalConfigLogic;

    public VatPayerProcessor(AHttpClient client, String url, PrInfo prInfo, ProposalConfigLogic proposalConfigLogic) {
        this.client = client;
        this.url = url;
        this.prInfo = prInfo;
        this.proposalConfigLogic = proposalConfigLogic;
    }

    public void process() throws ProcessApplicationException {
        try {
            Response response = client.sendGetRequest(url);
            UploadFilesUtil.uploadAndSignFiles(response, client, prInfo, proposalConfigLogic, "company/application-vat");
        } catch (Exception e) {
            throw new ProcessApplicationException("Не удалось добавить свидетельство о постановке на учет по НДС.", e);
        }
//        $(".panel-heading").$(byText("Свидетельство о постановке на учет по НДС")).shouldBe(visible);
//        PortalUtils.upload(proposal.getVatPayerDocuments());
//        $(byText("Вернуться в список документов")).shouldBe(visible).click();
    }
}

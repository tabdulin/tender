package kz.inessoft.egz.ejb.reports;

import kz.inessoft.egz.dbentities.CGeneralInfo;
import kz.inessoft.egz.dbentities.CGeneralInfoAnnouncementInfo_;
import kz.inessoft.egz.dbentities.CGeneralInfo_;
import kz.inessoft.egz.dbentities.CObject;
import kz.inessoft.egz.dbentities.CObject_;
import kz.inessoft.egz.dbentities.DicContractStatus_;
import kz.inessoft.egz.dbentities.DicContractType_;
import kz.inessoft.egz.dbentities.DicTru_;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TAnnouncement_;
import kz.inessoft.egz.dbentities.TImplementerLot;
import kz.inessoft.egz.dbentities.TImplementerLot_;
import kz.inessoft.egz.dbentities.TLot;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncementId_;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement_;
import kz.inessoft.egz.dbentities.TLot_;
import kz.inessoft.egz.ejbapi.reports.TendersResultsLogic;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by alexey on 26.05.17.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TendersResultsLogicBean implements TendersResultsLogic {
    @PersistenceContext(unitName = "egzUnit")
    protected EntityManager em;

    public void getITExcellTendersResults(OutputStream stream) throws IOException {
        getExcellTendersResults(stream, new String[]{"62%", "63%"});
    }


    public void getTranslateExcellTendersResults(OutputStream stream) throws IOException {
        getExcellTendersResults(stream, new String[]{"74.30%"});
    }

    private void getExcellTendersResults(OutputStream stream, String[] truLikes) throws IOException {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery();
        Root<TLotCrossAnnouncement> lotCrossAnnouncementRoot = query.from(TLotCrossAnnouncement.class);
        Root<TImplementerLot> implementerRoot = query.from(TImplementerLot.class);
        Subquery<CObject> subquery = query.subquery(CObject.class);
        Root<CObject> cObjectRoot = subquery.from(CObject.class);
        subquery.select(cObjectRoot);
        subquery.where(
                builder.and(
                        builder.equal(cObjectRoot.get(CObject_.planId),
                                lotCrossAnnouncementRoot.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.lot).get(TLot_.planUnitId)),
                        builder.equal(cObjectRoot.get(CObject_.generalInfo).get(CGeneralInfo_.announcementInfo).get(CGeneralInfoAnnouncementInfo_.docNumber),
                                lotCrossAnnouncementRoot.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement).get(TAnnouncement_.number)),
                        builder.equal(cObjectRoot.get(CObject_.generalInfo).get(CGeneralInfo_.contractType).get(DicContractType_.id), 1), // Только основной договор
                        builder.notEqual(cObjectRoot.get(CObject_.generalInfo).get(CGeneralInfo_.contractStatus).get(DicContractStatus_.id), 4) // Исключим которые имеют статус "Не заключен"
                )
        );
        query.multiselect(lotCrossAnnouncementRoot, implementerRoot, subquery.getSelection());
//        query.multiselect(lotCrossAnnouncementRoot, implementerRoot);
        Predicate tru;
        if (truLikes.length == 1) {
            tru = builder.like(lotCrossAnnouncementRoot.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.lot).get(TLot_.dicTru).get(DicTru_.code), truLikes[0]);
        } else {
            List<Predicate> predicates = new ArrayList<>();
            for (String truLike : truLikes) {
                predicates.add(builder.like(lotCrossAnnouncementRoot.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.lot).get(TLot_.dicTru).get(DicTru_.code), truLike));
            }
            tru = builder.or(predicates.toArray(new Predicate[predicates.size()]));
        }
        query.where(builder.and(
                tru,
                builder.equal(implementerRoot.get(TImplementerLot_.lotCrossAnnouncement), lotCrossAnnouncementRoot)
        ));
        query.orderBy(builder.asc(lotCrossAnnouncementRoot.get(TLotCrossAnnouncement_.id).get(TLotCrossAnnouncementId_.announcement).get(TAnnouncement_.publicationDate)));
        List<Object[]> resultList = em.createQuery(query).getResultList();

        CriteriaQuery<CObject> cObjectQuery = builder.createQuery(CObject.class);
        Root<CObject> root1 = cObjectQuery.from(CObject.class);
        ParameterExpression<Long> planUnitId = builder.parameter(Long.class);
        cObjectQuery.where(
                builder.and(
                        builder.equal(root1.get(CObject_.planId), planUnitId),
                        builder.equal(root1.get(CObject_.generalInfo).get(CGeneralInfo_.contractType).get(DicContractType_.id), 1)
                )
        );
        TypedQuery<CObject> cObjectTypedQuery = em.createQuery(cObjectQuery);
        XSSFWorkbook wb = new XSSFWorkbook(TendersResultsLogicBean.class.getResourceAsStream("/reports/tendersResult.xlsx"));
        XSSFSheet sheet = wb.getSheet("Данные");
        int row = 1;
        CellStyle cellStyle = wb.createCellStyle();
        CreationHelper createHelper = wb.getCreationHelper();
        cellStyle.setDataFormat(
                createHelper.createDataFormat().getFormat("dd.mm.yyyy"));
        for (Object[] data : resultList) {
            TLotCrossAnnouncement lotCrossAnnouncement = (TLotCrossAnnouncement) data[0];
            TImplementerLot implementerLot = (TImplementerLot) data[1];
            CObject cObject = (CObject) data[2];
            TAnnouncement announcement = lotCrossAnnouncement.getId().getAnnouncement();
            XSSFRow row1 = sheet.createRow(row++);
            TLot lot = lotCrossAnnouncement.getId().getLot();
            row1.createCell(0, XSSFCell.CELL_TYPE_STRING).setCellValue(replaceName(lot.getCustomer().getNameRu()));
            row1.createCell(1, XSSFCell.CELL_TYPE_STRING).setCellValue(lot.getDicTru().getNameRu());
            row1.createCell(2, XSSFCell.CELL_TYPE_STRING).setCellValue(announcement.getNumber());
            row1.createCell(3, XSSFCell.CELL_TYPE_STRING).setCellValue(announcement.getShortNameRu());
            row1.createCell(4, XSSFCell.CELL_TYPE_STRING).setCellValue(announcement.getAnnouncementStatus().getNameRu());
            XSSFCell cell = row1.createCell(5);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(announcement.getPublicationDate());
            row1.createCell(6, XSSFCell.CELL_TYPE_STRING).setCellValue(lotCrossAnnouncement.getLotNumber());
            row1.createCell(7, XSSFCell.CELL_TYPE_STRING).setCellValue(lotCrossAnnouncement.getStatus().getNameRu());
            row1.createCell(8, XSSFCell.CELL_TYPE_STRING).setCellValue(lot.getShortCharacteristicRu());
            row1.createCell(9, XSSFCell.CELL_TYPE_STRING).setCellValue(lot.getAdditionalCharacteristicRu());
            row1.createCell(10, XSSFCell.CELL_TYPE_NUMERIC).setCellValue(lot.getPriceForUnit());
            row1.createCell(11, XSSFCell.CELL_TYPE_STRING).setCellValue(implementerLot.getImplementer().getNameRu());
            if (implementerLot.getFinalPrice() != null)
                row1.createCell(12, XSSFCell.CELL_TYPE_NUMERIC).setCellValue(implementerLot.getFinalPrice());
            if (implementerLot.getAccessGranted() != null)
                row1.createCell(13, XSSFCell.CELL_TYPE_BOOLEAN).setCellValue(implementerLot.getAccessGranted());
            row1.createCell(14, XSSFCell.CELL_TYPE_BOOLEAN).setCellValue(implementerLot.isWinnerFlag());
            if (cObject != null) {
                CGeneralInfo contract = cObject.getGeneralInfo();
                row1.createCell(15, XSSFCell.CELL_TYPE_STRING).setCellValue(contract.getNumber());
                if (contract.getConclusionDate() != null) {
                    cell = row1.createCell(16);
                    cell.setCellStyle(cellStyle);
                    cell.setCellValue(contract.getConclusionDate());
                }
                row1.createCell(17, XSSFCell.CELL_TYPE_STRING).setCellValue(contract.getContractStatus().getNameRu());
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(announcement.getPublicationDate());
            row1.createCell(18, XSSFCell.CELL_TYPE_NUMERIC).setCellValue(calendar.get(Calendar.YEAR));
        }
        wb.write(stream);
    }

    private String replaceName(String name) {
        if (name == null)
            return null;
        return name.replace("Некоммерческое акционерное общество", "НАО")
                .replace("Государственное учреждение", "ГУ")
                .replace("Республиканское государственное предприятие на праве хозяйственного ведения", "РГППХВ")
                .replace("Республиканское государственное предприятие", "РГП")
                .replace("Акционерное общество", "АО")
                .replace("Республиканское государственное учреждение", "РГУ")
                .replace("Республиканское государственное казенное предприятие", "РГКП")
                .replace("Государственное коммунальное предприятие на праве хозяйственного ведения", "ГКППХВ")
                .replace("Государственное коммунальное казенное предприятие", "ГККП")
                .replace("Коммунальное государственное учреждение", "КГУ")
                .replace("Коммунальное государственное предприятие на праве хозяйственного ведения", "КГППХВ")
                .replace("Товарищество с ограниченной ответственностью", "ТОО")
                .replace("Республики Казахстан", "РК");
    }
}

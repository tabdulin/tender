package kz.inessoft.egz.ejb.sign;

/**
 * Created by alexey on 23.02.17.
 */
public class HashData {
    private String md5Hash;
    private String sha256Hash;

    public HashData(String md5Hash, String sha256Hash) {
        this.md5Hash = md5Hash;
        this.sha256Hash = sha256Hash;
    }

    public String getMd5Hash() {
        return md5Hash;
    }

    public String getSha256Hash() {
        return sha256Hash;
    }
}

package kz.inessoft.egz.ejb.sign;

import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import org.apache.xml.security.utils.Base64;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

/**
 * Created by alexey on 15.09.16.
 */
class KeyInfo {
    static class InitKeyInfoException extends Exception {
        InitKeyInfoException(Throwable cause) {
            super(cause);
        }
    }
    private X509Certificate cert = null;
    private PrivateKey privKey = null;
    private Certificate[] chain = null;

    KeyInfo(byte[] privateKey, String keyPassword) throws InitKeyInfoException {
        try {
            KeyStore store = KeyStore.getInstance("PKCS12", KalkanProvider.PROVIDER_NAME);
            store.load(new ByteArrayInputStream(privateKey), keyPassword.toCharArray());
            Enumeration en = store.aliases();
            String alias = null;
            //В данном цикле для получения ключа используется последний alias.
            while (en.hasMoreElements()) alias = en.nextElement().toString();
            chain = store.getCertificateChain(alias);
            cert = (X509Certificate) chain[0];
            privKey = (PrivateKey) store.getKey(alias, keyPassword.toCharArray());
        } catch (UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException | NoSuchProviderException |
                IOException | CertificateException e) {
            throw new InitKeyInfoException(e);
        }
    }

    X509Certificate getCert() {
        return cert;
    }

    String getCertBase64() throws CertificateEncodingException {
        return Base64.encode(cert.getEncoded());
    }

    PrivateKey getPrivKey() {
        return privKey;
    }

    Certificate[] getChain() {
        return chain;
    }
}

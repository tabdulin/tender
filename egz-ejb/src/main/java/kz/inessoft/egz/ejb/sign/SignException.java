package kz.inessoft.egz.ejb.sign;

/**
 * Created by alexey on 16.09.16.
 */
public class SignException extends Exception {
    public SignException(String message, Throwable cause) {
        super(message, cause);
    }
}

package kz.inessoft.egz.ejb.sign;

import kz.gov.pki.kalkan.asn1.cryptopro.CryptoProObjectIdentifiers;
import kz.gov.pki.kalkan.asn1.knca.KNCAObjectIdentifiers;
import kz.gov.pki.kalkan.asn1.pkcs.PKCSObjectIdentifiers;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;
import kz.gov.pki.kalkan.jce.provider.cms.CMSException;
import kz.gov.pki.kalkan.jce.provider.cms.CMSProcessable;
import kz.gov.pki.kalkan.jce.provider.cms.CMSProcessableByteArray;
import kz.gov.pki.kalkan.jce.provider.cms.CMSSignedData;
import kz.gov.pki.kalkan.jce.provider.cms.CMSSignedDataGenerator;
import kz.gov.pki.kalkan.util.encoders.Base64;
import kz.gov.pki.kalkan.xmldsig.KncaXS;
import org.apache.xml.security.encryption.XMLCipherParameters;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.Arrays;

/**
 * Created by alexey on 15.09.16.
 */
public class SignUtils {
    static {
        if (Security.getProvider(KalkanProvider.PROVIDER_NAME) == null) {
            Provider provider = new KalkanProvider();
            Security.addProvider(provider);
        }
        KncaXS.loadXMLSecurity();
    }

    public static SignXMLInfo signXML(String xml, byte[] privateKey, String keyPassword) throws SignException {
        StringWriter os = null;
        try {
            KeyInfo keyInfo = new KeyInfo(privateKey, keyPassword);
            String signMethod;
            String digestMethod;
            String sigAlgOID = keyInfo.getCert().getSigAlgOID();
            if (sigAlgOID.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption.getId())) {
                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha1";
                digestMethod = Constants.MoreAlgorithmsSpecNS + "sha1";
            } else if (sigAlgOID.equals(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId())) {
                signMethod = Constants.MoreAlgorithmsSpecNS + "rsa-sha256";
                digestMethod = XMLCipherParameters.SHA256;
            } else {
                signMethod = Constants.MoreAlgorithmsSpecNS + "gost34310-gost34311";
                digestMethod = Constants.MoreAlgorithmsSpecNS + "gost34311";
            }
            Document doc = parseDocument(xml);
            XMLSignature sig = new XMLSignature(doc, "", signMethod);
            String res = "";
            if (doc.getFirstChild() != null) {
                doc.getFirstChild().appendChild(sig.getElement());
                Transforms transforms = new Transforms(doc);
                transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
                transforms.addTransform(XMLCipherParameters.N14C_XML_CMMNTS);
                sig.addDocument("", transforms, digestMethod);
                sig.addKeyInfo(keyInfo.getCert());
                sig.sign(keyInfo.getPrivKey());
                os = new StringWriter();
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer trans = tf.newTransformer();
                trans.transform(new DOMSource(doc), new StreamResult(os));
                os.flush();
                res = os.toString();
                os.close();
            }
            return new SignXMLInfo(res, keyInfo.getCertBase64().replaceAll("\n", ""));
        } catch (SAXException | TransformerException | ParserConfigurationException | XMLSecurityException |
                IOException | KeyInfo.InitKeyInfoException | CertificateEncodingException e) {
            throw new SignException("Can't sign xml", e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static Document parseDocument(String xml)
            throws IOException, ParserConfigurationException, SAXException {
        ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes("UTF-8"));

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
        return documentBuilder.parse(bais);
    }

    public static String signBinaryData(byte[] data, byte[] privateKey, String keyPassword) throws SignException {
        try {
            CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
            KeyInfo keyInfo = new KeyInfo(privateKey, keyPassword);
            X509Certificate x509Certificate = keyInfo.getCert();
            CertStore chainStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(Arrays.asList(keyInfo.getChain())), KalkanProvider.PROVIDER_NAME);
            if (x509Certificate.getSigAlgOID().equals(PKCSObjectIdentifiers.sha1WithRSAEncryption.getId())) {
                generator.addSigner(keyInfo.getPrivKey(), x509Certificate, CMSSignedDataGenerator.DIGEST_SHA1);
            } else if (x509Certificate.getSigAlgOID().equals(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId())) {
                generator.addSigner(keyInfo.getPrivKey(), x509Certificate, CMSSignedDataGenerator.DIGEST_SHA256);
            } else if (x509Certificate.getSigAlgOID().equals(KNCAObjectIdentifiers.gost34311_95_with_gost34310_2004.getId())) {
                generator.addSigner(keyInfo.getPrivKey(), x509Certificate, CMSSignedDataGenerator.DIGEST_GOST34311_95);
            } else if (x509Certificate.getSigAlgOID().equals(CryptoProObjectIdentifiers.gostR3411_94_with_gostR34310_2004.getId())) {
                generator.addSigner(keyInfo.getPrivKey(), x509Certificate, CMSSignedDataGenerator.DIGEST_GOST3411_GT);
            }
            generator.addCertificatesAndCRLs(chainStore);
            CMSProcessable content = new CMSProcessableByteArray(data);
            CMSSignedData signedData = generator.generate(content, false, KalkanProvider.PROVIDER_NAME);
            byte[] signedDataEncoded = signedData.getEncoded();
            return new String(Base64.encode(signedDataEncoded), "ASCII");
        } catch (KeyInfo.InitKeyInfoException | IOException | CertStoreException | CMSException |
                NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
            throw new SignException("Can't sign data", e);
        }
    }

    public static HashData getHash(byte[] data) throws IOException {
        try {
            MessageDigest digestMd5 = MessageDigest.getInstance("MD5");
            MessageDigest digestSha256 = MessageDigest.getInstance("SHA-256");
            byte[] bytesMd5 = digestMd5.digest(data);
            byte[] bytesSha256 = digestSha256.digest(data);
            return new HashData(convertToString(bytesMd5), convertToString(bytesSha256));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static String convertToString(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte aByte : bytes) {
            builder.append(Integer.toHexString(0xFF & aByte));
        }
        return builder.toString();
    }
}

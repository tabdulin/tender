package kz.inessoft.egz.ejb.sign;

/**
 * Created by alexey on 15.09.16.
 */
public class SignXMLInfo {
    private String signedXML;
    private String base64Certificate;

    public SignXMLInfo(String signedXML, String base64Certificate) {
        this.signedXML = signedXML;
        this.base64Certificate = base64Certificate;
    }

    public String getSignedXML() {
        return signedXML;
    }

    public String getBase64Certificate() {
        return base64Certificate;
    }
}

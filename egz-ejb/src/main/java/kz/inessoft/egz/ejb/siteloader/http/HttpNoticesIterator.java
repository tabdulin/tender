package kz.inessoft.egz.ejb.siteloader.http;

import kz.inessoft.egz.dbentities.IPortalUser;
import kz.inessoft.egz.ejb.XMLUtils;
import kz.inessoft.egz.ejb.httpclient.AuthorizedHttpClient;
import kz.inessoft.egz.ejb.httpclient.HttpClientsPool;
import kz.inessoft.egz.ejb.httpclient.RequestException;
import kz.inessoft.egz.ejb.httpclient.Response;
import kz.inessoft.egz.ejbapi.PortalUrlUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by alexey on 16.09.16.
 */
public class HttpNoticesIterator implements Iterator<SiteNotice> {
    private Iterator<SiteNotice> delegator;

    public HttpNoticesIterator(Date notBefore, IPortalUser portalUser) throws RequestException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        AuthorizedHttpClient authorizedHttpClient = HttpClientsPool.getInstance().getClient(portalUser);
        try {
            abstract class PageIterator {
                List<SiteNotice> getNotices() throws ParseException, RequestException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    int page = 0;
                    List<SiteNotice> retVal = new ArrayList<>();
                    breakPoint:
                    while (true) {
                        Response response = authorizedHttpClient.sendGetRequest(getPageURL(page++));
                        NodeList nodeList = response.evaluateXPathNodeList("//form[@id='notices-form']/table/tbody/tr");
                        for (int i = 0; i < nodeList.getLength(); i++) {
                            Node row = nodeList.item(i);
                            Node node = XMLUtils.evaluateXPathNode("./td[3]", row);
                            if (node == null)
                                // Если нет 3-ей ячейки это обычно обозначает что в таблице всего одна ячейка, где написано что уведомлений нет. По этому прервем цикл
                                break breakPoint;
                            String strDate = node.getTextContent().trim();
                            Date recvDate = sf.parse(strDate);
                            if (recvDate.before(notBefore) || recvDate.equals(notBefore))
                                break breakPoint;
                            SiteNotice notice = new SiteNotice();
                            notice.setPortalId(Long.valueOf(XMLUtils.evaluateXPathNode("./td[1]/input/@value", row).getNodeValue()));
                            notice.setNoticeDate(recvDate);
                            notice.setSubject(XMLUtils.evaluateXPathNode("./td[2]/a", row).getTextContent());
                            notice.setText(XMLUtils.evaluateXPathNode("./td[2]/span", row).getTextContent());
                            retVal.add(notice);
                        }
                    }
                    return retVal;
                }

                abstract String getPageURL(int pageNumber);
            }

            List<SiteNotice> notices = new PageIterator() {
                @Override
                String getPageURL(int pageNumber) {
                    return PortalUrlUtils.createNoticesURL(pageNumber);
                }
            }.getNotices();

            notices.addAll(new PageIterator() {
                @Override
                String getPageURL(int pageNumber) {
                    return PortalUrlUtils.createDeletedNoticesURL(pageNumber);
                }
            }.getNotices());

            Collections.sort(notices, (o1, o2) -> o1.getNoticeDate().compareTo(o2.getNoticeDate()));
            delegator = notices.iterator();
        } catch (ParseException e) {
            throw new RuntimeException("Error while getting notices", e);
        } finally {
            HttpClientsPool.getInstance().freeClient(authorizedHttpClient);
        }
    }

    @Override
    public boolean hasNext() {
        return delegator.hasNext();
    }

    @Override
    public SiteNotice next() {
        return delegator.next();
    }
}

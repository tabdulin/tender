package kz.inessoft.egz.ejb.siteloader.http;

import java.util.Date;

/**
 * Created by alexey on 09.09.16.
 */
public class SiteNotice {
    private Long portalId;
    private Date noticeDate;
    private String subject;
    private String text;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Date getNoticeDate() {
        return noticeDate;
    }

    public void setNoticeDate(Date noticeDate) {
        this.noticeDate = noticeDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

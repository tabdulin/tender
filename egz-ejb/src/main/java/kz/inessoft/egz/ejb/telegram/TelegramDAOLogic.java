package kz.inessoft.egz.ejb.telegram;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 13.09.16.
 */
@Local
public interface TelegramDAOLogic {
    void addChat(Long chatId, long dicParticipantId);

    void deleteChat(Long chatId, long dicParticipantId);

    List<Long> getChats(long dicParticipantId);
}

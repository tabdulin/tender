package kz.inessoft.egz.ejb.telegram;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.DicTelegramChat;
import kz.inessoft.egz.dbentities.DicTelegramChatId;
import kz.inessoft.egz.dbentities.DicTelegramChatId_;
import kz.inessoft.egz.dbentities.DicTelegramChat_;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by alexey on 13.09.16.
 */
@Stateless(name = "TelegramDAOLogic")
public class TelegramDAOLogicBean implements TelegramDAOLogic {
    @PersistenceContext(unitName = "egzUnit")
    private EntityManager em;

    @Override
    public void addChat(Long chatId, long dicParticipantId) {
        DicTelegramChatId id = new DicTelegramChatId();
        id.setChatId(chatId);
        id.setParticipantCompany(em.getReference(DicParticipantCompany.class, dicParticipantId));
        DicTelegramChat dicTelegramChat = em.find(DicTelegramChat.class, id);
        if (dicTelegramChat == null) {
            dicTelegramChat = new DicTelegramChat();
            dicTelegramChat.setId(id);
            em.persist(dicTelegramChat);
        }
    }

    @Override
    public void deleteChat(Long chatId, long dicParticipantId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaDelete<DicTelegramChat> queryDelete = builder.createCriteriaDelete(DicTelegramChat.class);
        Root<DicTelegramChat> root = queryDelete.from(DicTelegramChat.class);
        queryDelete.where(builder.and(
                builder.equal(root.get(DicTelegramChat_.id).get(DicTelegramChatId_.chatId), chatId),
                builder.equal(root.get(DicTelegramChat_.id).get(DicTelegramChatId_.participantCompany), em.getReference(DicParticipantCompany.class, dicParticipantId))
        ));

        em.createQuery(queryDelete).executeUpdate();
    }

    @Override
    public List<Long> getChats(long dicParticipantId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<DicTelegramChat> root = query.from(DicTelegramChat.class);
        query.select(root.get(DicTelegramChat_.id).get(DicTelegramChatId_.chatId));
        query.where(builder.equal(root.get(DicTelegramChat_.id).get(DicTelegramChatId_.participantCompany), em.getReference(DicParticipantCompany.class, dicParticipantId)));
        List<Long> resultList = em.createQuery(query).getResultList();
        resultList.size();
        return resultList;
    }
}

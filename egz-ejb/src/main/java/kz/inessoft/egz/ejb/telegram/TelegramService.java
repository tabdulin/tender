package kz.inessoft.egz.ejb.telegram;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.updatesreceivers.BotSession;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 13.09.16.
 */
@Singleton(name = "TelegramService")
@Startup
public class TelegramService implements TelegramServiceLogic {
    /**
     * Created by alexey on 13.09.16.
     */
    private class LongPollingBot extends TelegramLongPollingBot {
        static final String PASSWORD_CMD_PREFIX = "/tender.password:";
        private String userName;
        private String token;
        private String password;
        private long dicParticipantId;
        private User me;

        LongPollingBot(String userName, String token, String password, long dicParticipantId) {
            this.userName = userName;
            this.token = token;
            this.password = password;
            this.dicParticipantId = dicParticipantId;
        }

        @Override
        // todo почему то если в конфе несколько ботов, и все они залогинены с одного жбосс-сервера, то сюда вызов приходит только одному боту
        // т.е. если например в конфе отдать команду /tender.password:password, то ее примет только один бот
        public void onUpdateReceived(Update update) {
            try {
                Message message = update.getMessage();
                LOGGER.info("Recieved telegram mesage.\nChat title: {}\nid: {}\nmessage text: {}", message.getChat().getTitle(),
                        message.getChatId(), message.getText());
                if (message.getLeftChatMember() != null &&
                        message.getLeftChatMember().getId().equals(getMe().getId()))
                    getTelegramDAOLogic().deleteChat(message.getChatId(), dicParticipantId);
                else {
                    if (message.getText() != null &&
                            message.getText().startsWith(PASSWORD_CMD_PREFIX) &&
                            (PASSWORD_CMD_PREFIX + password).equals(message.getText())) {
                        getTelegramDAOLogic().addChat(message.getChatId(), dicParticipantId);
                        sendMessage(message.getChatId(), "Tenders bot активирован. В этот чат будут поступать сообщения об уведомлениях.");
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Error while process telegram message", e);
            }
        }

        private TelegramDAOLogic getTelegramDAOLogic() throws NamingException {
            return InitialContext.doLookup("java:global/egz-ear/egz-ejb/TelegramDAOLogic");
        }

        @Override
        public String getBotUsername() {
            return userName;
        }

        @Override
        public String getBotToken() {
            return token;
        }

        @Override
        public User getMe() throws TelegramApiException {
            if (me == null)
                me = super.getMe();
            return me;
        }

        void sendMessage(String message) throws NamingException, TelegramApiException {
            TelegramDAOLogic daoLogic = getTelegramDAOLogic();
            for (Long chatId : daoLogic.getChats(dicParticipantId)) {
                try {
                    sendMessage(chatId, message);
                } catch (TelegramApiException e) {
                    if ("Bot was blocked by the user".equals(e.getApiResponse()) ||
                            "Forbidden: bot was kicked from the group chat".equals(e.getApiResponse())) {
                        LOGGER.info("User closed chat {} with bot. Remove this chat from list.", chatId);
                        daoLogic.deleteChat(chatId, dicParticipantId);
                    } else
                        throw e;
                }
            }
        }

        void sendMessage(Long chatId, String message) throws TelegramApiException {
            SendMessage sm = new SendMessage();
            sm.enableHtml(true);
            sm.setText(message);
            sm.setChatId(chatId.toString());
            sendMessage(sm);
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramService.class);
    @EJB
    private DicParticipantCompanyLogic dicParticipantCompanyLogic;
    private Map<Long, BotSession> botSessions;
    private Map<Long, LongPollingBot> longPollingBots;

    @PostConstruct
    public void startService() {
        List<DicParticipantCompany> participantCompanies = dicParticipantCompanyLogic.getDicParticipantCompanies();
        botSessions = new HashMap<>();
        longPollingBots = new HashMap<>();
        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            for (DicParticipantCompany participantCompany : participantCompanies) {
                LongPollingBot longPollingBot = new LongPollingBot(participantCompany.getTelegramUserId(),
                        participantCompany.getTelegramToken(), participantCompany.getTelegramPassword(), participantCompany.getId());
                longPollingBots.put(participantCompany.getId(), longPollingBot);
                botSessions.put(participantCompany.getId(), telegramBotsApi.registerBot(longPollingBot));
                LOGGER.info("Telegram bot started for {}", participantCompany.getNameRu());
            }
        } catch (TelegramApiException e) {
            LOGGER.error("Can't start telegram bot", e);
        }
    }

    @PreDestroy
    public void stopService() {
        for (Map.Entry<Long, BotSession> next : botSessions.entrySet()) {
            next.getValue().close();
            LOGGER.info("Telegram bot stoped for {}", next.getKey());
        }
    }

    public void sendMessage(String message, long participantId) throws TelegramApiException, NamingException {
        longPollingBots.get(participantId).sendMessage(message);
    }
}

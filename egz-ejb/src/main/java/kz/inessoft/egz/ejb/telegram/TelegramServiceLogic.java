package kz.inessoft.egz.ejb.telegram;

import org.telegram.telegrambots.TelegramApiException;

import javax.ejb.Local;
import javax.naming.NamingException;

/**
 * Created by alexey on 13.09.16.
 */
@Local
public interface TelegramServiceLogic {
    void sendMessage(String message, long participantId) throws TelegramApiException, NamingException;
}

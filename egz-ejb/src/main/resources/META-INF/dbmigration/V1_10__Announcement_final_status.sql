ALTER TABLE dic_announcement_status
  ADD COLUMN final_status BOOL;

UPDATE dic_announcement_status
SET final_status = FALSE;

ALTER TABLE dic_announcement_status
  ALTER COLUMN final_status SET NOT NULL;

UPDATE dic_announcement_status
SET final_status = TRUE
WHERE name_ru IN ('Завершено', 'Изменена документация', 'Отменено', 'Отказ от закупки');
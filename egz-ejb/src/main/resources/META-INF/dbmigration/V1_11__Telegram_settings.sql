insert into system_properties (code, "value", description) VALUES
  ('telegram.userId', '', 'ID пользователя для подключения к Telegram'),
  ('telegram.token', '', 'Токен для подключения к Telegram');
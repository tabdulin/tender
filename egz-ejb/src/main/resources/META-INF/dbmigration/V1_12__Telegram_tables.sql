/*==============================================================*/
/* Table: dic_telegram_chat                                     */
/*==============================================================*/
create table dic_telegram_chat (
  id                   INT8                 not null,
  constraint PK_DIC_TELEGRAM_CHAT primary key (id)
);

comment on table dic_telegram_chat is
'Справочник чатов, в которых участвует наш бот';

ALTER TABLE dic_participant_company
  ADD COLUMN portal_password VARCHAR(20) NOT NULL DEFAULT 'password';
ALTER TABLE dic_participant_company
  ADD COLUMN eds_private_key OID NOT NULL  DEFAULT 0;
ALTER TABLE dic_participant_company
  ADD COLUMN eds_password VARCHAR(20) NOT NULL DEFAULT 'password';
ALTER TABLE dic_participant_company
  ADD COLUMN refresh_credential BOOL NOT NULL DEFAULT FALSE;

COMMENT ON COLUMN dic_participant_company.portal_password IS
'Пароль пользователя на портале гос. закупок';

COMMENT ON COLUMN dic_participant_company.eds_private_key IS
'Приватный ключ пользователя для ЭПЦ';

COMMENT ON COLUMN dic_participant_company.eds_password IS
'Пароль к приватному ключу';

ALTER TABLE dic_participant_company
  ALTER COLUMN portal_password DROP DEFAULT;
ALTER TABLE dic_participant_company
  ALTER COLUMN eds_private_key DROP DEFAULT;
ALTER TABLE dic_participant_company
  ALTER COLUMN eds_password DROP DEFAULT;
ALTER TABLE dic_participant_company
  ALTER COLUMN refresh_credential DROP DEFAULT;

UPDATE dic_participant_company
SET refresh_credential = TRUE
WHERE name_ru = 'IServ';

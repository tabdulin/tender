/*==============================================================*/
/* Table: t_portal_file                                         */
/*==============================================================*/
create table t_portal_file (
  id                   INT8                 not null,
  file_name            VARCHAR(512)         null,
  mime_type            VARCHAR(30)          null,
  file_size            INT8                 null,
  file_content         OID                  null,
  constraint PK_T_PORTAL_FILE primary key (id)
);

comment on table t_portal_file is
'Файлы, загруженные с портала. Базовая абстракция';

create sequence seq$t_portal_file
increment 10;

/*==============================================================*/
/* Table: t_portal_uploaded_file                                */
/*==============================================================*/
create table t_portal_uploaded_file (
  id                   INT8                 not null,
  portal_id            INT8                 not null,
  constraint PK_T_PORTAL_UPLOADED_FILE primary key (id)
);

comment on table t_portal_uploaded_file is
'Загруженные пользователем файлы';

/*==============================================================*/
/* Index: idx_t_portal_uploaded_file_port                       */
/*==============================================================*/
create unique index idx_t_portal_uploaded_file_port on t_portal_uploaded_file (
  portal_id
);

alter table t_portal_uploaded_file
  add constraint ал_t_portal_uploaded_file_t_portal_file foreign key (id)
references t_portal_file (id)
on delete cascade on update restrict;

/*==============================================================*/
/* Table: t_portal_recieved_file                                */
/*==============================================================*/
create table t_portal_recieved_file (
  id                   INT8                 not null,
  portal_id            INT8                 not null,
  url                  VARCHAR(256)         not null,
  dic_participant_company_id INT8                 not null,
  constraint PK_T_PORTAL_RECIEVED_FILE primary key (id)
);

comment on table t_portal_recieved_file is
'Принятые системой файлы (как правило - седения об отсутсвии налоговой задолженности)';

/*==============================================================*/
/* Index: idx_t_portal_recieved_file_port                       */
/*==============================================================*/
create unique index idx_t_portal_recieved_file_port on t_portal_recieved_file (
  portal_id
);

alter table t_portal_recieved_file
  add constraint fk_t_portal_recieved_file_dic_participant_company foreign key (dic_participant_company_id)
references dic_participant_company (id)
on delete restrict on update restrict;

alter table t_portal_recieved_file
  add constraint ал_t_portal_recieved_file_t_portal_file foreign key (id)
references t_portal_file (id)
on delete cascade on update restrict;


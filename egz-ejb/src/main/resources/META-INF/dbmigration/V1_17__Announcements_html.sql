/*==============================================================*/
/* Table: t_annoucement_html                                    */
/*==============================================================*/
create table t_annoucement_html (
  t_announcement_id    INT8                 not null,
  dic_participant_company_id INT8                 not null,
  general_tab          TEXT                 not null,
  lots_tab             TEXT                 not null,
  documentation_tab    TEXT                 not null,
  discussions_tab      TEXT                 null,
  proposals_info       TEXT                 null,
  protocols_tab        TEXT                 not null,
  constraint PK_T_ANNOUCEMENT_HTML primary key (t_announcement_id, dic_participant_company_id)
);

comment on table t_annoucement_html is
'HTML-код объявления, загруженный с портала';

alter table t_annoucement_html
  add constraint fk_t_annoucement_html_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

alter table t_annoucement_html
  add constraint fk_t_annoucement_html_dic_participant_company foreign key (dic_participant_company_id)
references dic_participant_company (id)
on delete restrict on update restrict;

alter table t_document add COLUMN source_html TEXT null;

alter table t_portal_file ALTER COLUMN file_name DROP NOT NULL;
alter table t_portal_file ALTER COLUMN mime_type DROP NOT NULL;
alter table t_portal_file ALTER COLUMN file_size DROP NOT NULL;
alter table t_portal_file ALTER COLUMN file_content DROP NOT NULL;

/*==============================================================*/
/* Table: t_discussion                                          */
/*==============================================================*/
create table t_discussion (
  id                   INT8                 not null,
  portal_id            INT8                 not null,
  t_announcement_id    INT8                 not null,
  source_html          TEXT                 not null,
  constraint PK_T_DISCUSSION primary key (id)
);

comment on table t_discussion is
'Обсуждения заявок';

/*==============================================================*/
/* Index: idx_t_discussion_portal_id                            */
/*==============================================================*/
create unique index idx_t_discussion_portal_id on t_discussion (
  portal_id
);

alter table t_discussion
  add constraint fk_t_discussion_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

create sequence seq$t_discussion
increment 10;

/*==============================================================*/
/* Table: dic_proposal_status                                   */
/*==============================================================*/
create table dic_proposal_status (
  id                   INT8                 not null,
  name_ru              VARCHAR(100)         not null,
  constraint PK_DIC_PROPOSAL_STATUS primary key (id)
);

comment on table dic_proposal_status is
'Справочник статусов заявок';

/*==============================================================*/
/* Index: idx_dic_proposal_status_name_ru                       */
/*==============================================================*/
create unique index idx_dic_proposal_status_name_ru on dic_proposal_status (
  name_ru
);

create sequence seq$dic_proposal_status
increment 10;

/*==============================================================*/
/* Table: t_proposal                                            */
/*==============================================================*/
create table t_proposal (
  t_announcement_id    INT8                 not null,
  dic_implementer_id   INT8                 not null,
  portal_id            INT8                 not null,
  dic_proposal_status_id INT8                 not null,
  proposal_time        TIMESTAMP            not null,
  general_tab_info_html TEXT                 not null,
  lots_tab_info_html   TEXT                 not null,
  docs_tab_info_html   TEXT                 not null,
  address              VARCHAR(512)         not null,
  bank_name            VARCHAR(512)         not null,
  iik                  VARCHAR(20)          not null,
  bik                  VARCHAR(20)          not null,
  kbe                  VARCHAR(20)          not null,
  fio                  VARCHAR(256)         not null,
  phone                VARCHAR(160)         not null,
  job_position         VARCHAR(256)         not null,
  consorcium           BOOL                 not null,
  constraint PK_T_PROPOSAL primary key (t_announcement_id, dic_implementer_id)
);

alter table t_proposal
  add constraint fk_t_proposal_dic_implementer foreign key (dic_implementer_id)
references dic_implementer (id)
on delete cascade on update restrict;

alter table t_proposal
  add constraint fk_t_proposal_dic_proposal_status foreign key (dic_proposal_status_id)
references dic_proposal_status (id)
on delete restrict on update restrict;

alter table t_proposal
  add constraint fk_t_proposal_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;


/*==============================================================*/
/* Table: t_proposal_document                                   */
/*==============================================================*/
create table t_proposal_document (
  id                   INT8                 not null,
  t_announcement_id    INT8                 not null,
  dic_implementer_id   INT8                 not null,
  t_document_id        INT8                 not null,
  t_portal_file_id     INT8                 not null,
  constraint PK_T_PROPOSAL_DOCUMENT primary key (id)
);

comment on table t_proposal_document is
'Документация, предоставляемая поставщиками на конкурс';

alter table t_proposal_document
  add constraint fk_t_proposal_document_t_document foreign key (t_document_id)
references t_document (id)
on delete cascade on update restrict;

alter table t_proposal_document
  add constraint fk_t_proposal_document_t_portal_file foreign key (t_portal_file_id)
references t_portal_file (id)
on delete restrict on update restrict;

alter table t_proposal_document
  add constraint fk_t_proposal_document_t_proposal foreign key (t_announcement_id, dic_implementer_id)
references t_proposal (t_announcement_id, dic_implementer_id)
on delete cascade on update restrict;


/*==============================================================*/
/* Table: t_document_cross_t_portal_file                        */
/*==============================================================*/
create table t_document_cross_t_portal_file (
  t_document_id        INT8                 not null,
  t_portal_file_id     INT8                 not null,
  constraint PK_T_DOCUMENT_CROSS_T_PORTAL_F primary key (t_document_id, t_portal_file_id)
);

comment on table t_document_cross_t_portal_file is
'Кросс-таблица документов из конкурсной документации и файлов';

alter table t_document_cross_t_portal_file
  add constraint fk_t_document_cross_t_portal_file_t_document foreign key (t_document_id)
references t_document (id)
on delete cascade on update restrict;

alter table t_document_cross_t_portal_file
  add constraint fk_t_document_cross_t_portal_file_t_portal_file foreign key (t_portal_file_id)
references t_portal_file (id)
on delete restrict on update restrict;

/*==============================================================*/
/* Table: t_lot_cross_t_proposal                                */
/*==============================================================*/
create table t_lot_cross_t_proposal (
  t_announcement_id    INT8                 not null,
  t_lot_id             INT8                 not null,
  dic_implementer_id   INT8                 not null,
  constraint PK_T_LOT_CROSS_T_PROPOSAL primary key (t_announcement_id, t_lot_id, dic_implementer_id)
);

comment on table t_lot_cross_t_proposal is
'Связь предложений с лотами';

alter table t_lot_cross_t_proposal
  add constraint fk_t_lot_cross_t_proposal_t_lot_cross_announcement foreign key (t_announcement_id, t_lot_id)
references t_lot_cross_announcement (t_announcement_id, t_lot_id)
on delete cascade on update restrict;

alter table t_lot_cross_t_proposal
  add constraint fk_t_lot_cross_t_proposal_t_proposal foreign key (t_announcement_id, dic_implementer_id)
references t_proposal (t_announcement_id, dic_implementer_id)
on delete cascade on update restrict;


/*==============================================================*/
/* Table: t_protocol                                            */
/*==============================================================*/
create table t_protocol (
  id                   INT8                 not null,
  t_announcement_id    INT8                 not null,
  protocol_type        VARCHAR(16)          not null,
  t_portal_file_id     INT8                 not null,
  constraint PK_T_PROTOCOL primary key (id)
);

comment on table t_protocol is
'Протоколы по конкурсу';

alter table t_protocol
  add constraint fk_t_protocol_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

alter table t_protocol
  add constraint fk_t_protocol_t_portal_file foreign key (t_portal_file_id)
references t_portal_file (id)
on delete restrict on update restrict;

create sequence seq$t_protocol
increment 10;

create sequence seq$t_proposal_document
increment 10;


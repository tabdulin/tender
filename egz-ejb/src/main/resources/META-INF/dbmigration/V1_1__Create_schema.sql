create sequence seq$dic_announcement_status
increment 10;

create sequence seq$dic_comission_role
increment 10;

create sequence seq$dic_conditional_discount
increment 10;

create sequence seq$dic_gov_organization
increment 10;

create sequence seq$dic_implementer
increment 10;

create sequence seq$dic_lot_status
increment 10;

create sequence seq$dic_purchase_mode
increment 10;

create sequence seq$dic_purchase_object_type
increment 10;

create sequence seq$dic_purchase_type
increment 10;

create sequence seq$dic_tender_documentation
increment 10;

create sequence seq$dic_tru
increment 10;

create sequence seq$dic_unit
increment 10;

create sequence seq$t_announcement
increment 10;

create sequence seq$t_comission
increment 10;

create sequence seq$t_discounts
increment 10;

create sequence seq$t_document
increment 10;

create sequence seq$t_implementer_lot
increment 10;

create sequence seq$t_lot
increment 10;

create sequence seq$t_lot_placement
increment 10;

/*==============================================================*/
/* Table: dic_announcement_status                               */
/*==============================================================*/
create table dic_announcement_status (
   id                   INT8                 not null,
   name_ru              VARCHAR(128)         not null,
   constraint PK_DIC_ANNOUNCEMENT_STATUS primary key (id)
);

comment on table dic_announcement_status is
'Справочник статусов объявления (опубликован, прием заявок и т.д.).';

/*==============================================================*/
/* Index: idx_dic_announcement_status_nam                       */
/*==============================================================*/
create unique index idx_dic_announcement_status_nam on dic_announcement_status (
   name_ru
);

/*==============================================================*/
/* Table: dic_comission_role                                    */
/*==============================================================*/
create table dic_comission_role (
   id                   INT8                 not null,
   name_ru              VARCHAR(128)         not null,
   constraint PK_DIC_COMISSION_ROLE primary key (id)
);

comment on table dic_comission_role is
'Справочник ролей членов конкурсных комиссий (председатель, секретарь и т.п.)';

/*==============================================================*/
/* Index: idx_dic_comission_role_name_ru                        */
/*==============================================================*/
create unique index idx_dic_comission_role_name_ru on dic_comission_role (
   name_ru
);

/*==============================================================*/
/* Table: dic_conditional_discount                              */
/*==============================================================*/
create table dic_conditional_discount (
   id                   INT8                 not null,
   name_ru              VARCHAR(512)         not null,
   constraint PK_DIC_CONDITIONAL_DISCOUNT primary key (id)
);

comment on table dic_conditional_discount is
'Справочник условных скидок';

/*==============================================================*/
/* Index: idx_dic_conditional_discount_na                       */
/*==============================================================*/
create unique index idx_dic_conditional_discount_na on dic_conditional_discount (
   name_ru
);

/*==============================================================*/
/* Table: dic_gov_organization                                  */
/*==============================================================*/
create table dic_gov_organization (
   id                   INT8                 not null,
   bin                  VARCHAR(12)          null,
   name_ru              VARCHAR(1024)        not null,
   address              VARCHAR(1024)        null,
   constraint PK_DIC_GOV_ORGANIZATION primary key (id)
);

comment on table dic_gov_organization is
'Справочник гос. органов';

/*==============================================================*/
/* Index: idx_dic_gov_organization_bin                          */
/*==============================================================*/
create unique index idx_dic_gov_organization_bin on dic_gov_organization (
   bin
);

/*==============================================================*/
/* Table: dic_implementer                                       */
/*==============================================================*/
create table dic_implementer (
   id                   INT8                 not null,
   xin                  VARCHAR(12)          not null,
   name_ru              VARCHAR(512)         not null,
   constraint PK_DIC_IMPLEMENTER primary key (id)
);

comment on table dic_implementer is
'Справочник участников конкурсов';

/*==============================================================*/
/* Index: idx_dic_implementer_xin                               */
/*==============================================================*/
create unique index idx_dic_implementer_xin on dic_implementer (
   xin
);

/*==============================================================*/
/* Table: dic_lot_status                                        */
/*==============================================================*/
create table dic_lot_status (
   id                   INT8                 not null,
   name_ru              VARCHAR(128)         not null,
   constraint PK_DIC_LOT_STATUS primary key (id)
);

comment on table dic_lot_status is
'Справочник статусов лотов (Закупка не состоялась и т.д.)';

/*==============================================================*/
/* Index: idx_dic_lot_status_name                               */
/*==============================================================*/
create unique index idx_dic_lot_status_name on dic_lot_status (
   name_ru
);

/*==============================================================*/
/* Table: dic_participant_company                               */
/*==============================================================*/
create table dic_participant_company (
   id                   INT8                 not null,
   name_ru              VARCHAR(256)         not null,
   constraint PK_DIC_PARTICIPANT_COMPANY primary key (id)
);

comment on table dic_participant_company is
'Справочник компаний, от которых идем на конкурсы (IServ, Inessoft и т.д.)';

/*==============================================================*/
/* Table: dic_participation_template                            */
/*==============================================================*/
create table dic_participation_template (
   id                   INT8                 not null,
   name_ru              VARCHAR(256)         not null,
   constraint PK_DIC_PARTICIPATION_TEMPLATE primary key (id)
);

comment on table dic_participation_template is
'Справочник видов участия (подача заявки, подача пустой заявки, простой мониторинг)';

/*==============================================================*/
/* Table: dic_purchase_mode                                     */
/*==============================================================*/
create table dic_purchase_mode (
   id                   INT8                 not null,
   name_ru              VARCHAR(128)         not null,
   constraint PK_DIC_PURCHASE_MODE primary key (id)
);

comment on table dic_purchase_mode is
'Справочник способов проведения закупки (запрос ценовый предложений и т.д.)';

/*==============================================================*/
/* Index: idx_dic_purchase_mode_name                            */
/*==============================================================*/
create unique index idx_dic_purchase_mode_name on dic_purchase_mode (
   name_ru
);

/*==============================================================*/
/* Table: dic_purchase_object_type                              */
/*==============================================================*/
create table dic_purchase_object_type (
   id                   INT8                 not null,
   name_ru              VARCHAR(128)         not null,
   constraint PK_DIC_PURCHASE_OBJECT_TYPE primary key (id)
);

comment on table dic_purchase_object_type is
'Справочник видов предметов закуки (услуга, товар и т.д.)';

/*==============================================================*/
/* Index: idx_dic_purchase_object_type_na                       */
/*==============================================================*/
create unique index idx_dic_purchase_object_type_na on dic_purchase_object_type (
   name_ru
);

/*==============================================================*/
/* Table: dic_purchase_type                                     */
/*==============================================================*/
create table dic_purchase_type (
   id                   INT8                 not null,
   name_ru              VARCHAR(128)         not null,
   constraint PK_DIC_PURCHASE_TYPE primary key (id)
);

comment on table dic_purchase_type is
'Справочник типов закупки (первая закупка и т.д.)';

/*==============================================================*/
/* Index: idx_dic_purchase_type_name_ru                         */
/*==============================================================*/
create unique index idx_dic_purchase_type_name_ru on dic_purchase_type (
   name_ru
);

/*==============================================================*/
/* Table: dic_tender_documentation                              */
/*==============================================================*/
create table dic_tender_documentation (
   id                   INT8                 not null,
   name_ru              VARCHAR(512)         not null,
   constraint PK_DIC_TENDER_DOCUMENTATION primary key (id)
);

comment on table dic_tender_documentation is
'Справочник видов прикрепляемых к конкурсу докумнетов';

/*==============================================================*/
/* Index: idx_dic_tender_documentation_na                       */
/*==============================================================*/
create unique index idx_dic_tender_documentation_na on dic_tender_documentation (
   name_ru
);

/*==============================================================*/
/* Table: dic_tru                                               */
/*==============================================================*/
create table dic_tru (
   id                   INT8                 not null,
   code                 VARCHAR(30)          not null,
   name_ru              VARCHAR(512)         not null,
   constraint PK_DIC_TRU primary key (id)
);

comment on table dic_tru is
'Единый номенклатурный справочник товаров, работ и услуг';

/*==============================================================*/
/* Index: idx_dic_tru_code                                      */
/*==============================================================*/
create unique index idx_dic_tru_code on dic_tru (
   code
);

/*==============================================================*/
/* Table: dic_unit                                              */
/*==============================================================*/
create table dic_unit (
   id                   INT8                 not null,
   name_ru              VARCHAR(128)         not null,
   constraint PK_DIC_UNIT primary key (id)
);

comment on table dic_unit is
'Справочник единиц измерения';

/*==============================================================*/
/* Index: idx_dic_unit_name                                     */
/*==============================================================*/
create unique index idx_dic_unit_name on dic_unit (
   name_ru
);

/*==============================================================*/
/* Table: security_role                                         */
/*==============================================================*/
create table security_role (
   role                 VARCHAR(20)          null,
   role_group           VARCHAR(20)          null,
   username             VARCHAR(20)          not null
);

/*==============================================================*/
/* Table: security_user                                         */
/*==============================================================*/
create table security_user (
   username             VARCHAR(20)          not null,
   password             VARCHAR(24)          not null,
   constraint PK_SECURITY_USER primary key (username)
);

/*==============================================================*/
/* Table: system_properties                                     */
/*==============================================================*/
create table system_properties (
   code                 VARCHAR(50)          not null,
   value                VARCHAR(512)         not null,
   description          VARCHAR(512)         null,
   constraint PK_SYSTEM_PROPERTIES primary key (code)
);

/*==============================================================*/
/* Table: t_announcement                                        */
/*==============================================================*/
create table t_announcement (
   id                   INT8                 not null,
   portal_id            INT8                 not null,
   number               VARCHAR(20)          not null,
   monitoring           BOOL                 not null,
   load_time            TIMESTAMP            not null,
   name_ru              VARCHAR(1024)        not null,
   dic_announcement_status_id INT8                 not null,
   publication_date     TIMESTAMP            not null,
   start_rcv_time       TIMESTAMP            null,
   end_rcv_time         TIMESTAMP            null,
   start_rcv_appendix_time TIMESTAMP            null,
   end_rcv_appendix_time TIMESTAMP            null,
   start_discussion_time TIMESTAMP            null,
   end_discussion_time  TIMESTAMP            null,
   dic_purchase_mode_id INT8                 not null,
   dic_purchase_type_id INT8                 null,
   dic_purchase_object_type_id INT8                 not null,
   properties           VARCHAR(128)         null,
   dic_gov_organizations_id INT8                 not null,
   organizator_fio      VARCHAR(128)         null,
   organizator_job_position VARCHAR(256)         null,
   organizator_phone    VARCHAR(80)          null,
   organizator_email    VARCHAR(128)         null,
   organizator_bank_requisites VARCHAR(512)         null,
   constraint PK_T_ANNOUNCEMENT primary key (id)
);

comment on table t_announcement is
'Объявления о конкурсе';

comment on column t_announcement.portal_id is
'Идентификатор конкурса на портале ГЗ';

comment on column t_announcement.number is
'Номер конкурса на сайте';

comment on column t_announcement.name_ru is
'Название конкурса';

comment on column t_announcement.dic_announcement_status_id is
'Статус конкурса';

comment on column t_announcement.publication_date is
'Дата публикации на портале';

comment on column t_announcement.start_rcv_time is
'Срок начала приема заявко';

comment on column t_announcement.end_rcv_time is
'Срок окончания приема заявок';

comment on column t_announcement.dic_purchase_mode_id is
'Способ проведения закупки';

comment on column t_announcement.dic_purchase_type_id is
'Тип закупки';

comment on column t_announcement.dic_purchase_object_type_id is
'Вид предмета закупки';

comment on column t_announcement.properties is
'Признаки';

comment on column t_announcement.dic_gov_organizations_id is
'Организатор конкурса';

/*==============================================================*/
/* Index: idx_t_announcement_portal_id                          */
/*==============================================================*/
create unique index idx_t_announcement_portal_id on t_announcement (
   portal_id
);

/*==============================================================*/
/* Index: idx_t_announcement_number                             */
/*==============================================================*/
create unique index idx_t_announcement_number on t_announcement (
   number
);

/*==============================================================*/
/* Table: t_comission                                           */
/*==============================================================*/
create table t_comission (
   id                   INT8                 not null,
   t_announcement_id    INT8                 not null,
   dic_comission_role_id INT8                 not null,
   fio                  VARCHAR(128)         not null,
   job_position         VARCHAR(512)         not null,
   constraint PK_T_COMISSION primary key (id)
);

comment on table t_comission is
'Состав конкурсной комиссии';

/*==============================================================*/
/* Table: t_cross_announcement_participant                      */
/*==============================================================*/
create table t_cross_announcement_participant (
   t_announcement_id    INT8                 not null,
   dic_participant_company_id INT8                 not null,
   dic_participation_template_id INT8                 not null,
   constraint PK_T_CROSS_ANNOUNCEMENT_PARTIC primary key (t_announcement_id, dic_participant_company_id)
);

/*==============================================================*/
/* Table: t_discount                                            */
/*==============================================================*/
create table t_discount (
   id                   INT8                 not null,
   t_implementer_lot_id INT8                 not null,
   dic_conditional_discount_id INT8                 not null,
   amount               DECIMAL(5,2)         not null,
   constraint PK_T_DISCOUNT primary key (id)
);

/*==============================================================*/
/* Table: t_document                                            */
/*==============================================================*/
create table t_document (
   id                   INT8                 not null,
   t_announcement_id    INT8                 not null,
   dic_tender_documentation_id INT8                 not null,
   required             BOOL                 not null,
   constraint PK_T_DOCUMENT primary key (id)
);

comment on table t_document is
'Документы по конкурсу';

/*==============================================================*/
/* Index: idx_t_document_ann_dic_doc                            */
/*==============================================================*/
create unique index idx_t_document_ann_dic_doc on t_document (
   t_announcement_id,
   dic_tender_documentation_id
);

/*==============================================================*/
/* Table: t_implementer_lot                                     */
/*==============================================================*/
create table t_implementer_lot (
   id                   INT8                 not null,
   t_announcement_id    INT8                 not null,
   t_lot_id             INT8                 not null,
   dic_implementer_id   INT8                 not null,
   access_granted       BOOL                 not null,
   access_denied_cause  VARCHAR(1024)        null,
   proposal_time        TIMESTAMP            not null,
   requisites           VARCHAR(1024)        null,
   requested_price      DECIMAL(13,2)        null,
   act26_price          DECIMAL(13,2)        null,
   final_price          DECIMAL(13,2)        null,
   winner_flag          BOOL                 not null,
   constraint PK_T_IMPLEMENTER_LOT primary key (id)
);

comment on table t_implementer_lot is
'Связь лота с участниками конкурса';

comment on column t_implementer_lot.proposal_time is
'Дата и время подачи заявки';

/*==============================================================*/
/* Index: idx_t_implementer_lot_lot_impl                        */
/*==============================================================*/
create unique index idx_t_implementer_lot_lot_impl on t_implementer_lot (
   t_announcement_id,
   t_lot_id,
   dic_implementer_id
);

/*==============================================================*/
/* Table: t_lot                                                 */
/*==============================================================*/
create table t_lot (
   id                   INT8                 not null,
   portal_id            INT8                 not null,
   dic_lot_status_id    INT8                 not null,
   dic_gov_organization_id INT8                 not null,
   dic_tru_id           INT8                 not null,
   short_characteristic_ru VARCHAR(1024)        null,
   additional_characteristic_ru VARCHAR(2048)        null,
   payment_source_ru    VARCHAR(1024)        null,
   price_for_unit       DECIMAL(13,2)        null,
   count                DECIMAL(13,2)        not null,
   dic_unit_id          INT8                 not null,
   first_year_sum       DECIMAL(13,2)        not null,
   second_year_sum      DECIMAL(13,2)        not null,
   third_year_sum       DECIMAL(13,2)        not null,
   total_sum            DECIMAL(13,2)        not null,
   prepayment           DECIMAL(5,2)         not null,
   delivery_time        VARCHAR(512)         null,
   incoterms            VARCHAR(512)         null,
   ingeneering          BOOL                 null,
   demping              VARCHAR(128)         null,
   customer_fio         VARCHAR(256)         null,
   customer_job_position VARCHAR(256)         null,
   customer_phone       VARCHAR(80)          null,
   customer_email       VARCHAR(128)         null,
   customer_bank_requisites VARCHAR(512)         null,
   constraint PK_T_LOT primary key (id)
);

comment on table t_lot is
'Лоты по конкурсу';

comment on column t_lot.dic_gov_organization_id is
'Заказчик';

comment on column t_lot.short_characteristic_ru is
'Краткая характеристика';

comment on column t_lot.additional_characteristic_ru is
'Дополнительная характеристика';

comment on column t_lot.payment_source_ru is
'Источник финансирования';

comment on column t_lot.first_year_sum is
'Сумма на 1-ый год';

comment on column t_lot.second_year_sum is
'Сумма на 2-ой год';

comment on column t_lot.third_year_sum is
'Сумма на 3-ий год';

comment on column t_lot.total_sum is
'Запланированная сумма';

comment on column t_lot.incoterms is
'Условия поставки ИНКОТЕРМС';

comment on column t_lot.customer_fio is
'ФИО представителя заказчика';

comment on column t_lot.customer_job_position is
'Должность представителя заказчика';

comment on column t_lot.customer_phone is
'Контактный телефон представителя заказчика';

comment on column t_lot.customer_email is
'EMail представителя заказчика';

comment on column t_lot.customer_bank_requisites is
'Банковские реквизиты представителя заказчика';

/*==============================================================*/
/* Index: idx_t_lot_portal_id                                   */
/*==============================================================*/
create unique index idx_t_lot_portal_id on t_lot (
   portal_id
);

/*==============================================================*/
/* Table: t_lot_cross_announcement                              */
/*==============================================================*/
create table t_lot_cross_announcement (
   t_announcement_id    INT8                 not null,
   t_lot_id             INT8                 not null,
   lot_number           VARCHAR(20)          not null,
   constraint PK_T_LOT_CROSS_ANNOUNCEMENT primary key (t_announcement_id, t_lot_id)
);

comment on table t_lot_cross_announcement is
'Кросс-таблица связи многие ко многим лоты-объявления';

/*==============================================================*/
/* Table: t_lot_placement                                       */
/*==============================================================*/
create table t_lot_placement (
   id                   INT8                 not null,
   t_lot_id             INT8                 not null,
   kato                 VARCHAR(12)          null,
   address              VARCHAR(512)         null,
   volume               DECIMAL(15,3)        null,
   constraint PK_T_LOT_PLACEMENT primary key (id)
);

comment on table t_lot_placement is
'Места поставки товара';

alter table security_role
   add constraint fk_security_role_security_user foreign key (username)
references security_user (username)
on delete cascade on update cascade;

alter table t_announcement
   add constraint fk_t_announcement_dic_announcement_status foreign key (dic_announcement_status_id)
references dic_announcement_status (id)
on delete restrict on update restrict;

alter table t_announcement
   add constraint fk_t_announcement_dic_gov_organization foreign key (dic_gov_organizations_id)
references dic_gov_organization (id)
on delete restrict on update restrict;

alter table t_announcement
   add constraint fk_t_announcement_dic_purchase_mode foreign key (dic_purchase_mode_id)
references dic_purchase_mode (id)
on delete restrict on update restrict;

alter table t_announcement
   add constraint fk_t_announcement_dic_purchase_object_type foreign key (dic_purchase_object_type_id)
references dic_purchase_object_type (id)
on delete restrict on update restrict;

alter table t_announcement
   add constraint fk_t_announcement_dic_purchase_type foreign key (dic_purchase_type_id)
references dic_purchase_type (id)
on delete restrict on update restrict;

alter table t_comission
   add constraint fk_t_comission_dic_comission_role foreign key (dic_comission_role_id)
references dic_comission_role (id)
on delete restrict on update restrict;

alter table t_comission
   add constraint fk_t_comission_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

alter table t_cross_announcement_participant
   add constraint fk_t_cross_announcement_participant_dic_participant_company foreign key (dic_participant_company_id)
references dic_participant_company (id)
on delete restrict on update restrict;

alter table t_cross_announcement_participant
   add constraint fk_t_cross_announcement_participant_dic_participation_template foreign key (dic_participation_template_id)
references dic_participation_template (id)
on delete restrict on update restrict;

alter table t_cross_announcement_participant
   add constraint fk_t_cross_announcement_participant_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

alter table t_discount
   add constraint fk_t_discounts_dic_conditional_discount foreign key (dic_conditional_discount_id)
references dic_conditional_discount (id)
on delete restrict on update restrict;

alter table t_discount
   add constraint fk_t_discounts_t_implementer_lot foreign key (t_implementer_lot_id)
references t_implementer_lot (id)
on delete cascade on update restrict;

alter table t_document
   add constraint fk_t_document_dic_tender_documentation foreign key (dic_tender_documentation_id)
references dic_tender_documentation (id)
on delete restrict on update restrict;

alter table t_document
   add constraint fk_t_documents_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

alter table t_implementer_lot
   add constraint fk_t_implementer_lot_dic_implementer foreign key (dic_implementer_id)
references dic_implementer (id)
on delete restrict on update restrict;

alter table t_implementer_lot
   add constraint fk_t_implementer_lot_t_lot_cross_announcement foreign key (t_announcement_id, t_lot_id)
references t_lot_cross_announcement (t_announcement_id, t_lot_id)
on delete cascade on update restrict;

alter table t_lot
   add constraint fk_t_lot_dic_gov_organization foreign key (dic_gov_organization_id)
references dic_gov_organization (id)
on delete restrict on update restrict;

alter table t_lot
   add constraint fk_t_lot_dic_lot_status foreign key (dic_lot_status_id)
references dic_lot_status (id)
on delete restrict on update restrict;

alter table t_lot
   add constraint fk_t_lot_dic_tru foreign key (dic_tru_id)
references dic_tru (id)
on delete restrict on update restrict;

alter table t_lot
   add constraint fk_t_lot_dic_unit foreign key (dic_unit_id)
references dic_unit (id)
on delete restrict on update restrict;

alter table t_lot_cross_announcement
   add constraint fk_t_lot_cross_announcement_t_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

alter table t_lot_cross_announcement
   add constraint fk_t_lot_cross_announcement_t_lot foreign key (t_lot_id)
references t_lot (id)
on delete cascade on update restrict;

alter table t_lot_placement
   add constraint fk_t_lot_placement_t_lot foreign key (t_lot_id)
references t_lot (id)
on delete cascade on update restrict;

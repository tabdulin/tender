/*==============================================================*/
/* Table: t_discussion_cross_announcement                       */
/*==============================================================*/
create table t_discussion_cross_announcement (
  t_discussion_id      INT8                 not null,
  t_announcement_id    INT8                 not null,
  constraint PK_T_DISCUSSION_CROSS_ANNOUNCE primary key (t_discussion_id, t_announcement_id)
);

comment on table t_discussion_cross_announcement is
'Связь многие ко многим обсуждений и объявлений, т.к. старые обсуждения появляются в новых переоткрытых конкурсах';

alter table t_discussion_cross_announcement
  add constraint fk_t_disc_cross_ann_announcement foreign key (t_announcement_id)
references t_announcement (id)
on delete cascade on update restrict;

alter table t_discussion_cross_announcement
  add constraint fk_t_disc_cross_ann_discussion foreign key (t_discussion_id)
references t_discussion (id)
on delete cascade on update restrict;

insert into t_discussion_cross_announcement(t_discussion_id, t_announcement_id)
    select id, t_announcement_id from t_discussion;

alter TABLE t_discussion DROP t_announcement_id;
/*==============================================================*/
/* Table: dic_portal_resources                                  */
/*==============================================================*/
create table dic_portal_resource (
  html_source_text     VARCHAR(512)         not null,
  html_replacement     TEXT                 not null,
  constraint PK_DIC_PORTAL_RESOURCES primary key (html_source_text)
);

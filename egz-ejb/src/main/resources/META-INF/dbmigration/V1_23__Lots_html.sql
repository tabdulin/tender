ALTER TABLE t_lot_cross_announcement
  ADD COLUMN original_html TEXT NOT NULL DEFAULT 'not downloaded';

ALTER TABLE t_lot_cross_announcement
  ALTER original_html DROP DEFAULT;
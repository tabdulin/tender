/*==============================================================*/
/* Table: dic_contract_type                                     */
/*==============================================================*/
create table dic_contract_type (
  id                   INT8                 not null,
  name_ru              VARCHAR(30)          null,
  constraint PK_DIC_CONTRACT_TYPE primary key (id)
);

/*==============================================================*/
/* Index: idx_dic_contract_type_name_ru                         */
/*==============================================================*/
create unique index idx_dic_contract_type_name_ru on dic_contract_type (
  name_ru
);

create sequence seq$dic_contract_type
increment 10;


/*==============================================================*/
/* Table: dic_contract_conclusion_type                          */
/*==============================================================*/
create table dic_contract_conclusion_type (
  id                   INT8                 not null,
  name_ru              VARCHAR(100)         not null,
  constraint PK_DIC_CONTRACT_CONCLUSION_TYP primary key (id)
);

comment on table dic_contract_conclusion_type is
'Справочник "Форма заключения договора"';

/*==============================================================*/
/* Index: idx_dic_contract_conclusion_typ                       */
/*==============================================================*/
create unique index idx_dic_contract_conclusion_typ on dic_contract_conclusion_type (
  name_ru
);

create sequence seq$dic_contract_conclusion_type
increment 10;


/*==============================================================*/
/* Table: dic_contract_status                                   */
/*==============================================================*/
create table dic_contract_status (
  id                   INT8                 not null,
  name_ru              VARCHAR(20)          not null,
  constraint PK_DIC_CONTRACT_STATUS primary key (id)
);

comment on table dic_contract_status is
'Справочник "Статус договора"';

/*==============================================================*/
/* Index: idx_dic_contract_status_name_ru                       */
/*==============================================================*/
create unique index idx_dic_contract_status_name_ru on dic_contract_status (
  name_ru
);

create sequence seq$dic_contract_status
increment 10;


/*==============================================================*/
/* Table: dic_contract_purchase_type                            */
/*==============================================================*/
create table dic_contract_purchase_type (
  id                   INT8                 not null,
  name_ru              VARCHAR(20)          not null,
  constraint PK_DIC_CONTRACT_PURCHASE_TYPE primary key (id)
);

comment on table dic_contract_purchase_type is
'Справочник "Тип закупки"';

/*==============================================================*/
/* Index: idx_dic_contract_buing_type_nam                       */
/*==============================================================*/
create unique index idx_dic_contract_buing_type_nam on dic_contract_purchase_type (
  name_ru
);


create sequence seq$dic_contract_buing_type
increment 10;


/*==============================================================*/
/* Table: dic_contract_budjet_type                              */
/*==============================================================*/
create table dic_contract_budjet_type (
  id                   INT8                 not null,
  name_ru              VARCHAR(200)         not null,
  constraint PK_DIC_CONTRACT_BUDJET_TYPE primary key (id)
);

comment on table dic_contract_budjet_type is
'Справочник "Вид бюджета"';

/*==============================================================*/
/* Index: idx_dic_contract_budjet_type_na                       */
/*==============================================================*/
create unique index idx_dic_contract_budjet_type_na on dic_contract_budjet_type (
  name_ru
);

create sequence seq$dic_contract_budjet_type
increment 10;


/*==============================================================*/
/* Table: dic_currency                                          */
/*==============================================================*/
create table dic_currency (
  id                   INT8                 not null,
  code                 VARCHAR(3)           not null,
  constraint PK_DIC_CURRENCY primary key (id)
);

comment on table dic_currency is
'Справочник валют';

/*==============================================================*/
/* Index: idx_dic_currency_code                                 */
/*==============================================================*/
create unique index idx_dic_currency_code on dic_currency (
  code
);

create sequence seq$dic_currency
increment 10;


/*==============================================================*/
/* Table: dic_contract_object_purchase_type                     */
/*==============================================================*/
create table dic_contract_object_purchase_type (
  id                   INT8                 not null,
  name_ru              VARCHAR(100)         not null,
  constraint PK_DIC_CONTRACT_OBJECT_PURCHAS primary key (id)
);

comment on table dic_contract_object_purchase_type is
'Справочник типов закупи в контрактах';

/*==============================================================*/
/* Index: idx_dic_contract_purchase_type_                       */
/*==============================================================*/
create unique index idx_dic_contract_purchase_type_ on dic_contract_object_purchase_type (
  name_ru
);


create sequence seq$dic_contract_purchase_type
increment 10;


/*==============================================================*/
/* Table: c_general_info                                        */
/*==============================================================*/
create table c_general_info (
  id                   INT8                 not null,
  portal_id            INT8                 not null,
  general_tab          TEXT                 not null,
  objects_tab          TEXT                 not null,
  customer_tab         TEXT                 not null,
  treasury_tab         TEXT                 not null,
  agreement_tab        TEXT                 not null,
  dic_contract_type_id INT8                 not null,
  reestr_number        VARCHAR(25)          not null,
  number               INT8                 not null,
  announcement_doc_name VARCHAR(50)          null,
  announcement_number  VARCHAR(20)          null,
  announcement_date    DATE                 null,
  conclusion_date      DATE                 not null,
  creation_time        TIMESTAMP            not null,
  short_description_kk VARCHAR(512)         not null,
  short_description_ru VARCHAR(512)         not null,
  dic_purchase_object_type_id INT8                 not null,
  dic_contract_conclusion_type_id INT8                 not null,
  dic_contract_status_id INT8                 not null,
  dic_contract_purchase_type_id INT8                 not null,
  financial_year       INT2                 not null,
  dic_contract_budjet_type_id INT8                 not null,
  planed_dic_purchase_mode_id INT8                 not null,
  real_dic_purchase_mode_id INT8                 not null,
  planed_sum          DECIMAL(13,2)        not null,
  result_contract_sum  DECIMAL(13,2)        not null,
  total_contract_sum   DECIMAL(13,2)        not null,
  real_contract_sum    DECIMAL(13,2)        null,
  dic_currency_id      INT8                 not null,
  rate                 DECIMAL(13,2)        not null,
  contract_time        DATE                 not null,
  planned_execution_date DATE                 not null,
  real_execution_date  DATE                 null,
  execution_mark_date  DATE                 null,
  doc_name_ru          VARCHAR(512)         not null,
  doc_name_kk          VARCHAR(512)         not null,
  doc_number           VARCHAR(40)          null,
  doc_date             DATE                 null,
  dic_gov_organization_id INT8                 not null,
  dic_implementer_id   INT8                 not null,
  constraint PK_C_GENERAL_INFO primary key (id)
);

comment on table c_general_info is
'Контракты. Основная информация';

comment on column c_general_info.general_tab is
'HTML-содержимое вкладки "Общее"';

comment on column c_general_info.objects_tab is
'HTML-содержимое вкладки "Предмет договора"';

comment on column c_general_info.customer_tab is
'HTML-содержимое вкладки "Заказчик и поставщик"';

comment on column c_general_info.treasury_tab is
'HTML-содержимое вкладки "Казначейство"';

comment on column c_general_info.agreement_tab is
'HTML-содержимое вкладки "Договор и согласование"';

comment on column c_general_info.dic_contract_type_id is
'Тип';

comment on column c_general_info.reestr_number is
'Номер договора в реестре договоров';

comment on column c_general_info.number is
'Номер договора';

comment on column c_general_info.announcement_doc_name is
'Наименование документа объявления о проведении государственных закупок';

comment on column c_general_info.announcement_number is
'Номер объявления о проведении государственных закупок';

comment on column c_general_info.announcement_date is
'Дата объявления о проведении государственных закупок';

comment on column c_general_info.conclusion_date is
'Дата заключения договора';

comment on column c_general_info.creation_time is
'Дата создания договора';

comment on column c_general_info.short_description_kk is
'Краткое содержание договора на казахском языке';

comment on column c_general_info.short_description_ru is
'Краткое соержание договора на русском языке';

comment on column c_general_info.dic_purchase_object_type_id is
'Вид предмета закупок';

comment on column c_general_info.dic_contract_conclusion_type_id is
'Форма заключения договора';

comment on column c_general_info.dic_contract_status_id is
'Статус договора';

comment on column c_general_info.dic_contract_purchase_type_id is
'Тип закупки';

comment on column c_general_info.financial_year is
'Финансовый год';

comment on column c_general_info.planed_dic_purchase_mode_id is
'Планируемый способ осуществления закупки';

comment on column c_general_info.real_dic_purchase_mode_id is
'Фактический способ осуществления закупки';

comment on column c_general_info.planed_sum is
'Общая плановая сумма договора';

comment on column c_general_info.result_contract_sum is
'Общая сумма договора по итогам закупки';

comment on column c_general_info.total_contract_sum is
'Общая итоговая сумма договора';

comment on column c_general_info.real_contract_sum is
'Общая фактическая сумма договора';

comment on column c_general_info.dic_currency_id is
'Валюта договора';

comment on column c_general_info.rate is
'Курс';

comment on column c_general_info.contract_time is
'Срок действия договора';

comment on column c_general_info.planned_execution_date is
'Планируемая дата исполнения';

comment on column c_general_info.real_execution_date is
'Фактическая дата исполнения';

comment on column c_general_info.execution_mark_date is
'Дата проставления отметки исполнения договора';

comment on column c_general_info.doc_name_ru is
'Реквизиты документа, подтверждающего основание заключения договора. Наименование документа на русском языке';

comment on column c_general_info.doc_name_kk is
'Реквизиты документа, подтверждающего основание заключения договора. Наименование документа на государственном языке';

comment on column c_general_info.doc_number is
'Реквизиты документа, подтверждающего основание заключения договора. Номер';

comment on column c_general_info.doc_date is
'Реквизиты документа, подтверждающего основание заключения договора. Дата';

/*==============================================================*/
/* Index: idx_c_general_info_portal_id                          */
/*==============================================================*/
create unique index idx_c_general_info_portal_id on c_general_info (
  portal_id
);

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_budjet_type foreign key (dic_contract_budjet_type_id)
references dic_contract_budjet_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_conclusion_type foreign key (dic_contract_conclusion_type_id)
references dic_contract_conclusion_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_purchase_type foreign key (dic_contract_purchase_type_id)
references dic_contract_purchase_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_status foreign key (dic_contract_status_id)
references dic_contract_status (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_type foreign key (dic_contract_type_id)
references dic_contract_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_currency foreign key (dic_currency_id)
references dic_currency (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_gov_organization foreign key (dic_gov_organization_id)
references dic_gov_organization (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_implementer foreign key (dic_implementer_id)
references dic_implementer (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_purchase_object_type foreign key (dic_purchase_object_type_id)
references dic_purchase_object_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_planned_c_general_info_dic_purchase_mode foreign key (planed_dic_purchase_mode_id)
references dic_purchase_mode (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_real_c_general_info_dic_purchase_mode foreign key (real_dic_purchase_mode_id)
references dic_purchase_mode (id)
on delete restrict on update restrict;

create sequence seq$c_general_info
increment 10;


/*==============================================================*/
/* Table: c_object                                              */
/*==============================================================*/
create table c_object (
  id                   INT8                 not null,
  portal_id            INT8                 not null,
  c_general_info_id    INT8                 not null,
  original_html        TEXT                 not null,
  lot_portal_id        INT8                 null,
  plan_id              INT8                 null,
  name_kk              VARCHAR(512)         not null,
  name_ru              VARCHAR(512)         not null,
  year                 INT2                 not null,
  dic_tru_id           INT8                 not null,
  dic_purchase_object_type_id INT8                 not null,
  short_description_kk VARCHAR(512)         not null,
  short_description_ru VARCHAR(512)         not null,
  additional_characteristic_kk VARCHAR(512)         not null,
  additional_characteristic_ru VARCHAR(512)         not null,
  price_without_vat    DECIMAL(13,2)        not null,
  price_with_vat       DECIMAL(13,2)        not null,
  value                DECIMAL(13,2)        not null,
  dic_unit_id          INT8                 not null,
  contract_sum_without_vat DECIMAL(13,2)        not null,
  contract_vat_sum     DECIMAL(13,2)        not null,
  contract_total_sum   DECIMAL(13,2)        not null,
  real_contract_sum    DECIMAL(13,2)        null,
  placement            VARCHAR(200)         not null,
  payment_source       VARCHAR(200)         not null,
  planned_buy_month    INT2                 not null
    constraint CKC_PLANNED_BUY_MONTH_C_OBJECT check (planned_buy_month between 1 and 12),
  planed_execution_date DATE                 not null,
  real_execution_date  DATE                 null,
  mark_execution_time  TIMESTAMP            null,
  dic_contract_object_purchase_type_id INT8                 not null,
  constraint PK_C_OBJECT primary key (id)
);

comment on table c_object is
'Контракт. Предметы договора';

comment on column c_object.lot_portal_id is
'ИД лота в закупке';

comment on column c_object.plan_id is
'ИД пункта плана';

comment on column c_object.name_kk is
'Наименование закупаемых товаров, работ или услуг (на государственном языке)';

comment on column c_object.name_ru is
'Наименование закупаемых товаров, работ или услуг (на русском языке)';

comment on column c_object.year is
'Год';

comment on column c_object.dic_tru_id is
'КТРУ';

comment on column c_object.dic_purchase_object_type_id is
'Вид предмета закупок';

comment on column c_object.short_description_kk is
'Краткая характеристика (на государственном языке)';

comment on column c_object.short_description_ru is
'Краткая характеристика (на русском языке)';

comment on column c_object.additional_characteristic_kk is
'Дополнительная характеристика (на государственном языке)';

comment on column c_object.additional_characteristic_ru is
'Дополнительная характеристика (на русском языке)';

comment on column c_object.value is
'Количество, объем';

comment on column c_object.dic_unit_id is
'Единица измерения';

comment on column c_object.contract_sum_without_vat is
'Сумма по предмету договора (без учета НДС)';

comment on column c_object.contract_vat_sum is
'Сумма НДС по предмету договора';

comment on column c_object.contract_total_sum is
'Сумма по предмету договора (с учетом НДС)';

comment on column c_object.real_contract_sum is
'Сумма предмета договора (лот) исполненная, фактическая';

comment on column c_object.placement is
'Место поставки, адрес';

comment on column c_object.payment_source is
'Источник финансирования';

comment on column c_object.planned_buy_month is
'Планируемый срок закупки (месяц)';

comment on column c_object.planed_execution_date is
'Планируемый срок исполнения лота';

comment on column c_object.real_execution_date is
'Фактический срок исполнения лота';

comment on column c_object.mark_execution_time is
'Дата установки признака исполнения лота';

/*==============================================================*/
/* Index: idx_c_object_portal_id                                */
/*==============================================================*/
create unique index idx_c_object_portal_id on c_object (
  portal_id
);

alter table c_object
  add constraint fk_c_object_c_general_info foreign key (c_general_info_id)
references c_general_info (id)
on delete cascade on update restrict;

alter table c_object
  add constraint fk_c_object_dic_contract_object_purchase_type foreign key (dic_contract_object_purchase_type_id)
references dic_contract_object_purchase_type (id)
on delete restrict on update restrict;

alter table c_object
  add constraint fk_c_object_dic_purchase_object_type foreign key (dic_purchase_object_type_id)
references dic_purchase_object_type (id)
on delete restrict on update restrict;

alter table c_object
  add constraint fk_c_object_dic_tru foreign key (dic_tru_id)
references dic_tru (id)
on delete restrict on update restrict;

alter table c_object
  add constraint fk_c_object_dic_unit foreign key (dic_unit_id)
references dic_unit (id)
on delete restrict on update restrict;

create sequence seq$c_object
increment 10;


/*==============================================================*/
/* Table: dic_contract_object_specific                          */
/*==============================================================*/
create table dic_contract_object_specific (
  id                   INT8                 not null,
  name_ru              CHAR(10)             not null,
  constraint PK_DIC_CONTRACT_OBJECT_SPECIFI primary key (id)
);

/*==============================================================*/
/* Index: idx_dic_contract_object_specifi                       */
/*==============================================================*/
create unique index idx_dic_contract_object_specifi on dic_contract_object_specific (
  name_ru
);

create sequence seq$dic_contract_object_specific
increment 10;


/*==============================================================*/
/* Table: c_payment_specific                                    */
/*==============================================================*/
create table c_payment_specific (
  id                   INT8                 not null,
  c_object_id          INT8                 not null,
  dic_contract_object_specific_id INT8                 not null,
  financial_year       INT2                 not null,
  sum                  DECIMAL(13,2)        not null,
  constraint PK_C_PAYMENT_SPECIFIC primary key (id)
);

comment on table c_payment_specific is
'Специфика на утвержденный финансовый год';

/*==============================================================*/
/* Index: idx_c_payment_specific                                */
/*==============================================================*/
create unique index idx_c_payment_specific on c_payment_specific (
  c_object_id,
  dic_contract_object_specific_id,
  financial_year
);

alter table c_payment_specific
  add constraint fk_c_payment_specific_c_object foreign key (c_object_id)
references c_object (id)
on delete cascade on update restrict;

alter table c_payment_specific
  add constraint fk_c_payment_specific_dic_contract_object_specific foreign key (dic_contract_object_specific_id)
references dic_contract_object_specific (id)
on delete restrict on update restrict;

create sequence seq$c_payment_specific
increment 10;
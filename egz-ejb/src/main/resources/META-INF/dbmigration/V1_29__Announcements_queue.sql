/*==============================================================*/
/* Table: t_announcement_load_queue                             */
/*==============================================================*/
create table t_announcement_load_queue (
  portal_id            INT8                 not null,
  dic_participant_company_id INT8                 not null,
  queue_time           TIMESTAMP            not null,
  constraint PK_T_ANNOUNCEMENT_LOAD_QUEUE primary key (portal_id, dic_participant_company_id)
);

comment on table t_announcement_load_queue is
'Очередь загружаемых объявлений';

comment on column t_announcement_load_queue.portal_id is
'Идентификатор объявления на портале';

comment on column t_announcement_load_queue.dic_participant_company_id is
'Под чьим аккаунтом должна быть произведена загрузка';

comment on column t_announcement_load_queue.queue_time is
'Время постановки в очередь';

alter table t_announcement_load_queue
  add constraint ал_t_announcement_load_queue_dic_participant_company foreign key (dic_participant_company_id)
references dic_participant_company (id)
on delete restrict on update restrict;

insert into system_properties (code, "value", description) VALUES
('browser.profile.directory', '/home/alexey/work/InesSoft/tender/materials/ffprofile', 'Каталог профиля браузера'),
('browser.binary', '/opt/firefox/firefox-bin', 'Путь к бинарному файлу браузера'),
('portal.password', 'mySuperPassword', 'Пароль пользователя на портале'),
('tenders.info.directory', '/home/alexey/work/InesSoft/tender/materials/tenders', 'Папка с файлами по тендерам');
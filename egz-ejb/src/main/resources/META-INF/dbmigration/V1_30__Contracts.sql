drop index idx_c_general_info_portal_id;

drop table c_general_info cascade;

/*==============================================================*/
/* Table: c_general_info                                        */
/*==============================================================*/
create table c_general_info (
  id                   INT8                 not null,
  portal_id            INT8                 not null,
  load_time            TIMESTAMP            not null,
  general_tab          TEXT                 not null,
  objects_tab          TEXT                 not null,
  customer_tab         TEXT                 not null,
  treasury_tab         TEXT                 not null,
  agreement_tab        TEXT                 not null,
  dic_contract_type_id INT8                 not null,
  reestr_number        VARCHAR(25)          not null,
  main_contract_reestr_number VARCHAR(25)          null,
  main_contract_portal_id INT8                 null,
  number               VARCHAR(10)          not null,
  announcement_doc_name VARCHAR(50)          null,
  announcement_number  VARCHAR(20)          null,
  announcement_date    DATE                 null,
  conclusion_date      DATE                 not null,
  last_changed_time    TIMESTAMP            not null,
  creation_time        TIMESTAMP            not null,
  short_description_kk VARCHAR(512)         not null,
  short_description_ru VARCHAR(512)         not null,
  dic_purchase_object_type_id INT8                 not null,
  dic_contract_conclusion_type_id INT8                 not null,
  dic_contract_status_id INT8                 not null,
  dic_contract_purchase_type_id INT8                 not null,
  financial_year       INT2                 not null,
  dic_contract_budjet_type_id INT8                 not null,
  financial_source     VARCHAR(20)          null,
  planed_dic_purchase_mode_id INT8                 not null,
  real_dic_purchase_mode_id INT8                 not null,
  planed_sum           DECIMAL(13,2)        not null,
  result_contract_sum  DECIMAL(13,2)        not null,
  total_contract_sum   DECIMAL(13,2)        not null,
  real_contract_sum    DECIMAL(13,2)        null,
  dic_currency_id      INT8                 not null,
  rate                 DECIMAL(13,2)        not null,
  contract_time        DATE                 not null,
  planned_execution_date DATE                 not null,
  real_execution_date  DATE                 null,
  execution_mark_date  DATE                 null,
  doc_name_ru          VARCHAR(512)         not null,
  doc_name_kk          VARCHAR(512)         not null,
  doc_number           VARCHAR(40)          null,
  doc_date             DATE                 null,
  dic_gov_organization_id INT8                 not null,
  customer_name_kk     VARCHAR(512)         not null,
  customer_name_ru     VARCHAR(512)         not null,
  customer_bin         VARCHAR(12)          not null,
  customer_rnn         VARCHAR(12)          null,
  dic_implementer_id   INT8                 not null,
  implementer_name_kk  VARCHAR(512)         not null,
  implementer_name_ru  VARCHAR(512)         not null,
  implementer_bin      VARCHAR(12)          null,
  implementer_iin      VARCHAR(12)          null,
  implementer_rnn      VARCHAR(12)          null,
  implementer_vat_info VARCHAR(512)         not null,
  constraint PK_C_GENERAL_INFO primary key (id)
);

comment on table c_general_info is
'Контракты. Основная информация';

comment on column c_general_info.general_tab is
'HTML-содержимое вкладки "Общее"';

comment on column c_general_info.objects_tab is
'HTML-содержимое вкладки "Предмет договора"';

comment on column c_general_info.customer_tab is
'HTML-содержимое вкладки "Заказчик и поставщик"';

comment on column c_general_info.treasury_tab is
'HTML-содержимое вкладки "Казначейство"';

comment on column c_general_info.agreement_tab is
'HTML-содержимое вкладки "Договор и согласование"';

comment on column c_general_info.dic_contract_type_id is
'Тип';

comment on column c_general_info.reestr_number is
'Номер договора в реестре договоров';

comment on column c_general_info.main_contract_reestr_number is
'Номер основного договора в реестре договоров';

comment on column c_general_info.number is
'Номер договора';

comment on column c_general_info.announcement_doc_name is
'Наименование документа объявления о проведении государственных закупок';

comment on column c_general_info.announcement_number is
'Номер объявления о проведении государственных закупок';

comment on column c_general_info.announcement_date is
'Дата объявления о проведении государственных закупок';

comment on column c_general_info.conclusion_date is
'Дата заключения договора';

comment on column c_general_info.last_changed_time is
'Дата последнего изменения';

comment on column c_general_info.creation_time is
'Дата создания договора';

comment on column c_general_info.short_description_kk is
'Краткое содержание договора на казахском языке';

comment on column c_general_info.short_description_ru is
'Краткое соержание договора на русском языке';

comment on column c_general_info.dic_purchase_object_type_id is
'Вид предмета закупок';

comment on column c_general_info.dic_contract_conclusion_type_id is
'Форма заключения договора';

comment on column c_general_info.dic_contract_status_id is
'Статус договора';

comment on column c_general_info.dic_contract_purchase_type_id is
'Тип закупки';

comment on column c_general_info.financial_year is
'Финансовый год';

comment on column c_general_info.financial_source is
'Источник финансирования';

comment on column c_general_info.planed_dic_purchase_mode_id is
'Планируемый способ осуществления закупки';

comment on column c_general_info.real_dic_purchase_mode_id is
'Фактический способ осуществления закупки';

comment on column c_general_info.planed_sum is
'Общая плановая сумма договора';

comment on column c_general_info.result_contract_sum is
'Общая сумма договора по итогам закупки';

comment on column c_general_info.total_contract_sum is
'Общая итоговая сумма договора';

comment on column c_general_info.real_contract_sum is
'Общая фактическая сумма договора';

comment on column c_general_info.dic_currency_id is
'Валюта договора';

comment on column c_general_info.rate is
'Курс';

comment on column c_general_info.contract_time is
'Срок действия договора';

comment on column c_general_info.planned_execution_date is
'Планируемая дата исполнения';

comment on column c_general_info.real_execution_date is
'Фактическая дата исполнения';

comment on column c_general_info.execution_mark_date is
'Дата проставления отметки исполнения договора';

comment on column c_general_info.doc_name_ru is
'Реквизиты документа, подтверждающего основание заключения договора. Наименование документа на русском языке';

comment on column c_general_info.doc_name_kk is
'Реквизиты документа, подтверждающего основание заключения договора. Наименование документа на государственном языке';

comment on column c_general_info.doc_number is
'Реквизиты документа, подтверждающего основание заключения договора. Номер';

comment on column c_general_info.doc_date is
'Реквизиты документа, подтверждающего основание заключения договора. Дата';

comment on column c_general_info.customer_name_kk is
'Заказчик
Наименование заказчика (на государственном языке)';

comment on column c_general_info.customer_name_ru is
'Заказчик
Наименование заказчика (на русском языке)';

comment on column c_general_info.customer_bin is
'Заказчик
БИН';

comment on column c_general_info.customer_rnn is
'Заказчик
РНН';

comment on column c_general_info.implementer_name_kk is
'Поставщик
Наименование поставщика (на государственном языке)';

comment on column c_general_info.implementer_name_ru is
'Поставщик
Наименование поставщика (на русском языке)';

comment on column c_general_info.implementer_bin is
'Поставщик
БИН';

comment on column c_general_info.implementer_iin is
'Поставщик
ИИН';

comment on column c_general_info.implementer_rnn is
'Поставщик
РНН';

comment on column c_general_info.implementer_vat_info is
'Поставщик
Плательщик НДС';

/*==============================================================*/
/* Index: idx_c_general_info_portal_id                          */
/*==============================================================*/
create unique index idx_c_general_info_portal_id on c_general_info (
  portal_id
);

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_budjet_type foreign key (dic_contract_budjet_type_id)
references dic_contract_budjet_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_conclusion_type foreign key (dic_contract_conclusion_type_id)
references dic_contract_conclusion_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_purchase_type foreign key (dic_contract_purchase_type_id)
references dic_contract_purchase_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_status foreign key (dic_contract_status_id)
references dic_contract_status (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_contract_type foreign key (dic_contract_type_id)
references dic_contract_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_currency foreign key (dic_currency_id)
references dic_currency (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_gov_organization foreign key (dic_gov_organization_id)
references dic_gov_organization (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_implementer foreign key (dic_implementer_id)
references dic_implementer (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_c_general_info_dic_purchase_object_type foreign key (dic_purchase_object_type_id)
references dic_purchase_object_type (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_planned_c_general_info_dic_purchase_mode foreign key (planed_dic_purchase_mode_id)
references dic_purchase_mode (id)
on delete restrict on update restrict;

alter table c_general_info
  add constraint fk_real_c_general_info_dic_purchase_mode foreign key (real_dic_purchase_mode_id)
references dic_purchase_mode (id)
on delete restrict on update restrict;

alter table c_object
  add constraint fk_c_object_c_general_info foreign key (c_general_info_id)
references c_general_info (id)
on delete cascade on update restrict;

/*==============================================================*/
/* Table: c_load_queue                                          */
/*==============================================================*/
create table c_load_queue (
  portal_id            INT8                 not null,
  dic_participant_company_id INT8                 not null,
  queue_time           TIMESTAMP            not null,
  constraint PK_C_LOAD_QUEUE primary key (portal_id)
);

comment on table c_load_queue is
'Очередь загрузки контрактов';

comment on column c_load_queue.portal_id is
'Идентификатор контракта на портале';

comment on column c_load_queue.dic_participant_company_id is
'Под чьим аккаунтом должна быть произведена загрузка';

comment on column c_load_queue.queue_time is
'Время постановки в очередь';

alter table c_load_queue
  add constraint fk_c_load_queue_dic_participant_company foreign key (dic_participant_company_id)
references dic_participant_company (id)
on delete restrict on update restrict;

INSERT INTO system_properties (code, value, description) VALUES ('contracts.last.scan.date', '', 'Дата последних просканированных контрактов');
INSERT INTO system_properties (code, value, description) VALUES ('contracts.last.date', '', 'Дата последних просканированных контрактов');

create sequence seq$dic_contract_object_purchase_type
increment 10;
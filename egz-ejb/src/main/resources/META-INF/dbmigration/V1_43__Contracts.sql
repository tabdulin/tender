/*==============================================================*/
/* Table: dic_interesting_tru                                   */
/*==============================================================*/
create table dic_interesting_tru (
  id                   INT8                 not null,
  code                 VARCHAR(10)          not null,
  constraint PK_DIC_INTERESTING_TRU primary key (id)
);

comment on table dic_interesting_tru is
'Справочник кодов ТРУ, которые нам интересны';

insert into dic_interesting_tru VALUES (1, '62');
insert into dic_interesting_tru VALUES (2, '63');
insert into dic_interesting_tru VALUES (3, '74.30');

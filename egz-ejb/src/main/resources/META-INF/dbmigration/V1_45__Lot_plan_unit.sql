ALTER TABLE t_lot
  ADD COLUMN plan_unit_id INT8;

COMMENT ON COLUMN t_lot.plan_unit_id IS 'Номер пункта плана';

UPDATE t_lot
SET plan_unit_id = (SELECT DISTINCT SUBSTRING(lot_number FROM 0 FOR position('-' IN lot_number)) :: INT8
                    FROM t_lot_cross_announcement
                    WHERE t_lot_id = t_lot.id);

ALTER TABLE t_lot
  ALTER plan_unit_id SET NOT NULL;

ALTER TABLE t_lot_cross_announcement
  ADD has_contract BOOL NOT NULL DEFAULT FALSE;

ALTER TABLE t_lot_cross_announcement
  ALTER has_contract DROP DEFAULT;

COMMENT ON COLUMN t_lot_cross_announcement.has_contract IS
'Признак наличия контракта в базе. Что бы не джойнить контракты с объявлениями';

ALTER TABLE t_lot_cross_announcement
  ADD dic_lot_status_id INT8 NOT NULL DEFAULT 0;

UPDATE t_lot_cross_announcement
SET dic_lot_status_id = (SELECT dic_lot_status_id
                         FROM t_lot
                         WHERE id = t_lot_cross_announcement.t_lot_id);

ALTER TABLE t_lot_cross_announcement
  ALTER dic_lot_status_id DROP DEFAULT;

COMMENT ON COLUMN t_lot_cross_announcement.dic_lot_status_id IS
'Статус лота';

ALTER TABLE t_lot_cross_announcement
  ADD CONSTRAINT fk_t_lot_cross_announcement_dic_lot_status FOREIGN KEY (dic_lot_status_id)
REFERENCES dic_lot_status (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE t_lot
  DROP dic_lot_status_id;

ALTER TABLE dic_lot_status
  ADD contract_status BOOL DEFAULT FALSE;

ALTER TABLE dic_lot_status
  ALTER contract_status DROP DEFAULT;

UPDATE dic_lot_status
SET contract_status = TRUE
WHERE name_ru = 'Закупка состоялась';

DELETE FROM system_properties
WHERE code IN ('contracts.last.scan.date', 'contracts.last.date');

ALTER TABLE t_protocol
  ADD COLUMN parsed BOOL NOT NULL DEFAULT FALSE;

ALTER TABLE t_protocol
  ALTER parsed DROP DEFAULT;

COMMENT ON COLUMN t_protocol.parsed IS
'Признак того, что протокол распарсен и уложен в БД';
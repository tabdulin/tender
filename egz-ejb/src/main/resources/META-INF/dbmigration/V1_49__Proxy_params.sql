insert into system_properties (code, "value", description) VALUES
  ('proxy.type', 'SOCKS', 'Тип прокси-сервера. (null/NONE/HTTP/SOCKS)'),
  ('proxy.host', 'ru12.friproxy.eu', 'Хост прокси-сервера'),
  ('proxy.port', '1080', 'Порт прокси-сервера')

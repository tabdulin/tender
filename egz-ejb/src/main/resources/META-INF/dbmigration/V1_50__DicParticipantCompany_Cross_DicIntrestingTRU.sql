ALTER TABLE t_announcement_load_queue
  DROP CONSTRAINT ал_t_announcement_load_queue_dic_participant_company;

ALTER TABLE t_announcement_load_queue
  ADD CONSTRAINT fk_t_announcement_load_queue_dic_participant_company FOREIGN KEY (dic_participant_company_id)
REFERENCES dic_participant_company (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

/*==============================================================*/
/* Table: dic_participant_company_cross_dic_interesting_tru     */
/*==============================================================*/
CREATE TABLE dic_participant_company_cross_dic_interesting_tru (
  dic_participant_company_id INT8      NOT NULL,
  dic_interesting_tru_id     INT8      NOT NULL,
  update_date                TIMESTAMP NOT NULL,
  CONSTRAINT pk_dic_participant_company_cross_dic_interesting_tru PRIMARY KEY (dic_participant_company_id, dic_interesting_tru_id)
);

COMMENT ON TABLE dic_participant_company_cross_dic_interesting_tru IS
'Кросс-связка компании с кодом КТРУ, по которым производится поиск объявлений';

COMMENT ON COLUMN dic_participant_company_cross_dic_interesting_tru.update_date IS
'Дата последнего обновления по этому КТРУ.';

ALTER TABLE dic_participant_company_cross_dic_interesting_tru
  ADD CONSTRAINT fk_dic_participant_company_cross_dic_interesting_tru_dic_interesting_tru FOREIGN KEY (dic_interesting_tru_id)
REFERENCES dic_interesting_tru (id)
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE dic_participant_company_cross_dic_interesting_tru
  ADD CONSTRAINT fk_dic_participant_company_cross_dic_interesting_tru_dic_participant_company FOREIGN KEY (dic_participant_company_id)
REFERENCES dic_participant_company (id)
ON DELETE CASCADE ON UPDATE RESTRICT;

INSERT INTO dic_participant_company_cross_dic_interesting_tru (dic_participant_company_id, dic_interesting_tru_id, update_date)
  SELECT
    dic_participant_company.id,
    dic_interesting_tru.id,
    '01-10-2016 00:00:00'
  FROM dic_interesting_tru, dic_participant_company
  WHERE dic_participant_company.refresh_credential = TRUE;

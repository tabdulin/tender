ALTER TABLE c_object
  DROP CONSTRAINT CKC_PLANNED_BUY_MONTH_C_OBJECT;

ALTER TABLE c_object
  ALTER COLUMN planned_buy_month TYPE VARCHAR(11);

UPDATE c_object
SET planned_buy_month = 'January'
WHERE planned_buy_month = '1';

UPDATE c_object
SET planned_buy_month = 'February'
WHERE planned_buy_month = '2';

UPDATE c_object
SET planned_buy_month = 'Mart'
WHERE planned_buy_month = '3';

UPDATE c_object
SET planned_buy_month = 'April'
WHERE planned_buy_month = '4';

UPDATE c_object
SET planned_buy_month = 'May'
WHERE planned_buy_month = '5';

UPDATE c_object
SET planned_buy_month = 'June'
WHERE planned_buy_month = '6';

UPDATE c_object
SET planned_buy_month = 'July'
WHERE planned_buy_month = '7';

UPDATE c_object
SET planned_buy_month = 'August'
WHERE planned_buy_month = '8';

UPDATE c_object
SET planned_buy_month = 'September'
WHERE planned_buy_month = '9';

UPDATE c_object
SET planned_buy_month = 'October'
WHERE planned_buy_month = '10';

UPDATE c_object
SET planned_buy_month = 'November'
WHERE planned_buy_month = '11';

UPDATE c_object
SET planned_buy_month = 'December'
WHERE planned_buy_month = '12';

ALTER TABLE c_object
  ADD CONSTRAINT CKC_PLANNED_BUY_MONTH_C_OBJECT CHECK (planned_buy_month IN
                                                       ('January', 'February', 'Mart', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'LastYear'));

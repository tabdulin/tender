/*==============================================================*/
/* Table: system_proxy                                          */
/*==============================================================*/
create table system_proxy (
  proxy_type           VARCHAR(5)           not null
    constraint CKC_PROXY_TYPE_SYSTEM_P check (proxy_type in ('HTTP','SOCKS')),
  proxy_host           VARCHAR(30)          not null,
  proxy_port           INT                  not null,
  constraint PK_SYSTEM_PROXY primary key (proxy_type, proxy_host, proxy_port)
);

comment on table system_proxy is
'Список прокси-серверов. Используется при работе через прокси в режиме RAND.
В этом случае для каждого последюущего запроса выбирается случайный прокси из этой таблицы.';

comment on column system_proxy.proxy_host is
'DNS-имя или IP-адрес прокси-сервера';

comment on column system_proxy.proxy_port is
'Номер TCP-порта прокси-сервера';

insert into system_proxy (proxy_type, proxy_host, proxy_port) VALUES
  ('SOCKS', 'ru12.friproxy.eu', '1080'),
  ('SOCKS', 'nl41.fri-gate0.org', '1080'),
  ('SOCKS', 'es31.friproxy0.eu', '1080'),
  ('SOCKS', 'us12.fri-gate0.org', '1080'),
  ('SOCKS', 'fr11.friproxy.biz', '1080'),
  ('SOCKS', 'ru11.friproxy0.biz', '1080'),
  ('HTTP', '46.165.34.201', '3128'),
  ('HTTP', '124.88.67.24', '80');

update system_properties set "value"='RAND',
  description = 'Тип прокси-сервера. (null/NONE/HTTP/SOCKS/RAND)'
  where code='proxy.type'
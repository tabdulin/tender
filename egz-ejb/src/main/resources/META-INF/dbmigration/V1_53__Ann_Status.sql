ALTER TABLE dic_announcement_status
  ADD COLUMN not_update_until_start_recv BOOL DEFAULT FALSE;

ALTER TABLE dic_announcement_status
  ALTER COLUMN not_update_until_start_recv SET NOT NULL;

ALTER TABLE dic_announcement_status
  ADD COLUMN not_update_until_end_recv BOOL DEFAULT FALSE;

ALTER TABLE dic_announcement_status
  ALTER COLUMN not_update_until_end_recv SET NOT NULL;

COMMENT ON COLUMN dic_announcement_status.final_status IS
'Признак того, что статус финальный. Если true, то заявки с этим статусом больше не обновляются с портала.';

COMMENT ON COLUMN dic_announcement_status.not_update_until_start_recv IS
'Если true, то заявки с этими статусами не будут обновляться с портала, пока не наступит дата принятия заявок.
Например статус "Опубликовано" - он будет таким пока не начнется прием. Зачем обновлять его каждый день?';

COMMENT ON COLUMN dic_announcement_status.not_update_until_end_recv IS
'Если true, то заявки с этими статусами не будут обновляться с портала, пока не наступит дата окончания принятия заявок.
Например статус "Опубликовано (прием заявок)" - он будет таким пока не закончится прием. Зачем обновлять его каждый день?';

UPDATE dic_announcement_status
SET not_update_until_start_recv = TRUE
WHERE name_ru = 'Опубликовано';

UPDATE dic_announcement_status
SET not_update_until_end_recv = TRUE
WHERE name_ru IN ('Опубликовано (прием заявок)', 'Опубликовано (прием ценовых предложений)');

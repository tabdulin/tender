INSERT INTO system_properties (code, "value", description) VALUES
  ('jira.url', 'https://inessoft.atlassian.net', 'URL-адрес JIRA'),
  ('jira.login', 'tenderrobot', 'Имя пользователя для подключения к JIRA'),
  ('jira.password', 'Ntylthflkzbc12!', 'Пароль для подключения к JIRA'),
  ('jira.project', 'TNDR', 'Проект в JIRA'),
  ('jira.issueTypeId', '3', 'ID типа записи (Task = 3)');

ALTER TABLE t_announcement
  ADD COLUMN jira_id INT8 NULL;

ALTER TABLE t_announcement
  ADD COLUMN jira_key VARCHAR(10) NULL;

ALTER TABLE dic_tender_documentation
add column upload_to_jira       BOOL                 not null DEFAULT false;

alter TABLE dic_tender_documentation
ALTER COLUMN upload_to_jira       DROP DEFAULT;

update dic_tender_documentation set upload_to_jira = true where name_ru in ('Приложение 2 (Техническая спецификация)', 'Техническая спецификация');
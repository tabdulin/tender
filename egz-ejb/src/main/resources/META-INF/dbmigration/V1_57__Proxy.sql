ALTER TABLE system_proxy
  ADD COLUMN available BOOL NOT NULL DEFAULT TRUE;

ALTER TABLE system_proxy
  ALTER COLUMN available DROP DEFAULT;

ALTER TABLE system_proxy
  ADD COLUMN unavailable_cause VARCHAR(30) NULL;

ALTER TABLE system_proxy
  ADD COLUMN unavailable_time TIMESTAMP NULL;
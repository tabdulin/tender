ALTER TABLE t_announcement
  ADD COLUMN dic_participant_company_id INT8 NULL,
  ADD COLUMN general_tab TEXT,
  ADD COLUMN lots_tab TEXT,
  ADD COLUMN documentation_tab TEXT,
  ADD COLUMN discussions_tab TEXT NULL,
  ADD COLUMN proposals_info TEXT NULL,
  ADD COLUMN protocols_tab TEXT;

UPDATE t_announcement
SET
  dic_participant_company_id = t_annoucement_html.dic_participant_company_id,
  general_tab                = t_annoucement_html.general_tab,
  lots_tab                   = t_annoucement_html.lots_tab,
  documentation_tab          = t_annoucement_html.documentation_tab,
  discussions_tab            = t_annoucement_html.discussions_tab,
  proposals_info             = t_annoucement_html.proposals_info,
  protocols_tab              = t_annoucement_html.protocols_tab
FROM t_annoucement_html
WHERE t_announcement.id = t_annoucement_html.t_announcement_id;

ALTER TABLE t_announcement
  ALTER COLUMN general_tab SET NOT NULL,
  ALTER COLUMN lots_tab SET NOT NULL,
  ALTER COLUMN documentation_tab SET NOT NULL,
  ALTER COLUMN protocols_tab SET NOT NULL,
  ALTER COLUMN dic_participant_company_id SET NOT NULL;

ALTER TABLE t_announcement
  ADD CONSTRAINT fk_t_announcement_dic_participant_company FOREIGN KEY (dic_participant_company_id)
REFERENCES dic_participant_company (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

DROP TABLE t_annoucement_html;

ALTER TABLE dic_participant_company
  ADD COLUMN last_notice_time TIMESTAMP NOT NULL DEFAULT now(),
  ADD COLUMN telegram_user_id VARCHAR(20) NOT NULL DEFAULT 'user',
  ADD COLUMN telegram_token VARCHAR(50) NOT NULL DEFAULT 'token',
  ADD COLUMN telegram_password VARCHAR(20) NOT NULL DEFAULT 'password',
  ADD COLUMN load_announcement_priority INT8 NOT NULL DEFAULT 1;

UPDATE dic_participant_company
SET last_notice_time         = (SELECT to_timestamp("value", 'DD.MM.YYYY HH24:MI:SS')
                                FROM system_properties
                                WHERE code = 'portal.notifications.lastTime'),
  telegram_user_id           = (SELECT "value"
                                FROM system_properties
                                WHERE code = 'telegram.userId'),
  telegram_token             = (SELECT "value"
                                FROM system_properties
                                WHERE code = 'telegram.token'),
  telegram_password          = (SELECT "value"
                                FROM system_properties
                                WHERE code = 'telegram.password'),
  load_announcement_priority = id
WHERE refresh_credential = TRUE;

UPDATE dic_participant_company
SET load_announcement_priority = id;

DELETE FROM system_properties
WHERE code IN ('portal.notifications.lastTime', 'telegram.userId', 'telegram.token', 'telegram.password');

ALTER TABLE dic_participant_company
  ALTER COLUMN last_notice_time DROP DEFAULT,
  ALTER COLUMN telegram_user_id DROP DEFAULT,
  ALTER COLUMN telegram_token DROP DEFAULT,
  ALTER COLUMN telegram_password DROP DEFAULT,
  ALTER COLUMN load_announcement_priority DROP DEFAULT;

COMMENT ON COLUMN dic_participant_company.last_notice_time IS
'Дата и время последнего загруженного уведомления с портала';

COMMENT ON COLUMN dic_participant_company.telegram_user_id IS
'Имя Telegram-пользователя для авторизации бота';

COMMENT ON COLUMN dic_participant_company.telegram_token IS
'Токет Telegram-пользователя для авторизации бота';

COMMENT ON COLUMN dic_participant_company.telegram_password IS
'Пароль, ожидаемый ботом для начала вещания в конференции';

COMMENT ON COLUMN dic_participant_company.load_announcement_priority IS
'Приоритет отбора организации для загрузки объявления. Если в объявлении участвует сразу несколько организаций, то загрузка будет произведена от того, у кого выше приоритет.';


ALTER TABLE t_announcement_load_queue
  DROP COLUMN dic_participant_company_id;

ALTER TABLE dic_interesting_tru
  ADD COLUMN update_date TIMESTAMP;

COMMENT ON COLUMN dic_interesting_tru.update_date IS
'Дата последнего обновления по этому КТРУ.';

UPDATE dic_interesting_tru
SET update_date =
(SELECT update_date
 FROM dic_participant_company_cross_dic_interesting_tru
 WHERE dic_participant_company_cross_dic_interesting_tru.dic_interesting_tru_id = dic_interesting_tru.id);

ALTER TABLE dic_interesting_tru
  ALTER COLUMN update_date SET NOT NULL;

DROP TABLE dic_participant_company_cross_dic_interesting_tru;


ALTER TABLE dic_telegram_chat
  DROP CONSTRAINT PK_DIC_TELEGRAM_CHAT;

ALTER TABLE dic_telegram_chat
  RENAME id TO chat_id;

ALTER TABLE dic_telegram_chat
  ADD COLUMN dic_participant_company_id INT8;

UPDATE dic_telegram_chat
SET dic_participant_company_id = (SELECT id
                                  FROM dic_participant_company
                                  WHERE refresh_credential = TRUE);

ALTER TABLE dic_telegram_chat
  ALTER COLUMN dic_participant_company_id SET NOT NULL;

ALTER TABLE dic_telegram_chat
  ADD CONSTRAINT PK_DIC_TELEGRAM_CHAT PRIMARY KEY (chat_id, dic_participant_company_id);

CREATE INDEX idx_dic_telegram_chat_dic_parti ON dic_telegram_chat (
  dic_participant_company_id
);

ALTER TABLE dic_telegram_chat
  ADD CONSTRAINT fk_dic_telegram_chat_dic_participant_company FOREIGN KEY (dic_participant_company_id)
REFERENCES dic_participant_company (id)
ON DELETE CASCADE ON UPDATE RESTRICT;


ALTER TABLE dic_participant_company
  RENAME refresh_credential TO default_refresh_credential;

CREATE SEQUENCE seq$t_my_proposals
INCREMENT 10;


CREATE TABLE t_my_proposal (
  id                         INT8 NOT NULL,
  dic_participant_company_id INT8 NOT NULL,
  dic_proposal_status_id     INT8 NOT NULL,
  portal_id                  INT8 NOT NULL,
  proposal_number            INT8 NOT NULL,
  announcement_portal_id     INT8 NOT NULL,
  html                       TEXT NOT NULL,
  CONSTRAINT PK_T_MY_PROPOSAL PRIMARY KEY (id)
);

COMMENT ON TABLE t_my_proposal IS
'Информация по заявкам с портала по каждой из организаций';

CREATE INDEX idx_t_my_proposal_announcement_ ON t_my_proposal (
  announcement_portal_id
);

ALTER TABLE t_my_proposal
  ADD CONSTRAINT fk_t_my_proposal_dic_participant_company FOREIGN KEY (dic_participant_company_id)
REFERENCES dic_participant_company (id)
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE t_my_proposal
  ADD CONSTRAINT fk_t_my_proposal_dic_proposal_status FOREIGN KEY (dic_proposal_status_id)
REFERENCES dic_proposal_status (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE dic_proposal_status
  ADD COLUMN skip_on_announcement_load BOOL DEFAULT FALSE;

ALTER TABLE dic_proposal_status
  ALTER COLUMN skip_on_announcement_load DROP DEFAULT;

COMMENT ON COLUMN dic_proposal_status.skip_on_announcement_load IS
'Если true, то заявки с такими статусами не должны быть основанием для выбора этого аккаунта для загрузки объявления. Например статус "Проект" - если заявка имеет этот статус, то при загрузке объявления не будет информации о других поданных заявках, стало быть такие статусы надо исключить.';


ALTER TABLE t_portal_recieved_file
  DROP CONSTRAINT ал_t_portal_recieved_file_t_portal_file;

ALTER TABLE t_portal_recieved_file
  ADD CONSTRAINT fk_t_portal_recieved_file_t_portal_file FOREIGN KEY (id)
REFERENCES t_portal_file (id)
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE t_portal_uploaded_file
  DROP CONSTRAINT ал_t_portal_uploaded_file_t_portal_file;

ALTER TABLE t_portal_uploaded_file
  ADD CONSTRAINT fk_t_portal_uploaded_file_t_portal_file FOREIGN KEY (id)
REFERENCES t_portal_file (id)
ON DELETE CASCADE ON UPDATE RESTRICT;

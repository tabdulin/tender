alter table t_implementer_lot alter COLUMN access_granted DROP NOT NULL;
update t_implementer_lot set access_granted = null where access_granted=false and access_denied_cause is null;
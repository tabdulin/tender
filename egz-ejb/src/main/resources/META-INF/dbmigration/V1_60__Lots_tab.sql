/*==============================================================*/
/* Table: t_announcement_lots_tab_html                          */
/*==============================================================*/
CREATE TABLE t_announcement_lots_tab_html (
  id                INT8 NOT NULL,
  t_announcement_id INT8 NOT NULL,
  page_number       INT2 NOT NULL,
  html              TEXT NOT NULL,
  CONSTRAINT PK_T_ANNOUNCEMENT_LOTS_TAB_HTM PRIMARY KEY (id)
);

COMMENT ON TABLE t_announcement_lots_tab_html IS
'HTML-содержимое вкладки Лоты';

ALTER TABLE t_announcement_lots_tab_html
  ADD CONSTRAINT fk_t_announcement_lots_tab_html_t_announcement FOREIGN KEY (t_announcement_id)
REFERENCES t_announcement (id)
ON DELETE CASCADE ON UPDATE RESTRICT;

CREATE SEQUENCE seq$t_announcement_lots_tab_html;

INSERT INTO t_announcement_lots_tab_html (id, t_announcement_id, page_number, html)
  SELECT
    nextval('seq$t_announcement_lots_tab_html'),
    id,
    1,
    lots_tab
  FROM t_announcement;

CREATE UNIQUE INDEX idx_t_announcement_lots_tab_htm ON t_announcement_lots_tab_html (
  t_announcement_id,
  page_number
);

ALTER TABLE t_announcement
  DROP lots_tab;

SELECT setval('seq$t_announcement_lots_tab_html', max(id) + 1)
FROM t_announcement_lots_tab_html;

ALTER SEQUENCE seq$t_announcement_lots_tab_html INCREMENT 10;
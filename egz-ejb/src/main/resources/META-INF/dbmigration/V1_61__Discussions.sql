CREATE TABLE dic_discussion_request_type (
  id      INT8        NOT NULL,
  name_ru VARCHAR(50) NOT NULL,
  CONSTRAINT PK_DIC_DISCUSSION_REQUEST_TYPE PRIMARY KEY (id)
);

COMMENT ON TABLE dic_discussion_request_type IS
'Справочник типов сообщений при обсуждении документации по конкурсу';

CREATE UNIQUE INDEX idx_dic_discussion_request_type ON dic_discussion_request_type (
  name_ru
);

CREATE SEQUENCE seq$dic_discussion_request_type
INCREMENT 10;

INSERT INTO dic_discussion_request_type (id, name_ru)
  SELECT
    nextval('seq$dic_discussion_request_type'),
    'Замечание к КД';

/*==============================================================*/
/* Table: dic_discussion_response_type                          */
/*==============================================================*/
CREATE TABLE dic_discussion_response_type (
  id      INT8         NOT NULL,
  name_ru VARCHAR(120) NOT NULL,
  CONSTRAINT PK_DIC_DISCUSSION_RESPONSE_TYP PRIMARY KEY (id)
);

COMMENT ON TABLE dic_discussion_response_type IS
'Справочник типов ответов в обсуждениях';

/*==============================================================*/
/* Index: idx_dic_discussion_response_typ                       */
/*==============================================================*/
CREATE UNIQUE INDEX idx_dic_discussion_response_typ ON dic_discussion_response_type (
  name_ru
);

CREATE SEQUENCE seq$dic_discussion_response_type
INCREMENT 10;

/*==============================================================*/
/* Table: t_discussion                                          */
/*==============================================================*/
ALTER TABLE t_discussion
  ADD COLUMN request_subject TEXT NOT NULL DEFAULT 'Not parces yet',
  ADD COLUMN dic_discussion_request_type_id INT8 NOT NULL DEFAULT 1,
  ADD COLUMN request_implementer VARCHAR(512) NOT NULL  DEFAULT 'Not parces yet',
  ADD COLUMN request_author VARCHAR(150) NOT NULL DEFAULT 'Not parces yet',
  ADD COLUMN request_time TIMESTAMP NOT NULL DEFAULT now(),
  ADD COLUMN request_text TEXT NOT NULL  DEFAULT 'Not parces yet',
  ADD COLUMN response_time TIMESTAMP,
  ADD COLUMN response_author VARCHAR(150),
  ADD COLUMN dic_discussion_response_type_id INT8,
  ADD COLUMN response_description TEXT,
  ADD COLUMN response_reject_cause TEXT,
  ADD COLUMN response_explanation TEXT;

COMMENT ON COLUMN t_discussion.request_subject IS
'Тема сообщения';

COMMENT ON COLUMN t_discussion.dic_discussion_request_type_id IS
'Тип сообщения';

COMMENT ON COLUMN t_discussion.request_implementer IS
'Поставщик';

COMMENT ON COLUMN t_discussion.request_author IS
'Представитель поставщика';

COMMENT ON COLUMN t_discussion.request_time IS
'Дата и время отправки сообщения';

COMMENT ON COLUMN t_discussion.request_text IS
'Текст сообщения';

COMMENT ON COLUMN t_discussion.response_time IS
'Дата и время ответа';

COMMENT ON COLUMN t_discussion.response_author IS
'Автор ответа';

COMMENT ON COLUMN t_discussion.dic_discussion_response_type_id IS
'Решение по вопросу';

COMMENT ON COLUMN t_discussion.response_description IS
'Описание внесения изменения';

comment on column t_discussion.response_reject_cause is
'Причина отклонения';

comment on column t_discussion.response_explanation is
'Текст разъяснения';

ALTER TABLE t_discussion
  ADD CONSTRAINT fk_t_discussion_dic_discussion_request_type FOREIGN KEY (dic_discussion_request_type_id)
REFERENCES dic_discussion_request_type (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE t_discussion
  ADD CONSTRAINT fk_t_discussion_dic_discussion_response_type FOREIGN KEY (dic_discussion_response_type_id)
REFERENCES dic_discussion_response_type (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE t_discussion
  ALTER COLUMN request_subject DROP DEFAULT,
  ALTER COLUMN dic_discussion_request_type_id DROP DEFAULT,
  ALTER COLUMN request_implementer DROP DEFAULT,
  ALTER COLUMN request_author DROP DEFAULT,
  ALTER COLUMN request_time DROP DEFAULT,
  ALTER COLUMN request_text DROP DEFAULT;

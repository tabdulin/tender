CREATE TABLE tmp_fix_lots (
  cnt                    INT2,
  lotnumber              VARCHAR(20),
  max_lot_portal_id      INT8,
  announcement_id        INT8,
  announcement_portal_id INT8
);

INSERT INTO tmp_fix_lots (cnt, lotnumber, max_lot_portal_id, announcement_id, announcement_portal_id)
  SELECT
    COUNT(*),
    egz.t_lot_cross_announcement.lot_number,
    MAX(egz.t_lot.portal_id),
    egz.t_lot_cross_announcement.t_announcement_id,
    egz.t_announcement.portal_id
  FROM
    egz.t_lot_cross_announcement
    INNER JOIN
    egz.t_lot
      ON
        (
          egz.t_lot_cross_announcement.t_lot_id = egz.t_lot.id)
    INNER JOIN
    egz.t_announcement
      ON
        (
          egz.t_lot_cross_announcement.t_announcement_id = egz.t_announcement.id)
  GROUP BY
    egz.t_lot_cross_announcement.lot_number,
    egz.t_lot_cross_announcement.t_announcement_id,
    egz.t_announcement.portal_id
  HAVING
    COUNT(*) > 1;

DELETE FROM t_lot_cross_announcement AS tlca
WHERE EXISTS(SELECT *
             FROM tmp_fix_lots
             WHERE announcement_id = tlca.t_announcement_id AND lotnumber = tlca.lot_number)
      AND t_lot_id IN (SELECT id
                       FROM t_lot
                       WHERE portal_id NOT IN (SELECT max_lot_portal_id
                                               FROM tmp_fix_lots));

CREATE UNIQUE INDEX idx_t_lot_cross_announcement_an ON t_lot_cross_announcement (
  t_announcement_id,
  lot_number
);
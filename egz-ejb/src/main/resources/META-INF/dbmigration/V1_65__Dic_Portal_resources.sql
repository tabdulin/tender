ALTER TABLE dic_portal_resource
  ADD COLUMN id INT8;

CREATE SEQUENCE seq$dic_portal_resource;

UPDATE dic_portal_resource
SET id = nextval('seq$dic_portal_resource');

ALTER SEQUENCE seq$dic_portal_resource INCREMENT 10;

ALTER TABLE dic_portal_resource
  ALTER id SET NOT NULL;

ALTER TABLE dic_portal_resource
  DROP CONSTRAINT pk_dic_portal_resources;

ALTER TABLE dic_portal_resource
  ADD CONSTRAINT pk_dic_portal_resources PRIMARY KEY (id);

/*==============================================================*/
/* Table: dic_portal_resource_mask                              */
/*==============================================================*/
CREATE TABLE dic_portal_resource_mask (
  html_source_text       VARCHAR(512) NOT NULL,
  dic_portal_resource_id INT8         NULL,
  CONSTRAINT PK_DIC_PORTAL_RESOURCE_MASK PRIMARY KEY (html_source_text)
);

ALTER TABLE dic_portal_resource_mask
  ADD CONSTRAINT fk_dic_portal_resource_mask_dic_portal_resource FOREIGN KEY (dic_portal_resource_id)
REFERENCES dic_portal_resource (id)
ON DELETE CASCADE ON UPDATE RESTRICT;

INSERT INTO dic_portal_resource_mask (html_source_text, dic_portal_resource_id)
  SELECT
    html_source_text,
    id
  FROM dic_portal_resource;

ALTER TABLE dic_portal_resource
  DROP COLUMN html_source_text;
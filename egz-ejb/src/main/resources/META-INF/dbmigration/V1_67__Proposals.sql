CREATE TABLE pr_info (
  id                         INT8         NOT NULL,
  dic_participant_company_id INT8         NOT NULL,
  t_announcement_id          INT8         NOT NULL,
  create_date                TIMESTAMP    NOT NULL,
  portal_id                  INT8         NULL,
  finished_flag              BOOL         NOT NULL,
  current_step_description   VARCHAR(200) NULL,
  error_message              VARCHAR(100) NULL,
  proposal_data              OID          NULL,
  CONSTRAINT PK_PR_INFO PRIMARY KEY (id)
);

COMMENT ON TABLE pr_info IS
'Основная информация о заявке';

COMMENT ON COLUMN pr_info.t_announcement_id IS
'Ссылка на объявление, в котором участвуем';

COMMENT ON COLUMN pr_info.proposal_data IS
'ZIP-архив с данными по заявке. В дальнейшем это поле должно будет удалиться и все перейти на профили.';

ALTER TABLE pr_info
  ADD CONSTRAINT fk_pr_info_dic_participant_company FOREIGN KEY (dic_participant_company_id)
REFERENCES dic_participant_company (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE pr_info
  ADD CONSTRAINT fk_pr_info_t_announcement FOREIGN KEY (t_announcement_id)
REFERENCES t_announcement (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE SEQUENCE seq$pr_info
INCREMENT 10;
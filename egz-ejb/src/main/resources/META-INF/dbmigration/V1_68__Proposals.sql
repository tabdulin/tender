ALTER TABLE t_proposal_document
  ADD COLUMN t_lot_id INT8;

UPDATE t_proposal_document
SET t_lot_id = (SELECT min(t_lot_id)
                FROM t_lot_cross_t_proposal AS lcp
                WHERE lcp.t_announcement_id = t_proposal_document.t_announcement_id AND
                      lcp.dic_implementer_id = t_proposal_document.dic_implementer_id);

ALTER TABLE t_proposal_document
  ALTER COLUMN t_lot_id SET NOT NULL;

ALTER TABLE t_proposal_document
  DROP CONSTRAINT fk_t_proposal_document_t_proposal;

ALTER TABLE t_proposal_document
  ADD CONSTRAINT fk_t_lot_cross_t_proposal_t_lot_cross_t_proposal FOREIGN KEY (t_announcement_id, t_lot_id, dic_implementer_id)
REFERENCES t_lot_cross_t_proposal (t_announcement_id, t_lot_id, dic_implementer_id)
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE t_lot_cross_t_proposal
  ADD COLUMN documents_html TEXT NOT NULL DEFAULT 'Необходимо скопировать и обработать из t_proposal.docs_tab_info_html';

ALTER TABLE t_lot_cross_t_proposal
  ALTER COLUMN documents_html DROP DEFAULT;
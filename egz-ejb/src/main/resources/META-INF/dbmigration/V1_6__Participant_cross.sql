/*==============================================================*/
/* Table: dic_participant_company_cross_template                */
/*==============================================================*/
create table dic_participant_company_cross_template (
  dic_participant_company_id INT8                 not null,
  dic_participation_template_id INT8                 not null,
  constraint PK_DIC_PARTICIPANT_COMPANY_CRO primary key (dic_participant_company_id, dic_participation_template_id)
);

alter table dic_participant_company_cross_template
  add constraint fk_dic_participant_cross_dic_participant_company foreign key (dic_participant_company_id)
references dic_participant_company (id)
on delete cascade on update restrict;

alter table dic_participant_company_cross_template
  add constraint fk_dic_participant_cross_dic_participation_template foreign key (dic_participation_template_id)
references dic_participation_template (id)
on delete cascade on update restrict;

insert into dic_participant_company(id, name_ru) VALUES
  (1, 'InesSoft'),
  (2, 'IServ');

insert into dic_participation_template(id, name_ru) VALUES
  (1, 'Полная заявка'),
  (2, 'Пустая заявка');

insert into dic_participant_company_cross_template(dic_participant_company_id, dic_participation_template_id) VALUES
  (1, 1),
  (2, 1),
  (2, 2);
ALTER TABLE system_properties
  ALTER COLUMN value TYPE TEXT;

INSERT INTO system_properties (code, "value", description) VALUES
  ('google.store.directory', 'googlestore', 'Каталог хранилища Google'),
  ('google.calendar.id', 'secret', 'Идентификатор календаря Google, куда вносятся события'),
  ('google.userId', 'secret', 'Идентификатор пользователя Google, для подключения к календарям'),
  ('google.oauth.credentials', 'secret', 'JSON-криденшиалы для авторизации в Google');
package kz.inessoft.egz.ejbapi;

import java.util.Date;

/**
 * Created by alexey on 26.10.16.
 */
public class AnnouncementFilter {
    public static enum ESortColumn {
        AnnouncementNumber, AnnouncementName, AnnouncementStatus, PublicationDate, StartRecvTime, EndRecvTime;
    }

    private String announcementNumber;
    private Long statusId;
    private String name;
    private Long customerId;
    private Long implementerId;
    private String tru;
    private Date startRecvDate;
    private Date endRecvDate;
    private Date startPublicationDate;
    private Date endPublicationDate;
    private boolean monitoring;
    private ESortColumn sortColumn;
    private boolean asc;

    public String getAnnouncementNumber() {
        return announcementNumber;
    }

    public void setAnnouncementNumber(String announcementNumber) {
        this.announcementNumber = normalizeString(announcementNumber);
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = normalizeString(name);
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getImplementerId() {
        return implementerId;
    }

    public void setImplementerId(Long implementerId) {
        this.implementerId = implementerId;
    }

    public String getTru() {
        return tru;
    }

    public void setTru(String tru) {
        this.tru = normalizeString(tru);
    }

    public Date getStartRecvDate() {
        return startRecvDate;
    }

    public void setStartRecvDate(Date startRecvDate) {
        this.startRecvDate = startRecvDate;
    }

    public Date getEndRecvDate() {
        return endRecvDate;
    }

    public void setEndRecvDate(Date endRecvDate) {
        this.endRecvDate = endRecvDate;
    }

    public Date getStartPublicationDate() {
        return startPublicationDate;
    }

    public void setStartPublicationDate(Date startPublicationDate) {
        this.startPublicationDate = startPublicationDate;
    }

    public Date getEndPublicationDate() {
        return endPublicationDate;
    }

    public void setEndPublicationDate(Date endPublicationDate) {
        this.endPublicationDate = endPublicationDate;
    }

    public boolean isMonitoring() {
        return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
        this.monitoring = monitoring;
    }

    public ESortColumn getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(ESortColumn sortColumn) {
        this.sortColumn = sortColumn;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    private String normalizeString(String val) {
        if (val != null) {
            val = val.trim();
            while (val.contains("  "))
                val = val.replaceAll("  ", " ");
            if (val.isEmpty())
                val = null;
        }
        return val;
    }
}

package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.TAnnouncement;

import javax.ejb.Local;
import javax.jms.JMSException;
import java.util.List;

/**
 * Created by alexey on 01.08.16.
 */
@Local
public interface AnnouncementLogic {
    void loadAnnouncement(long portalAnnouncementId) throws JMSException;

    /**
     * Загрузить все объявления, дата начала приема заявлений который не меньше чем указанная
     */
    void loadAllAnnouncements();

    TAnnouncement getAnnouncementByPortalId(long portalAnnouncementId, boolean initCollections);

    TAnnouncement getAnnouncementById(long announcementId, boolean initCollections);

    void setMonitoring(AnnouncementMonitoringInfo monitoringInfo) throws AnnouncementSaveException;

    long getAnnouncementsCount(AnnouncementFilter filter);

    List<TAnnouncement> findAnnouncements(AnnouncementFilter filter, int maxResults, int firstResult);

    double getAnnouncementAmount(Long announcementId);

    void refreshInterestingAnnouncements();
}

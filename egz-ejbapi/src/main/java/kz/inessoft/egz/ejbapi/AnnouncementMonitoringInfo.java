package kz.inessoft.egz.ejbapi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexey on 07.09.16.
 */
public class AnnouncementMonitoringInfo {
    public static class Participation {
        private long companyId;
        private long templateId;

        public Participation(long companyId, long templateId) {
            this.companyId = companyId;
            this.templateId = templateId;
        }

        public long getCompanyId() {
            return companyId;
        }

        public long getTemplateId() {
            return templateId;
        }
    }
    private Long annoucementId;
    private List<Participation> participations = new ArrayList<>();
    private boolean monitorIt;

    public List<Participation> getParticipations() {
        return participations;
    }

    public Long getAnnoucementId() {

        return annoucementId;
    }

    public void setAnnoucementId(Long annoucementId) {
        this.annoucementId = annoucementId;
    }

    public boolean isMonitorIt() {
        return monitorIt;
    }

    public void setMonitorIt(boolean monitorIt) {
        this.monitorIt = monitorIt;
    }
}

package kz.inessoft.egz.ejbapi;

import javax.ejb.Local;
import javax.jms.JMSException;

/**
 * Created by alexey on 15.08.16.
 */
@Local
public interface AnnouncementQueueInfo {
    boolean isAnnouncementLoading(long portalId);

    void queueAnnouncementLoading(long portalId) throws JMSException;

    void unregisterAnnouncementLoading(long portalId);
}

package kz.inessoft.egz.ejbapi;

/**
 * Created by alexey on 10.08.16.
 */
public class AnnouncementSaveException extends Exception {
    public AnnouncementSaveException(Throwable cause) {
        super(cause);
    }
}

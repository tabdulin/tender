package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.TAnnouncement;

import java.util.List;

/**
 * Created by alexey on 26.10.16.
 */
public class AnnouncementsData {
    private int count;
    private List<TAnnouncement> announcements;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<TAnnouncement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(List<TAnnouncement> announcements) {
        this.announcements = announcements;
    }
}

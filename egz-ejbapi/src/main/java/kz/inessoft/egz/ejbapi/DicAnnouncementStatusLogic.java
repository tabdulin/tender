package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Local
public interface DicAnnouncementStatusLogic extends ISimpleDictionaryDAO<DicAnnouncementStatus> {
    DicAnnouncementStatus getPublishedStatus();

    DicAnnouncementStatus getPublishedRecievingStatus();

    DicAnnouncementStatus getDocumentationChangedStatus();

    List<DicAnnouncementStatus> getAnnouncementStatuses();
}

package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicComissionRole;

import javax.ejb.Local;

/**
 * Created by alexey on 11.08.16.
 */
@Local
public interface DicComissionRoleLogic extends ISimpleDictionaryDAO<DicComissionRole> {
}

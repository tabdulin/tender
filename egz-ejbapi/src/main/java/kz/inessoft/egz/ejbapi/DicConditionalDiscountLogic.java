package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicConditionalDiscount;

import javax.ejb.Local;

/**
 * Created by alexey on 11.08.16.
 */
@Local
public interface DicConditionalDiscountLogic extends ISimpleDictionaryDAO<DicConditionalDiscount> {
}

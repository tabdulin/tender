package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicContractBudjetType;

import javax.ejb.Local;

/**
 * Created by alexey on 14.10.16.
 */
@Local
public interface DicContractBudjetTypeLogic extends ISimpleDictionaryDAO<DicContractBudjetType> {
}

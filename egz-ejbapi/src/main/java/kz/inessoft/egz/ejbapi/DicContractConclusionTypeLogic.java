package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicContractConclusionType;

import javax.ejb.Local;

/**
 * Created by alexey on 14.10.16.
 */
@Local
public interface DicContractConclusionTypeLogic extends ISimpleDictionaryDAO<DicContractConclusionType> {
}

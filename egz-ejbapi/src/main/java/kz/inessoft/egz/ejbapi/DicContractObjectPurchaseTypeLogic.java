package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicContractObjectPurchaseType;

import javax.ejb.Local;

/**
 * Created by alexey on 14.10.16.
 */
@Local
public interface DicContractObjectPurchaseTypeLogic extends ISimpleDictionaryDAO<DicContractObjectPurchaseType> {
}

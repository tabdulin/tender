package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicContractStatus;

import javax.ejb.Local;

/**
 * Created by alexey on 14.10.16.
 */
@Local
public interface DicContractStatusLogic extends ISimpleDictionaryDAO<DicContractStatus> {
}

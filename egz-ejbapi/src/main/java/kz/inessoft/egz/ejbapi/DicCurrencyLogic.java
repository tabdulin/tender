package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicCurrency;

import javax.ejb.Local;

/**
 * Created by alexey on 14.10.16.
 */
@Local
public interface DicCurrencyLogic {
    DicCurrency getValueByCode(String code);
}

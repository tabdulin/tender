package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicDiscussionRequestType;

import javax.ejb.Local;

/**
 * Created by alexey on 15.02.17.
 */
@Local
public interface DicDiscussionRequestTypeLogic extends ISimpleDictionaryDAO<DicDiscussionRequestType> {
}

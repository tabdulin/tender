package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicDiscussionResponseType;

import javax.ejb.Local;

/**
 * Created by alexey on 15.02.17.
 */
@Local
public interface DicDiscussionResponseTypeLogic extends ISimpleDictionaryDAO<DicDiscussionResponseType> {
}

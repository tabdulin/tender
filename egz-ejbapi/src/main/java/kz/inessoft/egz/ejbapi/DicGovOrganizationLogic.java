package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicGovOrganization;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Local
public interface DicGovOrganizationLogic {
    DicGovOrganization getById(Long id);

    DicGovOrganization getValueByBin(String bin, String name, String address);

    List<DicGovOrganization> getFirst10GOByFilter(String filter);
}

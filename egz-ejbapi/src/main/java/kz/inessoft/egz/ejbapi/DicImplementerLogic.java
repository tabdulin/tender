package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicImplementer;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 11.08.16.
 */
@Local
public interface DicImplementerLogic {
    DicImplementer getValueByXin(String xin, String name);

    DicImplementer getValueById(long id);

    List<DicImplementer> getFirst10ImplementersByFilter(String filter);
}

package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicInterestingTru;

import javax.ejb.Local;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 02.02.17.
 */
@Local
public interface DicInterestingTruLogic {
    List<DicInterestingTru> getInterestingTRUs();

    void setLastUpdatedDate(Long dicInterestingTruId, Date date);
}

package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicLotStatus;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Local
public interface DicLotStatusLogic extends ISimpleDictionaryDAO<DicLotStatus> {
    List<DicLotStatus> getLotStatuses();
}

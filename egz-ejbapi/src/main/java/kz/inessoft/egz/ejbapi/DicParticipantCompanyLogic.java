package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicParticipantCompany;

import javax.ejb.Local;
import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 07.09.16.
 */
@Local
public interface DicParticipantCompanyLogic {
    List<DicParticipantCompany> getDicParticipantCompanies();

    DicParticipantCompany getDefaultCompanyForRefreshAnnouncement();

    void setLastNoticeTime(Date noticeTime, long id);
}

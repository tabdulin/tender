package kz.inessoft.egz.ejbapi;

import javax.ejb.Local;
import java.util.Map;

/**
 * Created by alexey on 29.09.16.
 */
@Local
public interface DicPortalResourcesLogic {
    Map<String, String> getResourcesMap();

    String replaceResources(String source);
}

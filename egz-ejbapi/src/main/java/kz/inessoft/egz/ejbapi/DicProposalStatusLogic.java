package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicProposalStatus;

import javax.ejb.Local;

/**
 * Created by alexey on 20.09.16.
 */
@Local
public interface DicProposalStatusLogic extends ISimpleDictionaryDAO<DicProposalStatus> {
}

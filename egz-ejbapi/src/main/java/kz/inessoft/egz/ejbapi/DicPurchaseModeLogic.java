package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicPurchaseMode;

import javax.ejb.Local;

/**
 * Created by alexey on 09.08.16.
 */
@Local
public interface DicPurchaseModeLogic extends ISimpleDictionaryDAO<DicPurchaseMode> {
}

package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicTenderDocumentation;

import javax.ejb.Local;

/**
 * Created by alexey on 10.08.16.
 */
@Local
public interface DicTenderDocumentationLogic extends ISimpleDictionaryDAO<DicTenderDocumentation> {
}

package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicTru;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Local
public interface DicTruLogic {
    DicTru getTruByCode(String code, String name);

    List<Long> getInterestingTruIds();
}

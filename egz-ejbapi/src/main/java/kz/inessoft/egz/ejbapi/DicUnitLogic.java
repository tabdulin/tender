package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicUnit;

import javax.ejb.Local;

/**
 * Created by alexey on 09.08.16.
 */
@Local
public interface DicUnitLogic extends ISimpleDictionaryDAO<DicUnit> {
}

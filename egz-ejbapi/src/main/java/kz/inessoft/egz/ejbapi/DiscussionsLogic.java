package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDiscussion;
import kz.inessoft.egz.dbentities.TDiscussionCrossAnnouncement;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Local
public interface DiscussionsLogic {
    List<TDiscussionCrossAnnouncement> getDiscussions(TAnnouncement announcement);

    TDiscussion getDiscussion(long id);
}

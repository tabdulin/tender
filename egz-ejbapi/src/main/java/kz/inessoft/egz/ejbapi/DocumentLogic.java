package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDocument;
import kz.inessoft.egz.dbentities.TPortalFile;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 10.08.16.
 */
@Local
public interface DocumentLogic {
    List<TDocument> getDocumentsWithFiles(long announcementId);

    TDocument getDocument(String documentTypeName, TAnnouncement announcement);

    TDocument getDocument(long id, boolean initCollection);

    List<TPortalFile> getJiraFiles(Long announcementId);
}

package kz.inessoft.egz.ejbapi;

/**
 * Created by alexey on 01.08.16.
 */
public enum  ESysParam {
    /**
     * Путь к папке - хранилищу Google
     */
    GOOGLE_STORE_DIRECTORY("google.store.directory"),
    /**
     * Идентификтаор пользователя Google
     */
    GOOGLE_USER_ID("google.userId"),
    /**
     * Идентификатор календаря Google
     */
    GOOGLE_CALENDAR_ID("google.calendar.id"),
    /**
     * Креденшиалы для подключения к Google
     */
    GOOGLE_CREDENTIALS("google.oauth.credentials"),
    /**
     * Тип прокси-сервера. Может быть HTTP, SOCKS, NONE (или null)
     */
    PROXY_TYPE("proxy.type"),
    /**
     * Хост прокси-сервера
     */
    PROXY_HOST("proxy.host"),
    /**
     * Порт прокси-сервера
     */
    PROXY_PORT("proxy.port"),
    /**
     * Jira URL
     */
    JIRA_URL("jira.url"),
    JIRA_LOGIN("jira.login"),
    JIRA_PASSWORD("jira.password"),
    JIRA_PROJECT("jira.project"),
    JIRA_ISSUE_TYPE_ID("jira.issueTypeId")
    ;


    private String id;

    ESysParam(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}

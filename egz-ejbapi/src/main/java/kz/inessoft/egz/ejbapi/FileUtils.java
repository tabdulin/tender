package kz.inessoft.egz.ejbapi;

/**
 * Created by alexey on 27.09.16.
 */
public class FileUtils {
    /**
     * Создать корректное имя файла из строки. Необходимо вырезать все недопустимые символы (будут заменены пробелами).
     * Кроме того заменим двойные пробелы одинарными для удобства.
     *
     * @param fileName
     * @return
     */
    public static String makeCorrectFileName(String fileName) {
        String retVal = fileName.replaceAll("/", " ")
                .replaceAll("\\\\", " ")
                .replaceAll("\\?", " ")
                .replaceAll("%", " ")
                .replaceAll("\\*", " ")
                .replaceAll(":", " ")
                .replaceAll("\\|", " ")
                .replaceAll("\"", " ")
                .replaceAll("<", " ")
                .replaceAll(">", " ")
                .replaceAll("\\.", " ")
                .replaceAll("\n", " ")
                .replaceAll("\r", " ");
        while (retVal.contains("  "))
            retVal = retVal.replaceAll("  ", " ");
        return retVal;
    }
}

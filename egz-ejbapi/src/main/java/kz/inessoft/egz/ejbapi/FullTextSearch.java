package kz.inessoft.egz.ejbapi;

import javax.ejb.Local;

/**
 * Created by alexey on 25.10.16.
 */
@Local
public interface FullTextSearch {
    void reindexData() throws InterruptedException;
}

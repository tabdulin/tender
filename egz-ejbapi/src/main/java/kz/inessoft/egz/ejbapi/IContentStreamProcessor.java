package kz.inessoft.egz.ejbapi;

import java.io.InputStream;

/**
 * Created by alexey on 23.02.17.
 */
public interface IContentStreamProcessor {
    void processContent(InputStream contentStream) throws Exception;
}

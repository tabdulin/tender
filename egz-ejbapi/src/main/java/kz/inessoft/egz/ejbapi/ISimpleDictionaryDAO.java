package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.ANamedEntity;

/**
 * Created by alexey on 09.08.16.
 */
public interface ISimpleDictionaryDAO<T extends ANamedEntity> {
    T getValueByName(String name);
}

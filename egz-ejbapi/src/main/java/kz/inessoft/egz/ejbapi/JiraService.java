package kz.inessoft.egz.ejbapi;

import javax.ejb.Local;
import javax.jms.JMSException;

/**
 * Created by alexey on 27.01.17.
 */
@Local
public interface JiraService {
    void createOrUpdateIssue(long announcementPortalId) throws JMSException;

    void registerMonitored() throws JMSException;
}

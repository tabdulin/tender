package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus;

import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 02.09.16.
 */
public class LotFilter {
    public enum EOrderByColumn {
        PublicationDate, Price, Customer, AnnoucementNumber, AnnouncementName, AnnouncementStatus, LotNumber, LotStatus, StartRecvTime, EndRecvTime,
        ShortCharacteristic, AdditionalCharacteristic;
    }

    public static class OrderBy {
        private EOrderByColumn orderByColumn;
        private boolean asc = false;

        public OrderBy(EOrderByColumn orderByColumn, boolean asc) {
            this.orderByColumn = orderByColumn;
            this.asc = asc;
        }

        public EOrderByColumn getOrderByColumn() {
            return orderByColumn;
        }

        public boolean isAsc() {
            return asc;
        }
    }

    private List<String> truCodesMask;
    private Date notBefore;
    private List<DicAnnouncementStatus> statuses;
    private Long implementerId;
    private Long customerId;
    private String lotNumber;
    private Long lotStatusId;
    private boolean monitoring;
    private List<String> keywords;
    private List<OrderBy> orderByList;

    public List<String> getTruCodesMask() {
        return truCodesMask;
    }

    public void setTruCodesMask(List<String> truCodesMask) {
        this.truCodesMask = truCodesMask;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = notBefore;
    }

    public List<DicAnnouncementStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<DicAnnouncementStatus> statuses) {
        this.statuses = statuses;
    }

    public List<OrderBy> getOrderByList() {
        return orderByList;
    }

    public void setOrderByList(List<OrderBy> orderByList) {
        this.orderByList = orderByList;
    }

    public Long getImplementerId() {
        return implementerId;
    }

    public void setImplementerId(Long implementerId) {
        this.implementerId = implementerId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        if (lotNumber != null) {
            lotNumber = lotNumber.trim();
            if (lotNumber.isEmpty())
                lotNumber = null;
        }
        this.lotNumber = lotNumber;
    }

    public Long getLotStatusId() {
        return lotStatusId;
    }

    public void setLotStatusId(Long lotStatusId) {
        this.lotStatusId = lotStatusId;
    }

    public boolean isMonitoring() {
        return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
        this.monitoring = monitoring;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
}

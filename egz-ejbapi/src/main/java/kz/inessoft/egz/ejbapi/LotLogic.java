package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 09.08.16.
 */
@Local
public interface LotLogic {
    int getLotsListPagesCount(long announcemenId);

    String getLotsListHtml(long announcemenId, int pageNumber);

    List<TLotCrossAnnouncement> getLots(LotFilter filter, Integer maxResults, Integer pageNumber);

    long countLots(LotFilter filter);

    List<TLotCrossAnnouncement> getLotsByAnnouncement(Long announcementId);

    TLotCrossAnnouncement getLotById(Long announcementId, Long lotId);

    TLotCrossAnnouncement getLotByNumber(TAnnouncement announcement, String lotNumber);

    void setLotHasContract(long planUnitId, String announcementNumberId);
}

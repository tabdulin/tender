package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.dbentities.TPortalRecievedFile;
import kz.inessoft.egz.dbentities.TPortalUploadedFile;

import javax.ejb.Local;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 * Created by alexey on 20.09.16.
 */
@Local
public interface PortalFilesLogic {

    TPortalUploadedFile queueLoadUploadedFile(long portalId);

    TPortalRecievedFile queueLoadRecievedFile(long portalId, String url, DicParticipantCompany participantCompany);

    int saveFile(long fileId, String fileName, String mimeType, long fileSize, InputStream contentStream) throws SQLException, IOException;

    void queueNextPortionUnloadedFiles();

    TPortalFile getPortalFile(long fileId);

    void processFileContent(long fileId, IContentStreamProcessor streamProcessor) throws Exception;

    void writeFileContent(long fileId, OutputStream outputStream) throws Exception;
}

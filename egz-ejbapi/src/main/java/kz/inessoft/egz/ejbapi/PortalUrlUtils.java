package kz.inessoft.egz.ejbapi;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author talgat.abdulin
 */
public class PortalUrlUtils {
    public static final String HOST_URL = "https://v3bl.goszakup.gov.kz";
    public static final String BASE_URL = HOST_URL + "/ru";

    public static String createAnnouncementUrl(long announcementPortalId) {
        return BASE_URL + "/announce/index/" + announcementPortalId;
    }

    public static String createAnnouncementDocumentsUrl(long number) {
        return createAnnouncementUrl(number) + "?tab=documents";
    }

    public static String createAnnouncementDiscussionsUrl(long announcementPortalId) {
        return createAnnouncementUrl(announcementPortalId) + "?tab=discussion";
    }

    public static String createAnnouncementProtocolUrl(long announcementPortalId) {
        return createAnnouncementUrl(announcementPortalId) + "/?tab=protocols";
    }

    public static String createAnnouncementLotsUrl(long announcementPortalId, int page) {
        String retVal = createAnnouncementUrl(announcementPortalId) + "?tab=lots";
        if (page > 1)
            retVal += "&page=" + page;
        return retVal;
    }

    public static String createAnnouncementLotLoadAjaxUrl(long announcementPortalId) {
        return BASE_URL + "/announce/ajax_load_lot/" + announcementPortalId + "?tab=lots";
    }

    public static String createAnnouncementProposalsUrl(long announcementPortalId) {
        return BASE_URL + "/consideration/index/" + announcementPortalId;
    }

    public static String createAnnouncementProposalLotDocumentsUrl(long announcementPortalId, long proposalPortalId, long lotPortalId) {
        return BASE_URL + "/consideration/ajax_show_app_lot_docs/" + announcementPortalId + "/" + proposalPortalId + "/" + lotPortalId;
    }

    public static String createMyPropposalsUrl() {
        return BASE_URL + "/myapp";
    }

    public static String createAnnouncementsListURL(int pageNumber, Long methodz, Date startRecvDate) {
        String suffix = "/searchanno?page=";
        if (pageNumber != 1)
            suffix += pageNumber;
        if (methodz != null)
            suffix += "&methodz=" + methodz;
        if (startRecvDate != null) {
            suffix += "&date_start=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startRecvDate)
                    .replaceAll(" ", "+").replaceAll(":", "%3A");
        }
        return BASE_URL + suffix;
    }

    public static String createTopKTRUListFilterURL() {
        return BASE_URL + "/subpriceoffer/index/1/?";
    }

    public static String buildKTRUFiterURL(String url, Date startDate, int page) {
        String suffix = null;
        if (startDate != null) {
            suffix = "date_start=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startDate)
                    .replaceAll(" ", "+").replaceAll(":", "%3A");
        }
        if (page != 1) {
            String pFilter = "page=" + page;
            if (suffix == null)
                suffix = pFilter;
            else
                suffix += "&" + pFilter;
        }
        return suffix == null ? url : url + suffix;
    }

    public static String createKTRURowsURL() {
        return BASE_URL + "/subpriceoffer/";
    }

    public static String createAnnouncementDocFilesList(long announcementPortalId, long docTypePortalId) {
        return BASE_URL + "/announce/actionAjaxModalShowFiles/" + announcementPortalId + "/" + docTypePortalId;
    }

    public static String createNoticesURL(int pageNumber) {
        String retVal = BASE_URL + "/notices/";
        if (pageNumber != 0)
            retVal += "index/" + (pageNumber + 1);
        return retVal;
    }

    public static String createDeletedNoticesURL(int pageNumber) {
        String retVal = BASE_URL + "/notices/basket/";
        if (pageNumber != 0)
            retVal += pageNumber + 1;
        return retVal;
    }

    public static String createDownloadFileUrl(long portalId) {
        return HOST_URL + "/files/download_file/" + portalId;
    }

    public static String createContractsListURL(Integer page) {
        String suffix = "/egzcontract/cpublic";
        if (page != null)
            suffix += "/index/" + page;
        return BASE_URL + suffix;
    }

    public static String createContractGeneralInfoURL(long portalId) {
        return BASE_URL + "/egzcontract/cpublic/show/" + portalId;
    }

    public static String createContractUnitsURL(long portalId) {
        return BASE_URL + "/egzcontract/cpublic/units/" + portalId;
    }

    public static String createContractCustomerURL(long portalId) {
        return BASE_URL + "/egzcontract/cpublic/customer_n_supplier/" + portalId;
    }

    public static String createContractTreasureURL(long portalId) {
        return BASE_URL + "/egzcontract/cpublic/kazna/" + portalId;
    }

    public static String createContractAgreementURL(long portalId) {
        return BASE_URL + "/egzcontract/cpublic/contract/" + portalId;
    }
}

package kz.inessoft.egz.ejbapi;

import javax.ejb.Local;
import java.io.InputStream;

/**
 * Created by alexey on 01.11.16.
 */
@Local
public interface ProposalLogic {
    void makeProposal(long participantId, long announcementId, InputStream zipData) throws ProposalLogicException;
}

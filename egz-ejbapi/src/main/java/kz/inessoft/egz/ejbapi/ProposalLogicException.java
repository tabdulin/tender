package kz.inessoft.egz.ejbapi;

/**
 * Created by alexey on 01.11.16.
 */
public class ProposalLogicException extends Exception {
    public ProposalLogicException(String message) {
        super(message);
    }

    public ProposalLogicException(String message, Throwable cause) {
        super(message, cause);
    }
}

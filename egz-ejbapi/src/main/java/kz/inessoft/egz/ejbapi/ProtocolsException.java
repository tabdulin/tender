package kz.inessoft.egz.ejbapi;

/**
 * Created by alexey on 21.09.16.
 */
public class ProtocolsException extends Exception {
    public ProtocolsException(String message, Throwable cause) {
        super(message, cause);
    }
}

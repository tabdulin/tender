package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TProtocol;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Local
public interface ProtocolsLogic {
    List<TProtocol> getProtocols(TAnnouncement announcement);

    TProtocol getProtocol(long id);
}

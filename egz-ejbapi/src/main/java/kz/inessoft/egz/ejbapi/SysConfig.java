package kz.inessoft.egz.ejbapi;

import javax.ejb.Local;

/**
 * Created by alexey on 01.08.16.
 */
@Local
public interface SysConfig {
    String getSysParamValue(ESysParam param);

    void saveSysParam(ESysParam param, String value);
}

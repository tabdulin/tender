package kz.inessoft.egz.ejbapi;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TProposal;
import kz.inessoft.egz.dbentities.TProposalDocument;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by alexey on 20.09.16.
 */
@Local
public interface TProposalsLogic {
    List<TProposal> getProposals(TAnnouncement announcement);

    TProposal getProposal(TAnnouncement announcement, long implementerId);

    TProposalDocument getProposalDocument(long id);

    List<TProposalDocument> getProposalDocuments(TAnnouncement announcement, long implementerId);

    List<TProposalDocument> getProposalDocuments(long tDocumentId, long implementerId);

    boolean hasProposals(TAnnouncement announcement);
}

package kz.inessoft.egz.ejbapi.reports;

import javax.ejb.Local;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by alexey on 26.05.17.
 */
@Local
public interface TendersResultsLogic {
    void getITExcellTendersResults(OutputStream stream) throws IOException;

    void getTranslateExcellTendersResults(OutputStream stream) throws IOException;
}

'use strict';

import * as React from 'react';
import * as ReactDom from 'react-dom';
import {createBrowserHistory} from 'history';
import {Router, Route} from 'react-router';
import {Provider} from 'react-redux';
import {syncHistoryWithStore} from 'react-router-redux';
import store from './scripts/store/index';
import View from './scripts/view'
import LastAnnouncements from './scripts/pages/lastannouncements/LastAnnouncements';

ReactDom.render((
    <Provider store={store}>
        <Router history={createBrowserHistory()}>
            <Route path="/egz/">
                <View>
                    <Route exact={true} path="/egz/">
                        <LastAnnouncements/>
                    </Route>
                </View>
            </Route>
        </Router>
    </Provider>
), document.getElementById('container'));
'use strict';

export function buildRestUrl(path) {
    return '/egz/rest' + path;
}

export function get(url) {
    return fetch(buildRestUrl(url), {credentials: 'include'});
}
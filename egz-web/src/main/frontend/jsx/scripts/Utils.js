'use strict';

export function getDate(timeInMillis) {
    return new Date(timeInMillis);
}

export function getFormatedDate(timeInMillis) {
    let date = getDate(timeInMillis);
    return append0(date.getDate(), 2) + '.' + append0(date.getMonth() + 1, 2) + '.' + append0(date.getFullYear(), 4);
}

// Дополняет незначащими нулями до получения указанного размера строки size символов
function append0(data, size) {
    data = '' + data;
    if (data.length >= size)
        return data;
    return '000'.substr(0, size - data.length) + data;
}

'use strict';

import * as React from 'react';
import {connect} from 'react-redux';
import {Nav, Navbar, NavItem, NavDropdown, MenuItem} from 'react-bootstrap'

class Menu extends React.Component<Props, any> {

    render():JSX.Element {
        return (
            <Navbar fluid={true} inverse={true}>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/egz">Tenders</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                    <NavItem href="/egz/searchAnnouncement.xhtml">Поиск</NavItem>
                    <NavDropdown title="Отчеты" id="basic-nav-dropdown">
                        <MenuItem href="/egz/reports/tendersResults/it">Результаты конкурсов по IT</MenuItem>
                        <MenuItem href="/egz/reports/tendersResults/translate">Результаты конкурсов по переводам</MenuItem>
                    </NavDropdown>
                </Nav>
            </Navbar>
        )
    }
}

export default connect()(Menu);
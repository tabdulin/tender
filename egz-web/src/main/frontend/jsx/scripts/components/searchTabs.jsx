'use strict';

import * as React from 'react';
import {connect} from 'react-redux';

const SearchTabs = React.createClass({
    render() {
       return (
           <ul className="nav nav-tabs" style={{marginBottom: '19px'}}>
               <li>
                   <a href="/egz/">Поиск объявлений</a>
               </li>
               <li>
                   <a href="/egz/searchLot">Поиск лотов</a>
               </li>
           </ul>
       ) 
    }
});

export default connect()(SearchTabs)
'use strict';

import { dispatch } from 'redux';
import { ActionTypes } from './constants';
import { get } from '../../FetchUtils';

export function sortByColumn(column, dataSet) {
    return {
        type: ActionTypes.SortColumn,
        sortColumn: column,
        dataSet: dataSet
    }
}

function itDataLoaded(data) {
    return {
        type: ActionTypes.ItDataLoaded,
        data: data
    }
}

function translateDataLoaded(data) {
    return {
        type: ActionTypes.TranslateDataLoaded,
        data: data
    }
}

export function fetchItData() {
    return (dispatch) => {
        get('/lot/itLastLots').then(response => response.json()).then(json => {
            dispatch(itDataLoaded(json))
        })
    }
}

export function fetchTranslateData() {
    return (dispatch) => {
        get('/lot/translateLastLots').then(response => response.json()).then(json => {
            dispatch(translateDataLoaded(json))
        })
    }
}
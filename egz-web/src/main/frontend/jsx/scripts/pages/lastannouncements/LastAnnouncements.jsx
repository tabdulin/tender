'use strict';

import * as React from 'react';
import {Tabs, Tab} from 'react-bootstrap';
import TabContent from './TabContent';
import {DataSets} from './constants';
import {bindActionCreators} from 'redux';
import * as actions from './Actions';
import {connect} from 'react-redux';

class LastAnnouncements extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.actions.fetchItData();
        this.props.actions.fetchTranslateData();
    }

    render() {
        return (
            <Tabs defaultActiveKey={DataSets.IT} id="lastAnnouncementsTabs">
                <Tab eventKey={DataSets.IT} title="IT за последние 40 дней">
                    <TabContent data={this.props[DataSets.IT]}
                                sortAction={column => this.props.actions.sortByColumn(column, DataSets.IT)}/>
                </Tab>
                <Tab eventKey={DataSets.TRANSLATE} title="Переводы за последние 40 дней">
                    <TabContent data={this.props[DataSets.TRANSLATE]}
                                sortAction={column => this.props.actions.sortByColumn(column, DataSets.TRANSLATE)}/>
                </Tab>
            </Tabs>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        [DataSets.IT]: state.pages.lastAnnouncements[DataSets.IT],
        [DataSets.TRANSLATE]: state.pages.lastAnnouncements[DataSets.TRANSLATE]
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LastAnnouncements);
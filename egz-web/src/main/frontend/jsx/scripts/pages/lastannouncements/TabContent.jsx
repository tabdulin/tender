'use strict';

import * as React from 'react';
import {OrderColumns} from './constants'
import {getFormatedDate} from '../../Utils'

class TabContent extends React.Component {
    render() {
        var data = this.props.data;
        if (!data)
            return (
                <span>Загрузка данных</span>
            );
        let announcements = data.announcements;
        let sortColumn = data.sortColumn;
        let sortAsc = data.sortAsc;
        return (
            announcements.length ?
                <table className="table table-stripped table-bordered">
                    <thead>
                    <tr>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.AnnouncementNumber}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.Customer}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.Name}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.Status}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.LotNumber}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <th colSpan={3} className="text-center">Дата</th>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.ShortCharacteristic}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.AdditionalCharacteristic}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader rowSpan={2}
                                   column={OrderColumns.Amount}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                    </tr>
                    <tr>
                        <TabHeader column={OrderColumns.PublicationDate}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader column={OrderColumns.StartRecvDate}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                        <TabHeader column={OrderColumns.EndRecvDate}
                                   sortAction={this.props.sortAction}
                                   sortColumn={sortColumn}
                                   sortAsc={sortAsc}/>
                    </tr>
                    </thead>
                    <tbody className="small">
                    {
                        announcements.map((announcement, index)=> (
                                <tr key={index} className={announcement.monitoring ? 'success' : null}>
                                    <td style={{whiteSpace: 'nowrap'}}>{announcement.announcementNumber}</td>
                                    <td>{announcement.customer}</td>
                                    <td>
                                        {announcement.announcementName}<br/>
                                        <a href={'/egz/viewAnnouncement.xhtml?announcePortalId=' + announcement.announcementPortalId}>Просмотр</a>{'\u00A0'}
                                        <a target="_blank"
                                           href={'/egz/files.xhtml?announcePortalId=' + announcement.announcementPortalId}>
                                            Документы
                                        </a>{'\u00A0'}
                                        <a target="_blank"
                                           href={'https://v3bl.goszakup.gov.kz/ru/announce/index/' + announcement.announcementPortalId}>
                                            Портал ГЗ
                                        </a>

                                    </td>
                                    <td>{announcement.status}</td>
                                    <td style={{whiteSpace: 'nowrap'}}>{announcement.lotNumber}</td>
                                    <td style={{whiteSpace: 'nowrap'}}>{getFormatedDate(announcement.publicationDate)}</td>
                                    <td style={{whiteSpace: 'nowrap'}}>{getFormatedDate(announcement.startRecvDate)}</td>
                                    <td style={{whiteSpace: 'nowrap'}}>{getFormatedDate(announcement.endRecvDate)}</td>
                                    <td>{announcement.shortCharacteristic}</td>
                                    <td>{announcement.additionalCharacteristic}</td>
                                    <td style={{whiteSpace: 'nowrap'}}>{announcement.amount}</td>
                                </tr>
                            )
                        )
                    }
                    </tbody>
                </table> : <span>Нет данных</span>
        )
    }
}

class TabHeader extends React.Component {
    render() {
        return (
            <th rowSpan={this.props.rowSpan} className="text-center">
                <a href="#" onClick={() => this.props.sortAction(this.props.column)}>{this.props.column.header}</a>
                {
                    this.props.sortColumn == this.props.column && '\u00A0'
                }
                {
                    this.props.sortColumn == this.props.column &&
                    (
                        this.props.sortAsc ?
                            <span className="glyphicon glyphicon-sort-by-attributes"/> :
                            <span className="glyphicon glyphicon-sort-by-attributes-alt"/>
                    )
                }
            </th>
        )
    }
}

export default TabContent;
'use strict';

const OrderColumns = {
    AnnouncementNumber: {header: 'Номер объявления'},
    Customer: {header: 'Заказчик'},
    Name: {header: 'Наименование'},
    Status: {header: 'Статус'},
    LotNumber: {header: 'Номер лота'},
    PublicationDate: {header: 'публикации'},
    StartRecvDate: {header: 'приема заявок'},
    EndRecvDate: {header: 'окончания приема заявок'},
    ShortCharacteristic: {header: 'Краткая характеристика'},
    AdditionalCharacteristic: {header: 'Дополнительная характеристика'},
    Amount: {header: 'Бюджет'}
};

const ActionTypes = {
    SortColumn: 'SortColumn',
    ItDataLoaded: 'ItDataLoaded',
    TranslateDataLoaded: 'TranslateDataLoaded'
};

const DataSets = {
    IT: 'itData',
    TRANSLATE: 'translateData'
};

export {OrderColumns, ActionTypes, DataSets};
'use strict';

import { ActionTypes, OrderColumns, DataSets } from './constants';

const initialState = {};

function makeSort(state, action) {
    let dataSet = action.dataSet;
    let data = state[dataSet];
    let sortColumn = action.sortColumn;
    let sortAsc = true;
    if (data.sortColumn === sortColumn)
        sortAsc = !data.sortAsc;
    let compareFunction;
    switch (sortColumn) {
        case OrderColumns.AdditionalCharacteristic:
            compareFunction = (a, b) => a.additionalCharacteristic.localeCompare(b.additionalCharacteristic);
            break;
        case OrderColumns.AnnouncementNumber:
            compareFunction = (a, b) => a.announcementNumber.localeCompare(b.announcementNumber);
            break;
        case OrderColumns.Customer:
            compareFunction = (a, b) => a.customer.localeCompare(b.customer);
            break;
        case OrderColumns.LotNumber:
            compareFunction = (a, b) => a.lotNumber.localeCompare(b.lotNumber);
            break;
        case OrderColumns.Name:
            compareFunction = (a, b) => a.announcementName.localeCompare(b.announcementName);
            break;
        case OrderColumns.ShortCharacteristic:
            compareFunction = (a, b) => a.shortCharacteristic.localeCompare(b.shortCharacteristic);
            break;
        case OrderColumns.Status:
            compareFunction = (a, b) => a.status.localeCompare(b.status);
            break;
        case OrderColumns.Amount:
            compareFunction = (a, b) =>  a.amount - b.amount;
            break;
        case OrderColumns.PublicationDate:
            compareFunction = (a, b) => a.publicationDate - b.publicationDate;
            break;
        case OrderColumns.StartRecvDate:
            compareFunction = (a, b) => a.startRecvDate - b.startRecvDate;
            break;
        case OrderColumns.EndRecvDate:
            compareFunction = (a, b) => a.endRecvDate - b.endRecvDate;
            break;
    }
    let announcements = data.announcements.concat().sort((a, b) => {
        let retVal = compareFunction(a, b);
        if (!sortAsc)
            retVal = -retVal;
        return retVal;
    });
    return Object.assign({}, state, {
        [dataSet]: {
            sortColumn: sortColumn,
            sortAsc: sortAsc,
            announcements: announcements
        }
    });
}

function processItData(state, action) {
    return Object.assign({}, state, {
        [DataSets.IT]: {
            announcements: action.data
        }
    })
}


function processTranslateData(state, action) {
    return Object.assign({}, state, {
        [DataSets.TRANSLATE]: {
            announcements: action.data
        }
    })
}

function lastAnnouncements(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.SortColumn:
            return makeSort(state, action);
        case ActionTypes.ItDataLoaded:
            return processItData(state, action);
        case ActionTypes.TranslateDataLoaded:
            return processTranslateData(state, action);
        default:
            return state;
    }
}

export default lastAnnouncements;
'use strict';

import * as React from 'react';
import {connect} from 'react-redux';
import SearchTabs from '../../components/searchTabs'

const SearchAnnouncement = React.createClass({
    render: function () {
        return (
            <div>
                <SearchTabs/>
                <div>Поиск объявлений</div>
            </div>
        )
    }
});

export default connect()(SearchAnnouncement);
'use strict';

import { combineReducers, Reducer } from 'redux';
import { routerReducer } from 'react-router-redux';
import pages from './pages';

export const rootReducer: Reducer<any> = combineReducers({
    routing: routerReducer,
    pages
});

export default rootReducer;
'use strict';

import { combineReducers } from 'redux';
import lastAnnouncements from '../pages/lastannouncements/reducer'

const pages = combineReducers({
    lastAnnouncements
});

export default pages;
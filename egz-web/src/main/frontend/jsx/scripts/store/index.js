'use strict';

import {createStore, applyMiddleware, Store} from 'redux';
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../reducers/index';

const store:Store<any> = createStore(rootReducer,
    applyMiddleware(thunkMiddleware));

export default store;
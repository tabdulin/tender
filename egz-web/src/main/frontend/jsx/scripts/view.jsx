'use strict';

import * as React from 'react';
import {connect} from 'react-redux';
import Menu from './components/menu';


class View extends React.Component {
    render() {
        return (
            <div>
                <Menu/>
                <div className="container-fluid">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default connect()(View);
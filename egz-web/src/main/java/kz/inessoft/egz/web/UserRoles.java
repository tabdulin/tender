package kz.inessoft.egz.web;

/**
 * Created by alexey on 21.02.17.
 */
public final class UserRoles {
    public static final String MAKE_PROPOSAL = "makeProposal";

    private UserRoles() {
    }
}
package kz.inessoft.egz.web.jsf;

import kz.inessoft.egz.ejbapi.PortalUrlUtils;

/**
 * Created by alexey on 06.09.16.
 */
public class JSFFunctions {
    public static String getAnnouncementOriginalURL(Long announcePortalId) {
        return PortalUrlUtils.createAnnouncementUrl(announcePortalId);
    }
}

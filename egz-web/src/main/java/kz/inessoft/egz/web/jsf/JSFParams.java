package kz.inessoft.egz.web.jsf;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Created by alexey on 05.09.16.
 */
@ManagedBean(name = "jsfParams")
@ApplicationScoped
public class JSFParams {
    public static final String ANNOUNCE_ID = "announceId";
    public static final String LOT_ID = "lotId";
    private static final String ANNOUNCE_PORTAL_ID = "announcePortalId";
    private static final String SORT_BY = "sortBy";
    private static final String SORT_ASC = "sortAsc";
    public static final String FILE_ID = "fileId";
    public static final String TDOCUMENT_ID = "tDocumentId";
    public static final String TPROTOCOL_ID = "tProtocolId";
    public static final String TDISCUSSION_ID = "tDiscussionId";
    public static final String IMPLEMENTER_ID = "implementerId";
    public static final String HTML_TYPE = "htmlType";
    public static final String DOWNLOAD_TYPE = "downloadType";
    public static final String PARTICIPANT_ID = "participantId";
    public static final String PAGE_NUMBER = "pageNumber";

    public String getAnnounceId() {
        return ANNOUNCE_ID;
    }

    public String getAnnouncePortalId() {
        return ANNOUNCE_PORTAL_ID;
    }

    public String getSortBy() {
        return SORT_BY;
    }

    public String getSortAsc() {
        return SORT_ASC;
    }

    public String getFileId() {
        return FILE_ID;
    }

    public String getTDocumentId() {
        return TDOCUMENT_ID;
    }

    public String getTProtocolId() {
        return TPROTOCOL_ID;
    }

    public String getImplementerId() {
        return IMPLEMENTER_ID;
    }

    public String getHtmlType() {
        return HTML_TYPE;
    }

    public String getDownloadType() {
        return DOWNLOAD_TYPE;
    }

    public String getParticipantId() {
        return PARTICIPANT_ID;
    }
}

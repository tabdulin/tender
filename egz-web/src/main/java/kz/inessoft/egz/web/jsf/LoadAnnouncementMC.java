package kz.inessoft.egz.web.jsf;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.AnnouncementQueueInfo;
import kz.inessoft.egz.ejbapi.SysConfig;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.jms.JMSException;

/**
 * Created by alexey on 15.08.16.
 */
@ManagedBean(name = "loadAnnouncement")
@ViewScoped
public class LoadAnnouncementMC {
    private Long portalId;
    private TAnnouncement announcement;
//    private boolean loadingFlag;

    @EJB
    private AnnouncementLogic announcementLogic;
    @EJB
    private AnnouncementQueueInfo announcementQueueInfo;
    @EJB
    private SysConfig sysConfig;

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public void startLoad() {
        announcement = announcementLogic.getAnnouncementByPortalId(portalId, false);
    }

    public void refreshAnnounsement() {
        announcement = null;
        try {
            announcementLogic.loadAnnouncement(portalId);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public String getDocsUrl() {
        String id = "0000000000".substring(String.valueOf(portalId).length()) + portalId;
        String retVal = "";
        for (int i = 0; i < id.length(); i++) {
            retVal += "/" + id.charAt(i);
        }
        return retVal;
    }
}

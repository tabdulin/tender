package kz.inessoft.egz.web.jsf.files;

import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import kz.inessoft.egz.ejbapi.DicPortalResourcesLogic;
import kz.inessoft.egz.ejbapi.DiscussionsLogic;
import kz.inessoft.egz.ejbapi.DocumentLogic;
import kz.inessoft.egz.ejbapi.LotLogic;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import kz.inessoft.egz.ejbapi.TProposalsLogic;
import kz.inessoft.egz.ejbapi.ProtocolsLogic;
import kz.inessoft.egz.web.jsf.files.filesystem.FileBuilder;
import kz.inessoft.egz.web.jsf.files.filesystem.IDataLoader;
import kz.inessoft.egz.web.jsf.files.filesystem.IFileInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by alexey on 26.09.16.
 */
@WebServlet("/docs/download/*")
public class DownloadFileServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadFileServlet.class);
    @EJB
    private PortalFilesLogic portalFilesLogic;

    @EJB
    private DicPortalResourcesLogic portalResourcesLogic;

    @EJB
    private AnnouncementLogic announcementLogic;

    @EJB
    private LotLogic lotLogic;

    @EJB
    private DocumentLogic documentLogic;

    @EJB
    private ProtocolsLogic protocolsLogic;

    @EJB
    private TProposalsLogic TProposalsLogic;

    @EJB
    private DicImplementerLogic dicImplementerLogic;

    @EJB
    private DiscussionsLogic discussionsLogic;

    @EJB
    private DicParticipantCompanyLogic participantCompanyLogic;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        IFileInfo fileInfo = new FileBuilder(req, new IDataLoader() {
            @Override
            public DocumentLogic getDocumentLogic() {
                return documentLogic;
            }

            @Override
            public PortalFilesLogic getPortalFilesLogic() {
                return portalFilesLogic;
            }

            @Override
            public AnnouncementLogic getAnnouncementLogic() {
                return announcementLogic;
            }

            @Override
            public ProtocolsLogic getProtocolsLogic() {
                return protocolsLogic;
            }

            public TProposalsLogic getTProposalsLogic() {
                return TProposalsLogic;
            }

            @Override
            public DicImplementerLogic getDicImplementerLogic() {
                return dicImplementerLogic;
            }

            @Override
            public DicPortalResourcesLogic getPortalResourcesLogic() {
                return portalResourcesLogic;
            }

            @Override
            public LotLogic getLotLogic() {
                return lotLogic;
            }

            @Override
            public DiscussionsLogic getDiscussionsLogic() {
                return discussionsLogic;
            }

            @Override
            public DicParticipantCompanyLogic getDicParticimantCompanyLogic() {
                return participantCompanyLogic;
            }
        }).build();
        try {
            fileInfo.writeFileContent(resp);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}

package kz.inessoft.egz.web.jsf.files;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import kz.inessoft.egz.ejbapi.DicPortalResourcesLogic;
import kz.inessoft.egz.ejbapi.DiscussionsLogic;
import kz.inessoft.egz.ejbapi.DocumentLogic;
import kz.inessoft.egz.ejbapi.LotLogic;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import kz.inessoft.egz.ejbapi.TProposalsLogic;
import kz.inessoft.egz.ejbapi.ProtocolsLogic;
import kz.inessoft.egz.web.jsf.files.filesystem.*;
import kz.inessoft.egz.web.jsf.files.filesystem.IFileInfo;
import kz.inessoft.egz.web.jsf.files.filesystem.IDirectoryInfo;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlOutcomeTargetLink;
import javax.faces.component.html.HtmlOutputLink;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 04.10.16.
 */
@ManagedBean(name = "filesBean")
@RequestScoped
public class FilesBean implements IDataLoader {
    public class DirectoryWrapper {
        private IDirectoryInfo directoryInfo;

        public DirectoryWrapper(IDirectoryInfo directoryInfo) {
            this.directoryInfo = directoryInfo;
        }

        public IDirectoryInfo getDirectoryInfo() {
            return directoryInfo;
        }

        public HtmlOutcomeTargetLink getLink() {
            return null;
        }

        public void setLink(HtmlOutcomeTargetLink link) {
            if (directoryInfo == null) return;
            if (directoryInfo.getUrlParams() != null)
                for (Map.Entry<String, Object> entry : directoryInfo.getUrlParams().entrySet()) {
                    UIParameter parameter = new UIParameter();
                    parameter.setName(entry.getKey());
                    parameter.setValue(entry.getValue());
                    link.getChildren().add(parameter);
                }
        }
    }

    public class FileWrapper {
        private IFileInfo fileInfo;

        public FileWrapper(IFileInfo fileInfo) {
            this.fileInfo = fileInfo;
        }

        public IFileInfo getFileInfo() {
            return fileInfo;
        }

        public HtmlOutputLink getLink() {
            return null;
        }

        public void setLink(HtmlOutputLink link) {
            if (fileInfo.getUrlParams() != null)
                for (Map.Entry<String, Object> entry : fileInfo.getUrlParams().entrySet()) {
                    UIParameter parameter = new UIParameter();
                    parameter.setName(entry.getKey());
                    parameter.setValue(entry.getValue());
                    link.getChildren().add(parameter);
                }
        }
    }

    @EJB
    private DocumentLogic documentLogic;

    @EJB
    private PortalFilesLogic filesLogic;

    @EJB
    private AnnouncementLogic announcementLogic;

    @EJB
    private ProtocolsLogic protocolsLogic;

    @EJB
    private TProposalsLogic TProposalsLogic;

    @EJB
    private DicImplementerLogic dicImplementerLogic;

    @EJB
    private DiscussionsLogic discussionsLogic;

    @EJB
    private LotLogic lotLogic;

    @EJB
    private DicPortalResourcesLogic portalResourcesLogic;

    @EJB
    private DicParticipantCompanyLogic dicParticipantCompanyLogic;

    private IDirectoryInfo directoryInfo;
    private TAnnouncement announcement;

    public Long getAnnouncementPortalId() {
        if (announcement == null) return null;
        return announcement.getPortalId();
    }

    public void setAnnouncementPortalId(Long portalId) {
        announcement = announcementLogic.getAnnouncementByPortalId(portalId, false);
    }

    @Override
    public DocumentLogic getDocumentLogic() {
        return documentLogic;
    }

    @Override
    public PortalFilesLogic getPortalFilesLogic() {
        return filesLogic;
    }

    @Override
    public AnnouncementLogic getAnnouncementLogic() {
        return announcementLogic;
    }

    @Override
    public ProtocolsLogic getProtocolsLogic() {
        return protocolsLogic;
    }

    public TProposalsLogic getTProposalsLogic() {
        return TProposalsLogic;
    }

    @Override
    public DicImplementerLogic getDicImplementerLogic() {
        return dicImplementerLogic;
    }

    @Override
    public DicPortalResourcesLogic getPortalResourcesLogic() {
        return portalResourcesLogic;
    }

    @Override
    public LotLogic getLotLogic() {
        return lotLogic;
    }

    @Override
    public DiscussionsLogic getDiscussionsLogic() {
        return discussionsLogic;
    }

    @Override
    public DicParticipantCompanyLogic getDicParticimantCompanyLogic() {
        return dicParticipantCompanyLogic;
    }

    public DirectoryWrapper getParentWrapper() {
        IDirectoryInfo parentDirectory = getDirectoryInfo().getParentDirectory();
        return new DirectoryWrapper(parentDirectory);
    }

    private IDirectoryInfo getDirectoryInfo() {
        if (directoryInfo == null)
            directoryInfo = new DirectoryBuilder((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest(),
                    this, announcement).build();
        return directoryInfo;
    }

    public TAnnouncement getAnnouncement() {
        return announcement;
    }

    public List<DirectoryWrapper> getDirs() {
        ArrayList<DirectoryWrapper> retVal = new ArrayList<>();
        for (IDirectoryInfo info : getDirectoryInfo().getChildrenDirectories()) {
            retVal.add(new DirectoryWrapper(info));
        }
        return retVal;
    }

    public List<FileWrapper> getFiles() {
        ArrayList<FileWrapper> retVal = new ArrayList<>();
        for (IFileInfo info : getDirectoryInfo().getChildrenFiles()) {
            retVal.add(new FileWrapper(info));
        }
        retVal.add(new FileWrapper(new AllFilesAsZip(getAnnouncement(), getDirectoryInfo())));
        return retVal;
    }
}

package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;

import java.util.Map;

/**
 * Created by alexey on 05.10.16.
 */
public abstract class AAnnouncementHtmlFile extends AHtmlFile {
    private TAnnouncement announcement;
    private long announcementId;

    public AAnnouncementHtmlFile(long announcementId, IDataLoader dataLoader) {
        super(dataLoader);
        this.announcementId = announcementId;
    }

    protected TAnnouncement getAnnoucement() {
        if (announcement == null) {
            announcement = getDataLoader().getAnnouncementLogic().getAnnouncementById(announcementId, false);
        }
        return announcement;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        return null;
    }

    public boolean isContentExists() {
        return getOriginalHtml() != null;
    }
}

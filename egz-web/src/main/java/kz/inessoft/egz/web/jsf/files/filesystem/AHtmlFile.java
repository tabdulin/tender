package kz.inessoft.egz.web.jsf.files.filesystem;

import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by alexey on 05.10.16.
 */
public abstract class AHtmlFile implements IFileInfo {
    private IDataLoader dataLoader;

    public AHtmlFile(IDataLoader dataLoader) {
        this.dataLoader = dataLoader;
    }

    protected abstract String getOriginalHtml();

    protected String getHtmlToWrite() {
        return dataLoader.getPortalResourcesLogic().replaceResources(getOriginalHtml());
    }

    protected IDataLoader getDataLoader() {
        return dataLoader;
    }

    @Override
    public long getFileLength() {
        return 0;
    }

    @Override
    public void writeFileContent(HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        writeFileContent(response.getOutputStream());
    }

    @Override
    public void writeFileContent(OutputStream outputStream) throws IOException {
        IOUtils.write(getHtmlToWrite(), outputStream, "UTF8");
    }

    @Override
    public boolean isOpenInNewWindow() {
        return true;
    }
}

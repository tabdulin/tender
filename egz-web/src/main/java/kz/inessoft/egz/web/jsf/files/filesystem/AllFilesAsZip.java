package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.web.jsf.JSFParams;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by alexey on 03.10.16.
 */
public class AllFilesAsZip implements IFileInfo {
    private IDirectoryInfo parent;
    private TAnnouncement announcement;

    public AllFilesAsZip(TAnnouncement announcement, IDirectoryInfo parent) {
        this.announcement = announcement;
        this.parent = parent;
    }

    @Override
    public String getFileName() {
        return announcement.getNumber() + " - " + parent.getName() + " - Одним архивом.zip";
    }

    @Override
    public long getFileLength() {
        return 0;
    }

    @Override
    public boolean isOpenInNewWindow() {
        return false;
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.AllFilesAsZip;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.HTML_TYPE, parent.getDirectoryType());
        if (parent.getUrlParams() != null)
            retVal.putAll(parent.getUrlParams());
        return retVal;
    }

    @Override
    public void writeFileContent(HttpServletResponse response) throws Exception {
        String header = "attachment; filename=\"" + URLEncoder.encode(getFileName(), "UTF-8").replaceAll("\\+", "%20") + "\"";
        response.setHeader("Content-Disposition", header);
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
        ZipOutputStream zous = new ZipOutputStream(response.getOutputStream());
        zous.setLevel(9);
        processFolder(parent, null, zous);
        zous.flush();
        zous.close();
    }

    @Override
    public void writeFileContent(OutputStream outputStream) {
        throw new NotImplementedException("This method not implemented here");
    }

    private void processFolder(IDirectoryInfo directoryInfo, String currentPath, ZipOutputStream outputStream) throws Exception {
        if (currentPath == null)
            currentPath = directoryInfo.getName();
        else
            currentPath += "/" + directoryInfo.getName();
        for (IDirectoryInfo info : directoryInfo.getChildrenDirectories()) {
            processFolder(info, currentPath, outputStream);
        }
        Set<String> fileNames = new HashSet<>();
        for (IFileInfo fileInfo : directoryInfo.getChildrenFiles()) {
            String fileName = fileInfo.getFileName();
            for (int i = 1; fileNames.contains(fileName); i++)
                fileName = fileInfo.getFileName() + "(" + i + ")";
            fileNames.add(fileName);
            ZipEntry entry = new ZipEntry(currentPath + "/" + fileName);
            outputStream.putNextEntry(entry);
            fileInfo.writeFileContent(outputStream);
            outputStream.closeEntry();
        }
    }
}

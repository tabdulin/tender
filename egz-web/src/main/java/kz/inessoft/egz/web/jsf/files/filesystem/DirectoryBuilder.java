package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.web.jsf.JSFParams;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by alexey on 03.10.16.
 */
public class DirectoryBuilder {
    private HttpServletRequest request;
    private IDataLoader dataLoader;
    private TAnnouncement announcement;

    public DirectoryBuilder(HttpServletRequest request, IDataLoader dataLoader, TAnnouncement announcement) {
        this.request = request;
        this.dataLoader = dataLoader;
        this.announcement = announcement;
    }

    public IDirectoryInfo build() {
        EDirectoryType directoryType = EDirectoryType.getByName(request.getParameter(JSFParams.HTML_TYPE));
        switch (directoryType) {
            case Root:
                return new RootDirectory(announcement, dataLoader);
            case TenderDocumentsList:
                return new TenderDocsDirectory(announcement, dataLoader);
            case TenderDocumentFilesList:
                return new TenderDocFilesDirectory(getTDocumentId(), dataLoader);
            case ProtocolsList:
                return new ProtocolsDirectory(announcement, dataLoader);
            case ProtocolFilesList:
                return new ProtocolFilesDirectory(getTProtocolId(), dataLoader);
            case ProposalsImplementersList:
                return new ProposalsImplementersListDirectory(announcement, dataLoader);
            case ProposalsDocumentsList:
                return new ProposalDocsListDirectory(announcement, getImplementerId(), dataLoader);
            case ProposalsDocumentFilesList:
                return new ProposalFilesDirectory(getTDocumentId(), getImplementerId(), dataLoader);
            case ScreenshotsAnnouncement:
                return new ScreenshotsAnnouncementDirectory(announcement, dataLoader);
            case ScreenshotsDiscussions:
                return new ScreenshotsDiscussionsDirectory(announcement, dataLoader);
            case ScreenshotsLots:
                return new ScreenshotsLotsDirectory(announcement, dataLoader);
            case ScreenshotsTopDir:
                return new ScreenshotsDirectory(announcement, dataLoader);
            default:
                throw new RuntimeException("Unable to instantiate directory");
        }
    }

    private long getTDocumentId() {
        return Long.valueOf(request.getParameter(JSFParams.TDOCUMENT_ID));
    }

    private long getTProtocolId() {
        return Long.valueOf(request.getParameter(JSFParams.TPROTOCOL_ID));
    }

    private long getImplementerId() {
        return Long.valueOf(request.getParameter(JSFParams.IMPLEMENTER_ID));
    }
}

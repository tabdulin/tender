package kz.inessoft.egz.web.jsf.files.filesystem;

/**
 * Created by alexey on 03.10.16.
 */
public enum EDirectoryType {
    Root,
    TenderDocumentsList, TenderDocumentFilesList,
    ProposalsImplementersList, ProposalsDocumentsList, ProposalsDocumentFilesList,
    ProtocolsList, ProtocolFilesList,
    ScreenshotsTopDir, ScreenshotsAnnouncement, ScreenshotsLots, ScreenshotsDiscussions;

    public static EDirectoryType getByName(String name) {
        if (name == null) return Root;
        return valueOf(name);
    }
}

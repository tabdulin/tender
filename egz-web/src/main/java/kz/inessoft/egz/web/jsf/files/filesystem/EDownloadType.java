package kz.inessoft.egz.web.jsf.files.filesystem;

/**
 * Created by alexey on 03.10.16.
 */
public enum EDownloadType {
    RegularFile, AllFilesAsZip,
    AnnouncementGeneralTab, AnnouncementDiscussions, AnnouncementLots, AnnouncementDocumentation, AnnouncementProposals, AnnouncementProtocols,
    Lot,
    Discussion;
}

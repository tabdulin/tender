package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.web.jsf.JSFParams;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by alexey on 04.10.16.
 */
public class FileBuilder {
    private HttpServletRequest request;
    private IDataLoader dataLoader;

    public FileBuilder(HttpServletRequest request, IDataLoader dataLoader) {
        this.request = request;
        this.dataLoader = dataLoader;
    }

    public IFileInfo build() {
        EDownloadType downloadType = EDownloadType.valueOf(request.getParameter(JSFParams.DOWNLOAD_TYPE));
        switch (downloadType) {
            case RegularFile:
                return buildRegularDownloadFile();
            case AllFilesAsZip:
                return buildAllFilesAsZip();
            case AnnouncementGeneralTab:
                return buildScreenshotGeneralFile();
            case AnnouncementDocumentation:
                return buildScreenshotDocumentationFile();
            case AnnouncementDiscussions:
                return buildScreenshotDiscussionsFile();
            case AnnouncementLots:
                return buildScreenshotLotsFile();
            case AnnouncementProposals:
                return buildScreenshotProposalsFile();
            case AnnouncementProtocols:
                return buildScreenshotProtocolsFile();
            case Lot:
                return buildScreenshotLotFile();
            case Discussion:
                return buildDiscussionFile();
            default:
                throw new RuntimeException("Can't instantiate file info");
        }
    }

    private RegularDownloadFile buildRegularDownloadFile() {
        return new RegularDownloadFile(Long.valueOf(request.getParameter(JSFParams.FILE_ID)), dataLoader);
    }

    private AllFilesAsZip buildAllFilesAsZip() {
        TAnnouncement announcement = dataLoader.getAnnouncementLogic().getAnnouncementById(getAnnouncementId(), false);
        IDirectoryInfo directoryInfo = new DirectoryBuilder(request, dataLoader, announcement).build();
        return new AllFilesAsZip(announcement, directoryInfo);
    }

    private ScreenshotGeneralFile buildScreenshotGeneralFile() {
        return new ScreenshotGeneralFile(getAnnouncementId(), dataLoader);
    }

    private ScreenshotDocumentationFile buildScreenshotDocumentationFile() {
        return new ScreenshotDocumentationFile(getAnnouncementId(), dataLoader);
    }

    private ScreenshotDiscussionsFile buildScreenshotDiscussionsFile() {
        return new ScreenshotDiscussionsFile(getAnnouncementId(), dataLoader);
    }

    private ScreenshotLotsFile buildScreenshotLotsFile() {
        return new ScreenshotLotsFile(getAnnouncementId(), Integer.valueOf(request.getParameter(JSFParams.PAGE_NUMBER)), dataLoader);
    }

    private ScreenshotProposalsFile buildScreenshotProposalsFile() {
        return new ScreenshotProposalsFile(getAnnouncementId(), dataLoader);
    }

    private ScreenshotProtocolsFile buildScreenshotProtocolsFile() {
        return new ScreenshotProtocolsFile(getAnnouncementId(), dataLoader);
    }

    private ScreenshotLotFile buildScreenshotLotFile() {
        return new ScreenshotLotFile(getAnnouncementId(), getLotId(), dataLoader);
    }

    private ScreenshotDiscussionFile buildDiscussionFile() {
        return new ScreenshotDiscussionFile(getDiscussionId(), dataLoader);
    }

    private long getAnnouncementId() {
        return Long.valueOf(request.getParameter(JSFParams.ANNOUNCE_ID));
    }

    private long getLotId() {
        return Long.valueOf(request.getParameter(JSFParams.LOT_ID));
    }

    private long getDiscussionId() {
        return Long.valueOf(request.getParameter(JSFParams.TDISCUSSION_ID));
    }
}

package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import kz.inessoft.egz.ejbapi.DicPortalResourcesLogic;
import kz.inessoft.egz.ejbapi.DiscussionsLogic;
import kz.inessoft.egz.ejbapi.DocumentLogic;
import kz.inessoft.egz.ejbapi.LotLogic;
import kz.inessoft.egz.ejbapi.PortalFilesLogic;
import kz.inessoft.egz.ejbapi.TProposalsLogic;
import kz.inessoft.egz.ejbapi.ProtocolsLogic;

/**
 * Created by alexey on 03.10.16.
 */
public interface IDataLoader {
    DocumentLogic getDocumentLogic();

    PortalFilesLogic getPortalFilesLogic();

    AnnouncementLogic getAnnouncementLogic();

    ProtocolsLogic getProtocolsLogic();

    TProposalsLogic getTProposalsLogic();

    DicImplementerLogic getDicImplementerLogic();

    DicPortalResourcesLogic getPortalResourcesLogic();

    LotLogic getLotLogic();

    DiscussionsLogic getDiscussionsLogic();

    DicParticipantCompanyLogic getDicParticimantCompanyLogic();
}

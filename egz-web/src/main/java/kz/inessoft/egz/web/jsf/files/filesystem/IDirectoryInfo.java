package kz.inessoft.egz.web.jsf.files.filesystem;

import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 03.10.16.
 */
public interface IDirectoryInfo {
    String getName();

    List<IDirectoryInfo> getChildrenDirectories();

    List<IFileInfo> getChildrenFiles();

    IDirectoryInfo getParentDirectory();

    Map<String, Object> getUrlParams();

    EDirectoryType getDirectoryType();
}

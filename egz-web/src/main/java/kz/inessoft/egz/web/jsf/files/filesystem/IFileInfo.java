package kz.inessoft.egz.web.jsf.files.filesystem;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Map;

/**
 * Created by alexey on 03.10.16.
 */
public interface IFileInfo {
    String getFileName();

    long getFileLength();

    boolean isOpenInNewWindow();

    EDownloadType getDownloadType();

    Map<String, Object> getUrlParams();

    void writeFileContent(HttpServletResponse response) throws Exception;

    void writeFileContent(OutputStream outputStream) throws Exception;
}

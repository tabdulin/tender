package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.DicImplementer;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TProposalDocument;
import kz.inessoft.egz.ejbapi.FileUtils;
import kz.inessoft.egz.web.jsf.JSFParams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by alexey on 04.10.16.
 */
public class ProposalDocsListDirectory implements IDirectoryInfo {
    private TAnnouncement announcement;
    private DicImplementer implementer;
    private IDataLoader dataLoader;
    private ProposalsImplementersListDirectory parent;
    private List<IDirectoryInfo> children;

    public ProposalDocsListDirectory(TAnnouncement announcement, long implementerId, IDataLoader dataLoader) {
        this(announcement, dataLoader.getDicImplementerLogic().getValueById(implementerId), dataLoader, null);
    }

    public ProposalDocsListDirectory(TAnnouncement announcement, DicImplementer implementer, IDataLoader dataLoader, ProposalsImplementersListDirectory parent) {
        this.announcement = announcement;
        this.implementer = implementer;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return FileUtils.makeCorrectFileName(implementer.getXin() + " - " + implementer.getNameRu());
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        if (children == null) {
            children = new ArrayList<>();
            List<TProposalDocument> documents = dataLoader.getTProposalsLogic().getProposalDocuments(announcement, implementer.getId());
            Set<Long> processed = new HashSet<>();
            for (TProposalDocument document : documents) {
                if (processed.contains(document.getDocument().getId()))
                    continue;
                processed.add(document.getDocument().getId());
                children.add(new ProposalFilesDirectory(document.getDocument(),
                        document.getLotProposal().getId().getProposal().getId().getImplementer().getId(), dataLoader, this));
            }
        }
        return children;
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        return Collections.emptyList();
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null) {
            parent = new ProposalsImplementersListDirectory(announcement, dataLoader);
        }
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.IMPLEMENTER_ID, implementer.getId());
        return retVal;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.ProposalsDocumentsList;
    }
}

package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TDocument;
import kz.inessoft.egz.dbentities.TProposalDocument;
import kz.inessoft.egz.ejbapi.FileUtils;
import kz.inessoft.egz.web.jsf.JSFParams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 04.10.16.
 */
public class ProposalFilesDirectory implements IDirectoryInfo {
    private TDocument document;
    private long implementerId;
    private IDataLoader dataLoader;
    private ProposalDocsListDirectory parent;
    private List<IFileInfo> children;

    public ProposalFilesDirectory(long tDocumentId, long implementerId, IDataLoader dataLoader) {
        this(dataLoader.getDocumentLogic().getDocument(tDocumentId, false), implementerId, dataLoader, null);
    }

    public ProposalFilesDirectory(TDocument document, long implementerId, IDataLoader dataLoader, ProposalDocsListDirectory parent) {
        this.document = document;
        this.implementerId = implementerId;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return FileUtils.makeCorrectFileName(document.getDocumentation().getNameRu());
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        return Collections.emptyList();
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        if (children == null) {
            children = new ArrayList<>();
            List<TProposalDocument> proposalDocuments = dataLoader.getTProposalsLogic().getProposalDocuments(document.getId(), implementerId);
            for (TProposalDocument proposalDocument : proposalDocuments) {
                children.add(new RegularDownloadFile(proposalDocument.getPortalFile(), dataLoader));
            }
        }
        return children;
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null) {
            parent = new ProposalDocsListDirectory(document.getAnnouncement(), implementerId, dataLoader);
        }
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.TDOCUMENT_ID, document.getId());
        retVal.put(JSFParams.IMPLEMENTER_ID, implementerId);
        return retVal;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.ProposalsDocumentFilesList;
    }
}

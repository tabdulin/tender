package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TProposal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 03.10.16.
 */
public class ProposalsImplementersListDirectory implements IDirectoryInfo {
    private TAnnouncement announcement;
    private IDataLoader dataLoader;
    private RootDirectory parent;
    private List<IDirectoryInfo> children;

    public ProposalsImplementersListDirectory(TAnnouncement announcement, IDataLoader dataLoader) {
        this(announcement, dataLoader, null);
    }

    public ProposalsImplementersListDirectory(TAnnouncement announcement, IDataLoader dataLoader, RootDirectory parent) {
        this.announcement = announcement;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return "Заявки";
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        if (children == null) {
            children = new ArrayList<>();
            List<TProposal> proposals = dataLoader.getTProposalsLogic().getProposals(announcement);
            for (TProposal proposal : proposals) {
                children.add(new ProposalDocsListDirectory(announcement, proposal.getId().getImplementer(), dataLoader, this));
            }
        }
        return children;
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        return Collections.emptyList();
    }

    @Override
    public RootDirectory getParentDirectory() {
        if (parent == null)
            parent = new RootDirectory(announcement, dataLoader);
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        return null;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.ProposalsImplementersList;
    }
}

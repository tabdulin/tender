package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TProtocol;
import kz.inessoft.egz.ejbapi.FileUtils;
import kz.inessoft.egz.web.jsf.JSFParams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 04.10.16.
 */
public class ProtocolFilesDirectory implements IDirectoryInfo {
    private TProtocol protocol;
    private IDataLoader dataLoader;
    private ProtocolsDirectory parent;
    private List<IFileInfo> children;

    public ProtocolFilesDirectory(long protocolId, IDataLoader dataLoader) {
        protocol = dataLoader.getProtocolsLogic().getProtocol(protocolId);
        this.dataLoader = dataLoader;
    }

    public ProtocolFilesDirectory(TProtocol protocol, IDataLoader dataLoader, ProtocolsDirectory parent) {
        this.protocol = protocol;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return FileUtils.makeCorrectFileName(protocol.getType().getName());
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        return Collections.emptyList();
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        if (children == null) {
            children = new ArrayList<>();
            children.add(new RegularDownloadFile(protocol.getPortalFile(), dataLoader));
        }
        return children;
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null) {
            parent = new ProtocolsDirectory(protocol.getAnnouncement(), dataLoader);
        }
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.TPROTOCOL_ID, protocol.getId());
        return retVal;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.ProtocolFilesList;
    }
}

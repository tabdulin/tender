package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TProtocol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 03.10.16.
 */
public class ProtocolsDirectory implements IDirectoryInfo {
    private RootDirectory parent;
    private TAnnouncement announcement;
    private IDataLoader dataLoader;
    private List<IDirectoryInfo> children;

    public ProtocolsDirectory(TAnnouncement announcement, IDataLoader dataLoader) {
        this(announcement, dataLoader, null);
    }

    public ProtocolsDirectory(TAnnouncement announcement, IDataLoader dataLoader, RootDirectory parent) {
        this.parent = parent;
        this.announcement = announcement;
        this.dataLoader = dataLoader;
    }

    @Override
    public String getName() {
        return "Протоколы";
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        if (children == null) {
            children = new ArrayList<>();
            List<TProtocol> protocols = dataLoader.getProtocolsLogic().getProtocols(announcement);
            for (TProtocol protocol : protocols) {
                children.add(new ProtocolFilesDirectory(protocol, dataLoader, this));
            }
        }
        return children;
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        return Collections.emptyList();
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null) {
            parent = new RootDirectory(announcement, dataLoader);
        }
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        return null;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.ProtocolsList;
    }
}

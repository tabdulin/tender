package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.web.jsf.JSFParams;
import org.apache.http.HttpHeaders;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexey on 03.10.16.
 */
public class RegularDownloadFile implements IFileInfo {
    private TPortalFile portalFile;
    private IDataLoader dataLoader;

    public RegularDownloadFile(long portalFileId, IDataLoader dataLoader) {
        this.portalFile = dataLoader.getPortalFilesLogic().getPortalFile(portalFileId);
        this.dataLoader = dataLoader;
    }

    public RegularDownloadFile(TPortalFile portalFile, IDataLoader dataLoader) {
        this.portalFile = portalFile;
        this.dataLoader = dataLoader;
    }

    @Override
    public String getFileName() {
        return portalFile.getFileName();
    }

    @Override
    public long getFileLength() {
        return portalFile.getFileSize();
    }

    @Override
    public boolean isOpenInNewWindow() {
        return false;
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.RegularFile;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.FILE_ID, portalFile.getId());
        return retVal;
    }

    @Override
    public void writeFileContent(HttpServletResponse response) throws Exception {
        if (portalFile == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } else if (portalFile.getFileName() != null) {
            String header = "attachment; filename=\"" + URLEncoder.encode(portalFile.getFileName(), "UTF-8") + "\"";
            response.setHeader("Content-Disposition", header);
            response.setHeader(HttpHeaders.CONTENT_TYPE, portalFile.getMimeType());

            ServletOutputStream outputStream = response.getOutputStream();
            dataLoader.getPortalFilesLogic().writeFileContent(portalFile.getId(), outputStream);
            outputStream.flush();
            outputStream.close();
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "File not loaded yet from portal");
        }
    }

    @Override
    public void writeFileContent(OutputStream outputStream) throws Exception {
        dataLoader.getPortalFilesLogic().writeFileContent(portalFile.getId(), outputStream);
    }
}

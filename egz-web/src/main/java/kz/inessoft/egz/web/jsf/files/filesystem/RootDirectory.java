package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 03.10.16.
 */
public class RootDirectory implements IDirectoryInfo {
    private TAnnouncement announcement;
    private IDataLoader dataLoader;
    private List<IDirectoryInfo> childrenDirs;

    public RootDirectory(TAnnouncement announcement, IDataLoader dataLoader) {
        this.announcement = announcement;
        this.dataLoader = dataLoader;
    }

    @Override
    public String getName() {
        return "Корневая";
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        if (childrenDirs == null) {
            childrenDirs = new ArrayList<>();
            childrenDirs.add(new TenderDocsDirectory(announcement, dataLoader, this));
            childrenDirs.add(new ProposalsImplementersListDirectory(announcement, dataLoader, this));
            childrenDirs.add(new ProtocolsDirectory(announcement, dataLoader, this));
            childrenDirs.add(new ScreenshotsDirectory(announcement, dataLoader, this));
        }
        return childrenDirs;
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        return Collections.emptyList();
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        return null;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        return null;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.Root;
    }
}

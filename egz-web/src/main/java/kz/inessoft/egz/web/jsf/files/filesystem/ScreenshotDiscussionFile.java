package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TDiscussion;
import kz.inessoft.egz.web.jsf.JSFParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotDiscussionFile extends AHtmlFile {
    private TDiscussion discussion;

    public ScreenshotDiscussionFile(long discussionId, IDataLoader dataLoader) {
        this(dataLoader.getDiscussionsLogic().getDiscussion(discussionId), dataLoader);
    }

    public ScreenshotDiscussionFile(TDiscussion discussion, IDataLoader dataLoader) {
        super(dataLoader);
        this.discussion = discussion;
    }

    @Override
    protected String getOriginalHtml() {
        return discussion.getSourceHtml();
    }

    @Override
    public String getFileName() {
        return "Вопрос " + discussion.getPortalId() + ".html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.Discussion;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.TDISCUSSION_ID, discussion.getId());
        return retVal;
    }
}

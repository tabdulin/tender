package kz.inessoft.egz.web.jsf.files.filesystem;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotDiscussionsFile extends AAnnouncementHtmlFile {
    public ScreenshotDiscussionsFile(long announcementId, IDataLoader dataLoader) {
        super(announcementId, dataLoader);
    }

    @Override
    protected String getOriginalHtml() {
        return getAnnoucement().getDiscussionsTab();
    }

    @Override
    public String getFileName() {
        return "Обсуждения.html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.AnnouncementDiscussions;
    }
}

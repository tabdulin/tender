package kz.inessoft.egz.web.jsf.files.filesystem;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotDocumentationFile extends AAnnouncementHtmlFile {
    public ScreenshotDocumentationFile(long announcementId, IDataLoader dataLoader) {
        super(announcementId, dataLoader);
    }

    @Override
    protected String getOriginalHtml() {
        return getAnnoucement().getDocumentationTab();
    }

    @Override
    public String getFileName() {
        return "Конкурсная документация.html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.AnnouncementDocumentation;
    }
}

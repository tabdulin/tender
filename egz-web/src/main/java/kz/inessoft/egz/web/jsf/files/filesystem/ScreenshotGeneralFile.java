package kz.inessoft.egz.web.jsf.files.filesystem;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotGeneralFile extends AAnnouncementHtmlFile {
    public ScreenshotGeneralFile(long announcementId, IDataLoader dataLoader) {
        super(announcementId, dataLoader);
    }

    @Override
    protected String getOriginalHtml() {
        return getAnnoucement().getGeneralTab();
    }

    @Override
    public String getFileName() {
        return "Основные данные.html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.AnnouncementGeneralTab;
    }
}

package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.web.jsf.JSFParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotLotFile extends AHtmlFile {
    public static final String HTML_PREFIX = "\n" +
            "<!DOCTYPE html>\n" +
            "<html lang=\"ru\">\n" +
            "<head>\n" +
            "    <meta charset=\"utf-8\">\n" +
            "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
            "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "    <link rel=\"shortcut icon\" href=\"/images/favicon.ico\" type=\"image/x-icon\"/>\n" +
            "\n" +
            "    <title>Портал государственных закупок Республики Казахстан</title>\n" +
            "    <script src=\"/js/jquery.js\"></script>\n" +
            "    <script src=\"/js/bootstrap.min.js\"></script>\n" +
            "    <script src=\"/js/timers.js\"></script>\n" +
            "    <script src=\"/js/confirmation.js\"></script>\n" +
            "    <script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/bootstrap.datepicker.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/helpers.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/announce.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/helpers.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/announce/price.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/subjectbills/billsSelectModal.plugin.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/bootstrap-filestyle.min.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/file/upload.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/jquery.blockUI.js' charset='utf-8'></script>\n" +
            "<script type='text/javascript' src='https://v3bl.goszakup.gov.kz/js/priceoffer/plugin.js' charset='utf-8'></script>\n" +
            "\n" +
            "\n" +
            "    <!-- Bootstrap -->\n" +
            "    <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">\n" +
            "    <link href=\"/css/style.css\" rel=\"stylesheet\">\n" +
            "    <link href='https://v3bl.goszakup.gov.kz/css/datepicker.css' rel='stylesheet'>\n" +
            "<link href='https://v3bl.goszakup.gov.kz/css/file/upload.css' rel='stylesheet'>\n" +
            "\n" +
            "\n" +
            "\n" +
            "    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->\n" +
            "    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->\n" +
            "    <!--[if lt IE 9]>\n" +
            "    <script src=\"//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\n" +
            "    <script src=\"//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>\n" +
            "    <![endif]-->\n" +
            "    <script>var user_id = 315326</script>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div class=\"container-full\">\n" +
            "    <!-- Static navbar -->\n" +
            "    <div class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
            "        <div class=\"container-fluid\">\n" +
            "            <div class=\"navbar-header\">\n" +
            "                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#main-nav\">\n" +
            "                    <span class=\"sr-only\">Toggle navigation</span>\n" +
            "                    <span class=\"icon-bar\"></span>\n" +
            "                    <span class=\"icon-bar\"></span>\n" +
            "                    <span class=\"icon-bar\"></span>\n" +
            "                </button>\n" +
            "                <a class=\"navbar-brand\" href=\"/\">АИИС ЭГЗ</a>\n" +
            "            </div>\n" +
            "            <div class=\"navbar-collapse collapse\" id=\"main-nav\">\n" +
            "                                <ul class=\"nav navbar-nav\"><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Справка <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Инструкции пользователей</a><ul><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_reg_rus.pdf\">Инструкция пользователя по регистрации на русском языке</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_cust_rus.pdf\">Инструкция заказчика на русском языке</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_org_rus.pdf\">Инструкция организатора на русском языке</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_sup_rus.pdf\">Инструкция поставщика на русском языке</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_bank_rus.pdf\">Инструкция банка</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_reg_kaz.pdf\">Инструкция пользователя по регистрации на казахском языке</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_cust_rus.pdf\">Инструкция Заказчика на казахском языке</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_org_kaz.pdf\">Инструкция Организатора на казахском языке</a></li><li><a href=\"http://portal.goszakup.gov.kz/portal/uploads/v3/doc_sup_kaz.pdf\">Инструкция Поставщика на казахском языке</a></li></ul></li></ul></li><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Реестры <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li><a href=\"https://v3bl.goszakup.gov.kz/ru/careless/reestr\">Реестр недобросовестных участников ГЗ</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/register/supplierreg\">Реестр участников ГЗ </a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/register/plansreg\">Планы ГЗ</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/egzcontract/cpublic\">Реестр договоров</a></li></ul></li><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Закупки <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li><a href=\"https://v3bl.goszakup.gov.kz/ru/subpriceoffer\">Поиск лотов</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/searchanno\">Поиск объявлений</a></li></ul></li><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Отчетность <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li><a href=\"https://v3bl.goszakup.gov.kz/ru/reports/plans_report_admin\">Отчет \"Годовые планы 2016\"</a></li></ul></li></ul>\n" +
            "                <ul class=\"nav navbar-nav navbar-right\">\n" +
            "                    \n" +
            "                <li class='dropdown'>\n" +
            "                    <a href='#' class='dropdown-toggle no-hover' data-toggle='dropdown'><span class='label label-danger' id='notify-count'>0</span>&nbsp;</a>\n" +
            "                    <ul class='dropdown-menu menu-sm' id='notify-container'>\n" +
            "                        \n" +
            "                        <li><a href='/ru/notices/'>Все уведомления</a></li>\n" +
            "                    </ul>\n" +
            "                </li>\n" +
            "                </ul>\n" +
            "\n" +
            "                <ul class=\"nav navbar-nav navbar-right\">\n" +
            "                    \n" +
            "                <li><a href=\"https://v3bl.goszakup.gov.kz/ru/announce/actionShowInfoLot/1022457/2295723\">Рус</a></li>\n" +
            "                <li><a href=\"https://v3bl.goszakup.gov.kz/kz/announce/actionShowInfoLot/1022457/2295723\">Қаз</a></li>\n" +
            "              <li class='dropdown'>\n" +
            "                    <a href='#' class='dropdown-toggle' data-toggle='dropdown'>\n" +
            "                      <span class='glyphicon glyphicon-user online' id='online-status'></span>\n" +
            "                      XXXXXXX XXXXXXX XXXXXXX\n" +
            "                      <b class='caret'></b>\n" +
            "                    </a>\n" +
            "                    <ul class='dropdown-menu'>\n" +
            "                        <li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/profile\">Кабинет</a></li>\n" +
            "                        <li class='divider'></li>\n" +
            "                        <li><a href=\"https://v3bl.goszakup.gov.kz/ru/user/logout\">Выход</a></li>\n" +
            "                    </ul>\n" +
            "                </li>\n" +
            "                </ul>\n" +
            "                <!--\n" +
            "                <div class=\"col-md-4 navbar-right\">\n" +
            "                    <form class=\"navbar-form navbar-right search-form-block\" role=\"search\" action=\"/\">\n" +
            "                        <div class=\"input-group input-group-sm\">\n" +
            "                            <input type=\"text\" class=\"form-control\" placeholder=\"Поиск\" name=\"filter[search]\" id=\"search\" />\n" +
            "                        <span class=\"input-group-btn\">\n" +
            "                            <button id=\"advanced-search-button\" class=\"btn btn-default\" type=\"button\"><span class=\"glyphicon glyphicon-cog\"></span></button>\n" +
            "                            <button class=\"btn btn-default\" type=\"submit\" name=\"filter[submit]\" id=\"mini-submit\"><span class=\"glyphicon glyphicon-search\"></span></button>\n" +
            "                        </span>\n" +
            "                        </div>\n" +
            "                        <div class=\"search-bar col-md-11\">\n" +
            "                            \n" +
            "                        </div>\n" +
            "                    </form>\n" +
            "                </div>\n" +
            "                -->\n" +
            "            </div>\n" +
            "            <!--/.nav-collapse -->\n" +
            "        </div>\n" +
            "        <!--/.container-fluid -->\n" +
            "    </div>\n" +
            "    <div class=\"col-md-12\">\n" +
            "        <div class=\"alert alert-danger\" id=\"offline-message\">\n" +
            "            <strong>Внимание!</strong> Соединение с приложением прервано. Дождитесь повторного подключения.\n" +
            "        </div>\n" +
            "        \n" +
            "        <!-- Main content start -->\n" +
            "        <div class=\"panel panel-default\">\n" +
            "            <div class=\"container-fluid\">\n" +
            "                <div class=\"navbar-header\">\n" +
            "                    <button data-toggle=\"collapse\" class=\"navbar-toggle\" type=\"button\" data-target=\"#cabinet_nav\">\n" +
            "                        <span class=\"sr-only\">Toggle navigation</span>\n" +
            "                        <span class=\"glyphicon glyphicon-arrow-down\"></span>\n" +
            "                    </button>\n" +
            "                </div>\n" +
            "                <div class=\"collapse navbar-collapse\" id=\"cabinet_nav\">\n" +
            "                    <ul class=\"nav navbar-nav\"><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Рабочий кабинет <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li><a href=\"https://v3bl.goszakup.gov.kz/ru/searchanno\">Поиск объявлений (общий)</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/rqc/rqc_announces\">Поиск объявлений (РКП)</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/egzcontract/mycontracts\">Мои договоры (Заказчик)</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/myapp\">Мои заявки</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/egzcontract/supcontract\">Мои договоры (Поставщик)</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/ebg\">Электронные банковские гарантии</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/change_org\">Изменение создателя объявления</a></li></ul></li><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Личные данные <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/user_reg_data\">Регистрационные данные</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/passport\">Паспортные данные</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/pcontact\">Контактные данные</a></li></ul></li><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Профиль участника <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li><a href=\"https://v3bl.goszakup.gov.kz/ru/notices\">Уведомления</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/subject/subject_reg_data\">Регистрационные данные</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/attributes_organ\">Атрибуты участника</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/scontact\">Контактные данные</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/bills\">Банковские счета</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/subject_personal\">Сотрудники организации</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/bodies_tax_reg\">Органы налоговой регистрации</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/data_on_founders\">Данные об учредителях</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/data_on_head\">Данные о руководителе</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/tax_debts\">Данные о налоговой задолженности</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/licenses\">Мои электронные лицензии</a></li><li><a href=\"https://v3bl.goszakup.gov.kz/ru/cabinet/permits\">Мои разрешительные документы</a></li></ul></li><li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Внешние сервисы <b class=\"caret\"></b> </a><ul class='dropdown-menu'><li><a href=\"http://office.sud.kz/index.xhtml\">Судебный кабинет (сайт Верховного суда РК)</a></li><li><a href=\"http://kds.gov.kz/index.php/en/2016-05-18-09-20-49/reestr-ingenerno-tech-rabotnikov\">Реестр аттестованных ИТР, участвующих в процессе проектирования и строительства</a></li><li><a href=\"http://kgd.gov.kz/ru/services/ndspayer_search\">Сервис проверки признака \"Плательщик НДС\" (сайт КГД МФ РК)</a></li><li><a href=\"http://kfm.gov.kz/ru/the-list-of-organizations-and-individuals-associa/\">Лица, организации, связанные с финанс. терроризма и экстремизма (сайт КФМ МФ РК)</a></li><li><a href=\"http://kgd.gov.kz/ru/content/spiski-nesostoyatelnyh-dolzhnikov-0\">Списки несостоятельных должников (сайт КГД МФ РК)</a></li><li><a href=\"http://kgd.gov.kz/ru/services/taxpayer_search_liquid\">Поиск налогоплательщиков, находящихся на стадии ликвидации (сайт КГД МФ РК)</a></li></ul></li></ul>\n" +
            "                </div>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        \n" +
            "        <div id=\"loading\" class=\"alert alert-info\" style=\"display: none;\">Подождите, идет загрузка данных</div>\n" +
            "        \n" +
            "        <div class=\"content-block\">\n" +
            "            <script type=\"application/javascript\" src=\"https://127.0.0.1:8087/js/\" charset=\"UTF-8\"></script>";

    public static final String HTML_SUFFIX = "<br>\n" +
            "<div class=\"row\">\n" +
            "    <div class=\"col-md-12\">\n" +
            "        <div id=\"formPrice\"></div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "        </div>\n" +
            "        <!-- Main content end -->\n" +
            "\n" +
            "    </div>\n" +
            "    <div class=\"clearfix\"></div>\n" +
            "    \n" +
            "    </div>\n" +
            "\n" +
            "<div id=\"footer\">\n" +
            "    <div class=\"container-full\"><div class=\"col-md-12\">\n" +
            "        <span class=\"pull-left\">\n" +
            "\t\tАИИС ЭГЗ © 2016 | <a href=\"http://www.minfin.gov.kz\">Министерство финансов Республики Казахстан</a>\n" +
            "            </span>\n" +
            "        <span class=\"pull-right\">\n" +
            "                <span class=\"ecc\">\n" +
            "                <a href=\"http://www.ecc.kz\">ТОО \"Центр Электронной Коммерции\"</a>\n" +
            "                </span>\n" +
            "            </span></div>\n" +
            "        <div class=\"col-md-12 text-center\"><br>Техническая поддержка пользователей портала (по будням с 09:00 до 19:00):  8 (7172) 72 90 90, <a href=mailto:support@ecc.kz>support@ecc.kz</a></div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\n" +
            "\n" +
            "<!-- Include all compiled plugins (below), or include individual files as needed -->\n" +
            "\n" +
            "<script>\n" +
            "    $(function () {\n" +
            "        $('#advanced-search-button').click(function () {\n" +
            "\n" +
            "            var bar = $('.search-bar');\n" +
            "\n" +
            "            bar.toggle();\n" +
            "\n" +
            "            if(bar.is(\":visible\"))\n" +
            "            {\n" +
            "                $('#search').focus();\n" +
            "                $('#mini-submit').addClass('btn-primary');\n" +
            "\n" +
            "            }\n" +
            "            else{\n" +
            "                $('#mini-submit').removeClass('btn-primary');\n" +
            "            }\n" +
            "        });\n" +
            "    });\n" +
            "</script>\n" +
            "\n" +
            "<!-- Google analytics -->\n" +
            "<script>\n" +
            "    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" +
            "            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" +
            "        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" +
            "    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n" +
            "\n" +
            "    ga('create', 'UA-8694305-1', 'auto');\n" +
            "    ga('send', 'pageview');\n" +
            "\n" +
            "</script>\n" +
            "</body>\n" +
            "</html>";
    private TLotCrossAnnouncement lot;

    public ScreenshotLotFile(long announcementId, long lotId, IDataLoader dataLoader) {
        this(dataLoader.getLotLogic().getLotById(announcementId, lotId), dataLoader);
    }

    public ScreenshotLotFile(TLotCrossAnnouncement lot, IDataLoader dataLoader) {
        super(dataLoader);
        this.lot = lot;
    }

    @Override
    protected String getOriginalHtml() {
        String retVal = lot.getOriginalHtml();
        if (!retVal.startsWith("<!DOCTYPE html>"))
            retVal = HTML_PREFIX + retVal + HTML_SUFFIX;
        return retVal;
    }

    @Override
    public String getFileName() {
        return lot.getLotNumber() + ".html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.Lot;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.LOT_ID, lot.getId().getLot().getId());
        return retVal;
    }
}

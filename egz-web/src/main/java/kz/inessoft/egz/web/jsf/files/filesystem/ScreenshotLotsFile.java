package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.web.jsf.JSFParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotLotsFile extends AHtmlFile {
    private int pageNumber;
    private long announcementId;

    public ScreenshotLotsFile(long announcementId, int pageNumber, IDataLoader dataLoader) {
        super(dataLoader);
        this.announcementId = announcementId;
        this.pageNumber = pageNumber;
    }

    @Override
    protected String getOriginalHtml() {
        return getDataLoader().getLotLogic().getLotsListHtml(announcementId, pageNumber);
    }

    @Override
    public String getFileName() {
        return "Лоты_" + pageNumber + ".html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.AnnouncementLots;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.PAGE_NUMBER, pageNumber);
        return retVal;
    }
}

package kz.inessoft.egz.web.jsf.files.filesystem;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotProposalsFile extends AAnnouncementHtmlFile {
    public ScreenshotProposalsFile(long announcementId, IDataLoader dataLoader) {
        super(announcementId, dataLoader);
    }

    @Override
    protected String getOriginalHtml() {
        return getAnnoucement().getProposalsInfo();
    }

    @Override
    public String getFileName() {
        return "Заявки.html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.AnnouncementProposals;
    }
}

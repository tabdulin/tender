package kz.inessoft.egz.web.jsf.files.filesystem;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotProtocolsFile extends AAnnouncementHtmlFile {
    public ScreenshotProtocolsFile(long announcementId, IDataLoader dataLoader) {
        super(announcementId, dataLoader);
    }

    @Override
    protected String getOriginalHtml() {
        return getAnnoucement().getProtocolsTab();
    }

    @Override
    public String getFileName() {
        return "Протоколы.html";
    }

    @Override
    public EDownloadType getDownloadType() {
        return EDownloadType.AnnouncementProtocols;
    }
}

package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotsAnnouncementDirectory implements IDirectoryInfo {
    private TAnnouncement announcement;
    private IDataLoader dataLoader;
    private ScreenshotsDirectory parent;
    private List<IFileInfo> children;

    public ScreenshotsAnnouncementDirectory(TAnnouncement announcement, IDataLoader dataLoader) {
        this(announcement, dataLoader, null);
    }

    public ScreenshotsAnnouncementDirectory(TAnnouncement announcement, IDataLoader dataLoader, ScreenshotsDirectory parent) {
        this.announcement = announcement;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return "Объявление";
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        return Collections.emptyList();
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        if (children == null) {
            children = new ArrayList<>();
            children.add(new ScreenshotGeneralFile(announcement.getId(), dataLoader));
            int pagesCount = dataLoader.getLotLogic().getLotsListPagesCount(announcement.getId());
            for (int i = 1; i <= pagesCount; i++)
                children.add(new ScreenshotLotsFile(announcement.getId(), i, dataLoader));
            children.add(new ScreenshotDocumentationFile(announcement.getId(), dataLoader));
            ScreenshotDiscussionsFile discussionsFile = new ScreenshotDiscussionsFile(announcement.getId(), dataLoader);
            if (discussionsFile.isContentExists()) children.add(discussionsFile);
            ScreenshotProposalsFile proposalsFile = new ScreenshotProposalsFile(announcement.getId(), dataLoader);
            if (proposalsFile.isContentExists()) children.add(proposalsFile);
            ScreenshotProtocolsFile protocolsFile = new ScreenshotProtocolsFile(announcement.getId(), dataLoader);
            if (protocolsFile.isContentExists()) children.add(protocolsFile);
        }
        return children;
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null) {
            parent = new ScreenshotsDirectory(announcement, dataLoader);
        }
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        return null;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.ScreenshotsAnnouncement;
    }
}

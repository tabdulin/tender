package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDiscussionCrossAnnouncement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 05.10.16.
 */
public class ScreenshotsDiscussionsDirectory implements IDirectoryInfo {
    private TAnnouncement announcement;
    private IDataLoader dataLoader;
    private ScreenshotsDirectory parent;
    private List<IFileInfo> children;

    public ScreenshotsDiscussionsDirectory(TAnnouncement announcement, IDataLoader dataLoader) {
        this(announcement, dataLoader, null);
    }

    public ScreenshotsDiscussionsDirectory(TAnnouncement announcement, IDataLoader dataLoader, ScreenshotsDirectory parent) {
        this.announcement = announcement;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return "Обсуждения";
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        return Collections.emptyList();
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        if (children == null) {
            children = new ArrayList<>();
            List<TDiscussionCrossAnnouncement> discussions = dataLoader.getDiscussionsLogic().getDiscussions(announcement);
            for (TDiscussionCrossAnnouncement discussion : discussions) {
                children.add(new ScreenshotDiscussionFile(discussion.getId().getDiscussion(), dataLoader));
            }
        }
        return children;
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null) {
            parent = new ScreenshotsDirectory(announcement, dataLoader);
        }
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        return null;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.ScreenshotsDiscussions;
    }
}

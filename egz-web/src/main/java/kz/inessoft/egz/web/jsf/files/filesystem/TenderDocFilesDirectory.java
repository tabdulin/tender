package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TDocument;
import kz.inessoft.egz.dbentities.TDocumentCrossTPortalFile;
import kz.inessoft.egz.dbentities.TPortalFile;
import kz.inessoft.egz.ejbapi.FileUtils;
import kz.inessoft.egz.web.jsf.JSFParams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 04.10.16.
 */
public class TenderDocFilesDirectory implements IDirectoryInfo {
    private TDocument document;
    private IDataLoader dataLoader;
    private TenderDocsDirectory parent;
    private List<IFileInfo> children;

    public TenderDocFilesDirectory(long documentId, IDataLoader dataLoader) {
        this.document = dataLoader.getDocumentLogic().getDocument(documentId, false);
        this.dataLoader = dataLoader;
    }

    public TenderDocFilesDirectory(TDocument document, IDataLoader dataLoader, TenderDocsDirectory parent) {
        this.document = document;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return FileUtils.makeCorrectFileName(document.getDocumentation().getNameRu());
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        return Collections.emptyList();
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        if (children == null) {
            children = new ArrayList<>();
            TDocument document = dataLoader.getDocumentLogic().getDocument(this.document.getId(), true);
            for (TDocumentCrossTPortalFile documentCrossTPortalFile : document.getPortalFiles()) {
                TPortalFile portalFile = documentCrossTPortalFile.getId().getPortalFile();
                IFileInfo fileInfo = new RegularDownloadFile(portalFile, dataLoader);
                children.add(fileInfo);
            }
        }
        return children;
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null)
            parent = new TenderDocsDirectory(document.getAnnouncement(), dataLoader);
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        Map<String, Object> retVal = new HashMap<>();
        retVal.put(JSFParams.TDOCUMENT_ID, document.getId());
        return retVal;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.TenderDocumentFilesList;
    }
}

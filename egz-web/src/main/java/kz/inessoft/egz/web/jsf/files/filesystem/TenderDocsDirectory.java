package kz.inessoft.egz.web.jsf.files.filesystem;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TDocument;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 03.10.16.
 */
public class TenderDocsDirectory implements IDirectoryInfo {
    private TAnnouncement announcement;
    private IDataLoader dataLoader;
    private List<IDirectoryInfo> children;
    private RootDirectory parent;

    public TenderDocsDirectory(TAnnouncement announcement, IDataLoader dataLoader) {
        this(announcement, dataLoader, null);
    }

    public TenderDocsDirectory(TAnnouncement announcement, IDataLoader dataLoader, RootDirectory parent) {
        this.announcement = announcement;
        this.dataLoader = dataLoader;
        this.parent = parent;
    }

    @Override
    public String getName() {
        return "Конкурсная документация";
    }

    @Override
    public List<IDirectoryInfo> getChildrenDirectories() {
        if (children == null) {
            children = new ArrayList<>();
            List<TDocument> documentsWithFiles = dataLoader.getDocumentLogic().getDocumentsWithFiles(announcement.getId());
            for (TDocument document : documentsWithFiles) {
                children.add(new TenderDocFilesDirectory(document, dataLoader, this));
            }
        }
        return children;
    }

    @Override
    public List<IFileInfo> getChildrenFiles() {
        return Collections.emptyList();
    }

    @Override
    public IDirectoryInfo getParentDirectory() {
        if (parent == null) {
            parent = new RootDirectory(announcement, dataLoader);
        }
        return parent;
    }

    @Override
    public Map<String, Object> getUrlParams() {
        return null;
    }

    @Override
    public EDirectoryType getDirectoryType() {
        return EDirectoryType.TenderDocumentsList;
    }
}

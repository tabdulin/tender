package kz.inessoft.egz.web.jsf.search;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus;
import kz.inessoft.egz.dbentities.DicGovOrganization;
import kz.inessoft.egz.dbentities.DicImplementer;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejbapi.AnnouncementFilter;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.DicAnnouncementStatusLogic;
import kz.inessoft.egz.ejbapi.DicGovOrganizationLogic;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 25.10.16.
 */
@ManagedBean(name = "searchAnnouncementBean")
@SessionScoped
public class SearchAnnouncementBean implements Serializable {
    private static final int ROWS_PER_PAGE = 20;
    private static final int PAGES_IN_PAGER = 21;
    private AnnouncementFilter filter = new AnnouncementFilter();
    private List<TAnnouncement> announcements;
    private int totalPages;
    private int currentPage;
    private DicGovOrganization customer;
    private DicImplementer implementer;

    @EJB
    private DicAnnouncementStatusLogic announcementStatusLogic;
    private List<SelectItem> announcementStatuses;

    @EJB
    private DicGovOrganizationLogic govOrganizationLogic;

    @EJB
    private DicImplementerLogic implementerLogic;

    @EJB
    private AnnouncementLogic announcementLogic;

    public AnnouncementFilter getFilter() {
        return filter;
    }

    public List<SelectItem> getAnnouncementStatuses() {
        if (announcementStatuses == null) {
            List<DicAnnouncementStatus> statuses = announcementStatusLogic.getAnnouncementStatuses();
            announcementStatuses = new ArrayList<>();
            for (DicAnnouncementStatus status : statuses) {
                announcementStatuses.add(new SelectItem(status.getId(), status.getNameRu()));
            }
        }
        return announcementStatuses;
    }

    public void findIt() {
        currentPage = 1;
        long announcementsCount = announcementLogic.getAnnouncementsCount(filter);
        if (announcementsCount == 0) {
            totalPages = 0;
            announcements = null;
            return;
        }
        totalPages = (int) (announcementsCount / ROWS_PER_PAGE + (announcementsCount % ROWS_PER_PAGE > 0 ? 1 : 0));
        initList();
    }

    private void initList() {
        announcements = announcementLogic.findAnnouncements(filter, ROWS_PER_PAGE, (currentPage - 1) * ROWS_PER_PAGE);
    }

    public List<Integer> getPages() {
        int minPage = Integer.max(1, currentPage - (PAGES_IN_PAGER - 1)/2);
        int maxPage = Integer.min(totalPages, currentPage + (PAGES_IN_PAGER - 1)/2);
        if (maxPage - minPage != PAGES_IN_PAGER - 1) {
            if (maxPage == totalPages)
                minPage = Integer.max(1, maxPage - PAGES_IN_PAGER + 1);
            else if (minPage == 1)
                maxPage = Integer.min(totalPages, minPage + PAGES_IN_PAGER - 1);
        }
        List<Integer> retVal = new LinkedList<>();
        for (int i = minPage; i <= maxPage; i++) {
            retVal.add(i);
        }
        return retVal;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
        initList();
    }

    public List<TAnnouncement> getAnnouncements() {
        return announcements;
    }

    public String getCustomerName() {
        if (filter.getCustomerId() == null)
            return null;
        if (customer == null || !filter.getCustomerId().equals(customer.getId())) {
            customer = govOrganizationLogic.getById(filter.getCustomerId());
        }
        return customer.getBin() + " " + customer.getNameRu();
    }

    public String getImplementerName() {
        if (filter.getImplementerId() == null)
            return null;
        if (implementer == null || !filter.getImplementerId().equals(implementer.getId())) {
            implementer = implementerLogic.getValueById(filter.getImplementerId());
        }
        return implementer.getXin() + " " + implementer.getNameRu();
    }

    public void setSortColumn(AnnouncementFilter.ESortColumn sortColumn) {
        if (filter.getSortColumn() == null || filter.getSortColumn() != sortColumn) {
            filter.setSortColumn(sortColumn);
            filter.setAsc(true);
        } else if (filter.isAsc())
            filter.setAsc(false);
        else
            filter.setSortColumn(null);
    }

    public void sortIt() {
        initList();
    }
}

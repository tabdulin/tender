package kz.inessoft.egz.web.jsf.search;

import kz.inessoft.egz.dbentities.DicGovOrganization;
import kz.inessoft.egz.dbentities.DicImplementer;
import kz.inessoft.egz.dbentities.DicLotStatus;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.ejbapi.DicGovOrganizationLogic;
import kz.inessoft.egz.ejbapi.DicImplementerLogic;
import kz.inessoft.egz.ejbapi.DicLotStatusLogic;
import kz.inessoft.egz.ejbapi.LotFilter;
import kz.inessoft.egz.ejbapi.LotLogic;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 26.10.16.
 */
@ManagedBean(name = "searchLotBean")
@SessionScoped
public class SearchLotBean {
    private static final int ROWS_PER_PAGE = 20;
    private static final int PAGES_IN_PAGER = 21;
    private LotFilter filter = new LotFilter();
    private List<TLotCrossAnnouncement> lots;
    private int totalPages;
    private int currentPage;
    private DicGovOrganization customer;
    private DicImplementer implementer;
    private LotFilter.EOrderByColumn selectedSortColumn;

    @EJB
    private DicLotStatusLogic dicLotStatusLogic;
    private List<SelectItem> lotStatuses;

    @EJB
    private DicGovOrganizationLogic govOrganizationLogic;

    @EJB
    private DicImplementerLogic implementerLogic;

    @EJB
    private LotLogic lotLogic;

    public LotFilter getFilter() {
        return filter;
    }

    public String getLotName() {
        if (filter.getKeywords() == null || filter.getKeywords().isEmpty())
            return null;
        return StringUtils.join(filter.getKeywords().toArray(), ' ');
    }

    public void setLotName(String lotName) {
        List<String> keywords = filter.getKeywords();
        if (keywords == null) {
            keywords = new ArrayList<>();
            filter.setKeywords(keywords);
        }
        keywords.clear();
        if (lotName == null) {
            return;
        }
        lotName = lotName.trim();
        if (lotName.isEmpty()) {
            return;
        }
        while (lotName.contains("  "))
            lotName = lotName.replaceAll("  ", " ");
        String[] words = lotName.split(" ");
        Collections.addAll(keywords, words);
    }

    public List<SelectItem> getLotStatuses() {
        if (lotStatuses == null) {
            List<DicLotStatus> lotStatuses = dicLotStatusLogic.getLotStatuses();
            this.lotStatuses = new ArrayList<>();
            for (DicLotStatus lotStatus : lotStatuses) {
                this.lotStatuses.add(new SelectItem(lotStatus.getId(), lotStatus.getNameRu()));
            }
        }
        return lotStatuses;
    }

    public String getTruCode() {
        if (filter.getTruCodesMask() == null || filter.getTruCodesMask().isEmpty())
            return null;
        return filter.getTruCodesMask().get(0);
    }

    public void setTruCode(String truCode) {
        if (filter.getTruCodesMask() == null)
            filter.setTruCodesMask(new ArrayList<>());
        filter.getTruCodesMask().clear();
        if (truCode != null) {
            truCode = truCode.trim();
            if (!truCode.isEmpty())
                filter.getTruCodesMask().add(truCode);
        }
    }

    public void findIt() {
        currentPage = 1;
        long lotsCount = lotLogic.countLots(filter);
        if (lotsCount == 0) {
            totalPages = 0;
            lots = null;
            return;
        }
        totalPages = (int) (lotsCount / ROWS_PER_PAGE + (lotsCount % ROWS_PER_PAGE > 0 ? 1 : 0));
        initList();
    }

    private void initList() {
        lots = lotLogic.getLots(filter, ROWS_PER_PAGE, (currentPage - 1) * ROWS_PER_PAGE);
    }

    public String getCustomerName() {
        if (filter.getCustomerId() == null)
            return null;
        if (customer == null || !filter.getCustomerId().equals(customer.getId())) {
            customer = govOrganizationLogic.getById(filter.getCustomerId());
        }
        return customer.getBin() + " " + customer.getNameRu();
    }

    public String getImplementerName() {
        if (filter.getImplementerId() == null)
            return null;
        if (implementer == null || !filter.getImplementerId().equals(implementer.getId())) {
            implementer = implementerLogic.getValueById(filter.getImplementerId());
        }
        return implementer.getXin() + " " + implementer.getNameRu();
    }

    public List<Integer> getPages() {
        int minPage = Integer.max(1, currentPage - (PAGES_IN_PAGER - 1) / 2);
        int maxPage = Integer.min(totalPages, currentPage + (PAGES_IN_PAGER - 1) / 2);
        if (maxPage - minPage != PAGES_IN_PAGER - 1) {
            if (maxPage == totalPages)
                minPage = Integer.max(1, maxPage - PAGES_IN_PAGER + 1);
            else if (minPage == 1)
                maxPage = Integer.min(totalPages, minPage + PAGES_IN_PAGER - 1);
        }
        List<Integer> retVal = new LinkedList<>();
        for (int i = minPage; i <= maxPage; i++) {
            retVal.add(i);
        }
        return retVal;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
        initList();
    }

    public List<TLotCrossAnnouncement> getLots() {
        return lots;
    }

    public void setSelectedSortColumn(LotFilter.EOrderByColumn selectedSortColumn) {
        this.selectedSortColumn = selectedSortColumn;
    }

    public LotFilter.EOrderByColumn getSortColumn() {
        List<LotFilter.OrderBy> orderByList = filter.getOrderByList();
        if (orderByList == null || orderByList.isEmpty())
            return null;
        return orderByList.get(0).getOrderByColumn();
    }

    public boolean getSortOrder() {
        return filter.getOrderByList().get(0).isAsc();
    }

    public void switchSort() {
        List<LotFilter.OrderBy> orderByList = filter.getOrderByList();
        if (orderByList == null) {
            orderByList = new ArrayList<>(1);
            filter.setOrderByList(orderByList);
        }
        if (orderByList.isEmpty()) {
            orderByList.add(new LotFilter.OrderBy(selectedSortColumn, true));
            initList();
        } else {
            LotFilter.OrderBy orderBy = orderByList.get(0);
            orderByList.clear();
            if (orderBy.getOrderByColumn() != selectedSortColumn) {
                orderByList.add(new LotFilter.OrderBy(selectedSortColumn, true));
            } else {
                if (orderBy.isAsc()) {
                    orderByList.add(new LotFilter.OrderBy(selectedSortColumn, false));
                }
            }
        }
        initList();
    }
}

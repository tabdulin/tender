package kz.inessoft.egz.web.jsf.viewannounce;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.DicParticipationTemplate;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TCrossAnnouncementParticipant;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.AnnouncementMonitoringInfo;
import kz.inessoft.egz.ejbapi.AnnouncementSaveException;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import kz.inessoft.egz.web.jsf.JSFFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 05.09.16.
 */
@ManagedBean(name = "viewAnnounceBean")
@SessionScoped
public class ViewAnnounceBean implements Serializable {
    public static class Participation implements Serializable {
        private DicParticipantCompany company;
        private long templateId = 0;

        public DicParticipantCompany getCompany() {
            return company;
        }

        public void setCompany(DicParticipantCompany company) {
            this.company = company;
        }

        public long getTemplateId() {
            return templateId;
        }

        public void setTemplateId(long templateId) {
            this.templateId = templateId;
        }

        public List<SelectItem> getSelectItems() {
            List<SelectItem> retVal = new LinkedList<>();
            for (DicParticipationTemplate template : company.getTemplates()) {
                retVal.add(new SelectItem(template.getId(), template.getNameRu()));
            }
            return retVal;
        }
    }

    private static final String SAVED_TEXT = ViewAnnounceBean.class.getName() + ".SavedMonitoring";
    private static final String ERROR_TEXT = ViewAnnounceBean.class.getName() + ".Error";
    private static final Logger LOGGER = LoggerFactory.getLogger(ViewAnnounceBean.class);

    @EJB
    private AnnouncementLogic announcementLogic;

    @EJB
    private DicParticipantCompanyLogic dicParticipantCompanyLogic;

    private Long announcePortalId;
    private TAnnouncement announcement;
    private List<DicParticipantCompany> participantCompanies;
    private List<Participation> participations;

    public Long getAnnouncePortalId() {
        return announcePortalId;
    }

    public void setAnnouncePortalId(Long announcePortalId) {
        this.announcePortalId = announcePortalId;
        if (announcement != null && !announcement.getPortalId().equals(announcePortalId))
            clearCache();
    }

    private void clearCache() {
        announcement = null;
        participantCompanies = null;
        participations = null;
    }

    public TAnnouncement getAnnouncement() {
        if (announcement == null) {
            announcement = announcementLogic.getAnnouncementByPortalId(announcePortalId, true);
        }
        if (announcement == null) {
            FacesContext.getCurrentInstance().getExternalContext().setResponseStatus(404);
            FacesContext.getCurrentInstance().responseComplete();
            return null;
        } else
            return announcement;
    }

    public String getAnnouncementOriginalURL() {
        return JSFFunctions.getAnnouncementOriginalURL(announcePortalId);
    }

    private List<DicParticipantCompany> getParticipantCompanies() {
        if (participantCompanies == null) {
            participantCompanies = dicParticipantCompanyLogic.getDicParticipantCompanies();
        }
        return participantCompanies;
    }

    public List<Participation> getParticipations() {
        if (participations == null) {
            participations = new LinkedList<>();
            for (DicParticipantCompany company : getParticipantCompanies()) {
                Participation participation = new Participation();
                participation.setCompany(company);
                for (TCrossAnnouncementParticipant announcementParticipant : getAnnouncement().getCrossAnnouncementParticipants()) {
                    if (announcementParticipant.getId().getDicParticipantCompany().equals(company))
                        participation.setTemplateId(announcementParticipant.getDicParticipationTemplate().getId());
                }
                participations.add(participation);
            }
        }
        return participations;
    }

    public void saveMonitoringInfo() {
        AnnouncementMonitoringInfo monitoringInfo = new AnnouncementMonitoringInfo();
        monitoringInfo.setAnnoucementId(announcement.getId());
        monitoringInfo.setMonitorIt(announcement.isMonitoring());
        for (Participation participation : getParticipations()) {
            if (participation.getTemplateId() != 0)
                monitoringInfo.getParticipations().add(new AnnouncementMonitoringInfo.Participation(participation.getCompany().getId(), participation.getTemplateId()));
        }
        try {
            announcementLogic.setMonitoring(monitoringInfo);
        } catch (AnnouncementSaveException e) {
            LOGGER.error("Error setting announcement to monitoring", e);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(ERROR_TEXT, "При постановке объявления на мониторинг возникли проблемы. Необходим анализ лог-файлов.");
        }
        clearCache();
        String text;
        if (monitoringInfo.isMonitorIt())
            text = "Объявление успешно поставлено на мониторинг.";
        else
            text = "Объявление успешно снято с мониторинга";
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(SAVED_TEXT, text);
    }

    public String getMonitoringStatus() {
        return (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(SAVED_TEXT);
    }

    public String getErrorMsg() {
        return (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(ERROR_TEXT);
    }
}

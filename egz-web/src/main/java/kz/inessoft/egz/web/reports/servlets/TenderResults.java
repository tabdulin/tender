package kz.inessoft.egz.web.reports.servlets;

import kz.inessoft.egz.ejbapi.reports.TendersResultsLogic;
import org.apache.http.HttpHeaders;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by alexey on 26.05.17.
 */
@WebServlet("/reports/tendersResults/*")
public class TenderResults extends HttpServlet {
    @EJB
    private TendersResultsLogic tendersResultsLogic;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] path = req.getPathInfo().split("/");
        resp.setHeader("Content-Disposition", "attachment; filename=\"tendersResult.xlsx\"");
        resp.setHeader(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
        if ("it".equals(path[path.length - 1]))
            tendersResultsLogic.getITExcellTendersResults(resp.getOutputStream());
        else
            tendersResultsLogic.getTranslateExcellTendersResults(resp.getOutputStream());
    }
}

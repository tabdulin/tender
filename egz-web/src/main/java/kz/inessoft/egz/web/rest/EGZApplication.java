package kz.inessoft.egz.web.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by alexey on 03.08.16.
 */
@ApplicationPath("/rest")
public class EGZApplication extends Application {
}

package kz.inessoft.egz.web.rest;

import kz.inessoft.egz.ejbapi.FullTextSearch;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by alexey on 25.10.16.
 */
@Path("/fullTextSearch")
public class FullTextSearchResource {
    @EJB
    private FullTextSearch fullTextSearch;

    @GET
    @Path("/reindex")
    @Produces("application/json")
    public Response reindex() {
        try {
            fullTextSearch.reindexData();
            return Response.ok().build();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }
}

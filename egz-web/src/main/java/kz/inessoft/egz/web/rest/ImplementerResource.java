package kz.inessoft.egz.web.rest;

import kz.inessoft.egz.ejbapi.DicImplementerLogic;

import javax.ejb.EJB;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by alexey on 25.10.16.
 */
@Path("/implementer")
public class ImplementerResource {
    @EJB
    private DicImplementerLogic implementerLogic;

    @POST
    @Path("/find")
    @Produces("application/json")
    public Response downloadAnnouncement(@FormParam("filter") String filter) {
        return Response.ok(implementerLogic.getFirst10ImplementersByFilter(filter)).build();
    }
}

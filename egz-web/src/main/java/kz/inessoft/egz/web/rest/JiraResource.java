package kz.inessoft.egz.web.rest;

import kz.inessoft.egz.ejbapi.JiraService;

import javax.ejb.EJB;
import javax.jms.JMSException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by alexey on 30.01.17.
 */
@Path("/jira")
public class JiraResource {
    @EJB
    private JiraService jiraService;

    @GET
    @Path("/queue/{id}")
    @Produces("application/json")
    public Response queue(@PathParam("id") Long id) {
        try {
            jiraService.createOrUpdateIssue(id);
            return Response.ok().build();
        } catch (JMSException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/queueMonitored")
    @Produces("application/json")
    public Response queueMonitored() {
        try {
            jiraService.registerMonitored();
            return Response.ok().build();
        } catch (JMSException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }
}

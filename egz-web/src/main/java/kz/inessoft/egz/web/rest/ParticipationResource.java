package kz.inessoft.egz.web.rest;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import kz.inessoft.egz.web.rest.dto.RestParticipation;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 07.03.17.
 */
@Path("/participation")
public class ParticipationResource {
    @EJB
    private DicParticipantCompanyLogic dicParticipantCompanyLogic;

    @GET
    @Path("/listParticipations")
    @Produces("application/json")
    public Response getParticipations() {
        List<DicParticipantCompany> participantCompanies = dicParticipantCompanyLogic.getDicParticipantCompanies();
        List<RestParticipation> retVal = new LinkedList<>();
        for (DicParticipantCompany company : participantCompanies) {
            retVal.add(new RestParticipation(company));
        }
        return Response.ok(retVal).build();
    }
}

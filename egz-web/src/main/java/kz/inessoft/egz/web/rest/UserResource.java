package kz.inessoft.egz.web.rest;

import kz.inessoft.egz.web.UserRoles;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 02.03.17.
 */
@Path("/user")
public class UserResource {
    @GET
    @Path("/userRoles")
    @Produces("application/json")
    public Response queue(@Context SecurityContext securityContext) {
        List<String> retVal = new LinkedList<>();
        if (securityContext.isUserInRole(UserRoles.MAKE_PROPOSAL))
            retVal.add(UserRoles.MAKE_PROPOSAL);
        return Response.ok(retVal).build();
    }
}

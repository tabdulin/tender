package kz.inessoft.egz.web.rest.announcement;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.ejbapi.AnnouncementLogic;
import kz.inessoft.egz.ejbapi.DicParticipantCompanyLogic;
import kz.inessoft.egz.web.rest.dto.RestAnnouncement;

import javax.ejb.EJB;
import javax.jms.JMSException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by alexey on 03.08.16.
 */
@Path("/announcement")
public class AnnouncementResource {
    @EJB
    private AnnouncementLogic announcementLogic;

    @EJB
    private DicParticipantCompanyLogic participantCompanyLogic;

    @GET
    @Path("/download/{id}")
    @Produces("application/json")
    public Response downloadAnnouncement(@PathParam("id") Long announcementId) {
        try {
            announcementLogic.loadAnnouncement(announcementId);
            return Response.ok().build();
        } catch (JMSException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/downloadNew")
    @Produces("application/json")
    public Response downloadNewAnnouncements() {
        announcementLogic.loadAllAnnouncements();
        return Response.ok().build();
    }

    @GET
    @Path("/refreshInteresting")
    @Produces("application/json")
    public Response refreshInterestingAnnouncements() {
        announcementLogic.refreshInterestingAnnouncements();
        return Response.ok().build();
    }

    @GET
    @Path("/byPortalId/{portalId}")
    @Produces("application/json")
    public Response getAnnouncementInfoByPortalId(@PathParam("portalId") Long portalId) {
        TAnnouncement announcement = announcementLogic.getAnnouncementByPortalId(portalId, true);
        return Response.ok(new RestAnnouncement(announcement)).build();
    }
}
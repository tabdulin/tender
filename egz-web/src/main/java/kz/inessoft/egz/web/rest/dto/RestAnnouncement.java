package kz.inessoft.egz.web.rest.dto;

import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TCrossAnnouncementParticipant;

import java.util.Date;
import java.util.List;

/**
 * Created by alexey on 06.03.17.
 */
public class RestAnnouncement {
    private String number;
    private String nameRu;
    private RestAnnouncementStatus announcementStatus;
    private Date publicationDate;
    private Date loadTime;
    private boolean monitoring;
    private List<RestAnnouncementParticipation> participations;

    public RestAnnouncement(TAnnouncement announcement) {
        number = announcement.getNumber();
        nameRu = announcement.getNameRu();
        announcementStatus = new RestAnnouncementStatus(announcement.getAnnouncementStatus());
        publicationDate = announcement.getPublicationDate();
        loadTime = announcement.getLoadTime();
        monitoring = announcement.isMonitoring();
        if (announcement.getCrossAnnouncementParticipants() != null)
            for (TCrossAnnouncementParticipant crossAnnouncementParticipant : announcement.getCrossAnnouncementParticipants()) {

            }
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public RestAnnouncementStatus getAnnouncementStatus() {
        return announcementStatus;
    }

    public void setAnnouncementStatus(RestAnnouncementStatus announcementStatus) {
        this.announcementStatus = announcementStatus;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(Date loadTime) {
        this.loadTime = loadTime;
    }

    public boolean isMonitoring() {
        return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
        this.monitoring = monitoring;
    }

    public List<RestAnnouncementParticipation> getParticipations() {
        return participations;
    }

    public void setParticipations(List<RestAnnouncementParticipation> participations) {
        this.participations = participations;
    }
}

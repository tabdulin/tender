package kz.inessoft.egz.web.rest.dto;

import kz.inessoft.egz.dbentities.TCrossAnnouncementParticipant;

/**
 * Created by alexey on 07.03.17.
 */
public class RestAnnouncementParticipation {
    private Long participantId;
    private RestParticipationTemplate participationTemplate;

    public RestAnnouncementParticipation(TCrossAnnouncementParticipant crossAnnouncementParticipant) {
        participantId = crossAnnouncementParticipant.getId().getDicParticipantCompany().getId();
        participationTemplate = new RestParticipationTemplate(crossAnnouncementParticipant.getDicParticipationTemplate());
    }

    public Long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Long participantId) {
        this.participantId = participantId;
    }

    public RestParticipationTemplate getParticipationTemplate() {
        return participationTemplate;
    }

    public void setParticipationTemplate(RestParticipationTemplate participationTemplate) {
        this.participationTemplate = participationTemplate;
    }
}

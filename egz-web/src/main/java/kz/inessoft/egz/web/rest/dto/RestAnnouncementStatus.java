package kz.inessoft.egz.web.rest.dto;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus;

/**
 * Created by alexey on 06.03.17.
 */
public class RestAnnouncementStatus {
    private String nameRu;

    public RestAnnouncementStatus(DicAnnouncementStatus announcementStatus) {
        nameRu = announcementStatus.getNameRu();
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }
}

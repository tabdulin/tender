package kz.inessoft.egz.web.rest.dto;

import kz.inessoft.egz.dbentities.DicParticipantCompany;

/**
 * Created by alexey on 07.03.17.
 */
public class RestDicParticipant {
    private Long id;
    private String nameRu;

    public RestDicParticipant(DicParticipantCompany company) {
        id = company.getId();
        nameRu = company.getNameRu();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }
}

package kz.inessoft.egz.web.rest.dto;

import kz.inessoft.egz.dbentities.DicParticipantCompany;
import kz.inessoft.egz.dbentities.DicParticipationTemplate;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey on 06.03.17.
 */
public class RestParticipation {
    private RestDicParticipant participant;
    private Long activeTemplateId;
    private List<RestParticipationTemplate> templates;

    public RestParticipation(DicParticipantCompany company) {
        participant = new RestDicParticipant(company);
        templates = new LinkedList<>();
        for (DicParticipationTemplate participationTemplate : company.getTemplates()) {
            templates.add(new RestParticipationTemplate(participationTemplate));
        }
    }

    public RestDicParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(RestDicParticipant participant) {
        this.participant = participant;
    }

    public Long getActiveTemplateId() {
        return activeTemplateId;
    }

    public void setActiveTemplateId(Long activeTemplateId) {
        this.activeTemplateId = activeTemplateId;
    }

    public List<RestParticipationTemplate> getTemplates() {
        return templates;
    }

    public void setTemplates(List<RestParticipationTemplate> templates) {
        this.templates = templates;
    }
}

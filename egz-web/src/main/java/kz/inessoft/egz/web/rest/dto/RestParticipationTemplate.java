package kz.inessoft.egz.web.rest.dto;

import kz.inessoft.egz.dbentities.DicParticipationTemplate;

/**
 * Created by alexey on 07.03.17.
 */
public class RestParticipationTemplate {
    private Long id;
    private String nameRu;

    public RestParticipationTemplate(DicParticipationTemplate participationTemplate) {
        id = participationTemplate.getId();
        nameRu = participationTemplate.getNameRu();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }
}

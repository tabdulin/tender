package kz.inessoft.egz.web.rest.lots;

import kz.inessoft.egz.dbentities.DicAnnouncementStatus;
import kz.inessoft.egz.dbentities.TAnnouncement;
import kz.inessoft.egz.dbentities.TLot;
import kz.inessoft.egz.dbentities.TLotCrossAnnouncement;
import kz.inessoft.egz.ejbapi.DicAnnouncementStatusLogic;
import kz.inessoft.egz.ejbapi.LotFilter;
import kz.inessoft.egz.ejbapi.LotLogic;
import kz.inessoft.egz.web.rest.lots.dto.LastLotsItem;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alexey on 27.03.17.
 */
@Path("/lot")
public class LotResource {
    @EJB
    private LotLogic lotLogic;

    @EJB
    private DicAnnouncementStatusLogic announcementStatusLogic;

    @GET
    @Path("/itLastLots")
    @Produces("application/json")
    public Response getItLastLots() {
        List<String> truCodes = new LinkedList<>();
        truCodes.add("62");
        truCodes.add("63");
        return Response.ok(getLotsItems(truCodes)).build();
    }


    @GET
    @Path("/translateLastLots")
    @Produces("application/json")
    public Response getTranslateLastLots() {
        List<String> truCodes = new LinkedList<>();
        truCodes.add("74.30.");
        return Response.ok(getLotsItems(truCodes)).build();
    }

    private List<LastLotsItem> getLotsItems(List<String> truCodes) {
        LotFilter filter = new LotFilter();
        filter.setNotBefore(new Date(System.currentTimeMillis() - 40L * 24L * 60L * 60L * 1000L));
        LinkedList<DicAnnouncementStatus> statuses = new LinkedList<>();
        statuses.add(announcementStatusLogic.getPublishedStatus());
        statuses.add(announcementStatusLogic.getPublishedRecievingStatus());
        statuses.add(announcementStatusLogic.getDocumentationChangedStatus());
        filter.setStatuses(statuses);
        filter.setTruCodesMask(truCodes);
        LinkedList<LotFilter.OrderBy> orderByList = new LinkedList<>();
        filter.setOrderByList(orderByList);
        List<TLotCrossAnnouncement> lots = lotLogic.getLots(filter, null, null);
        return lots.stream().map(lotInfo -> {
            LastLotsItem retVal = new LastLotsItem();
            TAnnouncement tAnnouncement = lotInfo.getId().getAnnouncement();
            retVal.setAnnouncementNumber(tAnnouncement.getNumber());
            TLot lot = lotInfo.getId().getLot();
            retVal.setAnnouncementPortalId(tAnnouncement.getPortalId());
            retVal.setCustomer(lot.getCustomer().getNameRu());
            retVal.setAnnouncementName(tAnnouncement.getNameRu());
            retVal.setStatus(tAnnouncement.getAnnouncementStatus().getNameRu());
            retVal.setLotNumber(lotInfo.getLotNumber());
            retVal.setPublicationDate(tAnnouncement.getPublicationDate());
            retVal.setStartRecvDate(tAnnouncement.getStartRecvTime());
            retVal.setEndRecvDate(tAnnouncement.getEndRecvTime());
            retVal.setShortCharacteristic(lot.getShortCharacteristicRu());
            retVal.setAdditionalCharacteristic(lot.getAdditionalCharacteristicRu());
            retVal.setAmount(lot.getPriceForUnit());
            retVal.setMonitoring(tAnnouncement.isMonitoring());
            return retVal;
        }).collect(Collectors.toList());
    }
}

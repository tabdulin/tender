package kz.inessoft.egz.web.rest.lots.dto;

import java.util.Date;

/**
 * Created by alexey on 27.03.17.
 * Данные о лоте, отображаемые на странице "Последние лоты за N дней в категории M"
 */
public class LastLotsItem {
    private Long announcementPortalId;
    private String announcementNumber;
    private String customer;
    private String announcementName;
    private String status;
    private String lotNumber;
    private Date publicationDate;
    private Date startRecvDate;
    private Date endRecvDate;
    private String shortCharacteristic;
    private String additionalCharacteristic;
    private double amount;
    private boolean monitoring;

    public Long getAnnouncementPortalId() {
        return announcementPortalId;
    }

    public void setAnnouncementPortalId(Long announcementPortalId) {
        this.announcementPortalId = announcementPortalId;
    }

    public String getAnnouncementNumber() {
        return announcementNumber;
    }

    public void setAnnouncementNumber(String announcementNumber) {
        this.announcementNumber = announcementNumber;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getAnnouncementName() {
        return announcementName;
    }

    public void setAnnouncementName(String announcementName) {
        this.announcementName = announcementName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getStartRecvDate() {
        return startRecvDate;
    }

    public void setStartRecvDate(Date startRecvDate) {
        this.startRecvDate = startRecvDate;
    }

    public Date getEndRecvDate() {
        return endRecvDate;
    }

    public void setEndRecvDate(Date endRecvDate) {
        this.endRecvDate = endRecvDate;
    }

    public String getShortCharacteristic() {
        return shortCharacteristic;
    }

    public void setShortCharacteristic(String shortCharacteristic) {
        this.shortCharacteristic = shortCharacteristic;
    }

    public String getAdditionalCharacteristic() {
        return additionalCharacteristic;
    }

    public void setAdditionalCharacteristic(String additionalCharacteristic) {
        this.additionalCharacteristic = additionalCharacteristic;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isMonitoring() {
        return monitoring;
    }

    public void setMonitoring(boolean monitoring) {
        this.monitoring = monitoring;
    }
}

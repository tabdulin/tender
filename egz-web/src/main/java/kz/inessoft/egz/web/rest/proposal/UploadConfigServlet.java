package kz.inessoft.egz.web.rest.proposal;

import kz.inessoft.egz.ejbapi.ProposalLogic;
import kz.inessoft.egz.ejbapi.ProposalLogicException;
import kz.inessoft.egz.web.jsf.JSFParams;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by alexey on 23.02.17.
 */
@WebServlet("/proposal/uploadConfig")
@MultipartConfig
public class UploadConfigServlet extends HttpServlet {
    @EJB
    private ProposalLogic proposalLogic;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String participantId = req.getParameter(JSFParams.PARTICIPANT_ID);
        String announcementId = req.getParameter(JSFParams.ANNOUNCE_ID);
        Part config = req.getPart("config");
        InputStream inputStream = config.getInputStream();
        try {
            proposalLogic.makeProposal(Long.valueOf(participantId), Long.valueOf(announcementId), inputStream);
        } catch (ProposalLogicException e) {
            e.printStackTrace();
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}

'use strict';

const URLConstants = function () {
    let URLContext = "/egz";
    let RestContext = URLContext + "/rest";
    let AnnouncementResourceContext = RestContext + "/announcement";
    let ParticipationsResourceContext = RestContext + "/participation";
    return {
        URLContext: URLContext,
        RestContext: RestContext,
        AnnouncementResourceContext: AnnouncementResourceContext,
        ParticipationsResourceContext: ParticipationsResourceContext
    }
}();

export {URLConstants};
'use strict';

const QueryString = function () {
    // This function is anonymous, is executed immediately and 
    // the return value is assigned to QueryString!
    let query_string = {};
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            query_string[pair[0]] = [query_string[pair[0]], decodeURIComponent(pair[1])];
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

// Дополняет незначащими нулями до получения указанного размера строки size символов
function append0(data, size) {
    data = '' + data;
    if (data.length >= size)
        return data;
    return '000'.substr(0, size - data.length) + data;
}

const DateUtils = {
    formatDateWithTime: function (timeInMillis) {
        let date = new Date(timeInMillis);
        return append0(date.getDate(), 2) + "." + append0((date.getMonth() + 1), 2) + "." + date.getFullYear() + " " +
            append0(date.getHours(), 2) + ":" + append0(date.getMinutes(), 2) + ":" + append0(date.getSeconds(), 2) + "." + append0(date.getMilliseconds(), 3);
    }
};


export {QueryString, DateUtils};
'use strict';

import AppContainer from './viewannouncement/AppContainer';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<AppContainer/>, document.getElementById('divViewAnnoucement'));
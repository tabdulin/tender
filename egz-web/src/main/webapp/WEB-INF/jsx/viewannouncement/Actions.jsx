'use strict';

import ActionTypes from './ActionTypes';
import Dispatcher from './Dispatcher';

const Actions = {
    announcementLoaded(announcement) {
        Dispatcher.dispatch({
            type: ActionTypes.ANNOUNCEMENT_LOAD_COMPLETE,
            announcement: announcement
        });
    },

    participationsLoaded(participations) {
        Dispatcher.dispatch({
            type: ActionTypes.PARTICIPATIONS_LOAD_COMPLETE,
            participations: participations
        });
    },

    selectParticipationPerformed(participantId, template, announcement, participationsList) {
        Dispatcher.dispatch({
            type: ActionTypes.SELECT_PARTICIPATION_PERFORMED,
            participantId: participantId,
            template: template,
            announcement: announcement,
            participationsList: participationsList
        })
    }
};

export default Actions;
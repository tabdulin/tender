'use strict';

import ActionTypes from "./ActionTypes";
import Dispatcher from "./Dispatcher";
import {ReduceStore} from 'flux/utils';

class AnnouncementStore extends ReduceStore {
    constructor() {
        super(Dispatcher);
    }

    getInitialState() {
        return null;
    }

    reduce(state, action) {
        switch (action.type) {
            case ActionTypes.ANNOUNCEMENT_LOAD_COMPLETE:
                return action.announcement;
            case ActionTypes.SELECT_PARTICIPATION_PERFORMED:
                let announcement = action.announcement;
                if (action.template.id === 0) {
                    announcement.participations = announcement.participations.filter(participation => {
                       return participation.participantId !== action.participantId;
                    });
                    return announcement;
                }
                announcement.participations = announcement.participations || [];
                let participation = announcement.participations.find(participation => participation.participantId === action.participantId);
                if (!participation) {
                    participation = {
                        participantId: action.participantId
                    };
                    announcement.participations.push(participation);
                }
                participation.participationTemplate = action.template;
                return announcement;
            default:
                return state;
        }
    }


}

export default new AnnouncementStore();
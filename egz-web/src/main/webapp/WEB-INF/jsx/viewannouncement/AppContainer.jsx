'use strict';

import View from './View';
import {Container} from 'flux/utils';
import AnnouncementStore from "./AnnouncementStore"
import ParticipationsStore from "./ParticipationsStore"
import Actions from "./Actions";
import {URLConstants} from "../Constants";
import {QueryString} from "../Utils";

function getStores() {
    return [AnnouncementStore, ParticipationsStore];
}

function getState() {
    return {
        announcement: AnnouncementStore.getState(),
        participations: ParticipationsStore.getState(),
        selectParticipationPerformed: Actions.selectParticipationPerformed
    }
}

fetch(URLConstants.AnnouncementResourceContext + "/byPortalId/" + QueryString.announcementPortalId, {
    credentials: 'include'
}).then(function (response) {
    return response.json();
}).then(function (data) {
    Actions.announcementLoaded(data);
});

fetch(URLConstants.ParticipationsResourceContext + "/listParticipations", {
    credentials: 'include'
}).then(function (response) {
    return response.json();
}).then(function (data) {
    Actions.participationsLoaded(data);
});

export default Container.createFunctional(View, getStores, getState);

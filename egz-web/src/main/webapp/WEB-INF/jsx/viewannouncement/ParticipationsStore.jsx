'use strict';

import ActionTypes from "./ActionTypes";
import Dispatcher from "./Dispatcher";
import {ReduceStore} from 'flux/utils';

class ParticipationsStore extends ReduceStore {
    constructor() {
        super(Dispatcher);
    }

    getInitialState() {
        return null;
    }

    reduce(state, action) {
        switch (action.type) {
            case ActionTypes.PARTICIPATIONS_LOAD_COMPLETE:
                return action.participations;
            default:
                return state;
        }
    }
}

export default new ParticipationsStore();
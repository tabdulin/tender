'use strict';

import React from 'react';
import {DateUtils} from "../Utils";
import Actions from "./Actions";

function View(props) {
    if (!props.announcement)
        return null;
    return (
        <div>
            <Header {...props}/>
            <VewGeneralInfo {...props}/>
        </div>
    );
}

function Header(props) {
    return <h2>Объявление № {props.announcement.number}</h2>;
}

function VewGeneralInfo(props) {
    return (
        <table className="table table-stripped">
            <tbody>
            <tr>
                <th className="text-right">Наименование объявления:</th>
                <td>{props.announcement.nameRu}</td>
            </tr>
            <tr>
                <th className="text-right">Статус объявления:</th>
                <td>{props.announcement.announcementStatus.nameRu}"</td>
            </tr>
            <tr>
                <th className="text-right">Дата публикации объявления:</th>
                <td>{DateUtils.formatDateWithTime(props.announcement.publicationDate)}</td>
            </tr>
            <tr>
                <th className="text-right">Дата загрузки данных:</th>
                <td>{DateUtils.formatDateWithTime(props.announcement.loadTime)}</td>
            </tr>
            <tr>
                <th className="text-right">Мониторинг:</th>
                <td><input type="checkbox" checked={props.announcement.monitoring}/></td>
            </tr>
            {
                props.participations.map(participation => (
                        <ParticipationRow {...props}
                            participation={participation}
                            key={'participation' + participation.participant.id}
                        />
                    )
                )
            }
            </tbody>
        </table>
    );
}

function ParticipationRow(props) {
    // props.announcement.participations.map(participation => (<ParticipationRow participation={participation}/>))
    if (!props.participation)
        return null;

    let participationTemplate = 0;
    if (props.announcement.participations) {
        let participation = props.announcement.participations.find(participation => participation.participantId === props.participation.participant.id);
        if (participation) participationTemplate = participation.participationTemplate.id;
    }

    return (
        <tr>
            <th className="text-right">
                Участие {props.participation.participant.nameRu}:
            </th>
            <td>
                <SelectParticipation
                    {...props}
                    checked={participationTemplate === 0}
                    companyId={props.participation.participant.id}
                    template={{id: 0, nameRu:'Не участвуем'}}
                />
                {props.participation.templates.map(template => (
                    <SelectParticipation
                        {...props}
                        checked={participationTemplate === template.id}
                        companyId={props.participation.participant.id}
                        template={template}
                        key={props.participation.participant.id + "_" + template.id}
                    />
                ))}
            </td>
        </tr>
    );
}

function SelectParticipation(props) {
    const selectParticipation = () => props.selectParticipationPerformed(props.companyId, props.template, props.announcement, props.participations);
    return (
        <label style={{marginRight: '15px', fontWeight: 'normal'}}>
            <input type="radio"
                   style={{marginRight: '5px'}}
                   checked={props.checked}
                   name={"participation" + props.companyId}
                   onChange={selectParticipation}
                   value={props.template.id}/>
            {props.template.nameRu}
        </label>
    )
}

export default View;
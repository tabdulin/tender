const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        vendor: ['react', 'react-dom', 'redux', 'react-router-redux', 'redux-thunk', 'react-bootstrap'],
        app: path.join(__dirname, 'src', 'main', 'frontend', 'jsx', 'index.jsx')
    },
    devtool: 'source-map',
    output: {
        filename: '[name]-[hash].js',
        path: path.resolve(__dirname, 'target', 'front-end'),
        publicPath: '/egz/'
    },

    module: {
        loaders: [
            {
                test: /((\.jsx)|(\.js))$/,
                exclude: /node_modules/,
                loader: 'babel'
            }
        ]
    },
    resolve: {
        extensions: ['', '.jsx', '.js', '.less', '.css'],
        modulesDirectories: ['node_modules']
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src', 'main', 'frontend', 'index.html')
        })]
};
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        vendor: ['react', 'react-dom', 'redux'],
        app: path.join(__dirname, 'src', 'index.tsx')
    },

    output: {
        filename: '[name]-[hash].js',
        path: path.resolve(__dirname, 'build'),
        publicPath: '/'
    },

    resolve: {
        root: [path.resolve('./src/stylesheets')],
        alias: {
            'react/lib/ReactMount': 'react-dom/lib/ReactMount',
            'react/lib/EventPluginHub': 'react-dom/lib/EventPluginHub',
            'react/lib/EventConstants': 'react-dom/lib/EventConstants',
            'react/lib/EventPluginUtils': 'react-dom/lib/EventPluginUtils',
            'react/lib/EventPropagators': 'react-dom/lib/EventPropagators',
            'react/lib/SyntheticUIEvent': 'react-dom/lib/SyntheticUIEvent',
            'react/lib/CSSPropertyOperations': 'react-dom/lib/CSSPropertyOperations',
            'react/lib/ViewportMetrics': 'react-dom/lib/ViewportMetrics'
        },
        extensions: ['', '.ts', '.tsx', '.js', '.scss', '.less'],
        modulesDirectories: [
            'node_modules',
            path.resolve(__dirname, 'node_modules')
        ]
    },

    module: {
        loaders: [{
            test: /\.tsx?$/,
            loaders: ['babel', 'ts'],
            exclude: /node_modules/
        }, {
            test: /\.less$/,
            loader: ExtractTextPlugin.extract('style', 'css!less?strictUnits=true&strictMath=true')
        }, {
            test: /\.(png|jpg)$/,
            loader: 'file'
        }, {
            test: /.(woff(2)?|eot|ttf|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: 'file'
        }, {
            test: /\.(js|jsx)$/,
            loader: 'babel',
            include: path.join(__dirname, 'src')
        }]
    },

    // postcss: [require('autoprefixer')],

    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'index.html')
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'vendor',
        //     filename: 'vendor-[hash].js'
        // }),
        // new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new ExtractTextPlugin('[name].css'),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ]
};
1. Настройка JBoss

Используемая версия JBoss: EAP 7.0

1.1 Конфигурирование датасурсов.
Исходные данные: хост, на котором установлен и настроен PostgreSQL, TCP-порт, имя пользователя/пароль пользователя, имеющего права на создание схемы в БД
Для конфигурирования необходимо:
1) В папке JBOS_HOME/modules создать подкаталоги org/postgresql/main. В этом подкаталоге создать файл module.xml с содержимым:
<?xml version="1.0" ?>

<module xmlns="urn:jboss:module:1.1" name="org.postgresql">

    <resources>
        <resource-root path="postgresql-9.3-1100-jdbc41.jar"/>
    </resources>

    <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
    </dependencies>
</module>

а так же скопировать сюда JDBC-драйвер PostgreSQL (postgresql-9.3-1100-jdbc41.jar, но можно и более свежую версию,
но тогда надо скорректировать имя файла в module.xml)

2) Открыть на редактирвоание файл JBOSS_HOME/standalone/configuration/standalone.xml
В секции
subsystem xmlns="urn:jboss:domain:datasources:4.0" -> datasources
добавить драйвер:
                    <driver name="postgresql" module="org.postgresql">
                        <xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
                    </driver>
В секции subsystem xmlns="urn:jboss:domain:datasources:4.0" -> datasources добавить датасурс:
                <xa-datasource jndi-name="java:jboss/datasources/EGZDS" pool-name="EGZDS" enabled="true" use-ccm="true">
                    <xa-datasource-property name="ServerName">
                        localhost
                    </xa-datasource-property>
                    <xa-datasource-property name="PortNumber">
                        5432
                    </xa-datasource-property>
                    <xa-datasource-property name="DatabaseName">
                        egzdb
                    </xa-datasource-property>
                    <driver>postgresql</driver>
                    <security>
                        <user-name>egz</user-name>
                        <password>password</password>
                    </security>
                    <validation>
                        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker"/>
                        <background-validation>true</background-validation>
                        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter"/>
                    </validation>
                </xa-datasource>
Где вместо localhost указать IP-адрес, или доменное имя хоста с СУБД, вместо 5432 указать номер TCP-порта СУБД
Вместо egzdb указать имя базы данных, в которой будут созданы таблицы
в секции
                    <security>
                        <user-name>egz</user-name>
                        <password>password</password>
                    </security>
ввести имя пользователя/пароль для подключения к СУБД

Настроить безопасность приложения. Для этого в секции subsystem xmlns="urn:jboss:domain:security:1.2"  -> security-domains
добавить такой домен:
                <security-domain name="egz-web" cache-type="default">
                    <authentication>
                        <login-module code="Database" flag="required">
                            <module-option name="dsJndiName" value="java:jboss/datasources/EGZDS"/>
                            <module-option name="principalsQuery" value="SELECT password FROM security_user WHERE username=?"/>
                            <module-option name="rolesQuery" value="SELECT role, role_group FROM security_role WHERE username=?"/>
                            <module-option name="hashAlgorithm" value="MD5"/>
                            <module-option name="hashEncoding" value="base64"/>
                        </login-module>
                    </authentication>
                </security-domain>

После деплоя приложения автоматически будет создана схема egz с таблицами.

Если необходимо что бы JBoss работал на всех сетевых интерфейсах, а не только на localhost, то надо прописать сответствующие интерфейсы в конфиге.
Например так: в секции interfaces меняем это
        <interface name="management">
            <inet-address value="${jboss.bind.address.management:127.0.0.1}"/>
        </interface>
        <interface name="public">
            <inet-address value="${jboss.bind.address:127.0.0.1}"/>
        </interface>

на это:

        <interface name="management">
            <any-address/>
        </interface>
        <interface name="public">
            <any-address/>
        </interface>

Так же можно поменять номера http-портов на стандартные:
        <socket-binding name="http" port="${jboss.http.port:80}"/>
        <socket-binding name="https" port="${jboss.https.port:443}"/>

Само собой есть более тонкие варианты настроек, но для этого уже пожалуйте в документацию.

Что бы получить возможность входа в консоль администрирования необходимо создать пользователя - администратора.
Запустить скрипт add-user, выбрать Management User, ввести логин/пароль.
На вопрос
Is this new user going to be used for one AS process to connect to another AS process?
e.g. for a slave host controller connecting to the master or for a Remoting connection for server to server EJB calls.
yes/no?

сказать "no".

Проблемы с Kalkan
Что бы при использовании криптопровайдера не появлялись ошибки что не найден класс sun.security.jca.GetInstance необходимо:
1) открыть на редактирование файл jboss-eap-7.0/modules/system/layers/base/sun/jdk/main/module.xml
После
<path name="sun/security"/>
Добавить:
<path name="sun/security/jca"/>



Проблемы с Jersey

Что бы не появлялась ошибка
18:25:41,563 INFO  [org.jboss.weld.Event] (MSC service thread 1-3) WELD-000411: Observer method [BackedAnnotatedMethod] com.sun.jersey.server.impl.cdi.CDIExtension.processAnnotatedType(@Observes ProcessAnnotatedType<T>) receives events for all annotated types. Consider restricting events using @WithAnnotations or a generic type with bounds.
18:25:41,651 ERROR [org.jboss.msc.service.fail] (MSC service thread 1-3) MSC000001: Failed to start service jboss.deployment.unit."egz-ear.ear".WeldStartService: org.jboss.msc.service.StartException in service jboss.deployment.unit."egz-ear.ear".WeldStartService: Failed to start service
Caused by: org.jboss.weld.exceptions.DefinitionException: Exception List with 1 exceptions:
Exception 0 :
java.lang.UnsupportedOperationException: WFLYNAM0043: Naming context is read-only
...


Надо либо через веб-консоль, либо в командной строке добавить свойство
com.sun.jersey.server.impl.cdi.lookupExtensionInBeanManager = true





1.3 Настройка SSL
Для настройки HTTPS на JBoss-е понадобится JKS-файл с ключем и сертификатом. Тут не буду описывать как генерировать ключ,
формировать запрос в удостоверяющий центр, импортировать сертификат и ключ в JKS. Для этого есть документация.
1) Открыть на редактирвоание файл JBOSS_HOME/standalone/configuration/standalone.xml
В секции server -> management -> security-realms добавить новый реалм:

            <security-realm name="SSLRealm">
                <server-identities>
                    <ssl>
                        <keystore path="/home/alexey/work/InesSoft/tender/materials/jboss-eap-7.0/key.jks" keystore-password="111" key-password="111"/>
                    </ssl>
                </server-identities>
            </security-realm>

Где вместо /home/alexey/work/InesSoft/tender/materials/jboss-eap-7.0/key.jks указать путь к JKS файлу с ключом,
в атрибуте  keystore-password="111" указать пароль на хранилище, в атрибуте key-password="111" указать пароль на доступ к ключу.

2) В секцию subsystem xmlns="urn:jboss:domain:undertow:3.1" -> server name="default-server" после тега http-listener добавить тег:
<https-listener name="httpsdefault" security-realm="SSLRealm" socket-binding="https"/>

3) Сохранить конфиг, перезапустить JBoss

1.4 Robots.txt
Скопировать файл robots.txt в папку welcome-content JBoss-а.

Генерация MD5 BASE64 пароля:
echo -n password | openssl dgst -md5 -binary | openssl enc -base64




Конфигурирование JMS
1) В конфиге standalone.xml подключить расширение:
<extensions>
  ...
  <extension module="org.wildfly.extension.messaging-activemq"/>
  ...
</extensions>

2) В секцию <subsystem xmlns="urn:jboss:domain:ejb3:4.0"> после тега </session-bean> добавить:

             <mdb>
                 <resource-adapter-ref resource-adapter-name="${ejb.resource-adapter-name:activemq-ra.rar}"/>
                 <bean-instance-pool-ref pool-name="mdb-strict-max-pool"/>
             </mdb>

3) После секции <subsystem xmlns="urn:jboss:domain:mail:2.0"> добавить секцию параметров JMS:
        <subsystem xmlns="urn:jboss:domain:messaging-activemq:1.0">
            <server name="default">
                <security-setting name="#">
                    <role name="guest" delete-non-durable-queue="true" create-non-durable-queue="true" consume="true" send="true"/>
                </security-setting>
                <address-setting name="#" message-counter-history-day-limit="10" page-size-bytes="2097152" max-size-bytes="10485760" max-delivery-attempts="2" expiry-address="jms.queue.ExpiryQueue" dead-letter-address="jms.queue.DLQ"/>
                <in-vm-connector name="in-vm" server-id="0"/>
                <in-vm-acceptor name="in-vm" server-id="0"/>
                <jms-queue name="ExpiryQueue" entries="java:/jms/queue/ExpiryQueue"/>
                <jms-queue name="DLQ" entries="java:/jms/queue/DLQ"/>
                <jms-queue name="AnnouncementsDownloadQueue" entries="java:/jms/queue/AnnouncementsDownloadQueue"/>
                <jms-queue name="ContractsDownloadQueue" entries="java:/jms/queue/ContractsDownloadQueue"/>
                <jms-queue name="JiraQueue" entries="java:/jms/queue/JiraQueue"/>
                <jms-queue name="PortalFileDownloadQueue" entries="java:/jms/queue/PortalFileDownloadQueue"/>
                <pooled-connection-factory name="activemq-ra" transaction="xa" entries="java:/JmsXA java:jboss/DefaultJMSConnectionFactory" connectors="in-vm"/>
            </server>
        </subsystem>

- подключение через локальную JVM. Пока других коннекторов нам не надо.





Что бы посмотреть привязываемые хибернейтом параметры надо в консоли JBoss-а зайти в
Configuration: Subsystems  >  Subsystem: Logging > LOGGING CATEGORIES и добавить категории:
org.hibernate.type TRACE
org.hibernate.SQL DEBUG

Use parent handlers: true в обоих случаях
Данные воводятся в лог-файле, но не на консоли.

Бэкап базы:
pg_dump -b -C -f /tmp/egz -F d -n egz egzdb
pg_restore -v -d egzdb /home/alexey/work/InesSoft/tender/materials/db/20170202
вместо /tmp/egz можно указать любой другой существующий и пустой каталог

update egz.dic_participant_company set eds_private_key=lo_import('/tmp/GOSTKNCA_30e0a3475cc43e65acfba58aedd3eab24cb0f7f5.p12') where id = 3;
grant all privileges on large object 651184 to egz;
grant select on all tables in schema egz to talgat;


MessageType='ContractDownload'
ContractsDownloadQueue

MessageType='AnnouncementDownload'
AnnouncementsDownloadQueue

MessageType='jiraMessage'
JiraQueue

MessageType='FileDownload'
PortalFileDownloadQueue
